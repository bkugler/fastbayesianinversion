using FastBayesianInversion.Store
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Models
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Shared
using FastBayesianInversion.Pipeline.TuneImis
using FastBayesianInversion.Log 

using Test
using Logging

Log.init(Logging.Info)

@testset "tuning imis" begin
    st = Store.Storage("tune_imis_store/")
    context = Models.LinearTrivial()
    _, Yobs = Models.data_training(context, 5, 0.1)
    Yobs_std = Yobs ./ 20
    gllim_params = Gllim.Params(
        Gllim.MultiInitParams(
            3,
            Gmms.EmParams(10, 3, 2, 1e-12)
        ),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(3, 1e-5),
            1e-12
        )
    )
    training_params = Shared.TrainingParams(1000, 0.001, gllim_params)
    Ns = 5_000
                
    mcmc_params = Mcmc.ParamsMH(training_params.train_std; Niter=70_000, Nkeep=10_000)
    out_mcmc, _ = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params)
    refs = Models.from_physical(context, out_mcmc.Xmean)

    setups = TuneImis._setup(st, context, Yobs, Yobs_std, refs, training_params, true)
    @test length(setups) == size(Yobs)[2]

    range = TuneImis.ImisParamsRange(
        2 .* [25, 50, 100, 200],
        [0.2, 0.5],
        [1,5,10]
    )
    res = TuneImis.run_imiss(setups, range, 2)
    @test res isa Array{TuneImis.ExpImis}
    @test length(res) == length(range.N0s) * length(range.B_ratios) * length(range.Ks)

    perf_is = TuneImis.perform(setups, 3, Ns)
    @test perf_is isa TuneImis.Perf

    out = TuneImis._select(perf_is, res)
    @test out isa Is.ImisParams

    Store.remove(st)
end
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Log 
using FastBayesianInversion.Models
using Test
using LinearAlgebra
using Logging

Log.init(Logging.Info) 


@testset "init gllim functionnal" begin
    L, K, N = 4, 10, 30, 1000
    model = Models.DoubleSolutions(L)
    D = Models.get_D(model)

    X, Y = rand(L, N), rand(D, N)

    F = Models.closure(model)

    params = Gllim.FunctionnalInitParams(K, F)
    g = Gllim.initialisation_gllim(X, Y, params)

    @test size(g.π) ==  (K,)
    @test size(g.c) ==  (L, K)
    @test size(g.b) ==  (D, K)
    @test all(size(m1) == (D, L) for m1 in g.A)
    @test all(size(m1) == (L, L) for m1 in g.Γ)
    @test all(size(m1) == (D, D) for m1 in g.Σ)

    @test isapprox(sum(g.π), 1)

    log_rnk, log_ll = Gllim.initialisation(X, Y, params)
    @test size(log_rnk) == (N, K)
    @test log_ll isa Float64
end

@testset "sub gllim" begin
    L, D, K = 10, 70, 50
    gl = Gllim.GLLiM_rand(K, L, D)

    subg = Gllim.sub_gllim(gl, [1,10,45])
    @test subg.K == gl.K
    @test subg.L == gl.L 
    @test subg.D == 3
    @test all(size(S) == (3, 3) for S in subg.Σ)
end

@testset "sampling" begin
    L, K, N, Ns = 10, 40, 100, 1100
    weights = rand(K, N)
    meanss = rand(L, K, N)
    covs = [ Probas.FullCov_rand(L) for _ in 1:K ]

    out = Probas.gmm_sampling(weights, meanss, covs, Ns)
    # @time Probas.gmm_sampling(weights, meanss, covs, Ns)
    @test size(out) == (L, Ns, N)
end

@testset "gllim_density" begin
    L, K, N, D = 4, 20, 1000, 30
    g = Gllim.GLLiM_rand(K, L, D)
    x = rand(L)
    Gllim.conditionnal_density(g, x)


    X = rand(L, N)
    w1, m1 = Gllim.conditionnal_density(g, X)
    # @time Gllim.conditionnal_density(g, X)
    @test size(w1) == (K, N)
    @test size(m1) == (D, K, N)
end

@testset "add bias" begin
    L, K, D = 4, 20, 30

    for ST in [IsoCov{Float64}, DiagCov{Float64}, FullCov{Float64}]
        g = Gllim.GLLiM{Float64,FullCov{Float64},ST}(K, L, D)
        @testset "Sigma type $ST cov type $(typeof(cov))" for cov in [Probas.IsoCov_rand(D), Probas.DiagCov_rand(D), Probas.FullCov_rand(D)]
            biased_g = Gllim.add_bias(g, 2., ones(D), cov)
            @test biased_g.K == g.K
        end
    end
end

@testset "gllim_gmm_equi" begin
    L = 4
    g = Gllim.GLLiM_rand(20, L, 30)
    
    gmm = Probas.Gmm(g)
    g2 = Gllim.GLLiM(gmm, L)

    @test isapprox(g.π, g2.π)
    @test isapprox(g.c, g2.c)
    @test isapprox(g.b, g2.b)
    @test all(isapprox(m1, m2) for (m1, m2) in zip(g.A, g2.A))
    @test all(isapprox(m1, m2) for (m1, m2) in zip(g.Γ, g2.Γ))
    @test all(isapprox(m1, m2) for (m1, m2) in zip(g.Σ, g2.Σ))
end

@testset "kmean" begin
    L, N, K = 4, 1000, 10
    X = rand(L, N)
    centers = rand(L, K)
    out, labels = Gmms.kmeans(X, centers, 20)
    # @time Gmms.kmeans(X, centers, 20)
    @test size(out) == size(centers)
    @test size(labels) == (N,)
end

@testset "em_gm" begin
    L, N, K = 4, 1000, 10
    X = rand(L, N)
    weights = ones(K) ./ K 
    centers = rand(L, K)
    chol_covs = [Probas.CholCov_rand(L) for _ in 1:K]
    out, log_rnk, log_ll = Gmms.em(Probas.Gmm{Float64}(weights, centers, chol_covs), X, 20, 1e-10)
    @test size(out.weights) == (K,)
    @test size(out.means) == (L, K)
    @test length(out.chol_covs) == K
    @test size(log_rnk) == (N, K)
    
    out, log_rnk, log_ll = Gmms.em(X, Gmms.EmParams(K, 10, 20, 1e-10))
    @test size(out.weights) == (K,)
    @test size(out.means) == (L, K)
    @test length(out.chol_covs) == K
    @test size(log_rnk) == (N, K)
end

@testset "init gllim multiple" begin
    L, D, K, N = 4, 10, 30, 1000
    X, Y = rand(L, N), rand(D, N)
    params = Gmms.EmParams(K, 30, 20, 1e-10)
    g, log_rnk, log_ll = Gllim.initialisation(X, Y, params)

    @test size(log_rnk) == (N, K)
    @test size(g.π) ==  (K,)
    @test size(g.c) ==  (L, K)
    @test size(g.b) ==  (D, K)
    @test all(size(m1) == (D, L) for m1 in g.A)
    @test all(size(m1) == (L, L) for m1 in g.Γ)
    @test all(size(m1) == (D, D) for m1 in g.Σ)

    @test isapprox(sum(g.π), 1)

    log_rnk, log_ll = Gllim.initialisation(X, Y, Gllim.MultiInitParams(5, params))
    @test size(log_rnk) == (N, K)
    @test log_ll isa Float64
end




@testset "train_gllim" begin
    L, K, N = 2, 10, 500
    model = Models.DoubleSolutions(L)
    D = Models.get_D(model)

    A = rand(D, L)
    X = rand(L, N)
    Y =  A * X + 0.01 * rand(D, N)
    em_params = Gmms.EmParams(K, 30, 20, 1e-10)
    stop_params = Gllim.DefaultStoppingParams(20, 1e-4)
    cov_floor = 1e-10

    F = Models.closure(model)

    for GT in [IsoCov{Float64}, DiagCov{Float64}, FullCov{Float64}]
        for ST in [IsoCov{Float64}, DiagCov{Float64}, FullCov{Float64}]
            for init_params in  [
                Gllim.MultiInitParams(5, em_params),
                Gllim.FunctionnalInitParams(K, F),
            ]
                gllim_params = Gllim.Params(init_params, Gllim.TrainParams{Float64,GT,ST}(stop_params, cov_floor))

                gllim, history = Gllim.train(gllim_params, X, Y)

                @testset "$GT $ST $(typeof(init_params))" begin
                    # constraints
                    @test gllim.Γ isa Vector{GT}
                    @test gllim.Σ isa Vector{ST}
    
                    @test isapprox(sum(gllim.π), 1)

                    # the likeloohd must be non decreasing
                    @test sortperm(history) == 1:length(history) 
                end
            end
        end
    end
end
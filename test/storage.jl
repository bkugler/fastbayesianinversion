using FastBayesianInversion.Store
using FastBayesianInversion.Log

using Test

Log.init()

@testset "store" begin
    st = Store.Storage("test_store/")
    path = Store.get_path(st, Store.FO_GLLIM)
    
    Y = rand(4, 5)
    meta = [rand(4), "dlsmkd", 4]
    Store.save(st, Y, Store.FO_PREDICTIONS, meta...)
    Y2 = Store.load(st, Store.FO_PREDICTIONS, meta...)
    @test isapprox(Y, Y2)

    # cleanup the tmp value 
    Store.remove(st)
end
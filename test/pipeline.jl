using FastBayesianInversion.Pipeline
using FastBayesianInversion.Shared
using FastBayesianInversion.Probas
using FastBayesianInversion.Models 
using FastBayesianInversion.Gmms 
using FastBayesianInversion.Gllim 
using FastBayesianInversion.Predictions 
using FastBayesianInversion.Log
using FastBayesianInversion.Noise
using FastBayesianInversion.Store
using FastBayesianInversion.Schemes
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Is
using Logging

Log.init(Logging.Info)

using Test
using Dates 

import Base:isapprox

context =  Models.LinearSmall()
L, D = Models.get_L(context), Models.get_D(context)
K, N = 15, 50
Xobs, Yobs = Models.data_training(context, N, 0.)
Yobs_std = rand(D, N)

function Base.isapprox(gmm1::Probas.Gmm, gmm2::Probas.Gmm)
    return isapprox(gmm1.weights, gmm2.weights) &&
    isapprox(gmm1.means, gmm2.means) &&
    all(isapprox(c1, c2) for (c1, c2) in zip(gmm1.chol_covs, gmm2.chol_covs))
end

@testset "setup_inversion" begin
    init_params = Gllim.MultiInitParams(1, Gmms.EmParams(3, 4, 2, 1e-10))
    stop_params = Gllim.DefaultStoppingParams(5, 1e-4)
    cov_floor = 1e-10
    gllim_params = Gllim.Params(init_params, Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(stop_params, cov_floor))
    trained_gllim, _ = Gllim.train(gllim_params, Xobs, Yobs)

    yobs, ystd = Yobs[:, 1], Yobs_std[:,1]
    train_std = 0.01
    merging_options = Predictions.MergingOptions(2)

    mean_gllim0, cov_gllim0, model0, model_modes0, full_posterior0, merged_posterior0 = Shared.setup_inversion(context, yobs, ystd, trained_gllim, train_std, merging_options; add_prop_std=0.)

    mean_gllim1, cov_gllim1, model1, model_modes1, full_posterior1, merged_posterior1 = Shared.setup_inversion(context, yobs, ystd, trained_gllim, train_std, merging_options; add_prop_std=0.1)

    @test isapprox(mean_gllim0, mean_gllim1)
    @test isapprox(cov_gllim0, cov_gllim1)
    @test isapprox(full_posterior0, full_posterior1)
    @test isapprox(merged_posterior0, merged_posterior1)
end


function nb_preds(params::Pipeline.InversionParams)
    K = params.merging_options.K_merged
    base = 1 + K # gllim mean, merged centroids
    if params.with_best_centroids
        base += K
        if params.with_mode_finding
            base += 2 * K 
        end
    else 
        if params.with_mode_finding
            base += K
        end
    end
    base += 1 + K # sampling mean + centroids
    base += 1 # best pred full
    base
end

@testset "pipeline" begin
    init_params = Gllim.MultiInitParams(1, Gmms.EmParams(3, 4, 2, 1e-10))
    stop_params = Gllim.DefaultStoppingParams(5, 1e-4)
    cov_floor = 1e-10
    gllim_params = Gllim.Params(init_params, Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(stop_params, cov_floor))
    trained_gllim, _ = Gllim.train(gllim_params, Xobs, Yobs)
    
    sampling_params = [Is.IsParams(200), Is.ImisParams(100, 50, 2)]
    with_best_centroids = [true, false]
    with_mode_finding = [true, false]

    @testset "sampling $samp" for samp in sampling_params
    @testset "best centroids $wbc" for wbc in with_best_centroids
        @testset "mode finding $wmf" for wmf in with_mode_finding
            @testset "Xobs == nothing : $(Xobs_ === nothing)" for Xobs_ in [Xobs, nothing]
                @testset "additional prop std $add_std" for add_std in [0,0.01]   
                    params = Pipeline.InversionParams(
                            0.01, Predictions.MergingOptions(2), 
                            samp, wbc, wmf,
                            Is.NonUniformityParams(5),
                            add_std
                            )    
                    out, dispersion, dura = Pipeline.complete_inversion(context, Yobs, Yobs_std, trained_gllim, params, Xobs_)

                    expected_nb_preds = nb_preds(params)
                    @test length(out) == expected_nb_preds
                    @test length(dispersion) == N
                    @test dura isa Millisecond
                        
                    best_err = out[Schemes.Best].Yerr
                    for (sch, serie) in pairs(out)
                        @test size(serie.Xmean) == (L, N)
                        @test size(serie.Xcovs) == (L, N)

                        @test serie.scheme !== "" # schemes must be tracked ...
                        @test serie.scheme == sch # ... and consistent

                        @test all(isfinite.(serie.Xmean))
                        @test all(isfinite.(serie.Xcovs))
                            # best minimize reconstruction error
                        @test all(best_err .<= serie.Yerr)
                    end
                end
            end
        end
    end
end
end



@testset "full procedure" begin
    inv_params =   Pipeline.InversionParams(
        0.01, Predictions.MergingOptions(3), 
        Is.ImisParams(200, 50, 2),  true, true,
        Is.NonUniformityParams(5),
        0
        )
    
    init_params = Gllim.MultiInitParams(5, Gmms.EmParams(K, 10, 5, 1e-10))
    stop_params = Gllim.DefaultStoppingParams(5, 1e-4)
    cov_floor = 1e-10
    gllim_params = Gllim.Params(init_params, Gllim.TrainParams{Float64,FullCov{Float64},IsoCov{Float64}}(stop_params, cov_floor))
    training_params = Shared.TrainingParams(1000, inv_params.train_std, gllim_params)
    procedure_params = Pipeline.ProcedureParams(training_params, inv_params)

    noise_params = Noise.EmProcedureParams(200, gllim_params, Noise.EmIsGLLiMParams(D=D))
    
    @testset "comp times" begin    
        ct = Comptimes.Times(context, procedure_params, N) 
        @test ct.kind ==  Comptimes.Invertion 
    
        ct = Comptimes.Times(context, noise_params, N)
        @test ct.kind ==  Comptimes.Noise 
    
        mcmc_params = Pipeline.Mcmc.ParamsMH(0.1; Niter=1_000, Nkeep=100) 
        ct = Comptimes.Times(context, mcmc_params, N)
        @test ct.kind ==  Comptimes.MCMC 

        mcmc_params = Pipeline.Mcmc.ParamsHMC(400, 200, 0.1) 
        ct = Comptimes.Times(context, mcmc_params, N)
        @test ct.kind ==  Comptimes.MCMC 

        ct2 = copy(ct)
        for k in Comptimes._fieldnames
    @test getfield(ct, k) == getfield(ct2, k) 
end
    end

    st = Store.Storage("test_convenience/")
    @testset "convenience inv" begin
        run = RGP  
        preds, entropies, ll_history, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, procedure_params, run, st; Xobs=nothing)

        @test length(entropies) == N 
        @test sortperm(ll_history) == 1:length(ll_history) # ll is increasing
        @test ct isa Comptimes.Times

        Pipeline.regularize_inversions!(preds, [
            Schemes.GllimBestCentroid(""),
            Schemes.GllimModeFromBestCentroid(""),
            Schemes.GllimModeFromMergedCentroid(""),
        ])
    end

    @testset "convenience noise" begin
    for Xobs_ in [Xobs, nothing]
        history, estimator, dura = Pipeline.complete_noise_estimation(context, Yobs, Xobs_, noise_params, 2, st)
        @test dura isa Comptimes.Times
        @test (estimator === nothing) == (Xobs_ === nothing)
    end
end

    Store.remove(st)
end

@testset "merge predictions" begin
    init_params = Gllim.MultiInitParams(1, Gmms.EmParams(3, 4, 2, 1e-10))
    stop_params = Gllim.DefaultStoppingParams(5, 1e-4)
    cov_floor = 1e-10
    gllim_params = Gllim.Params(init_params, Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(stop_params, cov_floor))
    trained_gllim, _ = Gllim.train(gllim_params, Xobs, Yobs)
    
    params = Pipeline.InversionParams(
        0.01, Predictions.MergingOptions(2), 
        Is.IsParams(100), false, false,
        Is.NonUniformityParams(5),
        0.
    ) 

    out1, _, _ = Pipeline.complete_inversion(context, Yobs, Yobs_std, trained_gllim, params, nothing)
    out2, _, _ = Pipeline.complete_inversion(context, Yobs[:, 1:10], Yobs_std[:, 1:10], trained_gllim, params, nothing)

    Shared.merge_result!(out1[Schemes.Best], out2[Schemes.Best], 12, 10)
    Shared.merge_result!(out1, out2, 20, 5)

    @test out1[Schemes.Best].Xmean[:, 12] == out2[Schemes.Best].Xmean[:, 10]
    @test out1[Schemes.IsMean].Xmean[:, 20] == out2[Schemes.IsMean].Xmean[:, 5]

    @test size(Shared.failures(out1[Schemes.IsMean], context)) == (N,)
end
using FastBayesianInversion.Perf
using FastBayesianInversion.Gllim
using FastBayesianInversion.Models

using Test

@testset "performance" begin
    model = Models.InjectifHard()
    K = 50
    gllim = Gllim.GLLiM_rand(K, Models.get_L(model), Models.get_D(model))
    X, Y = Models.data_training(model, 10_000, 0.001)
    
    @test Perf.direct_error(model, gllim) isa Number
    @test Perf.sigma_error(gllim) isa Number
    @test Perf.dimension(gllim) isa Number
    bic, ll, dim =  Perf.bic(gllim, X, Y) 
    @test bic isa Number && ll isa Number
    @test dim isa Integer
end
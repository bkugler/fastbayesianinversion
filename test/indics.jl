using FastBayesianInversion.Indics 
using Test 

@testset "dispersion" begin
    L = 4
    for _ in 1:100
        preds = [Indics.WeightedMean(rand(L), 5 * rand()) for _ in 1:10] 
        e, _ = Indics.dispersion(preds)

        @test e >= 0
        # when the predictions are in [0,1], the theory ensures that 
        # the dispersion is <= 1 
        @test e <= 1   
    end

    # special case 
    d, _ = Indics.dispersion([Indics.WeightedMean(rand(L), 5 * rand())])
    @test d <= eps()
    d, _ = Indics.dispersion([
        Indics.WeightedMean([0, 1], 1),
        Indics.WeightedMean([1, 0], 1)
    ])
    @test d == 1
end

@testset "non uniformity" begin
    X = rand(1_000_000)
    @test Indics.non_uniformity(X) <= 0.01
end
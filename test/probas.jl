using FastBayesianInversion.Probas
using Test 

@testset "logsumexp" begin
    logx = 100 * rand(200) .- 100
    @test isfinite(Probas.logsumexp(logx))

    normal_log = rand(200)
    @test isapprox(Probas.logsumexp(normal_log), log(sum(exp.(normal_log))))
end

@testset "weighted_logsumpexp" begin
    logp1 = - 1000 * rand()
    logp2 = - 1000 * rand()
    @test isfinite(Probas.weighted_logsumpexp(logp1, logp2, 50, 150))

    @test isapprox(Probas.weighted_logsumpexp(1, 2, 10, 20), log(10 * exp(1) + 20 * exp(2)))
end


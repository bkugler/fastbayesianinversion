using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Schemes
using FastBayesianInversion.Models
using FastBayesianInversion.Pipeline.Mcmc

using Test
using Logging
using Dates 

Log.init(Logging.Info)


@testset "samples" begin
    ct = Models.Linear()
    D = Models.get_D(ct)
    L = Models.get_L(ct)
    _, Y = Models.data_observations(ct, 1)
    Sigma = Probas.DiagCov_rand(D)

    for params in [Mcmc.ParamsHMC(300, 200, 0.01), Mcmc.ParamsMH(300, 200, 0.02, 0.01)]
        out = Mcmc._sample_from_posterior(ct, Y[:, 1], Sigma, params)
        @test size(out) == (L, 200)
        @test all(Models.is_valid(out[:, i], L) for i in 1:200)
    end
end

@testset "MCMC - complete prediction" begin
    context =  Models.LinearSmall()
    L, D = Models.get_L(context), Models.get_D(context)
    N = 10
    _, Yobs = Models.data_training(context, N, 0.)
    Yobs_std = rand(D, N)

    @test_throws ErrorException  Mcmc.ParamsMH(0.1; Niter=1_000, Nkeep=10_000) 
    @test_throws ErrorException  Mcmc.ParamsHMC(300, 400, 0.01) 
    @test_throws ErrorException  Mcmc.ParamsHMC(100, 200, 0.01) 
    
    for params in [
        Mcmc.ParamsMH(0.1; Niter=1_000, Nkeep=100),
        Mcmc.ParamsHMC(300, 200, 0.01)
    ]
        out, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, params)

        @test size(out.Xmean) == (L, N) 
        @test dura isa Millisecond
        @test all(out.Yerr .> 0)
        @test out.scheme in [Schemes.McmcHamiltonian, Schemes.McmcMetropolis]
    end
end
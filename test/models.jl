using FastBayesianInversion
using FastBayesianInversion.Models
using Test
using JSON 
using LinearAlgebra

@testset "linear contexts" begin 
    m = Models.LinearSmall()
    @test Models.get_L(m) == 2
    @test Models.get_D(m) == 3
end

const geoms = Models.HapkeGeometries(
    [0, 10, 0, 0 ,20],
    [45, 45, 50, 50 ,50],
    [80, 130, 130, 80 ,150],
)

# import from JSON
function import_data_hapke(file::String)
    BASE_FOLDER = dirname(dirname(pathof(FastBayesianInversion)))
    file = joinpath(BASE_FOLDER, "test", file)

    f = open(file)
    d = JSON.parse(f)
    close(f)

    inc = Vector{Float64}(d["inc"])
    eme = Vector{Float64}(d["eme"])
    phi = Vector{Float64}(d["phi"])
    omega = Vector{Float64}(d["omega"])
    theta_bar = Vector{Float64}(d["theta_bar"])
    b = Vector{Float64}(d["b"])
    c = Vector{Float64}(d["c"])
    hh = Vector{Float64}(d["hh"])
    b0 = Vector{Float64}(d["b0"])
    

    geoms_ref = Models.HapkeGeometries(inc, eme, phi)
    m = Models.Hapke("test ref", geoms_ref; partiel=false, w_linear=false) # 2002 variant
    X  = Matrix(hcat(omega, theta_bar, b, c, hh, b0)')
    # data has already scaled theta_bar 
    X, _ = Models.from_physical(m, X, copy(X))
    Yref = hcat([y for y in d["y"]]...)
    m, X, Yref
end

@testset "Hapke" begin
    @test_throws ErrorException Models.HapkeGeometries(
        [0, 10, 0, 0],
        [45, 45, 50, 50 ,50],
        [80, 130, 130, 80 ,150],
    )

    m = Models.Hapke("test geoms", geoms)
    geoms2 = Models.geometries(m) 
    @test isapprox(geoms.inc, geoms2.inc)
    @test isapprox(geoms.eme, geoms2.eme)
    @test isapprox(geoms.azi, geoms2.azi)

    @test size(Models.matrix(geoms2)) == (Models.get_D(m), 3)

    # comparison against reference implementation
    m1, X1, Yref1 = import_data_hapke("data_hapke.json")
    Y1 = Models.F(m1, X1)
    @test isapprox(Y1, Yref1)

    # other dataset
    m2, X2, Yref2 = import_data_hapke("data_hapke_2.json")
    Y2 = Models.F(m2, X2)
    @test isapprox(Y2, Yref2)
end

@testset "simple functions" begin 
    for m in [
        Models.Linear(),
        Models.LinearSmall(),
        Models.LinearTrivial(),
        Models.SimpleSurface(),
        Models.InjectifHard(),
        Models.DoubleSolutions(1),
        Models.DoubleSolutions(4),
        Models.TripleSolutions(1),
        Models.DoubleHard(),
        Models.Hapke("test geoms", geoms)
    ]   
        L, D  = Models.get_L(m), Models.get_D(m)
        # F 
        x = zeros(L)
        y = zeros(D)
        Models.f!(m, x, y, D)
        Models.F(m, rand(L, 10_000))
        @test true

        # training data 
        Ntrain = 100
        Xtrain, Ytrain = Models.data_training(m, Ntrain, 0.1)
        @test size(Xtrain) == (L, Ntrain)
        @test size(Ytrain) == (D, Ntrain)

        # scaling
        x, cov = rand(L), rand(L)
        xp, covp = Models.to_physical(m, x, cov)
        x2, cov2 = Models.from_physical(m, xp, covp)
        @test isapprox(x, x2)
        @test isapprox(cov, cov2)

        # observations
        Nobs = 10
        Xobs, Yobs = Models.data_observations(m, Nobs)
        @test size(Xobs) == (L, Nobs)
        @test size(Yobs) == (D, Nobs)
    end
end

@testset "closure" begin
    for m in [
        Models.Linear(),
        Models.LinearSmall(),
        Models.LinearTrivial(),
        Models.SimpleSurface(),
        Models.InjectifHard(),
        Models.DoubleSolutions(1),
        Models.DoubleSolutions(4),
        Models.TripleSolutions(4),
        Models.DoubleHard(),
    ] 
        F = Models.closure(m)
        @test F isa Function
        x = rand(Models.get_L(m))
        y = F(x)
        @test size(y) == (Models.get_D(m),)
    end
end

@testset "triple solutions" begin
    c = Models.TripleSolutions(1)
    X1, X2, X3, Y = Models.data_observations(c, 100, 0.1)
    @test size(X1) == (1, 100)
    @test size(X2) == (1, 100)
    @test size(X3) == (1, 100)
    @test size(Y) == (1, 100)

    @test all(Models.is_valid(X1[:, n], 1) for n in 1:100)
    @test all(Models.is_valid(X2[:, n], 1) for n in 1:100)
    @test all(Models.is_valid(X3[:, n], 1) for n in 1:100)

    @test isapprox(Models.F(c, X1), Y)
    @test isapprox(Models.F(c, X2), Y)
    @test isapprox(Models.F(c, X3), Y)
end
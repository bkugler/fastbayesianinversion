using FastBayesianInversion.Predictions
using FastBayesianInversion.Probas

using LinearAlgebra
using Test 

@testset "mode_finding_f" begin
    L, K = 7, 60
    weights = rand(K)
    weights ./= sum(weights)
    gmm = Gmm{Float64}(weights, rand(L, K), [Probas.CholCov_rand(L) for k in 1:K])
    tmp = Predictions.tmpStorage{Float64}(L)
    x = rand(L)
    out = zeros(L)
    Predictions.f_rec!(gmm, x, tmp, out)
    @test true
end

@testset "search_modes" begin
    L, K = 4, 30
    weights = rand(K)
    weights ./= sum(weights)
    gmm = Gmm{Float64}(weights, rand(L, K), [Probas.CholCov_rand(L) for k in 1:K])

    res = Predictions.search_modes(gmm, gmm.means, 1e-8)
    @test length(res) == K
end

@testset "regularization" begin
    L, K, N = 4, 5, 100
    Xs = [ rand(L, N) for k in 1:K] 
    out = Predictions.regularization(Xs)
    @test length(out) == N
end

@testset "merging" begin
    L, K = 4, 40
    weights = rand(K)
    weights ./= sum(weights)
    means = rand(L, K)
    covs = [ Probas.FullCov_rand(L) for _ in 1:K ]
    options = Predictions.MergingOptions(3)
    weights_m, means_m, covs_m = Predictions.merge(options, weights, means, covs)

    @test length(weights_m) == options.K_merged
    @test size(means_m) == (L, options.K_merged)
    @test length(covs_m) == options.K_merged
    @test isapprox(sum(weights_m), 1.)

    # merging preserves mean and cov 
    mean = Probas.mean_mixture(weights, means)
    mean_m = Probas.mean_mixture(weights_m, means_m)
    @test isapprox(mean, mean_m)
    cov  = Probas.covariance_mixture(weights, means, covs) 
    cov_m  = Probas.covariance_mixture(weights_m, means_m, covs_m) 
    @test isapprox(cov, cov_m)
end
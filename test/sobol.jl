using FastBayesianInversion.Sobol 
using FastBayesianInversion.Gllim 
using FastBayesianInversion.Models 
using Test 



@testset "sobol" begin
    L, D, K = 10, 70, 50
    gl = Gllim.GLLiM_rand(K, L, D)
    sobols, covs = Sobol.indice_sobol_inverse(gl, 100)
    @test size(sobols) == (L,  D)
    @test size(covs) == (L,  D)

    sobols, covs = Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiMInverse(gl), 10)
    @test size(sobols) == (D, L)
    @test size(covs) == (L,)

    c = Models.LinearSmall()
    D, L = Models.get_D(c), Models.get_L(c)
    sobols, covs = Sobol.indice_sobol_direct(c, 100)
    @test size(sobols) == (L, D)
    @test size(covs) == (D,)
end

@testset "determination" begin
    L, D, K = 10, 70, 50
    gl = Gllim.GLLiM_rand(K, L, D)
    J = collect(1:2:70)

    det = Sobol.indice_determination(gl, J, 100)
    @test size(det) == (L,)
end
using Serialization
using FastBayesianInversion
using FastBayesianInversion.Is

using Test 

# this test Imis on a real example
BASE_FOLDER = dirname(dirname(pathof(FastBayesianInversion)))
is_model = deserialize(joinpath(BASE_FOLDER, "test", "imis_hapke.ser"))

@testset "imis on howardite" begin
    imis_params = Is.ImisParams(2000, 1000, 25)
    for _ in 1:10
        pred, _ =  Is.exploit_samples(Is.imis_ext(imis_params, is_model)..., Is.NonUniformityParams(1))
        @test all(pred.mean .> 0)
    end
end 
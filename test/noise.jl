using FastBayesianInversion.Models 
using FastBayesianInversion.Noise 
using FastBayesianInversion.Gllim 
using FastBayesianInversion.Probas 
using FastBayesianInversion.Is 
using FastBayesianInversion.Log 
using Test 
using LinearAlgebra

Log.init()

@testset "noise_em" begin
    model = Models.InjectifHard()
    L, D = Models.get_L(model), Models.get_D(model)
    K, Ns, Nobs = 20, 10, 10
    gllim_chol_covs = [ Probas.CholCov_rand(L) for _ in 1:K ]
    weightss = rand(K, Nobs)
    weightss ./= sum(weightss, dims=1)
    meanss = rand(L, K, Nobs)
    gmms = [Gmm{Float64}(weightss[:,i], meanss[:,:,i], gllim_chol_covs)  for i in 1:Nobs ]

    Xobs, Yobs = Models.data_training(model, Nobs, 0.001)

    ws = rand(Ns, Nobs)
    FXs = rand(D, Ns, Nobs)

    estimator = Noise.theorical_estimators(model, Xobs, Yobs)

    current_a = 1.
    current_mean = rand(D)
    current_cov = Diagonal(rand(D) .+ 2)
    bias = Noise.Bias(current_a, current_mean, current_cov)

    F = Models.closure(model)
    params = Gllim.FunctionnalInitParams(K, F)
    perfect_gllim = Gllim.initialisation_gllim(Xobs, Yobs, params)

    for is_params in [Is.IsParams(Ns), Is.ImisParams(2000, 500, 3)] 
    Noise.e_step(is_params, Yobs, model, gmms, bias)

    params = Noise.EmIsGLLiMParams(init=bias, max_iterations=3)
    params.is_params = is_params
    history = Noise.run(params, Yobs,  model, perfect_gllim)
    @test length(history) == 4 #  3 + 1
end
end

@testset "noise_gradient" begin
    Nobs = 100
    # Xtrain = rand(10, 10000) .+ 1
    Ytrain = rand(10, 100000) .+ 2
    Xobs = rand(10, Nobs) .+ 1
    Yobs = 0.9 * rand(10, Nobs) .+ 2
    p = Noise.GradientParams(max_iterations=5, fixed_a=0.9, beta=0.5)

    x = zeros(10)
    a = 1.2
    dir = 20 * rand(11)

    Y_generator = (N) -> rand(10, 100000)
    history = Noise.run(p, Yobs, Y_generator) 
    @test length(history) <= 5
end
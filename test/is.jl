using Test 
using LinearAlgebra
using Random

using FastBayesianInversion.Probas
using FastBayesianInversion.Models
using FastBayesianInversion.Is

struct SimpleGaussianModel <: Is.IsModel
    prop::Is.IsProposition
end
SimpleGaussianModel() = SimpleGaussianModel(Is.GaussianProposition([0.5, 0.5], Diagonal(0.5 * ones(2))))

Is.get_L(m::SimpleGaussianModel) = 2
Is.get_proposition(m::SimpleGaussianModel) = m.prop
Is.log_p(m::SimpleGaussianModel, x::VecOrSub, _::Int) = Probas.log_gaussian_density(x, [0.1, 0.9], UniformScaling(0.05), 2)

@testset "imis" begin
    model =SimpleGaussianModel()
    L = Is.get_L(model)
    Xs, weigths = Is.imis_ext(Is.ImisParams(50, 30, 5), model)
    @test size(Xs) == (2, 50 + 30 * 5)
    @test isapprox(sum(weigths), 1)

    # a zero iteration IMIS is equivalent to bare IS
    Ns = 1000
    Random.seed!(1234)
    Xs1, weights1 = zeros(L, Ns), zeros(Ns)
    Is.importance_sampling!(model, Xs1, weights1)
    Random.seed!(1234)
    Xs2, weights2 = Is.imis_ext(Is.ImisParams(Ns,0,0), model)
    @test isapprox(Xs1, Xs2)
    @test isapprox(weights1, weights2)
end

struct dummyModel <: Is.IsModel
    p::Is.IsProposition
    A::Matrix 
    y::Vector
end 
@inline function Is.get_L(m::dummyModel)
    _, L = size(m.A)
    L
end
@inline Is.get_proposition(m::dummyModel)::Is.IsProposition = m.p
# enforce value in [0,1]
function Is.log_p(m::dummyModel, x::VecOrSub, j::Int) 
    if Models.is_valid(x, Is.get_L(m))
        - 0.5 * norm(m.A * x - m.y)
    else 
        -Inf
    end
end

@testset "samples" begin
    model = SimpleGaussianModel()
    imis_params = Is.ImisParams(50, 30, 5)
    Xs1, ws1 = Is.importance_sampling(model, imis_params )
    Xs2, ws2 = Is.importance_sampling(model, Is.IsParams(50))
    @test size(Xs1) == (2, Is.Ns(imis_params))
    @test size(ws1) == (Is.Ns(imis_params), )
    @test size(Xs2) == (2, 50)
    @test size(ws2) == (50, )
end

function convex_hull(X::Matrix)
    L, _ = size(X)
    out = Vector{Pair{Float64,Float64}}(undef, L)
    for l in 1:L
        min = minimum(X[l, :])
        max = maximum(X[l, :])
        out[l] = min => max
    end
    out
end

@testset "convergence" begin
    ref = [0.1, 0.9]
    Nss = 3 .^ [3,8,13]
    L,K = length(ref), 2
    w = rand(K)
    @testset "$(typeof(prop))" for prop in [
        Is.GaussianProposition([0.5, 0.5], Diagonal(0.5 * ones(2))),
        Is.GaussianMixtureProposition{Float64}(w ./ sum(w), rand(L, K), [LowerTriangular(Matrix(Diagonal(0.5 * ones(L)))) for _ in 1:K])
    ]
        m = SimpleGaussianModel(prop)
        errs = []
        nb_try = 10 # to avoid stat. noise
        for Ns in Nss 
            err = 0
            for _ in nb_try
                pred , _ = Is.importance_sampling(m, Is.IsParams(Ns), Is.NonUniformityParams(20))
                err += norm(ref .- pred.mean)
            end
            push!(errs, err / nb_try)
        end
        # error must go down when N raises
        @test sortperm(errs; rev = true) == 1:length(Nss)
    end
end

@testset "propositions" begin
    T = Float64
    L, D, Ns = 4, 70, 100
    K = 5
    mean, cov = rand(L), Probas.FullCov_rand(L)
    A = rand(D, L)
    y = rand(D)
    for p in [
        Is.GaussianProposition(mean, Probas.DiagCov_rand(L))
        Is.GaussianMixtureProposition{T}(rand(K), rand(L, K), [Probas.CholCov_rand(L) for _ in 1:K])
    ]
        m = dummyModel(p, A, y)
        X, weights = zeros(L, Ns), zeros(Ns)
        Is.importance_sampling!(m, X, weights)
        res::Is.IsInversion, _ = Is.exploit_samples(X, weights, Is.NonUniformityParams(5))

        @test size(res.mean) == (L,)
        @test size(res.cov) == (L,)
        
        X[:, weights .== 0] .= 0 # remove invalid points 
        @testset "$(weights[i]) $(X[:, i])" for i in 1:Ns         
            @test Models.is_valid(X[:, i], L) 
        end
        @test Models.is_valid(res.mean, L)

        convex = convex_hull(X)
        for (i, pair) in enumerate(convex)
            @test pair.first <= res.mean[i] && res.mean[i] <= pair.second
        end

    end
end
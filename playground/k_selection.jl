module KSel

using Dates

using FastBayesianInversion.Models
using FastBayesianInversion.Perf
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Pipeline

struct KSelection
    direct_error::Vector{Float64}
    sigma_error::Vector{Float64}
    bic::Vector{Float64}
    log_likelihood::Vector{Float64} # additionnal bic info
    dims::Vector{Int} # additionnal bic info
    bic_test::Vector{Float64}
    models::Vector{Gllim.GLLiM}
    KSelection(Nk::Int) = new(zeros(Nk), zeros(Nk), zeros(Nk), zeros(Nk), zeros(Int, Nk), zeros(Nk), Vector{Gllim.GLLiM}(undef, Nk))
end

function KSelectionParams(K::Int)::Gllim.Params
    params = Gllim.Params(Gllim.MultiInitParams(10, Gmms.EmParams(50, 30, 20, 1e-12)), Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(300, 1e-5), 1e-12))
    Gllim.setK!(params.init, K)
    params 
end

const Krange = collect(range(5, 100; step=5))

N_train_select_K(Ks::Vector{Int}) = maximum(Ks) * 1_000

function train_select_K(context::Models.Model, Ks::Vector{Int}, std_train::Float64)
    # generate one training set, suitable for all choices ok K
    Ntrain = N_train_select_K(Ks)
    X, Y = Models.data_training(context, Ntrain, std_train)
    # separated test dataset to assess overfitting
    Xtest, Ytest = Models.data_training(context, Ntrain, std_train)
    L, _ = size(X)
    D, _ = size(Y)

    out = KSelection(length(Ks))
    Log.indent()
    for (i, K) in enumerate(Ks)
        @info "Starting model $i (K = $K)..."

        params = KSelectionParams(K)
    
        Log.indent() # training
        gllim, _  = Gllim.train(params, X, Y)
        Log.deindent()

        # criteria computation
        out.direct_error[i] = Perf.direct_error(context, gllim)
        out.sigma_error[i] = Perf.sigma_error(gllim)
        bic, ll, dimension = Perf.bic(gllim, X, Y)
        out.bic[i] = bic
        out.log_likelihood[i] = ll
        out.dims[i] = dimension
        bic_test, _, _ = Perf.bic(gllim, Xtest, Ytest)
        out.bic_test[i] = bic_test

        # save the learned model 
        out.models[i] = gllim 
        
        flush(Base.stdout) # useful in batch mode 
    end
    Log.deindent()
    out 
end

function train_select_K(st::Store.Storage, context::Models.Model, Ks::Vector{Int}, Ystd::Float64)
    ti = Dates.now()
    out = train_select_K(context, Ks, Ystd)
    duration = Dates.now() - ti

    Store.save(st, [out, duration], Store.FO_MISC, context, Ks, Ystd)
end


end
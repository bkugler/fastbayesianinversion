using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Gmms
using FastBayesianInversion.Gllim
using FastBayesianInversion.Models
using FastBayesianInversion.Predictions
using FastBayesianInversion.Sobol
using FastBayesianInversion.Store
using FastBayesianInversion.Shared
using FastBayesianInversion.Is
using FastBayesianInversion.Moni

using Statistics
using Combinatorics
using LinearAlgebra

include("plotting/sobol.jl")
include("env.jl")

Log.init()
st = Store.Storage(ROOT_PATH_STORE)

struct SobolGeoms 
    incs::Vector 
    emes::Vector 
    phis::Vector 
end

# generate each combinaison of the given geometries
function setup_geometries(sg::SobolGeoms)
    I,  E, P =  length(sg.incs), length(sg.emes), length(sg.phis)
    out = Models.HapkeGeometries(I * E * P)
    index = 1
    for i in sg.incs 
        for e in sg.emes
            for p  in sg.phis 
                out.inc[index] = i
                out.eme[index] = e 
                out.azi[index] = p 
                index += 1
            end
        end
    end
    out
end


struct SobolIndices
    direct_gllim::Matrix # using the formula for GLLiM
    direct_F_gllim::Matrix # using the approximated function F_theta
    direct_F::Matrix # using the original function
    inverse_gllim::Matrix # using the formula for GLLiM inverted
    inverse_F_gllim::Matrix # using the function yielded by the GLLiM inverse

    determination::Matrix # average conditionnal variance
end

function _run_sobol(c::Models.Model, gllim::Gllim.GLLiM, run::Bool)
    if run
        @time sobols_d, _ = Sobol.indice_sobol_direct(gllim)
        @time sobols_d2, _ = Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiM(gllim), 10000)
        @time sobols_d3, _ = Sobol.indice_sobol_direct(c)
        @time sobols_i, determination = Sobol.indice_sobol_inverse(gllim)
        @time sobols_i2, _ = Sobol.indice_sobol_direct(Sobol.FunctionnalGLLiMInverse(gllim), 1000)
        sobols_i2 = Matrix(sobols_i2') # to respect plot convention
        indices = SobolIndices(sobols_d, sobols_d2, sobols_d3, sobols_i, sobols_i2, determination)
        Store.save(st, indices, Store.FO_MISC, c, gllim)
    end
    indices = Store.load(st, Store.FO_MISC, c, gllim)
end

function _plot_one_sobol(sampled_geoms, label, sobols, varnames, normalize)
    p = plot_sobol(Models.matrix(sampled_geoms), sobols; varnames=varnames,normalize=normalize)
    path = Store.get_path(st, Store.FO_GRAPHS, sampled_geoms, normalize; label=label)
    png(p, path)
    @info "Saved in $path"
end

function _train_gllim(sampled_geoms::Models.HapkeGeometries, train_std::Number, run)
    c = Models.Hapke("custom geometries", sampled_geoms)
    
    _gllim_params = Gllim.Params(
        Gllim.FunctionnalInitParams(60, Models.closure(c)),
        # Gllim.MultiInitParams(5, Gmms.EmParams(40, 20, 10, 1e-12)),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(150, 1e-5), 1e-12)
    )
    _training_params = Shared.TrainingParams(100_000, train_std, _gllim_params)
    
    gllim, _, _ = Shared.generate_train_gllim(c, _training_params, st, run >= 2)
    c, gllim
end

function run_plot_sobol(sampled_geoms::Models.HapkeGeometries, run, normalize)
    c, gllim = _train_gllim(sampled_geoms, 0.001, run)
    
    indices = _run_sobol(c, gllim, run >= 1)
    
    varnames = Models.variables(c)
    _plot_one_sobol(sampled_geoms, "sobol direct gllim", indices.direct_gllim, varnames, normalize)
    _plot_one_sobol(sampled_geoms, "sobol direct F gllim", indices.direct_F_gllim, varnames, normalize)
    _plot_one_sobol(sampled_geoms, "sobol direct F", indices.direct_F, varnames, normalize)
    _plot_one_sobol(sampled_geoms, "sobol inverse", indices.inverse_gllim, varnames, normalize)
    _plot_one_sobol(sampled_geoms, "sobol inverse F-1", indices.inverse_F_gllim, varnames, normalize)
    _plot_one_sobol(sampled_geoms, "determination", indices.determination, varnames, normalize)
end 

struct Determination
    J::Vector{Int}
    det::Vector{Float64} # size L
    det_imis::Vector{Float64} # size L
    xerr::Vector{Float64} # size L
    yerr::Float64 # average relative reconstruction error
end

# same logic as Pipeline.complete_inversion, but simplified
# to the mean; no additional std on yobs
function _imis_mean_inversion(context::Models.Model, Yobs::Matrix, trained_gllim::Gllim.GLLiM, train_std::Number)
    sampling_params = Is.ImisParams(2000, 1000, 20)
    D, N = size(Yobs)
    out_mean = zeros(Models.get_L(context), N)
    out_cov = similar(out_mean)
    # Threads.@threads for n in 1:N
    for n in 1:N
        _, _, model, _, _, _ = Shared.setup_inversion(context, Yobs[:,  n], zeros(D), trained_gllim, train_std, Predictions.MergingOptions(2))
        # sampling computation
        mean_with_is, _ = Is.importance_sampling(model, sampling_params, Is.NonUniformityParams(2))
        # do no scale to physical
        out_mean[:, n] = mean_with_is.mean
        out_cov[:, n] = mean_with_is.cov
    end
    out_mean, out_cov
end


""" 
Try the geometries given in `subsets`, as indexes in `geom`.
We use 3 indicators to assess the quality of a subset:
    - the average variance of the posterior
    - the average mean prediction error
    - the average relative reconstruction error
"""
function run_determination(geom::Models.HapkeGeometries, subsets::Vector{Vector{Int}}, run)
    train_std = 0.00001
    c, gllim = _train_gllim(geom, train_std, run)
    Nobs = 100
    Xobs, Yobs = Models.data_observations(c, Nobs)
    L, N = size(Xobs)
    if run >= 1
        LS = length(subsets)
        out = Vector{Determination}(undef, LS)

        @debug "Starting $LS determination computations..."

        mon = Moni.Monitor(LS; step=5)
        Threads.@threads for i in 1:LS
            J = subsets[i]
            determination = Sobol.indice_determination(gllim, J, 10000)

            sub_g = Gllim.sub_gllim(gllim, J)
            # IS/IMIS need a context with the right geometries 
            sub_geoms = Models.HapkeGeometries(geom.inc[J], geom.eme[J], geom.azi[J])
            sub_c = Models.Hapke("custom geometries $i", sub_geoms)
            Xmean, Xcov = _imis_mean_inversion(sub_c, Yobs[J, :], sub_g, train_std)
            determination_imis = sum(Xcov; dims=2)[:, 1] / Nobs

            xerr = [norm(Xmean[l, :] .- Xobs[l, :]) / N for l in 1:L]
            yerr = Statistics.mean(Y_relative_err(sub_c, Yobs[J, :], Xmean, 1., 0.))

            out[i] =  Determination(J, determination, determination_imis, xerr, yerr)

            Moni.update!(mon)
        end
        Store.save(st, out, Store.FO_MISC, :determination, geom, subsets)
    end
    out = Store.load(st, Store.FO_MISC, :determination, geom, subsets)
    c, out
end

function _plot_geoms(geoms::Models.HapkeGeometries, label="geometries")
    subplots = []
    for (j, inc) in enumerate(unique(geoms.inc))
        if j > 1 # for now, only show one incidence
            break
        end
        ids = geoms.inc .== inc
        azi = geoms.azi * pi / 180  # azimuth en radians
        p = scatter(azi[ids], geoms.eme[ids], proj=:polar, label=label, legendfontsize=12, title="incidence $inc °")
        push!(subplots, p)
    end
    subplots
end

function plot_determinations(c::Models.Model, geom::Models.HapkeGeometries, res::Vector{Determination}, colors; show_labels=true, show_geoms=true)
    D = maximum(length(r.J) for r in res)
    varnames = Models.variables(c)
    L = length(varnames)
    ds = unique([length(r.J) for r in res])
    # for each d, find the best value in everage over the variables
    best_indexes_det, best_indexes_det_imis, best_indexes_xerr = [], [], []
    for d in ds 
        indexes = [i for (i, r) in enumerate(res) if length(r.J) == d]
        _, i_det = findmin([sum(r.det) for r in res[indexes]])
        _, i_det_imis = findmin([sum(r.det_imis) for r in res[indexes]])
        _, i_xerr = findmin([sum(r.xerr) for r in res[indexes]])
        push!(best_indexes_det, indexes[i_det])
        push!(best_indexes_det_imis, indexes[i_det_imis])
        push!(best_indexes_xerr, indexes[i_xerr])
    end

    subplots_det_gllim = []
    subplots_det_imis = []
    subplots_errors = []
    xs = [length(r.J) for r in res]
    for l in 1:L # plot variable per variable
        ys_xerr = [r.xerr[l] for r in res]

        # determination
        ys_det = [r.det[l] for r in res]
        p_det_gllim = scatter(xs, ys_det; title=varnames[l], xlabel="D", label="determination (Gllim)", markercolor=colors)
        ybests = [r.det[l] for r in res[best_indexes_det]]
        scatter!(p_det_gllim, ds, ybests; markershape=:rect, label="best determination (average over variables)")

        # determination
        ys_det_imis = [r.det_imis[l] for r in res]
        p_det_imis = scatter(xs, ys_det_imis; title=varnames[l], xlabel="D", label="determination (Imis)", markercolor=colors, yscale=:log10)
        ybests = [r.det[l] for r in res[best_indexes_det_imis]]
        scatter!(p_det_imis, ds, ybests; markershape=:rect, label="best determination (average over variables)")

        # x errors
        p_xerr = scatter(xs, ys_xerr; title=varnames[l], xlabel="D", label="x error", markercolor=colors, yscale=:log10)
        ybests = [r.xerr[l] for r in res[best_indexes_xerr]]
        scatter!(p_xerr, ds, ybests; markershape=:rect, label="best x error (average over variables)")



        if show_labels
            annots = ["$i" for i in 1:length(res)]
            annotate!(p_det_gllim, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_det, annots))])
            annotate!(p_det_gllim, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_det_imis, annots))])
            annotate!(p_xerr, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_xerr, annots))])
        end

        push!(subplots_det_gllim, p_det_gllim)
        push!(subplots_det_imis, p_det_imis)
        push!(subplots_errors, p_xerr)
    end

    # y errors
    ys_yerr = [r.yerr for r in res]
    p_yerr = scatter(xs, ys_yerr; title="reconstruction error", xlabel="D", label="y error", markercolor=colors, yscale=:log10)
    ybests = [r.yerr for r in res[best_indexes_det]]
    scatter!(p_yerr, ds, ybests; markershape=:rect, label="best determination (average over variables)")
    push!(subplots_errors, p_yerr)

    # we plot the main set of geometries as a summary in the main file ...
    if show_geoms
        push!(subplots_det_gllim, _plot_geoms(geom)...)
        push!(subplots_det_imis, _plot_geoms(geom)...)
    end
    
    # and we also detail all the sub geometries for clarity...
    subplots_details_geoms = []
    for (i, r) in enumerate(res) 
        sub_geoms = Models.HapkeGeometries(geom.inc[r.J], geom.eme[r.J], geom.azi[r.J])
        push!(subplots_details_geoms, _plot_geoms(sub_geoms, "config $i")...)
    end

    # and we plot the best geometries for each size 
    subplots_best_geoms = []
    for index in best_indexes_det
        r = res[index]
        sub_geoms = Models.HapkeGeometries(geom.inc[r.J], geom.eme[r.J], geom.azi[r.J])
        push!(subplots_best_geoms, _plot_geoms(sub_geoms, "d = $(length(r.J)) -> config $index")...)
    end

    subplots_best_geoms_errors = []
    for index in best_indexes_xerr
        r = res[index]
        sub_geoms = Models.HapkeGeometries(geom.inc[r.J], geom.eme[r.J], geom.azi[r.J])
        push!(subplots_best_geoms_errors, _plot_geoms(sub_geoms, "d = $(length(r.J)) -> config $index")...)
    end

    p_det_gllim = plot(subplots_det_gllim..., dpi=200, size=(2000, 1000))
    p_det_imis = plot(subplots_det_imis..., dpi=200, size=(2000, 1000))
    p_errors = plot(subplots_errors..., dpi=200, size=(2000, 1000))
    # p_det = plot(subplots_details_geoms..., dpi=200, size=(3000, 2500))
    p_best = plot(subplots_best_geoms..., dpi=200, size=(2000, 2000))
    p_best_errors = plot(subplots_best_geoms_errors..., dpi=200, size=(2000, 2000))

    path = Store.get_path(st, Store.FO_GRAPHS, c, geom, [r.J for r in res])
    png(p_det_gllim, path * "determinations_gllim")
    png(p_det_imis, path * "determinations_imis")
    png(p_errors, path * "errors")
    # png(p_det, path * "geometries")
    png(p_best, path * "bests")
    png(p_best_errors, path * "bests_errors")
    @info "Saved in $path"
end

function plot_geometries(all_geoms::Models.HapkeGeometries, subsets::Vector{Vector{Int}})
    subplots_details_geoms = []
    for (i, subset) in enumerate(subsets) 
        sub_geoms = Models.HapkeGeometries(all_geoms.inc[subset], all_geoms.eme[subset], all_geoms.azi[subset])
        push!(subplots_details_geoms, _plot_geoms(sub_geoms, "config $i")...)
    end
    p = plot(subplots_details_geoms..., dpi=200, size=(3000, 2500))
    path = Store.get_path(st, Store.FO_GRAPHS, all_geoms, subsets) * "details"
    png(p, path)
    @info "Plotted in $path"
end

# g_samples = SobolGeoms([10,30,50], [15, 30, 40, 50, 70, 75], [20, 50, 70, 90, 120,170])
# geom = setup_geometries(g_samples, false)
# run_plot_sobol(geom, 0, true)


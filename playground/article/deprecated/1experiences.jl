using FastBayesianInversion.Models
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Predictions

include("../k_selection.jl")
include("2utils.jl")

using .KSel

""" Provides decent default values """
TrainingParams(train_std, K) =  Shared.TrainingParams(
    50_000,
    train_std,
    Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(K, 30, 15, 1e-12)
        ),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )
)
InversionParams(train_std, N_is::Int, imis::Is.ImisParams) = Pipeline.InversionParams(
    train_std, 
    Predictions.MergingOptions(2),
    N_is, 
    imis,
    false,
    false,
    Is.NonUniformityParams(1),
    0
)
# ProcedureParams(train_std, K::Int, N_is, imis::Is.ImisParams) = Pipeline.ProcedureParams(
#     TrainingParams(train_std, K),
#     InversionParams(train_std, N_is, imis),
# )

exp_double_sol = Experience("Simple double solutions problem",
    "Illustrer graphiquement l'apport de la prédiction par les centroids $(N.GllimMergedCentroid1) et $(N.GllimMergedCentroid2) par rapport à la moyenne.",
    "Inversion d'une fonction à deux solutions, simple.", 
    Synthetiques(),
    Models.DoubleSolutions(1), 
    double_solutions_simple,
    [
      ("Inversion settings", ProcedureParams(0.01, 40, 10_000, Is.ImisParams(2000, 500, 10))),      
      ("Number of observations ",  100),
      ("Observations std. ratio", 1000)
    ]
)

exp_centroids_hard = Experience("Centroids for double solutions", 
    "Cas de solutions multiples : $(N.GllimMean) n'est assez performant. $(N.GllimMergedCentroid1) et $(N.GllimMergedCentroid2) sont nécessaires, et peuvent être améliorés par IS.", 
    "Inversion d'une fonctionnelle synthétique construite pour avoir exactement 2 solutions pour chaque observation.", 
    Synthetiques(),
    Models.DoubleHard(),
    double_solutions_complete,
    [
        ("Inversion settings", ProcedureParams(0.01, 50, 20_000, Is.ImisParams(4000, 1000, 10))),      
        ("K variants", [30, 50, 70]),
        ("Number of observations ",  1000),
        ("Observations std. ratio", 1000)
    ]
)

exp_hapke_synth = Experience("Hapke - Synthetic data", 
    "Comportement du modèle cible : mise en évidence des occasionnelles solutions multiples.", 
    "Inversion du modèle de Hapke (géometries de la série Olivine)", 
    Synthetiques(), 
    HapkeNontronite(),
    hapke_synthetique,
    [
        ("Inversion settings", ProcedureParams(0.0001, 50, 50_000, Is.ImisParams(2500, 1000, 20))),      
        ("K variants", [40, 70, 100]),
        ("Number of observations ",  1000),
        ("Observations std. ratio", 1000)
    ],
)
    
exp_hapke_lab = Experience("Hapke - Laboratory observations",
    "Comparaison avec la référence MCMC",
    """Inversion des données réelles (série Basalt ou une autres des 5) et comparaison avec MCMC. Plot des solutions X ($(N.IsMean) et $(N.Best)). Plot des observables les plus différentes.
    Note : selon Pilorget : "An uncertainty of 5\\%, that represents the uncertainty on the measurement is used in the inversion procedure."
    """,
    Reelles(),
    HapkeNontronite(),
     hapke_lab,
     [
         ("Learning settings",  Shared.TrainingParams(
             100_000, # equal max(Ks) * 1000
             0.001, # one of the noise studied on cluster
             KSel.KSelectionParams(1) # K will be set later
            )),
        ("K variants", [
            100, # best K
            25, # slope heuristic
        ]),
        ("Inversion settings", InversionParams(0.001, 50_000, Is.ImisParams(2500, 1000, 20))),
        ("Observations std. ratio", 20)
     ],
)

function _high_dim_settings(ct::Models.Model, floor::Number; add_prop_std::Number=0) 
    [
        ("Inversion settings", Pipeline.ProcedureParams(
            Shared.TrainingParams(
                50_000,
                0.005,
                Gllim.Params(
                    # Gllim.MultiInitParams(
                    #     10,
                    #     Gmms.EmParams(70, 30, 15, 1e-12)
                    # ),
                    Gllim.FunctionnalInitParams(50, Models.closure(ct)),
                    Gllim.TrainParams{Float64,FullCov{Float64},IsoCov{Float64}}(
                        Gllim.DefaultStoppingParams(200, 1e-5),
                        1e-12
                    )
                )
            ),
            Pipeline.InversionParams(
                0.005, 
                Predictions.MergingOptions(2),
                50_000, 
                Is.ImisParams(2500, 1000, 20),
                false,
                false,
                Is.NonUniformityParams(1),
                add_prop_std
            )
        )),        
        ("Number of observations ",  1000),
        ("Observations std. ratio", 50),
        ("Std floor", floor)
    ]
end

_chhd1 = HapkeHighDim1()
exp_hapke_high_dim1 = Experience("Hapke High Dim 1 - Synthetic data", 
    "Modèle en grande dimention", 
    "Inversion du modèle de Hapke (géometries arbitraires)", 
    Synthetiques(), 
    _chhd1,
    hapke_synthetique_high_dim,
    _high_dim_settings(_chhd1, 0.05),
)

_chhd2 = HapkeHighDim2()
exp_hapke_high_dim2 = Experience("Hapke High Dim 2 - Synthetic data", 
    "Modèle en grande dimention", 
    "Inversion du modèle de Hapke (géometries arbitraires)", 
    Synthetiques(), 
    _chhd2,
    hapke_synthetique_high_dim,
    _high_dim_settings(_chhd2, 0.05; add_prop_std=0.1),
)

_chhd3 = HapkeHighDim3()
exp_hapke_high_dim3 = Experience("Hapke High Dim 3 - Synthetic data", 
    "Modèle en grande dimention", 
    "Inversion du modèle de Hapke (géometries arbitraires)", 
    Synthetiques(), 
    _chhd3,
    hapke_synthetique_high_dim,
    _high_dim_settings(_chhd3, 0.05),
)

_chhd4 = HapkeMediumDim()
exp_hapke_high_dim4 = Experience("Hapke Medium Dim 4 - Synthetic data", 
    "Modèle en moyenne dimention", 
    "Inversion du modèle de Hapke (géometries arbitraires)", 
    Synthetiques(), 
    _chhd4,
    hapke_synthetique_high_dim,
    _high_dim_settings(_chhd4, 0),
)

exp_hapke_glace = Experience("Hapke - Glace observations",
     "Inverson massive, impossible à faire en MCMC",
     """Jeu de données le plus complet, avec dépendance spatiale et spectrale. Plot en fausse couleur : RGB(x1, x2, x3), où xi est la prédiction (normalisée) à la longueur d'onde i.
 
    On utilise l'écart-type fourni avec chaque observation.
    """, 
    Reelles(),
    HapkeGlaceMars(),
    hapke_massive,
    [
       ("Inversion settings", ProcedureParams(0.001, 50, 20_000, Is.ImisParams(2500, 800, 20)))
    ],
)

exp_noise_synth_1 = Experience("Noise estimation on synthetic data - \$ \\sigma = 0.2 \$", 
   "Montrer qu'on peut retrouver \$\\Sigma\$ sur données synthétiques", 
   "Estimation du bruit via EM-IS-GLLIM (sur le modèle de Hapke). Plot de la convergence.
   ", 
   Synthetiques(), 
   HapkeNontronite(),
   noise_synthetique,
   [
       ("Training settings",  TrainingParams(0.0001, 50)),
       ("Number of observations",    2000),
       (L"$\sigma$",    0.2),
       (L"$\sigma_{init}$",    0.5),
       ("Number of iterations",    25)
   ],
   )

exp_noise_synth_2 = Experience("Noise estimation on synthetic data - \$ \\sigma = 0.03 \$", 
   "Montrer qu'on peut retrouver \$\\Sigma\$ sur données synthétiques", 
   "Estimation du bruit via EM-IS-GLLIM (sur le modèle de Hapke). Plot de la convergence.
   ", 
   Synthetiques(), 
   HapkeNontronite(),
   noise_synthetique,
   [
        ("Training settings",  TrainingParams(0.0001, 50)),
       ("Number of observations",    2000),
       (L"$\sigma$",    0.03),
       (L"$\sigma_{init}$",    0.5),
       ("Number of iterations",    25)
   ],
   )
    
   # experience = Experience("Hapke - Noise estimation", 
    # "Comparer l'estimation du biais au bruit donné par l'expert",
    #  """Estimation d'un biais systématique sur un jeu d'observations faiblement bruitées.
    # D'un côté, l'algorithme proposé estime la somme du biais et du bruit moyen des observations. De l'autre, on dispose d'une référence de l'expert donnant le bruit moyen sur les observations (série Mukundpura). La comparaison qualitative des deux quantités donne une indication sur la pertinence du modèle physique.
    # """, 
    # Reelles(), 
    # Models.MukundpuraPoudreContext(),
    # noise_reel,
    # [
    #  ("GLLiM settings",   Gllim.GLLiMTraining(Gllim.MultiInit(), 50)),
    #  (L"$\sigma_{init}$",   1.),
    #  ("Number of iterations",   30)
    # ],
    # ),

exp_noise_nontronite = Experience("Noise estimation on Nontronite", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    HapkeNontronite(),
    noise_reel,
    [
        ("Training settings",  TrainingParams(0.001, 50)),
        (L"$\sigma_{init}$",   1.),
        ("Number of iterations",   30)
    ],
)

exp_noise_basalt = Experience("Noise estimation on Basalt", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    HapkeBasalt(),
    noise_reel,
    [
        ("Training settings",  TrainingParams(0.001, 50)),
     (L"$\sigma_{init}$",   1.),
     ("Number of iterations",   30)
    ],
)

exp_noise_olivine = Experience("Noise estimation on Olivine", 
    "Comparer l'estimation du biais au bruit donné par l'expert",
     """Idem. Jeu de données différent.""", 
    Reelles(), 
    HapkeOlivine(),
    noise_reel,
    [
        ("Training settings",  TrainingParams(0.001, 50)),
     (L"$\sigma_{init}$",   1.),
     ("Number of iterations",   30)
    ],
)




experiences = [
    exp_double_sol,
    exp_centroids_hard,
    exp_hapke_synth,
    exp_hapke_lab,
    exp_hapke_high_dim1,
    exp_hapke_high_dim2,
    exp_hapke_high_dim3,
    exp_hapke_high_dim4,
    exp_hapke_glace,
    exp_noise_synth_1,
    exp_noise_synth_2,
    exp_noise_nontronite,
    exp_noise_basalt,
    exp_noise_olivine,
]
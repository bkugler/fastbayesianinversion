using FastBayesianInversion.Models
using FastBayesianInversion.Noise 
using FastBayesianInversion.Gllim
using FastBayesianInversion.Store
using FastBayesianInversion.Schemes
using FastBayesianInversion.Pipeline.Mcmc

using LaTeXStrings
using Statistics 
using MAT 
using Dates 

include("latex_tables.jl")
include("../plotting/plots.jl")
include("../inversion_cube.jl")

""" Run and plot not using imis """
const IS_ONLY = 0
""" Run and plot not using is """ 
const IMIS_ONLY = 1 
""" Run and plot using both is and imis """
const IS_AND_IMIS = 2

# return a copy of `params`where is or imis are disabled (according to mode)
function _switch_is_imis(params::Pipeline.ProcedureParams, mode::Int) 
    in::Pipeline.InversionParams = params.inv_params
    if mode == IS_ONLY 
        in = Pipeline.InversionParams(in.train_std, in.merging_options, in.N_is, nothing, in.with_best_centroids, 
        in.with_mode_finding, in.non_uniformity, in.additional_proposition_std)
    elseif mode == IMIS_ONLY
        in = Pipeline.InversionParams(in.train_std, in.merging_options, nothing, in.imis_params, in.with_best_centroids, in.with_mode_finding, in.non_uniformity, in.additional_proposition_std) 
    end
    # else : nothing to do
    Pipeline.ProcedureParams(params.training_params, in)
end

_T = Vector{Pair{String,Pipeline.SerieConfig}}

function _push_is_imis_configs!(configs::_T, is::_T, imis::_T, mode::Int)
    if mode == IS_ONLY
        push!(configs, is...)
    elseif mode == IMIS_ONLY
        push!(configs, imis...)
    else 
        push!(configs, is..., imis...)
    end
end

"""
 ignore nan values 
"""
_safe_err(V) = (v = V[isfinite.(V)]; St(Statistics.mean(v), Statistics.std(v)))
"""
 use infinite norm so error stays in [0,1] 
"""
_err(pred, ref) = (err = maximum(abs.(pred .- ref); dims=1); _safe_err(err))


"""
 Compute errors statistics for the given results
"""
function basic_measure(context::Models.Model, Xobs::Matrix, preds::Vector{Shared.OneInversion}, is_imis_mode::Int)
    # we compute errors in math. space 
    Xobs = Models.from_physical(context, Xobs)
    
    meths = [
        Schemes.GllimMean, Schemes.GllimMergedCentroid(1), Schemes.GllimMergedCentroid(2), 
        Schemes.Best,
    ]
    if is_imis_mode in [IS_ONLY, IS_AND_IMIS]
        push!(meths, Schemes.IsMean, Schemes.IsCentroid(1), Schemes.IsCentroid(2))
    end 
    if is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]
       push!(meths, Schemes.ImisMean, Schemes.ImisCentroid(1), Schemes.ImisCentroid(2))
    end

    series = Pipeline.extract_series(preds, meths)

    aggregateY = serie -> _safe_err([v.y_err for v in serie])

    errors = Dict() # simple errors 
    for meth in meths 
        # x error 
        Xpred = Pipeline.aggregate(series[meth])
        # we normalise to have comparable values (in [0,1])
        Xpred = Models.from_physical(context, Xpred)
        err_X = _err(Xpred, Xobs)
        
        # residual error
        err_Ypred = aggregateY(series[meth])

        errors[meth]  = BasicError(err_X, err_Ypred)
    end 
    
    # special case for double solutions 
    alt_X = Models.alternative_solution(context, Xobs)
    # centroid errors 
    double = nothing
    # centroid with IS errors 
    doubleIS = nothing
    # centroid with IMIS errors 
    doubleIMIS = nothing
    if alt_X !== nothing 
        # we match centroids predictions
        ref, alt = unmix_centroids(Xobs, 
            series[Schemes.GllimMergedCentroid(1)], series[Schemes.GllimMergedCentroid(2)])
        err_X1 = _err(ref, Xobs)
        err_X2 = _err(alt, alt_X)
        double = DoubleSolutionError(err_X1, err_X2)
        
        if is_imis_mode in [IS_ONLY, IS_AND_IMIS]
            ref_IS, alt_IS = unmix_centroids(Xobs, 
                series[Schemes.IsCentroid(1)], series[Schemes.IsCentroid(2)])
            err_XIS1 = _err(ref_IS, Xobs)
            err_XIS2 = _err(alt_IS, alt_X)
            doubleIS = DoubleSolutionError(err_XIS1, err_XIS2)
        end 

        if is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]
            ref_IMIS, alt_IMIS = unmix_centroids(Xobs, 
            series[Schemes.ImisCentroid(1)], series[Schemes.ImisCentroid(2)])
            err_XIMIS1 = _err(ref_IMIS, Xobs)
            err_XIMIS2 = _err(alt_IMIS, alt_X)
            doubleIMIS = DoubleSolutionError(err_XIMIS1, err_XIMIS2)
        end
    end

    # group the results
    return DoubleSolutionErrors(errors, double, doubleIS, doubleIMIS)
end

function double_solutions_simple(st::Store.Storage, run, is_imis_mode::Int, context::Models.DoubleSolutions, params::Pipeline.ProcedureParams, Nobs::Int, obs_std_ratio::Number)
    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio
    params = _switch_is_imis(params, is_imis_mode)
    preds_vec, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)

    show_is, show_imis = is_imis_mode in [IS_ONLY, IS_AND_IMIS], is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]
    reg_meths = [ Schemes.GllimMergedCentroid("") ]
    if show_is 
        push!(reg_meths, Schemes.IsCentroid(""))
    end
    if show_imis
        push!(reg_meths, Schemes.ImisCentroid(""))
    end
    Pipeline.regularize_inversions!(preds_vec, reg_meths)
   
    configs = [
        Schemes.GllimMean => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha=0.8),
        Schemes.GllimMergedCentroid(1) => Pipeline.SerieConfig(N.GllimMergedCentroid1, :basic, "blue"; alpha=0.4),
        Schemes.GllimMergedCentroid(2) => Pipeline.SerieConfig(N.GllimMergedCentroid2, :basic, "orange"; alpha=0.5),
    ]
    sc_is = [Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :dashed, "red")]
    sc_imis = [Schemes.ImisMean => Pipeline.SerieConfig(N.ImisMean, :dashed, "brown")]
    _push_is_imis_configs!(configs, sc_is, sc_imis, is_imis_mode)

    preds = Pipeline.format_inversions(preds_vec, configs)

    preds = [preds...,
        Pipeline.Prediction(Xobs; config=Pipeline.SerieConfig(N.Xalt1, :basic, "green"; alpha=0.3)),
        Pipeline.Prediction(Xobs_alt; config=Pipeline.SerieConfig(N.Xalt2, :basic, "green"; alpha=0.3)),
    ]

    K = Gllim.getK(params.training_params.gllim_params.init)
    path = Store.get_path(st, Store.FO_GRAPHS, context, params; label="double_solutions_simple_K_$K")
    plot_components(preds;  savepath=path, varnames=[L"x"], xlabel=L"n")
    return [ct]
end

function double_solutions_complete(st::Store.Storage, run, is_imis_mode::Int, context::Models.DoubleHard, params::Pipeline.ProcedureParams,  K_variants::Vector{Int}, Nobs::Int, obs_std_ratio::Number)
    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio

    cts = []
    measures::Vector{DoubleSolutionErrors} = []
    params = _switch_is_imis(params, is_imis_mode)
    show_is, show_imis = is_imis_mode in [IS_ONLY, IS_AND_IMIS], is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]

    for K in K_variants 
        Gllim.setK!(params.training_params.gllim_params.init, K)

        preds_vec, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
        reg_meths = [ Schemes.GllimMergedCentroid("") ]
        if show_is 
            push!(reg_meths, Schemes.IsCentroid(""))
        end
        if show_imis
            push!(reg_meths, Schemes.ImisCentroid(""))
        end
        Pipeline.regularize_inversions!(preds_vec, reg_meths)

        # Plotting 
        configs = [
            # Schemes.GllimMean => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.5),
            # Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :dashed, "red"; alpha = 0.7),
            Schemes.GllimMergedCentroid(1) => Pipeline.SerieConfig(N.GllimMergedCentroid1, :circle, "blue"),
            Schemes.GllimMergedCentroid(2) => Pipeline.SerieConfig(N.GllimMergedCentroid2, :circle, "blue"),
        ]
        sc_is = [
            Schemes.IsCentroid(1) => Pipeline.SerieConfig(N.IsCentroid1, :circle, "orange"),
            Schemes.IsCentroid(2) => Pipeline.SerieConfig(N.IsCentroid2, :circle, "orange"),
        ]
        sc_imis = [
            Schemes.ImisCentroid(1) => Pipeline.SerieConfig(N.ImisCentroid1, :circle, "brown"),
            Schemes.ImisCentroid(2) => Pipeline.SerieConfig(N.ImisCentroid2, :circle, "brown"),
        ]
        _push_is_imis_configs!(configs, sc_is, sc_imis, is_imis_mode)

        preds = Pipeline.format_inversions(preds_vec, configs)

        preds = [preds...,
            Pipeline.Prediction(Xobs; config=Pipeline.SerieConfig(N.Xalt1, :basic, "green"; alpha=0.3)),
            Pipeline.Prediction(Xobs_alt; config=Pipeline.SerieConfig(N.Xalt2, :basic, "green"; alpha=0.3)),
        ]

        path = Store.get_path(st, Store.FO_GRAPHS, context, params; label="double_solutions_is_K_$K")
        plot_components(preds;  savepath=path, varnames=Models.variables(context), xlabel=L"n")

        # Errors stats (table)
        measure = basic_measure(context, Xobs, preds_vec, is_imis_mode)
        push!(measures, measure)
        push!(cts, ct)
    end

    table_meths = [
        # Schemes.IsMean,
        Schemes.GllimMergedCentroid(1),
        Schemes.GllimMergedCentroid(2),
    ]
    if show_is 
        push!(table_meths, Schemes.IsCentroid(1), Schemes.IsCentroid(2))
    end
    if show_imis 
        push!(table_meths, Schemes.ImisCentroid(1), Schemes.ImisCentroid(2))
    end
    options = TableOptions(table_meths, true, show_is, show_imis, true)
    latex_table = render_errors("double_solutions_is", measures, K_variants, options)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, params; label="double_solutions_sampling") * ".tex"
    write(path, latex_table)
    @info "Saved in $path"

    return cts
end

function hapke_synthetique(st::Store.Storage, run, is_imis_mode::Int, context::Models.Hapke,  params::Pipeline.ProcedureParams,  K_variants::Vector{Int}, Nobs::Int, obs_std_ratio::Number)
    Xobs, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs, similar(Xobs))
    Yobs_std = Yobs ./ obs_std_ratio

    cts = []
    measures::Vector{DoubleSolutionErrors} = []

    params = _switch_is_imis(params, is_imis_mode)
    show_is, show_imis = is_imis_mode in [IS_ONLY, IS_AND_IMIS], is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]

    for K in K_variants 
        Gllim.setK!(params.training_params.gllim_params.init, K)

        preds_vec, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)

        reg_meths = [ Schemes.GllimMergedCentroid("") ]
        if show_is 
            push!(reg_meths, Schemes.IsCentroid(""))
        end
        if show_imis
            push!(reg_meths, Schemes.ImisCentroid(""))
        end
        Pipeline.regularize_inversions!(preds_vec, reg_meths)
        
        # Plotting 
        configs = [
            Schemes.Best => Pipeline.SerieConfig(N.Best, :circle, "blue"; alpha=0.5),
            Schemes.GllimMean => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha=0.5),
        ]
        _push_is_imis_configs!(configs, 
            [Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :circle, "red"; alpha=0.5)], 
            [Schemes.ImisMean => Pipeline.SerieConfig(N.ImisMean, :circle, "brown"; alpha=0.5)], 
            is_imis_mode)
        preds = Pipeline.format_inversions(preds_vec, configs)

        preds = [preds...,
            Pipeline.Prediction(Xobs; config=Pipeline.SerieConfig(N.Xobs, :basic, "green")),
        ]
        # plot for the article with Best 
        path = Store.get_path(st, Store.FO_GRAPHS, context, params; label="hapke_synthetique_K:$K")
        plot_components(preds;  savepath=path,xlabel=L"n", varnames=Models.variables(context))

        # extra plot without Best 
        path = path * "_no_best"
        plot_components(preds[2:end];  savepath=path, varnames=Models.variables(context), xlabel=L"n")

        # Errors stats (table)
        measure = basic_measure(context, Xobs, preds_vec, is_imis_mode)
        push!(measures, measure)
        push!(cts, ct)
    end

    table_meths = [
        Schemes.GllimMean,
        Schemes.Best,
    ]
    if show_is 
        push!(table_meths,  Schemes.IsMean, Schemes.IsCentroid(1), Schemes.IsCentroid(2))
    end
    if show_imis 
        push!(table_meths,  Schemes.ImisMean, Schemes.ImisCentroid(1), Schemes.ImisCentroid(2))
    end
    options = TableOptions(table_meths, false, false, false, false)
    latex_table = render_errors("hapke_synthetique", measures, K_variants, options)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, params; label="hapke_synthetique") * ".tex"
    write(path, latex_table)
    @info "Saved in $path"

    return cts
end

# floor = 0 to desactivate it
function hapke_synthetique_high_dim(st::Store.Storage, run, is_imis_mode::Int, context::Models.Hapke,  params::Pipeline.ProcedureParams, Nobs::Int, obs_std_ratio::Number, floor::Number)
    Xobs, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs, similar(Xobs))
    Yobs_std = Yobs ./ obs_std_ratio

    if floor > 0
        # very low std may give GLLiM predictions outside of [0,1]
        # which in turn break samplings steps (IS and IMIS)
        Yobs_std[ Yobs_std .< floor ] .= floor
    end 

    params = _switch_is_imis(params, is_imis_mode)
    show_is, show_imis = is_imis_mode in [IS_ONLY, IS_AND_IMIS], is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]

    preds_vec, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)

    reg_meths = [ Schemes.GllimMergedCentroid("") ]
    if show_is 
        push!(reg_meths, Schemes.IsCentroid(""))
    end
    if show_imis
        push!(reg_meths, Schemes.ImisCentroid(""))
    end
    Pipeline.regularize_inversions!(preds_vec, reg_meths)
    
    # Plotting 
    configs = [
        Schemes.Best => Pipeline.SerieConfig(N.Best, :circle, "blue"; alpha=0.5),
        Schemes.GllimMean => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha=0.5),
    ]
    _push_is_imis_configs!(configs, 
        [Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :circle, "orange"; alpha=0.5)], 
        [Schemes.ImisMean => Pipeline.SerieConfig(N.ImisMean, :circle, "purple"; alpha=0.5)], 
        is_imis_mode)
    preds = Pipeline.format_inversions(preds_vec, configs)

    preds = [preds...,
        Pipeline.Prediction(Xobs; config=Pipeline.SerieConfig(N.Xobs, :dashed, "green")),
    ]

    path = Store.get_path(st, Store.FO_GRAPHS, context, params, obs_std_ratio, floor; label="hs_high_dim_SNR_$(obs_std_ratio)_FLOOR_$(floor)_ADD_STD_$(params.inv_params.additional_proposition_std)")
    plot_components(preds;  savepath=path,xlabel=L"n", varnames=Models.variables(context))

    return [ct]
end

function _hapke_lab(st::Store.Storage, run, is_imis_mode::Int, context::Models.Hapke, gllim_params::Shared.TrainingParams, inv_params::Pipeline.InversionParams, Yobs, Yobs_std, wavelengths, Xmcmc, Covsmcmc, is_floor::Bool)
    
    params = Pipeline.ProcedureParams(gllim_params, inv_params)
    params = _switch_is_imis(params, is_imis_mode)

    # we use the gllim models learned during K selection 
    # in practice, it is more convenient and it should be sufficient to remember K and train again 
    # but to be thorough we use here exactly the same parameters values
    if run >= 1
        trained_gllim  = Store.load(st, Store.FO_GLLIM, context, gllim_params, :K_selection)
        ti = Dates.now()
        preds_vec_selection, _ = Pipeline.complete_inversion(context, Yobs, Yobs_std, trained_gllim, params.inv_params, nothing)
        dura = Comptimes.since(ti)
        Store.save(st, [preds_vec_selection, dura], Store.FO_PREDICTIONS, context, gllim_params, params.inv_params, :K_selection)
    end
    preds_vec_selection, _ = Store.load(st, Store.FO_PREDICTIONS, context, gllim_params, params.inv_params, :K_selection)

    # we use the normal approach
    preds_vec, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)

    _plot_hapke_lab(st, context, is_imis_mode, Yobs, wavelengths, params, Xmcmc, Covsmcmc, preds_vec_selection, true, is_floor)
    _plot_hapke_lab(st, context, is_imis_mode, Yobs, wavelengths, params, Xmcmc, Covsmcmc, preds_vec, false, is_floor)
    return ct
end

function _plot_hapke_lab(st::Store.Storage, context, is_imis_mode::Int, Yobs, wavelengths, params::Pipeline.ProcedureParams, Xmcmc, Covsmcmc, preds_vec, is_from_K_selection::Bool, is_floor::Bool)
    K = Gllim.getK(params.training_params.gllim_params.init)
    label = "hapke_lab_K$K"
    if is_floor
        label *= "_floor" 
    end
    if is_from_K_selection 
        label *= "_from_K_sel"
    end

    configs = [
        Schemes.Best => Pipeline.SerieConfig(N.Best, :dashed, "blue"; hide_std=false),
    ]
    _push_is_imis_configs!(configs, 
        [Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :basic, "red"; hide_std=false)], 
        [Schemes.ImisMean => Pipeline.SerieConfig(N.ImisMean, :basic, "brown"; hide_std=false)], 
        is_imis_mode)
    preds = Pipeline.format_inversions(preds_vec, configs)

    mcmcYerr = Pipeline.Y_relative_err(context, Yobs, Models.from_physical(context, Xmcmc), 1., zeros(Models.get_D(context)))
    refPred = Pipeline.Prediction(Xmcmc, Covsmcmc;
        config=Pipeline.SerieConfig("MCMC", :basic, "green"; hide_std=false, hideX_err=true), Yerr=mcmcYerr)

    # bestWithFilter = median_filter(preds[1], context, Yobs, Pipeline.SerieConfig("GLLiM IS Mean with filter", :dashed, "black"); width = 2)
    preds = [preds..., refPred ]

    path = Store.get_path(st, Store.FO_GRAPHS, context, params; label=label)
    plot_components(preds;  savepath=path, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")

    X_best = Pipeline.get_by_key(preds, Schemes.Best)
    
    # second plots : observables 
    geom = Models.matrix(Models.geometries(context))
    series = [X_best, Xmcmc]

    noMathIs, noMathImis, noMathBest = N.noMath(N.IsMean), N.noMath(N.ImisMean), N.noMath(N.Best)
    labelIs, labelImis, labelBest = "\$F($noMathIs)\$", "\$F($noMathImis)\$", "\$F($noMathBest)\$"
    labels = [labelBest, L"F(\mathbf{x}_{MCMC})"]
    colors = ["black", "green", "red"]
    if is_imis_mode in [IS_ONLY, IS_AND_IMIS]
        X_ismean = Pipeline.get_by_key(preds, Schemes.IsMean)
        push!(series, X_ismean)
        push!(labels, labelIs)
        push!(colors, "blue")
    end
    if is_imis_mode in [IMIS_ONLY, IS_AND_IMIS]
        X_imismean = Pipeline.get_by_key(preds, Schemes.ImisMean)
        push!(series, X_imismean)
        push!(labels, labelImis)
        push!(colors, "pink")
    end
    # we compute the reconstruction
    Ys = [Models.F(context, Models.from_physical(context, X)) for X in series]
    Ys = [Yobs, Ys...]

    # obs_indexes = sortperm(mcmcYerr; rev = true)[1:3] #3 worst ref preds
    _, Nobs = size(Yobs)
    obs_indexes = [8, 44, 76] # expert value 
    subplots = []
    for (i, index) in enumerate(obs_indexes)
        wl = wavelengths[index] * 1000
        Yplot = hcat([Y[:,index] for Y in Ys]...)
        labels_wl = [N.Yobs * L" - $\lambda = " * string(wl) *  L" nm$",  labels...]
        titlefunc  = (inc, azi) -> "wavelength : $wl nm"
        p = plot_polar(Yplot, geom, labels, colors; title=titlefunc)
        push!(subplots, p)
    end 
    obs_plot = plot(subplots..., layout=(1, length(obs_indexes)), size=(length(obs_indexes) * 500, 500))
    obs_path = path * "observables.png"
    png(obs_plot, obs_path)
    @debug "Saved in $obs_path"
end

function hapke_lab(st::Store.Storage,run, is_imis_mode::Int,context::Models.Hapke, training_params::Shared.TrainingParams,
    K_variants::Vector{Int}, inv_params::Pipeline.InversionParams, obs_std_ratio::Number)
    datas::Data2D  = load_observations(context)
    Yobs, wavelengths = datas.Yobs, datas.wavelengths
    _, Nobs = size(Yobs)

    Yobs_std =  Yobs ./ obs_std_ratio # first std

    Yobs_std_floor = copy(Yobs_std)
    Yobs_std_floor[ Yobs_std_floor .< 0.01 ] .= 0.01 # floor at 0.01
    
    # we compute our own MCMC inversions
    mcmc_params = Mcmc.ParamsMH(training_params.train_std)    
    ct_mcmc, ct_mcmc_floor = Comptimes.Times(context, mcmc_params, Nobs), Comptimes.Times(context, mcmc_params, Nobs)
    if run >= 3
        invs_mcmc, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params)
        invs_mcmc_floor, dura_floor = Mcmc.complete_inversion(context, Yobs, Yobs_std_floor, mcmc_params)
        Store.save(st, [invs_mcmc, dura], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
        Store.save(st, [invs_mcmc_floor, dura_floor], Store.FO_PREDICTIONS, context, Yobs, Yobs_std_floor, mcmc_params)
    end
    invs_mcmc, ct_mcmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
    invs_mcmc_floor, ct_mcmc_floor.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std_floor, mcmc_params)

    Xmcmc, Covsmcmc = Pipeline.aggregate(invs_mcmc), Pipeline.aggregate_std(invs_mcmc).^2
    Xmcmc_floor, Covsmcmc_floor = Pipeline.aggregate(invs_mcmc_floor), Pipeline.aggregate_std(invs_mcmc_floor).^2

    # we only show computation times for floor version
    cts = [ct_mcmc_floor]
    for K in K_variants
        Gllim.setK!(training_params.gllim_params.init, K)
        _hapke_lab(st, run, is_imis_mode, context, training_params, inv_params, Yobs, Yobs_std, wavelengths, Xmcmc, Covsmcmc, false)
        ct = _hapke_lab(st, run, is_imis_mode, context, training_params, inv_params, Yobs, Yobs_std_floor, wavelengths, Xmcmc_floor, Covsmcmc_floor, true)
        push!(cts, ct)
    end
    return cts
end



# sigma is an std, not a covariance
function noise_synthetique(st::Store.Storage,run,_::Int, context::Models.Model, train_params::Shared.TrainingParams, Nobs::Int,
    sigma_target::Float64, sigma_init::Float64, max_iterations::Int)
    synthetic = Noise.Bias(1., 0. * ones(Models.get_D(context)), Diagonal(sigma_target^2 * ones(Models.get_D(context))))
    init = Noise.Bias(1., 0. * ones(Models.get_D(context)), Diagonal(sigma_init^2 * ones(Models.get_D(context))))

    noise_params = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init,        
        max_iterations=max_iterations, Ns=20_000)
    params = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params)

    # we save the Yobs between runs
    if run >= 1
        Xobs, Yobs = Models.data_training(context, Nobs, sigma_target, synthetic.a, synthetic.mu)
        Store.save(st, [Xobs, Yobs], Store.FO_NOISE, context, sigma_target, synthetic)
    end
    Xobs, Yobs = Store.load(st, Store.FO_NOISE, context, sigma_target, synthetic)

    history, estimator, ct = Pipeline.complete_noise_estimation(context, Yobs, Xobs, params, run, st)

    path = Store.get_path(st, Store.FO_GRAPHS, context, synthetic, init, params; label="noise_synthetic")
    plot_noise_estimation(history, synthetic, estimator; savepath=path)

    # polar plot for the last iteration
    geom = Models.matrix(Models.geometries(context)) # D x 3
    stds = hcat(sqrt.(diag(synthetic.sigma)), sqrt.(diag(estimator.sigma)), sqrt.(diag(history[end][1].sigma)))
    plot_polar(stds, geom, [L"\sigma_D", L"\sigma^{est}_D", L"\sigma^{last}_D"], ["black", "red", "blue"]; savepath=path * "polar")
    return [ct]
end

function noise_reel(st::Store.Storage, run, _::Int, context::Models.Hapke, train_params::Shared.TrainingParams, sigma_init::Float64, max_iterations::Int)
    # we compare diagonal constraint with isotropic constraint
    init_diag = Noise.Bias(1., 0. * ones(Models.get_D(context)), Diagonal(sigma_init * ones(Models.get_D(context))))
    init_iso = Noise.Bias(1., 0. * ones(Models.get_D(context)), UniformScaling(sigma_init))

    noise_params_diag = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_diag, max_iterations=max_iterations)
    noise_params_iso = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_iso, max_iterations=max_iterations)

    params_diag = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_diag)
    params_iso = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_iso)

    datas::Data2D  = load_observations(context)

    history_diag, _, ct_diag = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_diag, run, st)
    run_iso = run == 2 ? 1 : run # avoid re-learning
    history_iso, _, ct_iso = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_iso, run_iso, st)

    # final estimation
    std_est_diag = sqrt.(diag(history_diag[end][1].sigma))
    std_est_iso = sqrt.(history_iso[end][1].sigma * ones(Models.get_D(context))) # converted as vector

    # comparison with the obs mean noise 
    std_obs = Statistics.mean(datas.Yobs_std, dims=2)[:, 1]

    # polar plot for easy visualisation
    path = Store.get_path(st, Store.FO_GRAPHS, context, params_diag; label="noise_reel_$(Models.label(context))")
    
    stds = hcat(std_obs, std_est_diag, std_est_iso)
    labels = [notation_obs_std, notation_bias_std * " - Diagonal",  notation_bias_std * " - Isotropic"]
    colors = ["black", "red", "orange"]

    # we dont plot the diagonal constraint
    stds, labels, colors = stds[:, [1,3]], labels[[1,3]], colors[[1,3]]
    
    titlefunc = (inc, azi) -> Models.label(context)
    geom = Models.matrix(Models.geometries(context))
    plot_polar(stds, geom, labels, colors; savepath=path * "polar", title=titlefunc)
    # we only expose iso constraint 
    return [ct_iso]
end

function hapke_massive(st::Store.Storage, run, is_imis_mode::Int, context::Models.Hapke, params::Pipeline.ProcedureParams)
    datas::Data3D = load_observations(context, "S")

    # the export function expects both is and imis, so just 
    # use dummy parameters instead of nothing
    in = params.inv_params
    if is_imis_mode == IS_ONLY 
        in = Pipeline.InversionParams(in.train_std, in.merging_options, in.N_is, Is.ImisParams(50, 0, 0), in.with_best_centroids, 
        in.with_mode_finding, in.non_uniformity, in.additional_proposition_std)
    elseif is_imis_mode == IMIS_ONLY
        in = Pipeline.InversionParams(in.train_std, in.merging_options, 50, in.imis_params, in.with_best_centroids, in.with_mode_finding, in.non_uniformity, in.additional_proposition_std) 
    end
    params = Pipeline.ProcedureParams(params.training_params, in)

    ct = inverse_export_cube(run, context, params, datas, st; label="glace")
    return [ct]
end

function double_solutions_pdf(st::Store.Storage, run, training_params::Shared.TrainingParams, obs_std_ratios::Vector)
    ct = Models.DoubleSolutions(1)
    trained_gllim, _, _ = Shared.generate_train_gllim(ct, training_params, st, run >= 2)
    merg = Predictions.MergingOptions(2)
    # we choose one observable
    Xobs, Yobs = Models.data_observations(ct, 20)
    Xobs_alt = Models.alternative_solution(ct, Xobs)
    index = 5
    xobs, yobs, xobs_alt = Xobs[:, index], Yobs[:, index], Xobs_alt[:, index]

    subplots = []
    for obs_std_ratio in obs_std_ratios
        if run >= 1
            yobs_std = yobs / obs_std_ratio
    
            _, _, _, _, full_posterior, merged_posterior = Shared.setup_inversion(ct, yobs, yobs_std, trained_gllim, 
                training_params.train_std, merg) 
            Store.save(st, (full_posterior, merged_posterior), Store.FO_PREDICTIONS, ct, training_params, merg, obs_std_ratio, :pdf)
        end
        (full_posterior, merged_posterior) = Store.load(st, Store.FO_PREDICTIONS, ct, training_params, merg, obs_std_ratio, :pdf)

        allocated = zeros(Models.get_L(ct)) # needed as tmp memory
        xs = range(0, stop=1; length=1000)
        fs_full = [Probas.gmm_density([x], full_posterior, allocated) for x in xs]
        fs_merged = [Probas.gmm_density([x], merged_posterior, allocated) for x in xs]
        p = plot(xs, fs_full, label="full GMM posterior"; title="Observation SNR : $(obs_std_ratio)")
        plot!(p, xs, fs_merged, label="merged GMM posterior"; linestyle=:dash)
        vline!(p, [xobs[1],xobs_alt[1]], label="true solutions", alpha=0.5)
        push!(subplots, p)
    end
    p = plot(subplots...; dpi=Config.DPI)
    path = Store.get_path(st, Store.FO_GRAPHS, ct, training_params, obs_std_ratios; label="pdf_double")
    png(p, path)
@debug "Plotted in $path"
    end


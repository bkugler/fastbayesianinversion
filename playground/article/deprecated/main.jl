using FastBayesianInversion.Models
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Log 

using Colors
using Plots

include("1experiences.jl")

include("../env.jl")

include("../k_selection_r.jl")


st = Store.Storage(ROOT_PATH_STORE)

function run_simulations(runs::Vector, is_imis_mode::Int)
    cts::Vector{Comptimes.Times} = []
    for (i, (run, exp::Experience)) in enumerate(zip(runs, experiences))
        if run <= -1
            @info "Skipping exp. $i "
            continue
        end
        @info "Starting exp. $i - $(exp.titre)"
        Log.indent()
        params = [p for (name, p) in exp.params]
        current_cts = exp.job(st, run, is_imis_mode, exp.context, params...)
        Log.deindent()
        for c in current_cts
            c.exp_index = i
            c.label = exp.titre
            if c.kind == Comptimes.MCMC 
                c.label *= " - MCMC"
            end    
        end
        push!(cts, current_cts...)
    end
    cts
end

Log.init()
Plots.pyplot()
Config.LEGEND_BACKGROUND = Colors.RGBA(0.9, 0.9, 0.9, 0.7)
Config.DPI = 200
Config.CIRCLE_SIZE = 3

# K selections results must be exploited once 
# exploit_K_selection(st, experience4.context, KSel.Krange, [0.001])


# Uncomment to launch the simulations :
# 2  : full simulation 
# 1  : re-use existing trained model 
# 0  : re-use trained model and prediction (only plot)
# -1 : skip the experience

# cts = run_simulations([-1,-1,-1,-1,-1,1,-1,-1,-1,-1,-1,-1,-1,-1], IS_AND_IMIS)
# cts = run_simulations([1,1,1,1,1,1,1,1,1,1])
# cts = run_simulations([0,0,0,0,0,0,0,0,0,0])


# Uncomment to write meta-data

# generate_exps_parameters(experiences, true, ARTICLE_EXPORT_DIR * "simulations/exps.tex")
# generate_times(cts, ARTICLE_EXPORT_DIR * "simulations/times.tex")
# N.render_def_notations(ARTICLE_EXPORT_DIR * "preamble_notations.tex")
# N.render_table_notations(ARTICLE_EXPORT_DIR * "notations.tex")

# tp = TrainingParams(0.001, 30)
# double_solutions_pdf(st, 1, tp, [4, 8])



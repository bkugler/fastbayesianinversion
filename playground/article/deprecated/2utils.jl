# Generate a description of all the parameters
# used in the simulations

using LaTeXStrings
using Serialization
using Formatting
using FastBayesianInversion.Models
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Comptimes

include("3jobs.jl")

abstract type Donnees end 

struct Synthetiques <: Donnees
    # bruit::Float64 # std
end

struct Reelles <: Donnees
end

struct Experience 
    titre::String 
    but::String
    description::String 
    donnees::Donnees
    context::Models.Model
    job::Function # first two args are `run`and `context`
    params::Vector # args of `job` (starting after `context`) 
end

description_donnees(::Synthetiques) = "{\\em  synthetic data}"
description_donnees(::Reelles) = "{\\em  real data}"

# Latex pretty printing

# fallback 

function Base.show(io, ::MIME"text/latex", v)
    show(io, v)
end

function Base.show(io, ::MIME"text/latex", v::MesureParams)
    print(io, "\$N_{test} = $(v.N_test)\$ - \$\\sigma_{test} = $(v.std_gen)\$ - \$\\sigma_{obs} = $(v.std_test)\$")
end

function Base.show(io, ::MIME"text/latex", v::Gllim.MultiInitParams)
    emparams = "Joint GMM iterations : $(v.em_params.max_iterations) - Kmeans iterations : $(v.em_params.max_iterations_kmeans)"
    print(io, "multiple - K : $(v.em_params.K) - $(v.nb_starts) starts - $emparams")
end

function Base.show(io, ::MIME"text/latex", v::Gllim.DefaultStoppingParams)
    print(io, "max iterations : $(v.max_iterations) - relative threshold : $(v.relative_ratio_threshold)")
end

function Base.show(io, ::MIME"text/latex", v::Shared.TrainingParams)
    init = repr("text/latex", v.gllim_params.init)
    stop = repr("text/latex", v.gllim_params.train.stop)
    s =  """
   GLLiM with EM - details :
    \\begin{description}
        \\item[training set] size $(v.N_train), std $(v.train_std)
        \\item[init.] $init
        \\item[stop] $stop
    \\end{description}
    """
    print(io, s)
end

function Base.show(io, ::MIME"text/latex", v::Vector{T}) where T 
    items = join([repr("text/latex", arg) for arg in v], ", ")
    print(io, items)
end

function Base.show(io, ::MIME"text/latex", v::Pipeline.InversionParams)
    s = ""
    if v.N_is !== nothing 
        s = "$(v.N_is) IS samples"
    end
    if v.imis_params !== nothing 
        p = v.imis_params
        s *= " IMIS: ($(p.N0), $(p.J), $(p.B))"
    end
    print(io, s)
end

function Base.show(io, ::MIME"text/latex", v::Pipeline.ProcedureParams)
    train = repr("text/latex", v.training_params)
    inv = repr("text/latex", v.inv_params)
    s =  """ Full procedure 
     \\begin{description}
         \\item[training] $train
         \\item[inversion] $inv
     \\end{description}
     """
    print(io, s)
end

function description_params(params::Vector)
    fprint = x -> repr("text/latex", x)
    items = join([ "\\item[$name] $(fprint(arg))" for (name, arg) in params],
        "\n")
    """
    \\begin{description}
        $items
    \\end{description}
    """
    # replace(s, "\n"=> "\\\\")
end

function genere_experience(i::Int, exp::Experience, hideNotes::Bool) 
    donnees = description_donnees(exp.donnees)
    params = description_params(exp.params)
    notes = """ \\\\
    {\\em Objectif } : $(exp.but) \\\\
    {\\em Description } : $(exp.description) 
    """
    
    if hideNotes
        notes = "" 
    end

    return """
    {\\footnotesize
    \\paragraph{$i - $(exp.titre)}
    $donnees - $(Models.label(exp.context)) (\$L = $(Models.get_L(exp.context)) ,  D = $(Models.get_D(exp.context))\$)
    $notes
    }
    {\\scriptsize
        $params
    }
    """
end

function generate_exps_parameters(exps::Vector{Experience}, hideNotes::Bool,  out::String) 
    s =  join([genere_experience(i, exp, hideNotes) for (i, exp) in enumerate(exps)])
    write(out, s)
end



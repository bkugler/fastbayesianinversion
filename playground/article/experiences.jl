# Definitions of the experiments, with their hyper-parameters 

using FastBayesianInversion.Models
using FastBayesianInversion.Noise 
using FastBayesianInversion.Gllim
using FastBayesianInversion.Store
using FastBayesianInversion.Schemes
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Pipeline.TuneImis
using FastBayesianInversion.Pipeline

using LaTeXStrings
using Statistics 
using MAT 
using Dates 

include("latex_tables.jl")
include("../plotting/plots.jl")
include("../inversion_cube.jl")
include("../env.jl") # needed for the models data 


""" Provides decent default values """
TrainingParams(train_std, K) =  Shared.TrainingParams(
    50_000,
    train_std,
    Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(K, 30, 15, 1e-12)
        ),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )
)

# no centroids nor non-uniformity computations; no std increase
InversionParams(train_std, sampling::Is.SamplingParams) = Pipeline.InversionParams(
    train_std, 
    Predictions.MergingOptions(2),
    sampling,
    false,
    false,
    Is.NonUniformityParams(1),
    0
)

ProcedureParams(train_std, K::Int, sampling::Is.SamplingParams) = Pipeline.ProcedureParams(
    TrainingParams(train_std, K),
    InversionParams(train_std, sampling),
)

""" assume training is already done and don't repeat it """
_no_re_train(run::Int) = min(run, 1)

function double_solutions_simple(st::Store.Storage, run::Int)
    context = Models.DoubleSolutions(1)
    Nobs =  100
    obs_std_ratio = 1000
    K = 40
    std_train = 0.01
    params_is = ProcedureParams(std_train, K, Is.IsParams(10_000))
    params_imis = ProcedureParams(std_train, K, Is.ImisParams(4000, 600, 10))
    sample_mcmc = 100_000

    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio

    # MCMC reference

    mcmc_params_mh = Mcmc.ParamsMH(std_train;Niter=sample_mcmc, Nkeep=sample_mcmc / 2)
    mcmc_params_hmc = Mcmc.ParamsHMC(sample_mcmc, sample_mcmc / 2, std_train)  

    ct_mcmc_mh = Comptimes.Times(context, mcmc_params_mh, Nobs)
    ct_mcmc_hmc = Comptimes.Times(context, mcmc_params_hmc, Nobs)
    if run >= 3
        invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
        invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
        Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
    end
    invs_mcmc_mh, ct_mcmc_mh.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
    invs_mcmc_hmc, ct_mcmc_hmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)

    preds_is, _, _, ct_is = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)
    preds_imis, _, _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, _no_re_train(run), st)

    # group is and imis
    preds = merge(preds_is, preds_imis)
    reg_meths = [ Schemes.GllimMergedCentroid(""),Schemes.IsCentroid(""),Schemes.ImisCentroid("")]
    Pipeline.regularize_inversions!(preds, reg_meths)

    # plot 1 : Gllim + sampling
    plot_data = [
        preds[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.8, :solid, hide_std=true),
        preds[Schemes.GllimMergedCentroid(1)] => PlotOptions(N.GllimMergedCentroid1, "blue", 0.4, :solid, hide_std=true),
        preds[Schemes.GllimMergedCentroid(2)] => PlotOptions(N.GllimMergedCentroid2, "blue", 0.4, :solid, hide_std=true),
        preds[Schemes.IsMean] => PlotOptions(N.IsMean, "yellow", 0.7, :dash, hide_std=true),
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "orange", 0.7, :dash, hide_std=true),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xalt1, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs_alt) => PlotOptions(N.Xalt2, "green", 0.3, :solid, hide_std=true, markersize=0),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label="double_solutions_simple_1")
    plot_components(plot_data;  savepath=path, varnames=[L"x"], xlabel=L"n")

    # plot 2: MCMC
    plot_data = [
        invs_mcmc_mh => PlotOptions(N.McmcMH, "purple", 0.5, :none; markershape=:star, markersize=5, hide_std=true),
        invs_mcmc_hmc => PlotOptions(N.McmcHMC, "brown", 0.5, :none; markershape=:rect, markersize=5, hide_std=true),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xalt1, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs_alt) => PlotOptions(N.Xalt2, "green", 0.3, :solid, hide_std=true, markersize=0),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label="double_solutions_simple_2")
    plot_components(plot_data;  savepath=path, varnames=[L"x"], xlabel=L"n")

     # plot 3 : sampling on centroids
     plot_data = [
        preds[Schemes.IsCentroid(1)] => PlotOptions(N.IsCentroid1, "blue", 0.4, :dash, hide_std=true),
        preds[Schemes.IsCentroid(2)] => PlotOptions(N.IsCentroid2, "blue", 0.4, :dash, hide_std=true),
        preds[Schemes.ImisCentroid(1)] => PlotOptions(N.ImisCentroid1, "orange", 0.4, :dot, hide_std=true),
        preds[Schemes.ImisCentroid(2)] => PlotOptions(N.ImisCentroid2, "orange", 0.4, :dot, hide_std=true),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xalt1, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs_alt) => PlotOptions(N.Xalt2, "green", 0.3, :solid, hide_std=true, markersize=0),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label="double_solutions_simple_3")
    plot_components(plot_data;  savepath=path, varnames=[L"x"], xlabel=L"n")

    ct_is.label = "IS"
    ct_imis.label = "IMIS"
    ct_mcmc_mh.label = "MH"
    return [
        # ct_mcmc_mh,
    ct_is, ct_imis]
end

function triple_solutions_simple(st::Store.Storage, run::Int)
    context = Models.TripleSolutions(1)
    Nobs =  100
    obs_std_ratio = 20
    K = 40
    std_train = 0.01
    margin = 0.01
    training_params = TrainingParams(std_train, K)
    merging_options = Predictions.MergingOptions(3)
    
    trained_gllim::Gllim.GLLiM, _, _ = Shared.generate_train_gllim(context, training_params, st, run >= 2)
    
    Xobs1, Xobs2, Xobs3, Yobs = Models.data_observations(context, Nobs, margin)
    Yobs_std = Yobs ./ obs_std_ratio
    preds_vect = Vector{Shared.OneInversion}(undef, Nobs)
    for n in 1:Nobs
        yobs, ystd = Yobs[:, n], Yobs_std[:, n]
        _, _, _, _, full_posterior, merged_posterior = Shared.setup_inversion(context, yobs, ystd, trained_gllim, std_train, merging_options)
        _, invs = Pipeline.best_centroids_to_inversions(full_posterior, merging_options.K_merged)
        push!(invs, Pipeline.merged_centroids_to_inversions(merged_posterior)...)
        # we compute y errors
        invs = [Pipeline.add_y_err(context, yobs, iv) for iv in invs]
        preds_vect[n] = Dict((iv.key, iv) for iv in invs)
    end
    preds = Shared.predictions_series(preds_vect)
    reg_meths = [ Schemes.GllimMergedCentroid(""),Schemes.GllimBestCentroid("")]
    Pipeline.regularize_inversions!(preds, reg_meths)

    # plot 1 : Gllim + sampling
    plot_data = [
        preds[Schemes.GllimMergedCentroid(1)] => PlotOptions(N.GllimMergedCentroid1, "blue", 0.4, :solid, hide_std=true),
        preds[Schemes.GllimMergedCentroid(2)] => PlotOptions(N.GllimMergedCentroid2, "blue", 0.4, :solid, hide_std=true),
        preds[Schemes.GllimMergedCentroid(3)] => PlotOptions(N.GllimMergedCentroid3, "blue", 0.4, :solid, hide_std=true),

        preds[Schemes.GllimBestCentroid(1)] => PlotOptions(N.GllimBestCentroid1, "red", 0.4, :dash, hide_std=true, markershape=:star),
        preds[Schemes.GllimBestCentroid(2)] => PlotOptions(N.GllimBestCentroid2, "red", 0.4, :dash, hide_std=true, markershape=:star),
        preds[Schemes.GllimBestCentroid(3)] => PlotOptions(N.GllimBestCentroid3, "red", 0.4, :dash, hide_std=true, markershape=:star),

        Shared.MethodResult(Xobs1) => PlotOptions(N.Xalt1, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs2) => PlotOptions(N.Xalt2, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs3) => PlotOptions(N.Xalt3, "green", 0.3, :solid, hide_std=true, markersize=0),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, training_params, merging_options; label="triple_solutions_simple")
    plot_components(plot_data;  savepath=path, varnames=[L"x"], xlabel=L"n")

    return []
end


"""
 Compute errors statistics for the given results
"""
function _measure_errs(context::Models.Model, Xobs::Matrix, preds::Dict{String,Shared.MethodResult})
    # we compute errors in math. space
    Xobs = Models.from_physical(context, Xobs)

    meths = [
        Schemes.GllimMean, Schemes.GllimMergedCentroid(1), Schemes.GllimMergedCentroid(2),
        Schemes.Best,
        Schemes.IsMean, Schemes.IsCentroid(1), Schemes.IsCentroid(2),
        Schemes.ImisMean, Schemes.ImisCentroid(1), Schemes.ImisCentroid(2),
    ]

    # simple errors 
    errors = Dict(meth => BasicError(preds[meth], context, Xobs) for meth in meths if haskey(preds,meth))

    # special case for double solutions
    alt_X = Models.alternative_solution(context, Xobs)
    
    # we match centroids predictions, to either "ref" or "alt".
    function unmix_centroids(X1::Shared.MethodResult, X2::Shared.MethodResult)
        _, N = size(Xobs)
        ref, alt = similar(Xobs), similar(Xobs)
        for n in 1:N
            e1, e2 =  norm(X1.Xmean[:, n] .- Xobs[:, n]), norm(X2.Xmean[:, n] .- Xobs[:,n])
            if e1 > e2 # 2 is closer to ref
                ref[:, n] = X2.Xmean[:, n]
                alt[:, n] = X1.Xmean[:, n]
            else # 1 is closer to ref
                ref[:, n] = X1.Xmean[:, n]
                alt[:, n] = X2.Xmean[:, n]
            end
        end
        return ref, alt
    end

    if alt_X === nothing
        return DoubleSolutionErrors(errors, nothing, nothing, nothing) 
    end

    # centroid errors 
    ref, alt = unmix_centroids(preds[Schemes.GllimMergedCentroid(1)], preds[Schemes.GllimMergedCentroid(2)])
    double = DoubleSolutionError(_err(ref, Xobs), _err(alt, alt_X))
        
    # centroid with IS errors 
    ref_IS, alt_IS = unmix_centroids(preds[Schemes.IsCentroid(1)], preds[Schemes.IsCentroid(2)])
    doubleIS = DoubleSolutionError(_err(ref_IS, Xobs), _err(alt_IS, alt_X))

    # centroid with IMIS errors 
    ref_IMIS, alt_IMIS = unmix_centroids(preds[Schemes.ImisCentroid(1)], preds[Schemes.ImisCentroid(2)])
    doubleIMIS = DoubleSolutionError(_err(ref_IMIS, Xobs), _err(alt_IMIS, alt_X))

    # group the results
    return DoubleSolutionErrors(errors, double, doubleIS, doubleIMIS)
end

function double_solutions_complete(st::Store.Storage, run::Int)
    context = Models.DoubleHard()
    params_is = ProcedureParams(0.01, 50, Is.IsParams(20_000))
    params_imis = ProcedureParams(0.01, 50, Is.ImisParams(4000, 1000, 10))
    K_variants = [30, 50, 70]
    Nobs = 500
    obs_std_ratio =  1000
    
    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio

    cts = []
    measures::Vector{DoubleSolutionErrors} = []

    reg_meths = [ Schemes.GllimMergedCentroid(""),Schemes.IsCentroid(""), Schemes.ImisCentroid("")]
    for K in K_variants 
        Gllim.setK!(params_is.training_params.gllim_params.init, K)
        Gllim.setK!(params_imis.training_params.gllim_params.init, K)

        preds_is, _,  _, ct_is = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)
        preds_imis, _,  _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, _no_re_train(run), st)
        
        preds = merge(preds_is, preds_imis)
        Pipeline.regularize_inversions!(preds, reg_meths)

        # Plotting 
        plot_data = [
            # preds[Schemes.GllimMean] => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.5),
            # preds[Schemes.IsMean] => Pipeline.SerieConfig(N.IsMean, :dashed, "red"; alpha = 0.7),
            preds[Schemes.GllimMergedCentroid(1)] => PlotOptions(N.GllimMergedCentroid1,  "blue", 0.5, :none, markershape=:circle, markersize=2, hide_std=true),
            preds[Schemes.GllimMergedCentroid(2)] => PlotOptions(N.GllimMergedCentroid2,  "blue", 0.5, :none, markershape=:circle, markersize=2, hide_std=true),
            preds[Schemes.IsCentroid(1)] => PlotOptions(N.IsCentroid1,  "orange", 0.5, :none, hide_std=true),
            preds[Schemes.IsCentroid(2)] => PlotOptions(N.IsCentroid2,  "orange", 0.5, :none, hide_std=true),
            preds[Schemes.ImisCentroid(1)] => PlotOptions(N.ImisCentroid1,  "brown", 0.5, :none, hide_std=true),
            preds[Schemes.ImisCentroid(2)] => PlotOptions(N.ImisCentroid2,  "brown", 0.5, :none, hide_std=true),
            Shared.MethodResult(Xobs) => PlotOptions(N.Xalt1, "green", 0.3, :solid, markersize=0, hide_std=true),
            Shared.MethodResult(Xobs_alt) => PlotOptions(N.Xalt2, "green", 0.3, :solid, markersize=0, hide_std=true),
        ]

        path = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label="double_solutions_is_K_$K")
        plot_components(plot_data;  savepath=path, varnames=Models.variables(context), xlabel=L"n", average_y_err=true)

        # Errors stats (table)
        measure = _measure_errs(context, Xobs, preds)
        push!(measures, measure)
        ct_is.label = "IS"
        ct_imis.label = "IMIS"
        push!(cts, ct_is, ct_imis)
    end

    table_meths = [
        # Schemes.IsMean,
        Schemes.GllimMergedCentroid(1), Schemes.GllimMergedCentroid(2),
        Schemes.IsCentroid(1), Schemes.IsCentroid(2),
        Schemes.ImisCentroid(1), Schemes.ImisCentroid(2),
    ]
    options = TableOptions(table_meths, true, true, true, true)
    latex_table = render_errors("double_solutions_sampling", measures, K_variants, options)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, params_is, params_imis; label="double_solutions_sampling") * ".tex"
    write(path, latex_table)
    @info "Latex exported in $path"

    return cts
end

function hapke_synthetique(st::Store.Storage, run::Int)
    context = HapkeNontronite()
    std_train = 0.0001

    imiss = [
        Is.ImisParams(200, 100, 10),
        Is.ImisParams(2500, 1000, 10),
        Is.ImisParams(2500, 1000, 20),
    ]
    
    K_variants = [40, 70, 100]
    obs_std_ratio  = 1000
    # to keep MCMC-HMC computation time reasonnable, we 
    # use rather small values
    Nobs = 100
    samples_MH = [10_000, 100_000]
    samples_HMC = [1_000, 10_000, 100_000]
    Xobs_math, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs_math, similar(Xobs_math))
    Yobs_std = Yobs ./ obs_std_ratio

    # we compare the 4 methods
    cts = []
    stats::Vector{MethodStats} = []
    measures::Vector{DoubleSolutionErrors} = []
    final_plot_data::Vector{Pair{Shared.MethodResult,PlotOptions}} = [] # for the article

    params_mcmc = [
        [Mcmc.ParamsMH(std_train;Niter=sample_mcmc, Nkeep=sample_mcmc / 2) for sample_mcmc in samples_MH]...
        [Mcmc.ParamsHMC(sample_mcmc, sample_mcmc / 2, std_train)  for sample_mcmc in samples_HMC]...
    ]
    for mcmc_param in params_mcmc
        ct_mcmc = Comptimes.Times(context, mcmc_param, Nobs)
        if run >= 3
            invs_mcmc, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_param)
            Store.save(st, [invs_mcmc, dura], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_param)
        end
        invs_mcmc, ct_mcmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_param)
        
        isMH = mcmc_param isa Mcmc.ParamsMH
        # stats table
        label = isMH ? N.McmcMH : N.McmcHMC
        push!(stats,  MethodStats(label * " - I : $(_fmt_int(mcmc_param.Niter))",    
            BasicError(invs_mcmc, context, Xobs_math), ct_mcmc.prediction), 
        )

        # computation times
        ct_mcmc.label = isMH ? "MH" : "HMC"
        push!(cts, ct_mcmc)

        # final plot
        if !isMH && mcmc_param.Niter == 10_000 
            push!(final_plot_data, invs_mcmc => PlotOptions(label, "teal", 0.5, :solid; markershape=:circle, hide_std=true))
        end
    end



    reg_meths = [ Schemes.GllimMergedCentroid(""),
    # Schemes.IsCentroid(""), 
    Schemes.ImisCentroid("")]
    for K in K_variants 
        for (j,imis) in enumerate(imiss)
            params_imis = ProcedureParams(std_train, K, imis)

            preds, _,  _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, _no_re_train(run), st)

            Pipeline.regularize_inversions!(preds, reg_meths)
            
            # final plot
            if Is.Ns(imis) == 22500 && K == 70 
                push!(final_plot_data,
                    preds[Schemes.Best] => PlotOptions(N.Best, "blue", 0.5, :dash, hide_std=true),
                    preds[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.5, :none, hide_std=true),
                    # preds[Schemes.IsMean] => PlotOptions(N.IsMean, "orange", 1, :none; markershape=:rect, markersize=0.5, hide_std=true),
                    preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "red", 0.6, :solid; markershape=:circle, hide_std=true), 
                    Shared.MethodResult(Xobs) => PlotOptions(N.Xobs, "green", 0.5, :solid, markersize=0, hide_std=true),
                )
            end

            # first err table
            if Is.Ns(imis) == 22500
                measure = _measure_errs(context, Xobs, preds)
                push!(measures, measure)
            end 

            # stats table: we only report the inversion time, not the training,
            # which is available in the global computation time table
            # to ease the lecture, we only keep one K variant
            if K == 70
                if j == 1 # include Gllim only once 
                push!(stats,  MethodStats(N.GllimMean * " - K : $K", BasicError(preds[Schemes.GllimMean], context, Xobs_math), ct_imis.gllim_only_prediction))
                end 
                push!(stats,    MethodStats(N.ImisMean * " - K : $K - $(N.format_imis(imis))", BasicError(preds[Schemes.ImisMean], context, Xobs_math), ct_imis.prediction))
            end

            ct_imis.label = "IMIS"
            push!(cts, ct_imis)
        end
    end

    # comparison with MCMC (with computing time)
    latex_table = compare_stats_table("hapke_synthetique_mcmc", stats)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, params_mcmc, imiss; label="hapke_synthetique_mcmc") * ".tex"
    write(path, latex_table)
    @info "Latex exported in $path"

    # comparison with centroids (no MCMC, no computing time)
    table_meths = [
        Schemes.GllimMean, Schemes.ImisMean,
        # Schemes.GllimMergedCentroid(1), Schemes.GllimMergedCentroid(2),
        # Schemes.IsCentroid(1), Schemes.IsCentroid(2),
        Schemes.ImisCentroid(1), Schemes.ImisCentroid(2),
        Schemes.Best,
    ]
    options = TableOptions(table_meths, false, false, false, false)
    latex_table = render_errors("hapke_synthetique", measures, K_variants, options)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, imiss; label="hapke_synthetique_centroids") * ".tex"
    write(path, latex_table)
    @info "Latex exported in $path"

    path = Store.get_path(st, Store.FO_GRAPHS, context, samples_HMC, samples_MH, imiss; label="hapke_synthetique")
    plot_components(final_plot_data;  savepath=path,xlabel=L"n", varnames=Models.variables(context))

    return cts
end

# floor = 0 to desactivate it
# add_prop_std is added in a second pass to the observations 
# which fail without it
function hapke_synthetique_high_dim(st::Store.Storage, run::Int, context::Models.Hapke,  floor::Number, add_prop_std::Number)
    train_std = 0.005
    K = 100
    train_params = Shared.TrainingParams(
        100_000,
        train_std,
        Gllim.Params(
            Gllim.FunctionnalInitParams(K, Models.closure(context)),
            Gllim.TrainParams{Float64,FullCov{Float64},IsoCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
    # params_is = Pipeline.ProcedureParams(train_params,
    #     Pipeline.InversionParams(
    #         train_std,Predictions.MergingOptions(2),
    #         Is.IsParams(50_000), false,false,
    #         Is.NonUniformityParams(1), add_prop_std
    #     )
    # )
    imis = Is.ImisParams(2500, 1000, 20)
    get_params_imis(prop_std) = Pipeline.ProcedureParams(train_params,
        Pipeline.InversionParams(
            train_std,Predictions.MergingOptions(2),
            imis, false,false,
            Is.NonUniformityParams(1), prop_std
        )
    )
    # we start without the std trick
    params_imis = get_params_imis(0.)

    obs_std_ratio = 50
    Nobs = 100

    Xobs_math, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs_math, similar(Xobs_math))
    Yobs_std = Yobs ./ obs_std_ratio

    if floor > 0
        # very low std may give GLLiM predictions outside of [0,1]
        # which in turn break samplings steps (IS and IMIS)
        # the parameter add_prop_std is also a way to prevent this effect
        Yobs_std[ Yobs_std .< floor ] .= floor
    end

    # we compute our own MCMC inversions
    mcmc_params_mh = Mcmc.ParamsMH(train_std;Niter=1_000_000, Nkeep=1_000_000 / 2)
    # HMC is really slow in high dim, and does not require many iterations
    mcmc_params_hmc = Mcmc.ParamsHMC(1_000, 1_000 / 2, train_std)
    ct_mcmc_mh = Comptimes.Times(context, mcmc_params_mh, Nobs)
    ct_mcmc_hmc = Comptimes.Times(context, mcmc_params_hmc, Nobs)
    if run >= 3
        invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
        Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
        Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
    end
    invs_mcmc_mh, ct_mcmc_mh.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
    invs_mcmc_hmc, ct_mcmc_hmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)

    # preds_is, _,  _, ct_is = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)
    preds_imis, _,  _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, run, st)

    # handle failures 
    mask_failure = Shared.failures(preds_imis[Schemes.ImisMean], context)
    dst_indexes = (1:Nobs)[mask_failure]
    @info "Fixing $(length(dst_indexes)) obs. with additional std $add_prop_std ..."
    params_imis_std = get_params_imis(add_prop_std)
    preds_imis_std, _,  _, _ = Pipeline.complete_inversion(context, Yobs[:, mask_failure], Yobs_std[:, mask_failure], params_imis_std, _no_re_train(run), st)
    for (i, j) in enumerate(dst_indexes)
        Shared.merge_result!(preds_imis, preds_imis_std, j, i)
    end

    preds = preds_imis  
    reg_meths = [ Schemes.GllimMergedCentroid(""),
    # Schemes.IsCentroid(""),
    Schemes.ImisCentroid("")]
    Pipeline.regularize_inversions!(preds, reg_meths)
    
    # Plotting 
    plot_data = [
        preds[Schemes.Best] => PlotOptions(N.Best, "blue", 0.5, :dash; markershape=:circle),
        # preds[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.5, :solid),
        # preds[Schemes.IsMean] => PlotOptions(N.IsMean, "orange", 0.5, :none; markershape=:circle),
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "purple", 0.8, :none; markershape=:star, markersize=10),
        invs_mcmc_mh => PlotOptions(N.McmcMH,  "brown", 0.3, :dash),
        invs_mcmc_hmc => PlotOptions(N.McmcHMC,  "teal", 0.3, :dash),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xobs, "green", 0.6, :solid; markersize=0, hide_std=true),
    ]

    path = Store.get_path(st, Store.FO_GRAPHS, context, params_imis, obs_std_ratio, floor; label="hs_high_dim_SNR_$(obs_std_ratio)_FLOOR_$(floor)_ADD_STD_$(add_prop_std)_K_$K")
    plot_components(plot_data; savepath=path,xlabel=L"n", varnames=Models.variables(context))

    # stat table 
    stats = [
        MethodStats(N.McmcMH * " - I : $(_fmt_int(mcmc_params_mh.Niter))", BasicError(invs_mcmc_mh, context, Xobs_math), ct_mcmc_mh.prediction), 
        MethodStats(N.McmcHMC * " - I : $(_fmt_int(mcmc_params_hmc.Niter))", BasicError(invs_mcmc_hmc, context, Xobs_math), ct_mcmc_hmc.prediction), 
        MethodStats(N.ImisMean * " - $(N.format_imis(imis))", BasicError(preds[Schemes.ImisMean], context, Xobs_math), ct_imis.prediction),
        MethodStats(N.Best * " (idem)", BasicError(preds[Schemes.Best], context, Xobs_math), ct_imis.prediction),
    ]
    latex_table = compare_stats_table("hapke_high_dim_mcmc", stats)
    path = Store.get_path(st, Store.FO_GRAPHS, context, Nobs, mcmc_params_mh,mcmc_params_hmc, imis; label="hapke_high_dim_mcmc") * ".tex"
    write(path, latex_table)
    @info "Latex exported in $path"

    ct_mcmc_mh.label = "MH"
    ct_mcmc_hmc.label = "HMC"
    # ct_is.label = "IS"
    ct_imis.label = "IMIS"
    return [ct_mcmc_mh, ct_mcmc_hmc, ct_imis]
end

function hapke_lab(st::Store.Storage, run::Int)
    context  = HapkeNontronite()
    # best K obtained from K selection
    # note that, out of simplicity, we train a new model with this K, while we could have 
    # saved and used the model learned in the K selection 
    K = 100 
    training_params =  Shared.TrainingParams(
        100_000, # equal max(Ks) * 1000
        0.001, # one of the noise studied in the selection of K (on cluster)
        KSel.KSelectionParams(K) # to be consistent, we use the same parameters than in the selection of K 
    )
    std_train = 0.001
   inv_params_is =  InversionParams(std_train, Is.IsParams(50_000))
   inv_params_imis =  InversionParams(std_train,  Is.ImisParams(2500, 1000, 20))
   samples_mcmc = 1_000_000 # we keep half of the samples
   obs_std_ratio = 20

    datas::Data2D  = load_observations(context)
    Yobs, wavelengths = datas.Yobs, datas.wavelengths
    _, Nobs = size(Yobs)

    Yobs_std =  Yobs ./ obs_std_ratio # first std
    # we follow the best practice in photometric measurement
    Yobs_std[ Yobs_std .< 0.01 ] .= 0.01 # floor at 0.01

    # we compute our own MCMC inversions
    mcmc_params_mh = Mcmc.ParamsMH(std_train;Niter=samples_mcmc, Nkeep=samples_mcmc / 2)
    mcmc_params_hmc = Mcmc.ParamsHMC(samples_mcmc, samples_mcmc / 2, std_train)
    ct_mcmc_mh = Comptimes.Times(context, mcmc_params_mh, Nobs)
    ct_mcmc_hmc = Comptimes.Times(context, mcmc_params_hmc, Nobs)
    if run >= 3
        invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
        invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
        Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
    end
    invs_mcmc_mh, ct_mcmc_mh.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
    invs_mcmc_hmc, ct_mcmc_hmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)

    # we then invert the model with Gllim + sampling 
    params_is = Pipeline.ProcedureParams(training_params, inv_params_is)
    params_imis = Pipeline.ProcedureParams(training_params, inv_params_imis)

    preds_is, _, _, ct_is = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)
    preds_imis, _, _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, _no_re_train(run), st)
    preds = merge(preds_is, preds_imis)

    # --- plot -----
    label = "hapke_lab_K$(K)_floor" 

    # 1 - X values
    plot_data = [
        preds[Schemes.Best] => PlotOptions(N.Best,  "blue", 1, :dash),
        # preds[Schemes.IsMean] => PlotOptions(N.IsMean,  "red", 1, :solid) ,
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean,  "red", 0.6, :solid),
        # invs_mcmc_mh => PlotOptions(N.McmcMH,  "green", 0.6, :none, markershape=:circle),
        invs_mcmc_hmc => PlotOptions(N.McmcHMC,  "teal", 0.6, :dash),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label=label)
    plot_components(plot_data;  savepath=path, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
    # 1 - bis - centroids 
    plot_data = [
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean,  "red", 0.6, :solid),
        preds[Schemes.ImisCentroid(1)] => PlotOptions(N.ImisCentroid1,  "blue", 0.6, :none,markershape=:star),
        preds[Schemes.ImisCentroid(2)] => PlotOptions(N.ImisCentroid2,  "blue", 0.6, :none,markershape=:star),
    ]
    path_c = Store.get_path(st, Store.FO_GRAPHS, context, params_is, params_imis; label=label*"_centroids")
    plot_components(plot_data;  savepath=path_c, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")


    # 2 - reconstructed observables 
    geom = Models.matrix(Models.geometries(context))
    series = [
        preds[Schemes.Best].Xmean, 
        # preds[Schemes.IsMean].Xmean,
        preds[Schemes.ImisMean].Xmean, 
        # invs_mcmc_mh.Xmean,
        invs_mcmc_hmc.Xmean
    ]
    labels = ["", ["\$F($(N.noMath(l)))\$" for l in [N.Best,
    # N.IsMean,
    N.ImisMean, 
    # N.McmcMH, 
    N.McmcHMC]]...]
    colors = ["black","blue",
    # "brown",
    "red",
    #  "green", 
    "teal"]

    # we compute the reconstruction
    Ys = [Models.F(context, Models.from_physical(context, X)) for X in series]
    Ys = [Yobs, Ys...]

    _, Nobs = size(Yobs)
    obs_indexes = [8, 44, 76] # expert value 
    subplots = []
    for (i, index) in enumerate(obs_indexes)
        wl = wavelengths[index] * 1000
        Yplot = hcat([Y[:,index] for Y in Ys]...)
        labels[1] = N.Yobs * L" - $\lambda = " * string(wl) *  L" nm$"
        titlefunc  = (inc,azi) -> "wavelength : $wl nm"
        p = plot_polar(Yplot, geom, labels, colors; title=titlefunc, hide_legend=true)
        push!(subplots, p)
    end 
    legend = legend_subplot()
    labels[1] = N.Yobs 
    plot!(legend, [1],  ones(1, length(Ys)), label=permutedims(labels), color=permutedims(colors))
    plot!(legend, [1], [1], label="incidence")
    push!(subplots, legend)

    obs_plot = plot(subplots..., layout=(2, 2), size=(700, 800), dpi=Config.DPI)
    obs_path = path * "observables.png"
    png(obs_plot, obs_path)
    @debug "Plotted in $obs_path"

    # ct_is.label = "IS"
    ct_imis.label = "IMIS"
    # ct_mcmc_mh.label = "MH"
    ct_mcmc_hmc.label = "HMC"
    cts = [
        # ct_is,
        ct_imis,
        # ct_mcmc_mh, 
        ct_mcmc_hmc]
    return cts
end

function _calibrate_hapke_massive(context::Models.Model, tp::Shared.TrainingParams, Nis::Int, run_gllim::Bool)

    datas::Data3D = load_observations(context, "S")

    # as a preliminary step, we calibrate imis params against a bound, with the help of reference
    # MCMC inversion
    mcmc_params_mh = Mcmc.ParamsHMC(50_000, 50_000/2, tp.train_std)
    Yobs, Yobs_std = datas.Yobs[:, 1, 1:5], datas.Yobs_std[:, 1, 1:5]
    invs_mcmc_mh, _ = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
    ref, _ = Models.from_physical(context, invs_mcmc_mh.Xmean, invs_mcmc_mh.Xcovs)
    imis_params = TuneImis.tune(st, context, Yobs, Yobs_std, ref, tp, Nis, run_gllim)
    @show imis_params
end


function hapke_massive(st::Store.Storage, run::Int)
    context = HapkeGlaceMars()

    train_std = 0.001
    tp = TrainingParams(train_std, 50)
    Nis = 20_000

    datas::Data3D = load_observations(context, "FRT144_S")
    # as a preliminary step, we calibrate imis params against a bound, with the help of reference MCMC inversion
    # _calibrate_hapke_massive(context, tp, Nis, run >= 2)
    imis_params = Is.ImisParams(1000, 200, 16)
    is_params = Is.IsParams(Nis)

    params = Pipeline.ProcedureParams(tp, InversionParams(train_std, imis_params))
    ct = inverse_export_cube(run, context, params, datas, st; label="glace_imis")
    ct.label = "IMIS"

    # IS inversion as comparaison ...
    params = Pipeline.ProcedureParams(tp, InversionParams(train_std, is_params))
    _ = inverse_export_cube(run, context, params, datas, st; label="glace_is")

    return [ct]
end

# -------------------- Noise estimation --------------------

# sigma is an std, not a covariance
function noise_synthetique(st::Store.Storage, run::Int)
    context = HapkeNontronite()
    train_params   =  TrainingParams(0.0001, 50)
    Nobs =    2000
    std_targets = [0.2, 0.03]
    std_init =  0.5
    max_iterations =   25
    D = Models.get_D(context)
    init = Noise.Bias(1., 0. * ones(D), Diagonal(std_init^2 * ones(D)))
    
    noise_params = Noise.EmIsGLLiMParams(
        targets=Noise.Targets(false, false, true), 
        init=init, max_iterations=max_iterations, Ns=20_000
    )
    params = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params)
    
    cts = []
    for std_target in std_targets
        synthetic = Noise.Bias(1., 0. * ones(D), Diagonal(std_target^2 * ones(D)))
        # we save the Yobs between runs
        if run >= 1
            Xobs, Yobs = Models.data_training(context, Nobs, std_target, synthetic.a, synthetic.mu)
            Store.save(st, [Xobs, Yobs], Store.FO_NOISE, context, std_target, synthetic)
        end
        Xobs, Yobs = Store.load(st, Store.FO_NOISE, context, std_target, synthetic)

        history, estimator, ct = Pipeline.complete_noise_estimation(context, Yobs, Xobs, params, run, st)

        path = Store.get_path(st, Store.FO_GRAPHS, context, synthetic, init, params; label="noise_synthetic")
        plot_noise_estimation(history, synthetic, estimator; savepath=path)

        # polar plot for the last iteration
        geom = Models.matrix(Models.geometries(context)) # D x 3
        stds = hcat(sqrt.(diag(synthetic.sigma)), sqrt.(diag(estimator.sigma)), sqrt.(diag(history[end][1].sigma)))
        plot_polar(stds, geom, [L"\sigma_D", L"\sigma^{est}_D", L"\sigma^{last}_D"], ["black", "red", "blue"]; savepath=path * "polar")

        ct.label = "Noise estimation on synthetic data - \$\\sigma = $(std_target)\$"
        push!(cts, ct)
    end
    return cts
end

function noise_reel(st::Store.Storage, run::Int)
    train_params = TrainingParams(0.001, 50)
    sigma_init =   1.
    max_iterations =  30
    contexts = [HapkeNontronite(), HapkeBasalt(), HapkeOlivine()]
    cts = []
    for context in contexts 
        D = Models.get_D(context)

        # we compare diagonal constraint with isotropic constraint
        init_diag = Noise.Bias(1., 0. * ones(D), Diagonal(sigma_init * ones(D)))
        init_iso = Noise.Bias(1., 0. * ones(D), UniformScaling(sigma_init))

        noise_params_diag = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_diag, max_iterations=max_iterations)
        noise_params_iso = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_iso, max_iterations=max_iterations)

        params_diag = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_diag)
        params_iso = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_iso)

        datas::Data2D  = load_observations(context)

        history_diag, _, ct_diag = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_diag, run, st)
        run_iso = _no_re_train(run) # avoid re-learning
        history_iso, _, ct_iso = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_iso, run_iso, st)

        # final estimation
        std_est_diag = sqrt.(diag(history_diag[end][1].sigma))
        std_est_iso = sqrt.(history_iso[end][1].sigma * ones(D)) # converted as vector

        # comparison with the obs mean noise 
        std_obs = Statistics.mean(datas.Yobs_std, dims=2)[:, 1]

        # polar plot for easy visualisation
        path = Store.get_path(st, Store.FO_GRAPHS, context, params_diag; label="noise_reel_$(Models.label(context))")
        
        stds = hcat(std_obs, std_est_diag, std_est_iso)
        labels = [notation_obs_std, notation_bias_std * " - Diagonal",  notation_bias_std * " - Isotropic"]
        colors = ["black", "red", "orange"]

        # we dont plot the diagonal constraint
        stds, labels, colors = stds[:, [1,3]], labels[[1,3]], colors[[1,3]]
        
        titlefunc = (inc, azi) -> Models.label(context)
        geom = Models.matrix(Models.geometries(context))
        plot_polar(stds, geom, labels, colors; savepath=path * "polar", title=titlefunc)
        # we only expose iso constraint 
        ct_iso.label = "Noise estimation on $(Models.label(context))"
        push!(cts, ct_iso)
    end
    return cts
end
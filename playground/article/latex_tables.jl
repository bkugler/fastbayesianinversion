using FastBayesianInversion.Gllim
using FastBayesianInversion.Models
using FastBayesianInversion.Shared
using FastBayesianInversion.Comptimes

using Dates 
using LaTeXStrings
using Formatting
using Printf

include("notations.jl")

const notation_obs_std = L"\mathbf{\sigma_D^{obs}}"
const notation_bias_std = L"\mathbf{\sigma_D^{last}}"

struct MesureParams 
    N_test::Int 
    std_test::Float64
    std_gen::Float64
end

""" Summary statistics for a serie of errors """
struct St 
    mean::Float64
    std::Float64
end

""" Group X prediction and Y reconstruction error """
struct BasicError
    X::St 
    Y::St 
end

"""
 ignore nan values
"""
_safe_err(V) = (v = V[isfinite.(V)]; St(Statistics.mean(v), Statistics.std(v)))
"""
 use infinite norm so error stays in [0,1]
"""
_err(pred, ref) = (err = maximum(abs.(pred .- ref); dims=1); _safe_err(err))


# Xobs is in math space, to have comparable result accross contexts
function BasicError(res::Shared.MethodResult, context::Models.Model, Xobs::Matrix)
    # x error
    Xpred = res.Xmean
    # we normalise to have comparable values (in [0,1])
    Xpred = Models.from_physical(context, Xpred)
    err_X = _err(Xpred, Xobs)

    # residual error
    err_Ypred = _safe_err(res.Yerr)

    BasicError(err_X, err_Ypred)
end

""" Store X errors for couple of double solutions.
Y error is already computed in "basic" errors.
"""
struct DoubleSolutionError 
    Pred1::St 
    Pred2::St
end

struct DoubleSolutionErrors 
    errors::Dict{String,BasicError}
    double::Union{DoubleSolutionError,Nothing}
    doubleIS::Union{DoubleSolutionError,Nothing}
    doubleIMIS::Union{DoubleSolutionError,Nothing}
end

function _fmt_time(time::Millisecond) 
    secs = Float32(time.value / 1000)
    mins = floor(secs / 60)
    hours = floor(secs / (60 * 60))
    if time < Second(1)
        return @sprintf("%.2f s", secs)
    elseif time < Minute(1)
        return @sprintf("%.1f s", secs)
    elseif time < Minute(10)
        return @sprintf("%d m %d s", mins, secs - mins * 60)
    elseif time < Hour(1)
        return @sprintf("%d m", mins)
    elseif time < Hour(10)
        return @sprintf("%d h %d m", hours, mins - hours * 60)
    else
        return @sprintf("%d h", hours)
    end
end

# ----------- Comparison between several methods (time and precision) -----------

struct MethodStats
    label::String # header
    err::BasicError
    comptime::Millisecond
end

""" returns a latex table as a string, referenced by the given label"""
function compare_stats_table(label::String, methods::Vector{MethodStats}; caption=nothing)
    fmt_float(x) = x == 0 ? "-" : _r4(x)

    cells = Matrix{String}(undef, length(methods), 3) # x_err, y_err, time
    for (i, m) in enumerate(methods)
        cells[i, 1] = "$(fmt_float(m.err.X.mean)) ($(fmt_float(m.err.X.std)))"
        cells[i, 2] = "$(fmt_float(m.err.Y.mean)) ($(fmt_float(m.err.Y.std)))"
        cells[i, 3] = "$(_fmt_time(m.comptime))"
    end
    
    _, best_x = findmin([m.err.X.mean == 0 ? Inf : m.err.X.mean for m in methods])
    _, best_y = findmin([m.err.Y.mean == 0 ? Inf : m.err.Y.mean for m in methods])
    _, best_ct = findmin([m.comptime for m in methods])
    apply_best!(i::Int, j::Int) = (cells[i,j] = "\\textbf{" * cells[i,j] * "}")
    apply_best!(best_x, 1)
    apply_best!(best_y, 2)
    apply_best!(best_ct, 3)
    
    def = "l || c | c | c "
    header = "Prediction scheme & \$\\bar{E}\$  & \$\\bar{R}\$ & Time  "
    row_list = [ "$(m.label) & " * join(cells[i, :], " & ") for (i, m) in enumerate(methods)]
    rows = join(row_list, "\\\\ \n")

    if caption === nothing 
        caption = """
        Performance comparison with MCMC approaches in terms of computational time, prediction and reconstruction error. Best values are in bold and standard deviations in parenthesis. \$I\$ denotes the number of MCMC iterations while \$I_0 + BJ\$ is the total number of simulations for IMIS.
        """
    end
    """
    \\begin{table}[h!]
    \\centering
    \\begin{tabular}{$def}
    $header \\\\
    \\hline
    $rows 
    \\end{tabular}
    \\caption{ \\label{tab:$label} $caption }
    \\end{table} 
    """
end



# --------------------------------- Latex tables ---------------------------------


_r(m) = round(m; digits=4)
_r4(m) = round(m; digits=4)

struct TableOptions 
    methods::Vector{String}
    show_double::Bool
    show_doubleIS::Bool
    show_doubleIMIS::Bool
    hide_X::Bool # methods only
end

struct Label 
    label::String
    kind::String # 'X' or 'Y'
end



""" aggregate the results to show """ 
function _select_errors(measures::Vector{DoubleSolutionErrors}, options::TableOptions)
    rows = []
    labels::Vector{Label} = [] # we be display in column 
    for meth in options.methods 
        notation = N.noMath(N.fromMethods[meth])
        labelY = Label("\t \$\\bar{R}($notation)\$", "Y")
        # columns
        rowY = [measure.errors[meth].Y  for measure in measures]
        push!(rows, rowY)
        push!(labels, labelY)
    end
    
    # show basic errors, grouped by X or Y
    if !options.hide_X 
        for meth in options.methods 
            notation = N.noMath(N.fromMethods[meth])
            labelX = Label("\t \$\\bar{E}($notation)\$", "X")
            # columns
            rowX = [measure.errors[meth].X  for measure in measures]
            push!(rows, rowX)
            push!(labels, labelX)
        end
    end
    
    # optionnal double solutions 
    if options.show_double
        labelX1 = Label("\t $(N.GllimMergedCentroid1)", "X")
        labelX2 = Label("\t $(N.GllimMergedCentroid2)", "X")
        # columns
        rowX1 = [measure.double.Pred1  for measure in measures]
        rowX2 = [measure.double.Pred2  for measure in measures]
        push!(rows, rowX1, rowX2)
        push!(labels, labelX1, labelX2)
    end
    
    # optionnal double solutions with IS
    if options.show_doubleIS
        labelX1 = Label("\t $(N.IsCentroid1)", "X")
        labelX2 = Label("\t $(N.IsCentroid2)", "X")
        # columns
        rowX1 = [measure.doubleIS.Pred1  for measure in measures]
        rowX2 = [measure.doubleIS.Pred2  for measure in measures]
        push!(rows, rowX1, rowX2)
        push!(labels, labelX1, labelX2)
    end
    
    # optionnal double solutions with IMIS
    if options.show_doubleIMIS
        labelX1 = Label("\t $(N.ImisCentroid1)", "X")
        labelX2 = Label("\t $(N.ImisCentroid2)", "X")
        # columns
        rowX1 = [measure.doubleIMIS.Pred1  for measure in measures]
        rowX2 = [measure.doubleIMIS.Pred2  for measure in measures]
        push!(rows, rowX1, rowX2)
        push!(labels, labelX1, labelX2)
    end
    
    mat = permutedims(hcat(rows...)) # display order
    @assert size(mat) == (length(labels), length(measures))
    return mat, labels 
end

""" returns an array of pairs of index (maybe -1) """ 
function _find_bests(mat::Matrix{St}, labels::Vector{Label})
    out::Vector{Pair{Int,Int}} = []

    function findMinByKind(col, kind::String)
        indexes = [ i for (i, label) in enumerate(labels) if label.kind ==  kind ] 
        errs = [ st.mean for (st, label) in zip(col, labels) if label.kind ==  kind ] 
        indexMin = - 1
        if length(errs) > 0 
            _, tmpI = findmin(errs) # local index
            indexMin = indexes[tmpI] # global index
        end
        return indexMin
    end
    
    for measure in eachcol(mat)
        # errors X 
        indexMinX = findMinByKind(measure, "X")
        # errors Y 
        indexMinY = findMinByKind(measure, "Y")
        push!(out, indexMinX => indexMinY)
    end
    return out
end

function _b(val, isBold::Bool)
    if isBold
        return "\\textbf{$val}"
    end
    return val
end

""" For each param (column), show the errors (row) """
function render_errors(label::String, measures::Vector{DoubleSolutionErrors}, params::Vector{Int}, options::TableOptions)
    def = " r | " * join(("c" for _ in params), "|")
    header = "Prediction schemes & " * join(("K = $K" for K in params), " & ")

    mat, labels = _select_errors(measures, options) # pre-process
    bests = _find_bests(mat, labels)
    I, _ = size(mat)
    rows = []
    for i in 1:I
        row = labels[i].label
        # columns
        for (j, best) in enumerate(bests)
            isBest = best.first == i || best.second == i
            st = mat[i, j]
            row *= "& $(_b(_r(st.mean), isBest)) ($(_r(st.std)))"
        end
        push!(rows, row)
    end
    
    rows = join(rows, "\\\\ \n")
    """
    \\begin{table}[h!]
    \\centering
    \\begin{tabular}{$def}
    $header \\\\
    \\hline
    $(rows) 
    \\end{tabular}
    \\caption{ \\label{tab:$label} Importance sampling for centroids. Average reconstruction (first XXX lines) and prediction (last XXX lines) errors, for XXX prediction schemes, 1000 tests, 3 GLLiM settings. Standard deviations are in parenthesis and best averages are in bold. }
    \\end{table}
    """
end



# noise comparison 

 _label_geometrie(g::Vector) = "$(Int(g[1])),$(Int(g[2])),$(Int(g[3]))"

_tabular_def(geometries::Matrix) = " l | " * join(("c" for _ in geometries[1,:]), "|")
_tabular_header(geometries::Matrix) = " & " * join((_label_geometrie(geometries[:, d]) for d in 1:size(geometries)[2]), " & ")

function render_noise(context::Models.Model, Ystd_reff::Vector, Ystd_est::Vector, geometries::Matrix) 
    def = _tabular_def(geometries)
    header = _tabular_header(geometries)
    row_ref = "\t $(notation_obs_std) & " * join(("$(_r4(s))" for s in Ystd_reff), " & ")
    row_est = "\t $(notation_bias_std) & " * join(("$(_r4(s))" for s in Ystd_est), " & ")
    rows = join([row_ref, row_est], "\\\\ \n")

    """
    \\begin{table}[h!]
    \\centering
    \\caption{ \\label{} $(Models.label(context)) - Biais estimation}
        \\begin{tabular}{$def}
            $header \\\\
            \\hline
            $rows 
        \\end{tabular}
    \\end{table} 
    """
end

function _fmt_int(N::Int)
    power = log10(N)
        if N > 100 && floor(power) == power 
        return "\$10^{$(Int(power))}\$"
    end
    if N > 1000 && N % (10^floor(power)) == 0 
        a = Int(N ÷ 10^floor(power))
        return "\$ $a \\, 10^{$(Int(floor(power)))}\$"
    end

    if N <= 10 
        return "-" # non significant 
    end 
    # latex thin space
    return replace(Formatting.format(N, commas=true), "," => "\\,")
end

    function genere_time(c::Comptimes.Times)
    fmt_learning = _fmt_time(c.learning)
    fmt_prediction = _fmt_time(c.prediction)
    fmt_gllim_prediction = _fmt_time(c.gllim_only_prediction)
    fmt_prediction_relative = " - "  * _fmt_time(c.prediction ÷ c.Nobs)
    if c.kind == Comptimes.Noise 
        fmt_prediction_relative = ""
        fmt_gllim_prediction = "-"
        end
    if c.kind == Comptimes.MCMC 
        fmt_learning = "-"
        fmt_gllim_prediction = "-"
    end

    fmt_Nobs = _fmt_int(c.Nobs) 
    fmt_Nis = _fmt_int(c.Nis) 
    fmt_Ntrain = _fmt_int(c.Ntrain) 
    fmt_K = _fmt_int(c.K) 
    
    return """
    {\\footnotesize $(c.label)} & $(c.L) & $(c.D) &  $fmt_K & $fmt_Ntrain & $fmt_Nis & $fmt_Nobs & $fmt_learning & $fmt_gllim_prediction & $fmt_prediction $fmt_prediction_relative 
    """
end
function generate_times(cts::Vector{Comptimes.Times}, out::String)
    cts = [ copy(x) for x in cts] # deep copy
    def = " b{3.5cm} || c | c | c | c | c | c || r | r | r"
    header = L"Experiment & $L$ & $D$ &  $K$ & $N$ &  $I \, ^* $ & $N_{obs}$ & Learn. & " * N.GllimMean * " & Pred. - 1 obs. "

    # post-processing : short repeated exp + exp numbers 
    last_index = ""
    for ct in cts
        if ct.kind == Comptimes.Noise
            continue    
        end
        if ct.exp_index == last_index # repeated exp, dont increment
            label = "\\qquad " * ct.label
        else 
            label = "$(ct.exp_index) : $(ct.label)"
        end
        last_index = ct.exp_index # save initial label
    ct.label = label # update label in place
    end
    # variant for noise
    last_label = ""
    for ct in cts
        if ct.kind !== Comptimes.Noise
            continue    
        end
        if ct.label == last_label # repeated exp
            label = "-"
        else 
            label = ct.label
        end
        last_label = ct.label # save initial label
    ct.label = label # update label in place
    end
    rows1 = join([genere_time(ct) for ct in cts if ct.kind != Comptimes.Noise ], "\\\\ \n")
    rows2 = join([genere_time(ct) for ct in cts if ct.kind == Comptimes.Noise ], "\\\\ \n")

    latex = """
    \\begin{table}[h!]
    \\centering
    \\begin{tabular}{$def}
    $header \\\\
    \\hline
    $rows1 \\\\
    \\hline 
    $rows2
    \\end{tabular}
    \\caption{ \\label{tab:comp_times} Settings and computation times for synthetic and real data experiments.  The column \$\\bar{\\mathbf{x}}_{G}\$ displays the time spend only for the computation of \$\\bar{\\mathbf{x}}_{G}\$ for all the observations, while the last column takes into account all the prediction schemes. \$^*\$ For MCMC algorithms, the number of simulations done for each observation is reported under \$I\$ while for IMIS \$I\$ corresponds to \$I_0 + BJ\$.}}
    \\end{table} 
    """
    
    write(out, latex)
end



using FastBayesianInversion.Models
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Log 

using Colors
using Plots

include("experiences.jl")

st = Store.Storage(ROOT_PATH_STORE)

function run_simulations(runs::Vector{Int})
    cts::Vector{Comptimes.Times} = []
    experiences = [
        double_solutions_simple,
        double_solutions_complete,
        hapke_synthetique,
        (st, run) -> hapke_synthetique_high_dim(st, run, HapkeHighDim2(), 0.05, 5),
        # (st, run) -> hapke_synthetique_high_dim(st, run, HapkeHighDim2(), 0.05, 0.2),
        hapke_lab,
        hapke_massive,
        noise_synthetique,
        noise_reel,
        triple_solutions_simple, # only in the response to reviewer 
    ]
    labels = [
        "Simple double solutions problem",
        "Centroids for double solutions",
        "Hapke - Synthetic data",
        "Hapke - High dimensional synthetic data",
        "Hapke - Laboratory observations",
        "Hapke - Glace observations",
        "",
        "",
        "",
    ]
    for (i, (run, fn)) in enumerate(zip(runs, experiences))
        if run <= -1
            @info "Skipping exp. $i "
            continue
        end
        @info "Starting exp. $i"
        Log.indent()
        current_cts = fn(st, run)
        Log.deindent()
        for c in current_cts
            c.exp_index = "Ex. $i : " * labels[i]
        end
        push!(cts, current_cts...)
    end
    cts
end

Log.init()
Plots.pyplot()
Config.LEGEND_BACKGROUND = Colors.RGBA(0.9, 0.9, 0.9, 0.7)
Config.DPI = 200

# Uncomment to launch the simulations :
# 2  : full simulation
# 1  : re-use existing trained model
# 0  : re-use trained model and prediction (only plot)
# -1 : skip the experience

cts = run_simulations([0,0,0,0,0,0,0,0,0,0,0,0])


# Uncomment to write meta-data

generate_times(cts, ARTICLE_EXPORT_DIR * "simulations/times.tex")
# N.render_def_notations(ARTICLE_EXPORT_DIR * "preamble_notations.tex")
# N.render_table_notations(ARTICLE_EXPORT_DIR * "notations.tex")



## Simulations for the article *FAST BAYESIAN INVERSION FOR HIGH DIMENSIONAL INVERSE PROBLEMS*

The simulations are defined in [experiences.jl](experiences.jl), with their hyper-parameters, and called in  [main.jl](main.jl)

[notations.jl](notations.jl) and [latex_tables.jl](latex_tables.jl) defines helpers functions used to automatically generate LaTeX tables of notations and computation times.
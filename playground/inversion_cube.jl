using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Models
using FastBayesianInversion.Predictions
using FastBayesianInversion.Is
using FastBayesianInversion.Store
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Shared
using FastBayesianInversion.Schemes

using MAT 

include("hapke_models/models.jl")


# entry size D x W x N -> out size D x WN
function _cube_to_mat(data::CubeOrSub)
    _, _, N = size(data)
    data = hcat([data[:,:, n] for n in 1:N]...)
    return data, N
end
# entry size : L x NW -> out size N x L x W
function _mat_to_cube(data::Matrix, N::Int)
    L, NW = size(data)
    W = Int(NW / N)
    out = zeros(N, L, W)
    for n in 1:N
        start = 1 + (n - 1) * W
        stop = start +  W - 1 # = n x W
        out[n,:, :] = data[:, start:stop]
    end
    out
end

# failed values are replaced by Nan
function _package_res(series::Dict{String,Shared.MethodResult}, W::Int, mask::Vector, geometries::Models.HapkeGeometries, variabilities::Vector{Pipeline.Variability})
    out = Dict("geometries" => geometries,  "mask" => mask)
    # restrict the export to Is, Imis and Best 
    for scheme in [Schemes.IsMean, Schemes.ImisMean, Schemes.Best]
        if haskey(series, scheme)
            pred::Shared.MethodResult = series[scheme]
            # pred has size D x NW, we split according to wavelengths

            mean, std = _mat_to_cube(pred.Xmean, W), _mat_to_cube(sqrt.(pred.Xcovs), W)
            mean[ mean .== 0] .= NaN
            std[ std .== 0] .= NaN
            out["x_" * scheme], out["std_" * scheme] = mean, std
        end
    end

    non_uniformity = hcat([v.non_uniformity for v in variabilities]...) # size L x NW
    dispersion = hcat([ [v.dispersion_cmp..., v.dispersion] for v in variabilities]...) # size (L+1) x NW
    out["non_uniformity"] = _mat_to_cube(non_uniformity, W)
    out["dispersion"] = _mat_to_cube(dispersion, W)

    out
end

function inverse_export_cube(run, context, params::Pipeline.ProcedureParams, datas::Data3D, st::Store.Storage; label="")
    @info "Observations $label loaded. Starting invertion procedure..."
    geometries = Models.geometries(context)
    _, Wtot, _ = size(datas.Yobs)

    # to invert all observations at once, we group them
    Yobs::Matrix{Float64}, W = _cube_to_mat(datas.Yobs)
    Ystd::Matrix{Float64}, _ = _cube_to_mat(datas.Yobs_std)

    preds, variabilities, _,  ct = Pipeline.complete_inversion(context, Yobs, Ystd, params, run, st)

    path = Store.get_path(st, Store.FO_PREDICTIONS, context, params, datas.Yobs; label=label)
    # we export raw preds for external plots
    d = _package_res(preds, W, datas.mask, geometries, variabilities)
    matwrite(path * ".mat", d) # compress doesn't do a lot here
    @debug "Exported in $path"
    return ct
end


const _gllim_params = Gllim.Params(
    Gllim.MultiInitParams(10, Gmms.EmParams(50, 30, 20, 1e-12)),
    Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
        Gllim.DefaultStoppingParams(300, 1e-6), 1e-12)
)
const _procedure_params = Pipeline.ProcedureParams(
    Shared.TrainingParams(50_000, 0.001, _gllim_params),
    Pipeline.InversionParams(0.001,
        Predictions.MergingOptions(2),
        Is.ImisParams(1000, 200, 16),
        false, false,
        Is.NonUniformityParams(10), 0
        )
)

function hapke_massive_B385(st::Store.Storage)
    context = HapkeB385()
    datas::Data3D = load_observations(context)

    ct = inverse_export_cube(2, context, _procedure_params, datas, st)
    Store.save(st, ct, Store.FO_MISC, context, _procedure_params, :time)
end

function hapke_massive_Mars2020(st::Store.Storage)
    context = HapkeMars2020_140520()

    exp = "FRT"
    datas = load_observations(context, exp)
    ct1 = inverse_export_cube(2, context,  _procedure_params, datas, st; label="mars2020_$(exp)")

    exp = "HRL"
    datas = load_observations(context, exp)
    ct2 = inverse_export_cube(1, context,  _procedure_params, datas, st; label="mars2020_$(exp)")

    Store.save(st, [ct1, ct2], Store.FO_MISC, context, _procedure_params, :time)
end

function hapke_massive_glace(st::Store.Storage, exp::String)
    context = HapkeGlaceMars()

    datas = load_observations(context, exp)
    ct = inverse_export_cube(2, context,  _procedure_params, datas, st; label="glace_$(exp)")

    Store.save(st, ct, Store.FO_MISC, context, _procedure_params, :time)
end

function hapke_massive_glace2(st::Store.Storage, exp::String)
    params = Pipeline.ProcedureParams(
    Shared.TrainingParams(50_000, 0.001, _gllim_params),
    Pipeline.InversionParams(0.001,
        Predictions.MergingOptions(2),
        Is.ImisParams(1000, 200, 16),
        false, false,
        Is.NonUniformityParams(10_000), 0
        )
    )

    context = HapkeGlaceMars()
    # for (i, exp) in enumerate([
    #     "FRT144_L",
    #     "FRT144_S",
    #     "FRT11D5E_S",
    #     "FRT11D08_S",
    #     "FRT11FE1_S",
    #     "FRT13D75_S",
    #     "FRT116E3_S",
    # ])
    datas = load_observations(context, exp)
    _ = inverse_export_cube(1, context,  params, datas, st; label="glace_$(exp)")
end
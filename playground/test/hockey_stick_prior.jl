using FastBayesianInversion.Store
using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Perf
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Models.HapkeFunc

using Statistics
using Plots 

Log.init()
Plots.pyplot()

include("../hapke_models/hockey_stick.jl")
include("../hapke_models/models.jl")

st = Store.Storage(ROOT_PATH_STORE)

# for hockeystick, we need to use a training data with hockey stick prior
function hockeystick_direct_error(c::Models.Model, gllim::Gllim.GLLiM{T}) where T
    Xobs, Yobs = hockeystick_data_training(c, Perf.Config.Nsample, 0.)
    Ypred, _ = Gllim.predict(gllim, Xobs; with_cov=false)
    errors = sum((Yobs .- Ypred).^2; dims=1)
    Statistics.mean(errors)
end

function compare_training(c::Models.Hapke, run::Bool)
    K, Ntrain = 50, 50_000
    T = Float64
    params = Gllim.Params(
        Gllim.MultiInitParams(
            10, 
            Gmms.EmParams(K, 30, 10, 1e-12)
        ),
        Gllim.TrainParams{T,FullCov{T},DiagCov{T}}(
            Gllim.DefaultStoppingParams(200, 1e-6),
            1e-12
        )
    )

    if run
        X_base, Y_base = Models.data_training(c, Ntrain, 0.001)
        X_hockeystick, Y_hockeystick = hockeystick_data_training(c, Ntrain, 0.001)

        out_base = Gllim.train(params, X_base, Y_base)
        out_hockeystick = Gllim.train(params, X_hockeystick, Y_hockeystick)

        Store.save(st, (out_base, out_hockeystick), Store.FO_GLLIM, c, params, Ntrain, :compare)
    end
    (gllim_base, lls_base), (gllim_hockeystick, lls_hockeystick) = Store.load(st, Store.FO_GLLIM, c, params, Ntrain, :compare)  

    # scores 
    di_base = Perf.direct_error(c, gllim_base)
    di_hockeystick = hockeystick_direct_error(c, gllim_hockeystick)

    sig_base = Perf.sigma_error(gllim_base)
    sig_hockeystick = Perf.sigma_error(gllim_hockeystick)

    @show di_base di_hockeystick sig_base sig_hockeystick

    datas = load_observations(c)
    Yobs, Yobs_std, wl = datas.Yobs, datas.Yobs_std, datas.wavelengths

    means_base, covs_base, d1 = Pipeline.gllim_prediction(Yobs, Yobs_std, gllim_base)
    means_hockeystick, covs_hockeystick, d2 = Pipeline.gllim_prediction(Yobs, Yobs_std, gllim_hockeystick)
    @debug "Prediction done in $d1 + $d2"

    _, N = size(Yobs)
    varnames = Models.variables(c)
    subplots = []
    for (i, var) in enumerate(varnames)
        p = plot(wl, means_base[i, :],  label="uniform", ylabel=var)
        plot!(p, wl, means_hockeystick[i, :], label="hockey stick")
        push!(subplots, p)
    end

    bref = collect(range(0, 1; length=100))
    cref = HapkeFunc.Crelation(bref)
    p_hockeytick = plot(bref, cref, label="hockeystick")
    scatter!(p_hockeytick, means_base[3, :], means_base[4, :], label="uniform")
    scatter!(p_hockeytick, means_hockeystick[3, :], means_hockeystick[4, :], label="hockey stick")
    push!(subplots, p_hockeytick)
    
    p_stats = plot(zeros(1, 4), labels=permutedims(hcat([
        "$(round(di_base, digits=6)) direct error - uniform",
        "$(round(di_hockeystick, digits=6)) direct error - hockey stick",
        "$(round(sig_base, digits=6)) average trace Sigma - uniform",
        "$(round(sig_hockeystick, digits=6)) average trace Sigma - hockey stick",
        ])), framestyle=:none)
    push!(subplots, p_stats)

    p = plot(subplots..., size=(1000, 500))

    path = Store.get_path(st, Store.FO_GRAPHS, c, params; label="training_with_prior_$(Models.label(c))")
    png(p, path)
    @debug "Saved in $path"
    # lls_base, lls_hockeystick
end


# b, c = hockey_stick_dataset(1000)
# scatter(b,c)
# c = HapkeNontronite()
c = HapkeCeres()
# lls1, lls2 = compare_training(c, false) 
# p = plot(1:length(lls1), lls1, label="uniform")
# plot!(p, 1:length(lls2), lls2, label="hockey stick")
compare_training(c, false) 

using FastBayesianInversion.Gllim
using FastBayesianInversion.Probas
using FastBayesianInversion.Gmms
using FastBayesianInversion.Models
using FastBayesianInversion.Is
using FastBayesianInversion.Shared
using FastBayesianInversion.Store
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Predictions
using FastBayesianInversion.Schemes
using FastBayesianInversion.Log

using Plots 
using LaTeXStrings 

include("../hapke_models/models.jl")
include("../plotting/base_plots.jl")

Log.init()
Plots.pyplot()

st = Store.Storage(ROOT_PATH_STORE)

function grow_geoms(run::Int)
    ct = HapkeHighDim1()

    # we do one training and then use a subset of the learned GLLiM
    params = Shared.TrainingParams(
        50_000,
        0.005,
        Gllim.Params(
            # Gllim.MultiInitParams(
            #     10,
            #     Gmms.EmParams(70, 30, 15, 1e-12)
            # ),
            Gllim.FunctionnalInitParams(50, Models.closure(ct)),
            Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
    trained_gllim, _, _ = Shared.generate_train_gllim(ct, params, st, run >= 2)

    SNRs = [1, 5, 10, 50]
    Nobs = 1000
    L = Models.get_L(ct)
    Xobs, Yobs = Models.data_observations(ct, Nobs)

    D_total = Models.get_D(ct)
    Ds = collect(30:5:D_total)
    
    if run >= 1
        rates = zeros(length(Ds), length(SNRs))
        for (i, D) in enumerate(Ds)
            @debug "D = $D"
            for (j, SNR) in enumerate(SNRs)
                @debug "SNR = $SNR"
                Yobs_std = Yobs / SNR 
                sub_g = Gllim.sub_gllim(trained_gllim, collect(1:D))
                Y, Ystd  = Yobs[1:D, :], Yobs_std[1:D, :]
                Xmean, _, _ = Pipeline.gllim_prediction(Y, Ystd, sub_g)
            # count the number of failures
                rate = sum([Models.is_valid(Xmean[:, i], L) for i in 1:Nobs]) / Nobs
                rates[i,j] = rate
            end
        end
        Store.save(st, rates, Store.FO_BENCHMARK, ct, params, SNRs, Ds)
    end
    rates = Store.load(st, Store.FO_BENCHMARK, ct, params, SNRs, Ds)

    p = plot(Ds, rates, labels=permutedims(hcat(["SNR = $SNR" for SNR in SNRs])), ylabel="Rate of valid values", xlabel="D")
    savepath = Store.get_path(st, Store.FO_GRAPHS, ct, params, SNRs, Ds, label="valid_preds")
    png(p, savepath)
    @debug "Plotted in $savepath"
    p
end

function _simple_high_dim_hapke(run::Number, std_train::Number, ct::Models.Model)
    # we use a constant std in the training set 
    # and no aditional obs std 

    params = Shared.TrainingParams(
        100_000,
        std_train,
        Gllim.Params(
            # Gllim.MultiInitParams(
            #     10,
            #     Gmms.EmParams(70, 30, 15, 1e-12)
            # ),
            Gllim.FunctionnalInitParams(50, Models.closure(ct)),
            Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
    trained_gllim, _, _ = Shared.generate_train_gllim(ct, params, st, run >= 2)

    L, D = Models.get_L(ct), Models.get_D(ct)
    Nobs = 1000
    Xobs, Yobs = Models.data_observations(ct, Nobs)
    Yobs = Yobs .+ randn(size(Yobs)) .* std_train
        
    if run >= 1 
        Xmean, _, _ = Pipeline.gllim_prediction(Yobs, zeros(size(Yobs)), trained_gllim)
        Store.save(st, Xmean, Store.FO_PREDICTIONS, ct, params, Nobs)
    end
    Xmean = Store.load(st, Store.FO_PREDICTIONS, ct, params, Nobs)

    rate = sum([Models.is_valid(Xmean[:, i], L) for i in 1:Nobs]) / Nobs
    rmse = sum(norm(Xmean[:,i] .- Xobs[:,i]) for i in 1:Nobs) / Nobs

    varnames = Models.variables(ct)
    subplots = []
    for l in 1:L 
        p = plot(1:Nobs, Xmean[l, :], label="GLLiM", ylabel="$(varnames[l])")
        plot!(p, 1:Nobs, Xobs[l, :], label="observation")
        push!(subplots, p)
    end

    p = plot(subplots..., dpi=300)
    savepath = Store.get_path(st, Store.FO_GRAPHS, ct, params; label="simple noise D=$(D) STD=$(std_train)")
    png(p, savepath)
    @debug "Plotted in $savepath"

    rate, rmse
end

function compare_simple_noise(run::Number)
    simple_geoms = _generate_geoms_0()
    full_geoms = _generate_geoms_1()
    stds = [0.01, 0.02, 0.1]
    cts = [Models.Hapke("small D", simple_geoms),Models.Hapke("high D", full_geoms)]
    rates = zeros(length(cts), length(stds))
    rmses = zeros(length(cts), length(stds))
    for (i, ct) in enumerate(cts) 
        for (j, std_train) in enumerate(stds)
            rates[i,j], rmses[i,j] = _simple_high_dim_hapke(run, std_train, ct)
        end
    end
    rates, rmses
end

# TODO: fix the api
function hapke_full(run, SNR::Number)
    ct = HapkeFull()
    Nobs = 1000 

    Xobs, Yobs = Models.data_observations(ct, Nobs)
    Yobs_std = Yobs ./ SNR
    Yobs_std[ Yobs_std .< 0.01 ] .= 0.01 # floor at 0.01

    # we compute our own MCMC inversions
    # mcmc_params = Mcmc.ParamsMH(training_params.train_std)
    # ct_mcmc = Comptimes.Times(context, mcmc_params, Nobs)
    # if run >= 3
    #     invs_mcmc, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params)
    #     Store.save(st, [invs_mcmc, dura], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
    # end
    # invs_mcmc, ct_mcmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
    # Xmcmc, Covsmcmc = Pipeline.aggregate(invs_mcmc), Pipeline.aggregate_std(invs_mcmc).^2

    training_params = Shared.TrainingParams(
        100_000,
        0.001,
        Gllim.Params(
            # Gllim.MultiInitParams(
            #     10,
            #     Gmms.EmParams(70, 30, 15, 1e-12)
            # ),
            Gllim.FunctionnalInitParams(50, Models.closure(ct)),
            Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
    inversions_params = Pipeline.InversionParams(
        0.001,
        Predictions.MergingOptions(2),
        Is.ImisParams(2000, 800, 20), 
        false,
        false,
        Is.NonUniformityParams(0),
        0
    )
    params = Pipeline.ProcedureParams(training_params, inversions_params)

    preds_vec, _, _, _ = Pipeline.complete_inversion(ct, Yobs, Yobs_std, params, run, st)

    # plotting 
    # mcmcYerr = Pipeline.Y_relative_err(context, Yobs, Models.from_physical(context, Xmcmc), 1., zeros(Models.get_D(context)))
    # refPred = Pipeline.Prediction(Xmcmc, Covsmcmc;
    #     config=Pipeline.SerieConfig("MCMC", :basic, "green"; hide_std=false, hideX_err=true, alpha=0.5), Yerr=mcmcYerr)
    Xref, _ = Models.to_physical(ct, Xobs, similar(Xobs))
    refPred = Pipeline.Prediction(Xref;config=Pipeline.SerieConfig("obs", :basic, "green"; hide_std=true, hideX_err=true, alpha=0.5))

    ip = params.inv_params.imis_params
    ips = " (N_0 = $(ip.N0), B = $(ip.B), J = $(ip.J))"
    configs_sampling = [
        # Schemes.IsMean => Pipeline.SerieConfig(L"x_{is}", :basic, "red"; hide_std=false, alpha=0.7),
        Schemes.ImisMean => Pipeline.SerieConfig(L"x_{imis}" * ips, :basic, "red"; hide_std=false, alpha=0.7),
        # Schemes.Best => Pipeline.SerieConfig(L"x_{best}", :dashed, "blue"; hide_std=false),
   ]
    configs_best = [
        Schemes.BestWithIs => Pipeline.SerieConfig(L"x_{best, is}", :dashed, "green"; hide_std=true),
        Schemes.BestWithImis => Pipeline.SerieConfig(L"x_{best, imis}", :dashed, "orange"; hide_std=true),
    ]
    configs_gllim = [
        Schemes.GllimMean => Pipeline.SerieConfig(L"x_{GLLiM}", :basic, "red"; hide_std=false)
    ]
    configs_centroids = [
        Schemes.ImisCentroid("1") => Pipeline.SerieConfig(L"x_{imis,c1}", :circle, "red"; hide_std=true),
        Schemes.ImisCentroid("2") => Pipeline.SerieConfig(L"x_{imis,c2}", :circle, "orange"; hide_std=true),
    ]
    preds_sampling = Pipeline.format_inversions(preds_vec, configs_sampling)
    preds_sampling = [preds_sampling..., refPred ]
    preds_best = Pipeline.format_inversions(preds_vec, configs_best)
    preds_best = [preds_best..., refPred ]
    preds_gllim = Pipeline.format_inversions(preds_vec, configs_gllim)
    preds_gllim = [preds_gllim..., refPred ]
    preds_centroids = Pipeline.format_inversions(preds_vec, configs_centroids)
    preds_centroids = [preds_centroids..., refPred ]

    path = Store.get_path(st, Store.FO_GRAPHS, ct, params; label=Models.label(ct) * "SNR=$SNR")
    varnames = Models.variables(ct)
    plot_components(preds_sampling;  savepath=path * "_sampling", varnames=varnames)
    plot_components(preds_best;  savepath=path * "_best", varnames=varnames)
    plot_components(preds_gllim;  savepath=path * "_gllim", varnames=varnames)
    plot_components(preds_centroids;  savepath=path * "_centroids", varnames=varnames)
end

# grow_geoms(1)
# rates, rmses = compare_simple_noise(0)
hapke_full(1, 100)

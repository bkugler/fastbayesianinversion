using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Predictions
using FastBayesianInversion.Models
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Schemes
using FastBayesianInversion.Shared
using FastBayesianInversion.Pipeline.TuneImis

using LaTeXStrings
using Plots 
using JSON 

include("../plotting/plots.jl")
include("../hapke_models/models.jl")
include("../env.jl")

Plots.pyplot()
Log.init()
st = Store.Storage(ROOT_PATH_STORE)

function hapke_lab(run, context::Models.Hapke, training_params::Shared.TrainingParams,
    inv_params::Pipeline.InversionParams)
    datas::Data2D  = load_observations(context)
    Yobs, wavelengths = datas.Yobs, datas.wavelengths
    _, Nobs = size(Yobs)

    Yobs_std =  datas.Yobs_std
    Yobs_std[ Yobs_std .< 0.01 ] .= 0.01 # floor at 0.01

    
    # we compute our own MCMC inversions
    mcmc_params = Mcmc.ParamsMH(training_params.train_std)

    ct_mcmc = Comptimes.Times(context, mcmc_params, Nobs)
    if run >= 3
        invs_mcmc, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params)
        Store.save(st, [invs_mcmc, dura], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
    end
    invs_mcmc, ct_mcmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)

    Xmcmc, Covsmcmc = Pipeline.aggregate(invs_mcmc), Pipeline.aggregate_std(invs_mcmc).^2

    # we calibrate the imis params
    if run >= 1
        _, N = size(Yobs)
        indexes = N - 10:N
        # Xmcmc is in physical space 
        refs = Models.from_physical(context, Xmcmc[:, indexes])
        imis_params = TuneImis.tune(st, context, Yobs[:, indexes], Yobs_std[:, indexes], refs, training_params, inv_params.N_is, run >= 2)
        Store.save(st, imis_params, Store.FO_BENCHMARK, context, Yobs, Yobs_std, mcmc_params, training_params, inv_params.N_is, :tune_imis)
    end
    inv_params.imis_params = Store.load(st, Store.FO_BENCHMARK, context, Yobs, Yobs_std, mcmc_params, training_params, inv_params.N_is, :tune_imis)

    
    procedure_params = Pipeline.ProcedureParams(training_params, inv_params)
    run = run >= 1 ? 1 : 0 # gllim learning is already done
    preds_vec, variabilities, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, procedure_params, run, st)

    _plot_hapke_lab(st, context, Yobs, wavelengths, procedure_params, Xmcmc, Covsmcmc, preds_vec, variabilities)
    _package_res_imis(context, preds_vec, Models.geometries(context))

    [ct_mcmc, ct]
end

function _package_res_imis(context::Models.Model, preds::Vector{Shared.OneInversion}, geometries::Models.HapkeGeometries)
    series = Pipeline.extract_series(preds, [Schemes.Best, Schemes.ImisMean])
   
    pred_imis_mean = Pipeline.aggregate(series[Schemes.ImisMean]) # size D x N
    std_imis_mean = Pipeline.aggregate_std(series[Schemes.ImisMean]) # size D x N
    pred_best = Pipeline.aggregate(series[Schemes.Best]) # size D x N
    std_best = Pipeline.aggregate_std(series[Schemes.Best]) # size D x N

    d = Dict("geometries" => geometries,
        "x_imis_mean" => pred_imis_mean,
        "std_imis_mean" => std_imis_mean,
        "x_best" => pred_best,
        "std_best" => std_best,
    ) 
    path = Store.get_path(st, Store.FO_MISC, context) * ".json"
    write(path, JSON.json(d))
    @info "exported in $path"
end

function _plot_hapke_lab(st::Store.Storage, context, Yobs, wavelengths::Vector, params::Pipeline.ProcedureParams, Xmcmc, Covsmcmc, preds_vec, variabilities)
    K = Gllim.getK(params.training_params.gllim_params.init)
    label = "hapke_lab_K$(K)_floor" 

    mcmcYerr = Pipeline.Y_relative_err(context, Yobs, Models.from_physical(context, Xmcmc), 1., zeros(Models.get_D(context)))
    refPred = Pipeline.Prediction(Xmcmc, Covsmcmc;
        config=Pipeline.SerieConfig("MCMC", :basic, "green"; hide_std=false, hideX_err=true, alpha=0.5), Yerr=mcmcYerr)

    ip = params.inv_params.imis_params
    ips = " (N_0 = $(ip.N0), B = $(ip.B), J = $(ip.J))"
    configs_sampling = [
        # Schemes.IsMean => Pipeline.SerieConfig(L"x_{is}", :basic, "red"; hide_std=false, alpha=0.7),
        Schemes.ImisMean => Pipeline.SerieConfig(L"x_{imis}" * ips, :basic, "red"; hide_std=false, alpha=0.7),
        # Schemes.Best => Pipeline.SerieConfig(L"x_{best}", :dashed, "blue"; hide_std=false),
   ]
    configs_best = [
        Schemes.BestWithIs => Pipeline.SerieConfig(L"x_{best, is}", :dashed, "green"; hide_std=true),
        Schemes.BestWithImis => Pipeline.SerieConfig(L"x_{best, imis}", :dashed, "orange"; hide_std=true),
    ]
    configs_gllim = [
        Schemes.GllimMean => Pipeline.SerieConfig(L"x_{GLLiM}", :basic, "red"; hide_std=false)
    ]
    configs_centroids = [
        Schemes.ImisCentroid("1") => Pipeline.SerieConfig(L"x_{imis,c1}", :circle, "red"; hide_std=true),
        Schemes.ImisCentroid("2") => Pipeline.SerieConfig(L"x_{imis,c2}", :circle, "orange"; hide_std=true),
    ]
    preds_sampling = Pipeline.format_inversions(preds_vec, configs_sampling)
    preds_sampling = [preds_sampling..., refPred ]
    preds_best = Pipeline.format_inversions(preds_vec, configs_best)
    # preds_best = [preds_best..., refPred ]
    preds_gllim = Pipeline.format_inversions(preds_vec, configs_gllim)
    preds_gllim = [preds_gllim..., refPred ]
    preds_centroids = Pipeline.format_inversions(preds_vec, configs_centroids)
    preds_centroids = [preds_centroids..., refPred ]

    path = Store.get_path(st, Store.FO_GRAPHS, context, params; label=label)
    varnames = Models.variables(context)
    plot_components(preds_sampling;  savepath=path * "_sampling", varnames=varnames, x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
    plot_components(preds_best;  savepath=path * "_best", varnames=varnames, x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
    plot_components(preds_gllim;  savepath=path * "_gllim", varnames=varnames, x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
    plot_components(preds_centroids;  savepath=path * "_centroids", varnames=varnames, x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
    plot_variabilities(variabilities; savepath=path * "variabilities.png", x_ticks=1000 .* wavelengths, varnames=varnames, xlabel="wavelength (nm)")

    X_ismean = Pipeline.aggregate(Pipeline.extract_series(preds_vec, [Schemes.IsMean])[Schemes.IsMean])
    X_imismean = Pipeline.aggregate(Pipeline.extract_series(preds_vec, [Schemes.ImisMean])[Schemes.ImisMean])
    X_best = Pipeline.aggregate(Pipeline.extract_series(preds_vec, [Schemes.Best])[Schemes.Best])
    # export results for further analysis (X in physical space)
    # serialize(Store.get_path(st, Store.FO_MISC, context, :tmp; label=label), 
    #     Dict(Schemes.IsMean => X_ismean, Schemes.Best => X_best))

    # second plots : observables
    geom = Models.matrix(Models.geometries(context))

    # we compute the reconstruction
    Yismean = Models.F(context, Models.from_physical(context, X_ismean))
    Yimismean = Models.F(context, Models.from_physical(context, X_imismean))
    Ybest = Models.F(context, Models.from_physical(context, X_best))
    Ymcmc = Models.F(context, Models.from_physical(context, Xmcmc))

    # obs_indexes = sortperm(mcmcYerr; rev = true)[1:3] #3 worst ref preds
    _, Nobs = size(Yobs)
    obs_indexes = [8, 44, 76] # expert value 
    subplots = []
    for (i, index) in enumerate(obs_indexes)
        wl = wavelengths[index] * 1000
        Yplot = hcat(Yobs[:,index], Ymcmc[:,index], Yismean[:,index], Yimismean[:,index], Ybest[:,index])
        labelIs, labelImis, labelBest = "\$F(x_{is})\$", "\$F(x_{imis})\$", "\$F(x_{best})\$"
        labels = [L"y_{obs} - $\lambda = " * string(wl) *  L" nm$",  L"F(\mathbf{x}_{MCMC})" ,labelIs, labelImis, labelBest]
        titlefunc  = (inc, azi) -> "wavelength : $wl nm"
        p = plot_polar(Yplot, geom, labels, ["black", "green", "red", "brown", "blue"]; title=titlefunc)
        push!(subplots, p)
    end 
    obs_plot = plot(subplots..., layout=(1, length(obs_indexes)), size=(length(obs_indexes) * 600, 700))
    obs_path = path * "observables.png"
    png(obs_plot, obs_path)
    @debug "Saved in $obs_path"
end


gllim_params = Gllim.Params(
    Gllim.MultiInitParams(10,  Gmms.EmParams(70, 30, 20, 1e-12)), 
    Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(100, 1e-5), 1e-12)
)
training_params = Shared.TrainingParams(
    50_000, 
    0.005, 
    gllim_params 
)
inversions_params = Pipeline.InversionParams(
    0.005, 
    Predictions.MergingOptions(2),
    50_000, 
    false,
    false,
    Is.NonUniformityParams(1000),
    0
)

# c = HapkeNontronite()
c = HapkeHowardite()
# c = HapkeCeres()
# c = HapkeMukundpuraBloc()
# c = HapkeMukundpuraPoudre()
# c = HapkeFull()
hapke_lab(3,c,training_params, inversions_params)


using Test
using FastBayesianInversion.Models
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Comptimes
using Logging

include("../inversion_cube.jl")
include("../k_selection_r.jl")

using .KSel

Log.init(Logging.Info)

@testset "cubes" begin
    D, W = 6, 13
    Ycube = rand(D, W, 101)
    mat, N = _cube_to_mat(Ycube)
    @test N == 101
    @test size(mat) == (D, W * N)

    Ycube2 = _mat_to_cube(mat, N)
    @test size(Ycube2) == (N, D, W)
    @test permutedims(Ycube2, [2, 3, 1]) == Ycube
end


@testset "inversion cube" begin
    st = Store.Storage("tmp/")

    gp = Gllim.Params(
         Gllim.MultiInitParams(1, Gmms.EmParams(3, 3, 2, 1e-12)),
         Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(5, 1e-4), 1e-12)
    )
    pp_is = Pipeline.ProcedureParams(
        Shared.TrainingParams(100, 0.01, gp),
        Pipeline.InversionParams(0.01, 
            Predictions.MergingOptions(2), 
            Is.IsParams(50), false, false,
            Is.NonUniformityParams(10), 0
        )
    )
    pp_imis = Pipeline.ProcedureParams(
        Shared.TrainingParams(100, 0.01, gp),
        Pipeline.InversionParams(0.01, 
            Predictions.MergingOptions(2), 
            Is.ImisParams(50, 40, 1), false, false,
            Is.NonUniformityParams(10), 0
        )
    )

    c1 = HapkeB385()
    d1::Data3D = load_observations(c1)

    c2 = HapkeMars2020_140520()
    d2 = load_observations(c2, "FRT")
    d3 = load_observations(c2, "HRL")

    c3 = HapkeGlaceMars()
    d4 = load_observations(c3, "FRT144_S")
    d5 = load_observations(c3, "FRT144_L")

    exps = [(c1, d1), (c2, d2), (c2, d3), (c3, d4), (c3, d5)]
    @testset "context $(Models.label(context))" for (context, datas) in exps
    datas = Data3D(datas.Yobs[:, 1:2, 1:3], datas.Yobs_std[:, 1:2, 1:3], datas.mask)
    ct = inverse_export_cube(2, context, pp_is, datas, st)
    @test ct isa Comptimes.Times
    ct = inverse_export_cube(2, context, pp_imis, datas, st)
    @test ct isa Comptimes.Times
end

    Store.remove(st)
end

@testset "hapke high dim" begin
    @test Models.get_D(HapkeHighDim1()) < Models.get_D(HapkeHighDim2()) < Models.get_D(HapkeHighDim3())
end

@testset "size Hapke obs" begin
    @testset "$name" for (name, path) in pairs(PATHS)
        c = _hapkeModel(name)
        fullData::HapkeData =  loads(path)
        @testset "obs $key" for (key, obs) in pairs(fullData.observations)
            @test size(obs.Yobs) == size(obs.Yobs_std) # both in Data2D and Data3D
            s = size(obs.Yobs)
            @test s[1] == Models.get_D(c)
            if obs isa Data3D
                @test s[2] <= s[3] # cheap test : wavelengths are always fewer than spatial points
            end
        end
    end
end

@testset "load obs glace" begin
    ct = HapkeGlaceMars()
    for exp in [
        "FRT11D5E_S",
        "FRT11D08_S",
        "FRT11FE1_S",
        "FRT13D75_S",
        "FRT116E3_S",
        ]
    load_observations(ct, exp)
end
end

@testset "K selection" begin
    @test KSel.N_train_select_K([55,10,2]) == 55 * 1_000

    model = Models.LinearTrivial()
    Ks = [3,4]

    out = KSel.train_select_K(model, Ks, 0.1)
    @test out isa KSel.KSelection
    @test length(out.models) == 2

    st = Store.Storage("tmp/")
    Ystds = [0.1, 0.2]
    for Ystd in Ystds
    KSel.train_select_K(st, model, Ks, Ystd)
end

    exploit_K_selection(st, model, Ks, Ystds)

    @test length(readdir("tmp/" * Store.FO_GLLIM)) > 1 # same K might be choosen

    Store.remove(st)
end
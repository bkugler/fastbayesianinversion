using FastBayesianInversion.Mcmc
using FastBayesianInversion.Log

using MAT
using Plots
using JSON

Log.init()

include("../hapke_models/models.jl")

function cmp_glace()
    # m = matread("/Users/kuglerb/Documents/Work2/Predictions/glace_is__3186064509-2384844823-84839464.mat")
    # m_imis = matread("/Users/kuglerb/Documents/Work2/Predictions/glace_FRT11FE1_S__436099000-803634506-2682725601.mat")
    m_imis = matread("/Users/kuglerb/Documents/Work2/Predictions/glace_FRT13D75_S__436099000-803634506-2429307336.mat")
    
    ct = HapkeGlaceMars()
    datas::Data3D = load_observations(ct, "FRT13D75_S")
    _, W, N = size(datas.Yobs)
    @show N
    @show W * N


    X_imis = m_imis["x_imis_mean"]
    # X = m["x_is_mean"]
    # X_best_is = m["x_best"]
    # X_best_imis = m_imis["x_best"]
    
    mask = isfinite.(X_imis)
    success = sum([all(mask[n, :, w]) for n in 1:N for w in 1:W])
    @show success
    @show W * N - success

    subplots = []
    # push!(subplots, scatter(X[:, 4, 1], label="IS", ylims=(0, 1)))
    push!(subplots, scatter(X_imis[:, 4, 1], label="IMIS - c", ylims=(0, 1)))
    # push!(subplots, scatter(X[:, 2, 14], label="IS", ylims=(0, 30)))
    push!(subplots, scatter(X_imis[:, 2, 14], label="IMIS - theta", ylims=(0, 30)))
    plot(subplots...)
end
# cmp_glace()

function check_glace_data()
    context = HapkeGlaceMars()
    datas::Data3D = load_observations(context, "FRT144_S")
    mat_init = matread("/Users/kuglerb/Documents/docs/DATA/HAPKE/deprecated/glace/ar_FRT144E9_S/Inv_FRT144E9_S_Wfix_0.60427_rho_mod.mat")
    mat = matread("/Users/kuglerb/Documents/docs/DATA/HAPKE/deprecated/glace/ar_FRT144E9_S/data.mat")

    Yobs_init = mat_init["cub_rho_mod"]
    @assert size(Yobs_init) == size(mat["obs"])
    @show maximum((Yobs_init .- mat["obs"])[isfinite.(Yobs_init)])
    @assert Yobs_init == mat["obs"]
    
    Yobs_std_init = mat_init["cub_drho_mod"]
    @assert size(Yobs_std_init) == size(mat["stds"])
    # @show maximum(Yobs_std_init .- mat["stds"])
    @assert isapprox(Yobs_std_init, mat["stds"])

    Yobs_matlab = permutedims(mat["obs"][datas.mask, :, :], [2, 3, 1])
    @assert size(Yobs_matlab) == size(datas.Yobs)
    # @show maximum(Yobs_matlab .- datas.Yobs)
    @assert isapprox(Yobs_matlab, datas.Yobs)

    Yobs_std_matlab = permutedims(mat["stds"][datas.mask, :, :], [2, 3, 1])
    mask_std = .~isfinite.(Yobs_std_matlab) # bad
    Yobs_std_matlab[mask_std] .= 0.03 * Yobs_matlab[mask_std]
    @assert size(Yobs_std_matlab) == size(datas.Yobs_std)
    # @show maximum(Yobs_std_matlab .- datas.Yobs_std)
    @assert isapprox(Yobs_std_matlab, datas.Yobs_std)
    
    # run some MCMC on a subsample 
    Yobs = datas.Yobs[:, 10, 1:100]
    Yobs_std = datas.Yobs_std[:, 10, 1:100]

    # mcmc_params_hmc = Mcmc.ParamsHMC(10_000, 10_000 / 2, 0.001)
    # invs_mcmc_hmc, _ = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
    
    # matwrite(ROOT_PATH_STORE * "FRT144_mcmc.mat", Dict(
    #     "hmc_mean" => invs_mcmc_hmc.Xmean,
    #     "hmc_std" => sqrt.(invs_mcmc_hmc.Xcovs),
    # ))

    mcmc = matread(ROOT_PATH_STORE * "FRT144_mcmc.mat")
    is = matread("/Users/kuglerb/Documents/Work2/Predictions/glace_is__673917509-2384844823-3445419935.mat")
    imis = matread("/Users/kuglerb/Documents/Work2/Predictions/glace_imis__673917509-4243815533-3445419935.mat")

    @assert size(is["x_is_mean"]) == (3093, 4, 50)
    @assert size(imis["x_imis_mean"]) == (3093, 4, 50)

    x_mcmc = mcmc["hmc_mean"]
    x_is =  is["x_is_mean"][1:100, :, 10]'
    x_imis = imis["x_imis_mean"][1:100, :, 10]'

    subplots = []
    for (i, l) in enumerate(Models.variables(context))
        push!(subplots, plot(1:100, hcat(x_mcmc[i, :], x_is[i, :], x_imis[i, :]); labels=["HMC" "IS" "IMIS"], ylabel=l, alpha=0.7)
        )
    end
    p = plot(subplots...)
    png(p, ROOT_PATH_STORE * "FRT144")

    matwrite(ROOT_PATH_STORE * "FRT144_obs.mat", Dict(
        "yobs" => Yobs,
        "ystd" => Yobs_std,
    ))
end

function export_geometries()
    rd = x -> round(x, digits = 2)
    out =  Dict()
    for ct in [
        HapkeNontronite(),
        HapkeGlaceMars(),
        HapkeMukundpuraBloc(),
        HapkeMukundpuraPoudre(),
        HapkeBlackyBloc(),
        HapkeBlackyPoudre(),
    ]
        geoms = Models.geometries(ct)
        out[Models.label(ct)] = Dict(
            "incidence" => rd.(geoms.inc),
            "emergence" => rd.(geoms.eme),
            "azimut" => rd.(geoms.azi),
        )
    end
    out
    write("geometries.json",JSON.json(out, 2))
end

# check_glace_data()

# export_geometries()

using FastBayesianInversion.Log 
using FastBayesianInversion.Store
using FastBayesianInversion.Mcmc 
using FastBayesianInversion.Pipeline 

using Plots 

include("../hapke_models/models.jl")

Plots.pyplot()
Log.init()

st = Store.Storage(ROOT_PATH_STORE)

std_train, std_ratio = 0.001, 50

context = HapkeNontronite()
_datas = load_observations(context)
Yobs, wavelengths  = _datas.Yobs, _datas.wavelengths
Yobs_std = Yobs ./ std_ratio
n = 33
n_try = 10
Yobs = repeat(Yobs[:, n], 1, n_try)
Yobs_std = repeat(Yobs_std[:, n], 1, n_try)

function inverse(Niter::Int, run::Bool)
    Nkeep = Niter / 2 
    params_1 = Mcmc.ParamsMH(std_train; Niter=Niter, Nkeep=Nkeep)
    params_2 = Mcmc.ParamsHMC(Niter, Nkeep, std_train)

    if run 
        out_1, dura1 = Mcmc.complete_inversion(context, Yobs, Yobs_std, params_1)
        out_2, dura2 = Mcmc.complete_inversion(context, Yobs, Yobs_std, params_2)

        @debug "Metropolis Hasting : $dura1 ; Hamiltonian Monte Carlo : $dura2"
        means_1 = Pipeline.aggregate(out_1)
        means_2 = Pipeline.aggregate(out_2)

        Store.save(st, (means_1, means_2), Store.FO_PREDICTIONS, context, params_1, params_2)
    end
    means_1, means_2 = Store.load(st, Store.FO_PREDICTIONS, context, params_1, params_2)

    means_1, means_2
end

function plot_means(m1::Matrix, m2::Matrix, Niter)
    p1 = scatter(m1[1,:], m1[2,:], label="Metropolis Hasting", xlabel="omega", ylabel="theta", xlims=(0,1), ylims=(0,30))
    scatter!(p1, m2[1, :], m2[2, :], label="Hamiltonian Monte Carlo")

    p2 = scatter(m1[3,:], m1[4,:], label="Metropolis Hasting", xlabel="b", ylabel="c", xlims=(0,1), ylims=(0,1))
    scatter!(p2, m2[3, :], m2[4, :], label="Hamiltonian Monte Carlo")


    p = plot(p1, p2; size=(1200, 1200))

    savepath = Store.get_path(st, Store.FO_GRAPHS, :mcmc, Niter, std_ratio; label="Niter_$Niter")
    png(p, savepath)
    @debug "Plotted in $savepath"
end

function study(exp_max::Int, run) 
    for Niter in 10 .^ (3:exp_max)
        @debug "Launching MCMC with Niter = $Niter ..."
        Log.indent()
        m1, m2 = inverse(Niter, run)
        plot_means(m1, m2, Niter)
        Log.deindent()
    end
end

study(7, false)

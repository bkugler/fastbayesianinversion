using FastBayesianInversion.Models

using Tables
using CSV 

include("../hapke_models/models.jl")

c = HapkeNontronite()

function export_dicts()
    datas = load_observations(c)

    Ystd = 0.05
    Xgllim, Ygllim = Models.data_training(c, 100_000, Ystd)
    Xabc, Yabc = Models.data_training(c, 100_000, Ystd)

# index 96 shows signs of bimodality
    obs = datas.Yobs[:, 96:96]
    obs_std = datas.Yobs_std[:, 96:96]

    files = [
    "Ygllim" => Ygllim,
    "Xgllim" => Xgllim,
    "Xabc" => Xabc,
    "Yabc" => Yabc,
    "Obs" => obs,
    "ObsStd" => obs_std,
]

    DIR = "/Users/kuglerb/Downloads/export/"
    rm(DIR, recursive=true, force=true)
    mkdir(DIR)
    for (filename, matrix) in files 
        CSV.write(DIR * filename * ".csv", Tables.table(matrix'); writeheader=false)
    end
end 

function compute_samples()
    samples = [
        # [0.6, 0.8, 0.4, 0.2],
        # [0.5, 0.5, 0.5, 0.5],
        # [0.6, 0.18, 0.17, 0.07],
        # [0.6, 0.42, 0.17, 0.07],
        [0.59, 0.15, 0.14, 0.06],
        [0.59, 0.42, 0.14, 0.06],
        [0.68, 0.04, 0.23, 0.04],
        [0.71, 0.40, 0.31, 0.01],
        [0.29, 0.30, 0.50, 0.07],
    ]

    f = Models.closure(c)
    for sample in samples 
        println(sample)
        println(f(sample))
        println()
    end
end

compute_samples()
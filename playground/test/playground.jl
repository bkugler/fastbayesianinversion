using Plots
using LinearAlgebra
using CSV

using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Probas
using FastBayesianInversion.Gmms
using FastBayesianInversion.Gllim
using FastBayesianInversion.Models
using FastBayesianInversion.Is
using FastBayesianInversion.Models.HapkeFunc
using FastBayesianInversion.Shared

include("../env.jl") # setup file paths for input and output data

Log.init()
Plots.pyplot()

""" Runs and plots the very standard Kmeans algorithm on synthetic data """
function example_kmeans()
    L, N, K = 2, 1000, 10
    X = rand(L, N)
    # two clusters
    X[1:500] ./= 3
    X[501:end] ./= 3
    X[501:end] .+= 0.5

    centers = rand(L, K)
    out, labels = Gmms.kmeans(X, centers, 20)

    p_X = scatter(X[1, :], X[2, :])
    p_centers_init = scatter(centers[1, :], centers[2, :])
    p_centers_final = scatter(out[1, :], out[2, :], c=1:K)
    p_X_final = scatter(X[1, :], X[2, :], c=labels)
    plot(p_X, p_centers_init, p_centers_final, p_X_final)
end

""" Learn GLLiM parameters against a random training dictionary (X,Y) """
function example_gllim()
    # using FastBayesianInversion.Log
    # using FastBayesianInversion.Probas # used to express the covariance constraints
    # using FastBayesianInversion.Gmms
    # using FastBayesianInversion.Gllim

    # Log.init()

    K = 50
    L, D = 4, 10

    # dummy training dictionnary
    N = 10_000
    X = rand(L, N)
    Y = rand(D, N)

    params = Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(K, 30, 15, 1e-12)
        ),
        # no constraint on Gamma, diagonal constraint on Sigma
        Gllim.TrainParams{Float64, FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )

    theta, log_likehood_history = Gllim.train(params, X, Y)

    theta_star = Gllim.inversion(theta)

    Nobs = 100
    Yobs = rand(D, Nobs)

    alphas, centroids = Gllim.conditionnal_density(theta_star, Yobs) # size (K, Nobs) ; (L, K, Nobs)
    covariances = theta_star.Σ # array of length K (since it does not depend on Yobs)

    # do something with the posterior GMM (alphas, centroids, covariances) ...
end



struct GaussianModel <: Is.IsModel
    std::Float64
end

Is.get_L(::GaussianModel) = 1
function Is.log_p(m::GaussianModel, x::VecOrSub, ::Int)
    Probas.log_gaussian_density(x, [0.5], UniformScaling(m.std^2), 1)
    # log(Probas.gmm_density(x, [0.4 0.8], [LowerTriangular(hcat(m.std)), LowerTriangular(hcat(m.std))]))
end
Is.get_proposition(::GaussianModel) = Is.GaussianMixtureProposition{Float64}([0.7, 0.3], [0.2 0.5], [LowerTriangular(hcat(0.3)), LowerTriangular(hcat(0.7))])

""" Runs and plots a simple Importance Sampling inversion """
function example_importance_sampling(N=10_000)
    Ntry = 1
    L = 1
    model = GaussianModel(0.2)
    proposition = Is.get_proposition(model)
    means = zeros(L, Ntry)
    Xss, weightss = zeros(L, N, Ntry), zeros(N, Ntry)
    Threads.@threads for ntry in 1:Ntry
        # Xs, weights = zeros(L, N), zeros(N)
        Is.importance_sampling!(model, view(Xss, :, :, ntry), view(weightss, :, ntry))
        pred, _ = Is.exploit_samples(Xss[:,:,ntry], weightss[:, ntry], Is.NonUniformityParams(1))
        means[:, ntry] .= pred.mean
    end
    @show sum(means, dims=2) ./ Ntry
    p = scatter(means[1, :], 0.4 * ones(Ntry), label="IS mean")
    scatter!(p, [0.5], 0.4 * ones(Ntry), label="IMIS mean")
    # plot one of the serie of samples
    max_weight = maximum(weightss[:,1])
    scatter!(p, Xss[1, :, 1], zeros(N), alpha=weightss[:,1] / max_weight,  label="samples", m=:star, xlims=(-0.1, 1.1))
    # best weight 
    _, i = findmax(weightss[:, 1])
    scatter!(p, Xss[1, i:i, 1], [0.2], m=:square, label="IMIS new centroid")
    points = range(0, 1, length=100)
    plot!(p, points, exp.([Is.log_p(model, [point], 1) for point in points]), label="true posterior")
    plot!(p, points, exp.([Is.log_q(proposition, [point]) for point in points]), label="proposition")
    # vline!(p, outMean)
    png(p, "is_imis_principe.png")
end

"""
Inverts a double-solutions simple function, showing the distinction between modes and mean 
Pass run = 2 to train and invert, run = 1 to only invert, run = 0 to only plot.
"""
function example_double_solutions(run::Int)
    st = Store.Storage(ROOT_PATH_STORE)
    training_params =  Shared.TrainingParams(
        50_000,
        0.001,
        Gllim.Params(
            Gllim.MultiInitParams(
                10,
                Gmms.EmParams(30, 30, 15, 1e-12)
            ),
            Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
    obs_std_ratios = [4,8]
    ct = Models.DoubleSolutions(1)

    trained_gllim, _, _ = Shared.generate_train_gllim(ct, training_params, st, run >= 2)
    merg = Predictions.MergingOptions(2)
    # we choose one observable
    Xobs, Yobs = Models.data_observations(ct, 20)
    Xobs_alt = Models.alternative_solution(ct, Xobs)
    index = 5
    xobs, yobs, xobs_alt = Xobs[:, index], Yobs[:, index], Xobs_alt[:, index]

    subplots = []
    for obs_std_ratio in obs_std_ratios
        if run >= 1
            yobs_std = yobs / obs_std_ratio

            _, _, _, _, full_posterior, merged_posterior = Shared.setup_inversion(ct, yobs, yobs_std, trained_gllim, 
                training_params.train_std, merg) 
            Store.save(st, (full_posterior, merged_posterior), Store.FO_PREDICTIONS, ct, training_params, merg, obs_std_ratio, :pdf)
        end
        (full_posterior, merged_posterior) = Store.load(st, Store.FO_PREDICTIONS, ct, training_params, merg, obs_std_ratio, :pdf)

        allocated = zeros(Models.get_L(ct)) # needed as tmp memory
        xs = range(0, stop=1; length=1000)
        fs_full = [Probas.gmm_density([x], full_posterior, allocated) for x in xs]
        fs_merged = [Probas.gmm_density([x], merged_posterior, allocated) for x in xs]
        p = plot(xs, fs_full, label="full GMM posterior"; title="Observation SNR : $(obs_std_ratio)")
        plot!(p, xs, fs_merged, label="merged GMM posterior"; linestyle=:dash)
        vline!(p, [xobs[1],xobs_alt[1]], label="true solutions", alpha=0.5)
        push!(subplots, p)
    end
    p = plot(subplots...; dpi=Config.DPI)
    path = Store.get_path(st, Store.FO_GRAPHS, ct, training_params, obs_std_ratios; label="pdf_double")
    png(p, path)
    @debug "Plotted in $path"
end


# example_kmeans()
# example_importance_sampling(30)
# example_double_solutions(2)
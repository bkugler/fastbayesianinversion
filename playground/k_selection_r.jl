using RCall
using Dates

using FastBayesianInversion.Models
using FastBayesianInversion.Probas
using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Pipeline

include("k_selection.jl")

using .KSel

function exploit_K_selection(st::Store.Storage, context::Models.Model, Ks::Vector{Int}, Ystds::Vector)
    N_train = KSel.N_train_select_K(Ks)

    Log.indent()
    for Ystd in Ystds
        # train_select_K(context, Ks, Ystd) has been executed

        res::KSel.KSelection, ti = Store.load(st, Store.FO_MISC, context, Ks, Ystd)
        @debug "K selection time : $(Dates.canonicalize(Dates.CompoundPeriod(ti)))"

        path = Store.get_path(st, Store.FO_GRAPHS, context, Ks, Ystd; label="$(Models.label(context))_$Ystd")

        index_bic = best_K_bic(res)
        index_slope, _, _ = best_K_slope_heuristic(res)

        Kbic, Kslope = Ks[index_bic], Ks[index_slope]

        # we save the learned gllim to ensure coherence with K selection, with a special tag
        train_bic = KSel.KSelectionParams(Kbic) # best K
        train_slope = KSel.KSelectionParams(Kslope) # slope heuristic

        params_bic = Shared.TrainingParams(N_train, Ystd, train_bic)
        params_slope = Shared.TrainingParams(N_train, Ystd, train_slope)
        Log.indent()
        Store.save(st, res.models[index_bic],  Store.FO_GLLIM, context, params_bic, :K_selection)
        Store.save(st, res.models[index_slope],  Store.FO_GLLIM, context, params_slope, :K_selection)
        Log.deindent()
        @info "K selection for noise $Ystd : Gllim models for K = $Kbic and K = $Kslope saved."
    end
    Log.deindent()
end

best_K_bic(res::KSel.KSelection) = findmin(res.bic)[2]
best_K_bic_test(res::KSel.KSelection) = findmin(res.bic_test)[2]

""" Returns the best indice """
function best_K_slope_heuristic(res::KSel.KSelection)
    ll, dims = res.log_likelihood, res.dims
    a, b = robust_regression(dims, ll)
    ll_pen = ll - 2 * a * dims
    _, i_best = findmax(ll_pen)
    i_best, a, b
end


"""
 Use R function rlm
https://www.rdocumentation.org/packages/MASS/versions/7.3-51.6/topics/rlm
"""
function robust_regression(dims::Vector, log_ll::Vector)
    @debug "Calling R from Julia ..."
    r_out = RCall.R"""
    library(MASS)

    SH <- function (dims,log_ll,disp=FALSE) {  
        dd=data.frame(nbp=dims,ll=log_ll)
        fit=rlm(ll  ~ nbp,data=dd,method='M')
        if(fit$coefficients[2]<0) fit$coefficients[2]=0
        if (disp){
            dd$llpen = dd$ll- 2* fit$coefficients[2]*dd$nbp
            plot(dd$nbp,dd$ll,type='p')
            abline(fit,col='red')
        }
        fit$coefficients
    }

    SH($dims, $log_ll)
    """
    @debug "Done."
    v = RCall.rcopy(r_out)
    intercept, slope = v[1], v[2]
    slope, intercept
end


using ArchGDAL

include("models.jl")

function _rename_obs()
    data::HapkeData = deserialize(PATHS["GlaceMars"])
    for k in collect(keys(data.observations))
        data.observations["FRT144_" * k] = data.observations[k]
        delete!(data.observations, k)
    end
    data = HapkeData(data.geometries, data.observations)
    serialize(PATHS["GlaceMars"], data)
end


function _serialize_one(name::String)
    path = ROOT_DATA_PATH * "HAPKE/MARS/GlaceMars/ar_site_glaceI_080321/"
    path_obs = path * name * "_rho_mod"
    path_std = path * "Produits_S/" * name * "_drho"

    data = ArchGDAL.read(path_obs)
    obs = ArchGDAL.read(data) # size ( D, N, W)
    obs = Array{Float32,3}(obs)

    data = ArchGDAL.read(path_std)
    stds = ArchGDAL.read(data) # size ( D, N, W)
    stds = Array{Float32,3}(stds)

    obs = permutedims(obs, [1, 3, 2])
    stds = permutedims(stds, [1, 3, 2])
    D, W, N = size(obs)

    # we keep spatials point for which all wavelengths are valid
    # we replace invalide stds by Yobs * 0.03
    mask = [!all(isfinite.(obs[:, :,i])) || all(obs[:, :, i] .== 0) for i in 1:N] # bad points : null points are useless to invert

    mask_std = .~isfinite.(stds) # bad
    stds[mask_std] .= 0.03 * obs[mask_std]
    
    mask .= .~mask # good

    @assert size(obs) == size(stds)
    @assert D < W && W < N # should generally be true 
    @assert all(isfinite.(obs))
    @assert all(isfinite.(stds))

    @show name, sum(mask), length(mask)

    Data3D(obs, stds, mask)
end



# when adding new observations series, we rename the older ones
# _rename_obs()

function import_series()
    series = [
        "Inv_FRT11D5E_S_Wfix_0.90789",
        "Inv_FRT11D08_S_Wfix_0.70321",
        "Inv_FRT11FE1_S_Wfix_0.59871",
        "Inv_FRT13D75_S_Wfix_0.67782",
        "Inv_FRT116E3_S_Wfix_0.66487",
    ]
    current = deserialize(PATHS["GlaceMars"])

    for name in series 
        data::Data3D = _serialize_one(name)
        short_name = name[5:14]
        @assert !haskey(current.observations, short_name)
        current.observations[short_name] = data
    end
    @assert length(current.observations) == 7 

    hdata::HapkeData = HapkeData(current.geometries, current.observations) # to check the dims
    serialize(PATHS["GlaceMars"], hdata)
    # OUTPUT: 
    # Inv_FRT11D5E_S_Wfix_0.90789: 3250 / 7000
    # Inv_FRT11D08_S_Wfix_0.70321: 3095 / 7000
    # Inv_FRT11FE1_S_Wfix_0.59871: 3668 / 7000
    # Inv_FRT13D75_S_Wfix_0.67782: 3095 / 7000
    # Inv_FRT116E3_S_Wfix_0.66487: 3169 / 7000
end 
# import_series()

# script to import observations from json to fbi
using JSON
using FastBayesianInversion.Models 
using Serialization

include("models.jl")

out = JSON.parsefile(ROOT_DATA_PATH * "HAPKE/LAB/Ceres/analog_Ceres_BRDF.json")
d = out["Ceres_analogue"]

wl = [Float64(w[1]) / 1000 for w in d["wavelengths"]]

inc  = [ Float64(g[1]) for g in  d["geometries"]]
eme  = [ Float64(g[2]) for g in  d["geometries"]]
azi  = [ Float64(g[3]) for g in  d["geometries"]]
geoms = Models.HapkeGeometries(inc, eme, azi)

Yobs, Yobs_std = zeros(length(inc), length(wl)),  zeros(length(inc), length(wl))
for (i, obs) in enumerate(d["reflectance_poudre"])
    # obs = yobs, ystd
    Yobs[:, i] = obs[1]
    Yobs_std[:, i] = obs[2]
end

serie = Data2D(Yobs, Yobs_std, wl)
data = HapkeData(geoms, Dict("" => serie))
serialize(ROOT_DATA_PATH *  "HAPKE/LAB/Ceres/data.fbi", data)

# This file implements the generation of an hockey stick prior
# and a GMM fitted model over it.

using FastBayesianInversion.Models
using FastBayesianInversion.Models.HapkeFunc
using Random

function hockeystick_dataset(N::Int)
    c_std = 0.2
    bs, cs = zeros(N), zeros(N)
    i = 1
    while i <= N 
        b = rand()
        c = HapkeFunc.Crelation(b) + c_std * randn() * (1.4 - b)
        valid = (c <= 1) && (c >= 0)
        if valid 
            bs[i] = b 
            cs[i] = c
            i += 1
        end
    end
    bs, cs
end

function hockeystick_data_training(ct::Models.Hapke, N::Int, Ystd)
    X, _ = Models.data_training(ct, N, 0.)
    b, c = hockeystick_dataset(N)
    X[3, :] .= b
    X[4, :] .= c
    Y = Models.F(ct, X) + Ystd .* randn(Models.get_D(ct), N)
    X, Y
end

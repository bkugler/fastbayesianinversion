using MAT

# replace the corrupted observations for the GlaceFRT144 S context 
# for std, we use 0.03 Yobs 

include("models.jl")

# m = matread("/Users/kuglerb/frt144S.mat")
# Yobs = m["cub_rho_mod"]
# @assert size(Yobs) == (7000, 11, 50)
# mask = [Yobs[i, :, :] != zeros(11, 50) for i in 1:7000] # valid 
# @show sum(mask)

# @assert all(isfinite.(Yobs[mask, :, :]))

# Yobs = permutedims(Yobs[mask, :, :], [2, 3, 1])
# @assert size(Yobs) == (11, 50, 3122)

# new_data = Data3D(Yobs, Yobs .* 0.03, mask)

# context = HapkeGlaceMars()
# geoms = Models.geometries(context)
# obs = deserialize(PATHS["GlaceMars"]).observations
# obs["FRT144_S"] = new_data
# data = HapkeData(geoms, obs)

# serialize(PATHS["GlaceMars"], data)

# cts = [HapkeMukundpuraBloc(), HapkeMukundpuraPoudre(), HapkeBlackyBloc(), HapkeBlackyPoudre()]
# cts = [HapkeHowardite(), HapkeCeres()]
# for ct in cts
#     data::HapkeData = _load_hapke_data(Models.label(ct))
#     for (k, v) in pairs(data.observations)
#         v.wavelengths ./= 1000 # convert from nano meters to micro meters
#     end
#     dumps(PATHS[Models.label(ct)], data)
# end

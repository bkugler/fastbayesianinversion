# implement Models.Model for the various Hapke dataset 
# the actual data (geometries and observations) are stored
# in a separated folder `ROOT_DATA_PATH` 

using FastBayesianInversion.Models
using MAT

include("../env.jl")

""" One dimentional serie of observations (a spectrum for example) """
struct Data2D
    Yobs::Matrix
    Yobs_std::Matrix
    """ wavelengths are expressed in micro meters """
    wavelengths::Vector
end

function as_dict(d::Data2D)
    return Dict("Yobs" => d.Yobs, "Yobs_std" => d.Yobs_std, "wavelengths" => d.wavelengths)
end


""" Two dimentional serie of observations (a map of spectrum for example)
N is the spatial dimension, W the spectral one.
"""
struct Data3D{T}
    """ size D, W, N """
    Yobs::Array{T,3}
    """ size D, W, N """
    Yobs_std::Array{T,3}
    """ spatial mask from the original, dirty datas (length(mask) >= N) """
    mask::Vector
end

function as_dict(d::Data3D)
    return Dict("Yobs" => d.Yobs, "Yobs_std" => d.Yobs_std, "mask" => d.mask)
end



Datas = Union{Data2D,Data3D}

""" Common structure for all Hapke models
Pre-treatment should be apply before before storing this object.
"""
struct HapkeData
    geometries::Models.HapkeGeometries

    """ observation name => datas """
    observations::Dict{String,Datas} # must have the same geometries of measures
    
    function HapkeData(g, o)
        D = length(g.inc)
        for (_, data) in o 
            D == size(data.Yobs)[1] || error("wrong dimensions")
        end
        new(g, o)
    end
end

function as_dict(d::HapkeData)
    observations = Dict()
    for (k, v) in pairs(d.observations)
        if k == ""
            k = "empty__"
        end
        observations[k] = as_dict(v)
    end
    Dict(
        "geometries" => Dict("inc" => d.geometries.inc, "eme" => d.geometries.eme, "azi" => d.geometries.azi),
        "observations" => observations
    )
end

dumps(file::String, d::HapkeData) = matwrite(file, as_dict(d))

function loads(file::String)
    dict = matread(file)
    g = dict["geometries"]
    o = dict["observations"]
    geometries = Models.HapkeGeometries(g["inc"], g["eme"], g["azi"])
    observations = Dict()
    for (k, v) in pairs(o)
        if k == "empty__"
            k = ""
        end
        if haskey(v, "mask") # 3D
            observations[k] = Data3D{Float64}(v["Yobs"], v["Yobs_std"], Vector{Int}(v["mask"]))
        else # 2D
            observations[k] = Data2D(v["Yobs"], v["Yobs_std"], Vector{Float32}(v["wavelengths"]))
        end
    end
    HapkeData(geometries, observations)
end


# ---------------------------- Constructor ----------------------------

_load_hapke_data(label::String) = loads(PATHS[label])

function _hapkeModel(label::String; w_linear=true)
    # load the geometric setup
    data::HapkeData = _load_hapke_data(label)
    Models.Hapke(label, data.geometries; w_linear=w_linear)
end

PATHS = Dict([
    "Nontronite" => ROOT_DATA_PATH *  "HAPKE/LAB/Nontronite/data.fbi.mat"
    "Basalt" => ROOT_DATA_PATH *  "HAPKE/LAB/Basalt/data.fbi.mat"
    "Olivine" => ROOT_DATA_PATH *  "HAPKE/LAB/Olivine/data.fbi.mat"
    "Magnesite" => ROOT_DATA_PATH *  "HAPKE/LAB/Magnesite/data.fbi.mat"
    "BasalticGlass" => ROOT_DATA_PATH *  "HAPKE/LAB/BasalticGlass/data.fbi.mat"
    "JSC1" => ROOT_DATA_PATH *  "HAPKE/LAB/JSC1/data.fbi.mat"
    "GlaceMars" => ROOT_DATA_PATH *  "HAPKE/MARS/GlaceMars/data.fbi.mat"
    "Mars2020_140520" => ROOT_DATA_PATH *  "HAPKE/MARS/Mars2020_140520/data.fbi.mat"
    "B385" => ROOT_DATA_PATH *  "HAPKE/MARS/B385/data.fbi.mat"
    "CrismFRT193" => ROOT_DATA_PATH *  "HAPKE/MARS/CrismFRT193/data.fbi.mat"
    "MukundpuraPoudre" => ROOT_DATA_PATH *  "HAPKE/LAB/METEORITES/MukundpuraPoudre/data.fbi.mat"
    "MukundpuraBloc" => ROOT_DATA_PATH *  "HAPKE/LAB/METEORITES/MukundpuraBloc/data.fbi.mat"
    "BlackyPoudre" => ROOT_DATA_PATH *  "HAPKE/LAB/METEORITES/BlackyPoudre/data.fbi.mat"
    "BlackyBloc" => ROOT_DATA_PATH *  "HAPKE/LAB/METEORITES/BlackyBloc/data.fbi.mat"
    "Howardite" => ROOT_DATA_PATH *  "HAPKE/LAB/METEORITES/Howardite/data.fbi.mat"
    "Ceres" => ROOT_DATA_PATH *  "HAPKE/LAB/Ceres/data.fbi.mat"
])

HapkeNontronite() = _hapkeModel("Nontronite")
HapkeBasalt() = _hapkeModel("Basalt")
HapkeOlivine() = _hapkeModel("Olivine")
HapkeMagnesite() = _hapkeModel("Magnesite")
HapkeBasalticGlass() = _hapkeModel("BasalticGlass")
HapkeJSC1() = _hapkeModel("JSC1")

# FRT144_S; FRT144_L
HapkeGlaceMars() = _hapkeModel("GlaceMars")
HapkeMars2020_140520() = _hapkeModel("Mars2020_140520") # FRT or HRL
HapkeB385() = _hapkeModel("B385")
HapkeCrismFRT193() = _hapkeModel("CrismFRT193")

HapkeMukundpuraPoudre() = _hapkeModel("MukundpuraPoudre")
HapkeMukundpuraBloc() = _hapkeModel("MukundpuraBloc")
HapkeBlackyPoudre() = _hapkeModel("BlackyPoudre")
HapkeBlackyBloc() = _hapkeModel("BlackyBloc")

HapkeHowardite() = _hapkeModel("Howardite")
HapkeCeres() = _hapkeModel("Ceres")

""" `exp` should be empty when there is only one serie 
`m` label must be registred in `PATHS`
"""
function load_observations(m::Models.Hapke, exp::String="")
    # load the datas
    data::HapkeData = _load_hapke_data(Models.label(m))
    data.observations[exp]
end

## Synthetic model for high dimentional experiment 
function _generate_geoms_3()
    _inc = [0, 20, 40 ,60]
    _azi = [ 0, 30, 45, 150, 135, 180]
    _eme = [0.5, 1., 2., 10, 18, 19, 19.5, 20, 20.5, 21, 22, 30, 38, 39, 39.5, 40, 
        40.5, 41., 42, 50, 58, 59, 59.5, 60, 60.5, 61, 62, 70]
    incs, emes, azis = [], [], []
    for i in _inc 
        for e in _eme
            for a in _azi 
                push!(incs, i)
                push!(emes, e)
                push!(azis, a)
            end
        end
    end
    Models.HapkeGeometries(incs, emes, azis)
end

# half the dimention
function _generate_geoms_2()
    _inc = [0, 20, 40 ,60]
    _eme1 = [10,  20, 30, 40, 50, 60, 70]
    _eme2 = [0.5, 1., 2., 18, 19, 19.5, 20.5, 21, 22, 38, 39, 39.5, 40.5, 41., 42, 58, 59, 59.5, 60.5, 61, 62]
    _azi1 = [0,30] # _eme1 and _eme2
    _azi2 = [45, 150, 135, 180] # only _eme1

    incs, emes, azis = [], [], []
    for i in _inc 
        for a in _azi1
            for e in [_eme1..., _eme2...]
                push!(incs, i)
                push!(emes, e)
                push!(azis, a)
            end
        end
        for a in _azi2
            for e in _eme1
                push!(incs, i)
                push!(emes, e)
                push!(azis, a)
            end
        end
    end
    Models.HapkeGeometries(incs, emes, azis)
end

# a third 
function _generate_geoms_1()
    # pour incidence=0 et pour azimut =0, 30; 
    emergence_0 = [0.5, 1., 2., 10,  20, 30, 40, 50, 60, 70]
    # pour incidence=20 et pour azimut =0, 30; 
    emergence_20 = [10, 18, 19, 19.5, 20, 20.5, 21, 22, 30, 40, 50, 60, 70] 
    # pour incidence=40 et pour azimut =0, 30; 
    emergence_40 = [ 10,  20, 30, 38, 39, 39.5, 40, 40.5, 41., 42, 50, 60, 70]
    # pour incidence=60 et pour azimut =0, 30; 
    emergence_60 = [ 10,  20, 30, 40, 50, 58, 59, 59.5, 60, 60.5, 61, 62, 70]
    
    incs, emes, azis = [], [], []
    for a in [0,30]
        for e in emergence_0
            push!(incs, 0)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_20
            push!(incs, 20)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_40
            push!(incs, 40)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_60
            push!(incs, 60)
            push!(emes, e)
            push!(azis, a)
        end
    end
    
    # pour toutes les autres combinaisons d'incidence et d'azimut; 
    _eme = [10,  20, 30, 40, 50, 60, 70]
    _inc = [0, 20, 40 ,60]
    _azi = [ 45, 150, 135, 180]
    for i in _inc 
        for a in _azi 
            for e in _eme
                push!(incs, i)
                push!(emes, e)
                push!(azis, a)
            end
        end
    end
    Models.HapkeGeometries(incs, emes, azis)
end

# very small set of "decent" geometries
function _generate_geoms_0() 
    # incidence=40° emergence=10, 20, 30, 60, 70 pour azimut=0, 180 
    incs, emes, azis = [], [], []
    for a in [0,180]
        for e in [10, 20, 30, 60, 70]
            push!(incs, 40)
            push!(emes, e)
            push!(azis, a)
        end
    end
    Models.HapkeGeometries(incs, emes, azis)
end

# small configuration without small phase angles
function _generate_geoms_4() 
    # pour incidence=0 et pour azimut =0, 30; 
    emergence_0 = [10,  20, 30, 40, 50, 60, 70]
    # pour incidence=20 et pour azimut =0, 30; 
    emergence_20 = [10, 30, 40, 50, 60, 70] 
    # pour incidence=40 et pour azimut =0, 30; 
    emergence_40 = [10,  20, 30, 50, 60, 70]
    # pour incidence=60 et pour azimut =0, 30; 
    emergence_60 = [10,  20, 30, 40, 50, 70 ]
      
    incs, emes, azis = [], [], []
    for a in [0,30]
        for e in emergence_0
            push!(incs, 0)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_20
            push!(incs, 20)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_40
            push!(incs, 40)
            push!(emes, e)
            push!(azis, a)
        end
        for e in emergence_60
            push!(incs, 60)
            push!(emes, e)
            push!(azis, a)
        end
    end
    Models.HapkeGeometries(incs, emes, azis)
end

# small configuration with small phase angles
function _generate_geoms_5() 
    # pour incidence=0 et pour azimut =0; 
    emergence_0 = [0.5, 1., 2., 10, 20, 30, 40, 50, 60, 70]
    # pour incidence=20 et pour azimut =0; 
    emergence_20 = [10, 18, 19, 19.5, 20.5, 21, 22, 30, 40, 50, 60, 70] 
    # pour incidence=40 et pour azimut =0; 
    emergence_40 = [ 10,  20, 30, 38, 39, 39.5, 40.5, 41., 42, 50, 60, 70]
    # pour incidence=60 et pour azimut =0; 
    emergence_60 = [10,  20, 30, 40, 50, 58, 59, 59.5, 60.5, 61, 62, 70]
      
    incs, emes, azis = [], [], []
    for e in emergence_0
        push!(incs, 0)
        push!(azis, 0)
        push!(emes, e)
    end
    for e in emergence_20
        push!(incs, 20)
        push!(azis, 0)
        push!(emes, e)
    end
    for e in emergence_40
        push!(incs, 40)
        push!(azis, 0)
        push!(emes, e)
    end
    for e in emergence_60
        push!(incs, 60)
        push!(azis, 0)
        push!(emes, e)
    end
    
    # pour toutes les autres combinaisons d'incidence (20,40,60) et d'azimut (180)
    for i in [20,40,60]
        for e in [10,  20, 30, 40, 50, 60, 70]
            push!(incs, i)
            push!(azis, 180)
            push!(emes, e)
        end
    end
    
    Models.HapkeGeometries(incs, emes, azis)
end

HapkeHighDim1() = Models.Hapke("fairly high dimentional Hapke model", _generate_geoms_1(); partiel=true)
HapkeHighDim2() = Models.Hapke("high dimentional Hapke model", _generate_geoms_2(); partiel=false)
HapkeHighDim3() = Models.Hapke("really high dimentional Hapke model", _generate_geoms_3(); partiel=false)

HapkeMediumDim() = Models.Hapke("medium dimentional Hapke model", _generate_geoms_4(); partiel=true)
HapkeFull() = Models.Hapke("Hapke - 6 params", _generate_geoms_5(); partiel=false)

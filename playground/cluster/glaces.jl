using FastBayesianInversion.Log
using FastBayesianInversion.Store

include("../inversion_cube.jl")

Log.init()

const st = Store.Storage(ROOT_PATH_STORE)

if length(ARGS) >= 1
    experience = ARGS[1]
    @info "Starting experience $experience ..."
    flush(Base.stdout) # useful in batch mode

    if experience == "B385"
        hapke_massive_B385(st)
    elseif experience == "144E9_L"
        hapke_massive_glace(st, "FRT144_L")
    elseif experience == "144E9_S"
        hapke_massive_glace(st, "FRT144_S")
    elseif experience == "glaces"
        if length(ARGS) < 2
            error("missing experience serie for glaces")
        end
        hapke_massive_glace2(st, ARGS[2]) # reusing previous gllim
    elseif experience == "140520"
        hapke_massive_Mars2020(st)
    else
        error("Experience not supported: $(experience)")
    end

    @info "Done."
end
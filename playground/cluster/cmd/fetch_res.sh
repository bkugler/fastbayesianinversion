# Fetch OUT directory from cluster with scp, before removing data from the cluster
CLUSTER="bkugler@access2-cp.inrialpes.fr"
CLUSTER_OUT_DIR="/services/scratch/mistis/bkugler/OUT/"
# LOCAL_OUT_DIR="/scratch/WORK/"
LOCAL_OUT_DIR="/Users/kuglerb/Documents/Work2/"
CLUSTER_OAR="/services/scratch/mistis/bkugler/OAR."
PROXY="bkugler@bastion.inrialpes.fr"

echo "Fetching results..."
scp -r -oProxyJump=$PROXY $CLUSTER:$CLUSTER_OUT_DIR* $LOCAL_OUT_DIR &&
echo "Done. Cleaning cluster..." &&
ssh -J $PROXY $CLUSTER "
    rm -r ${CLUSTER_OUT_DIR}*;
    rm ${CLUSTER_OAR}*
" &&
echo "Done."


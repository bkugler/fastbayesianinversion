# launch script to execute on cluster node
# the first argument must be the path of the julia file to run, other arguments will be forwaded to it

#OAR -l /nodes=1,walltime=72:00:00

SINGULARITY_IMG=/services/scratch/mistis/bkugler/img.sif
export JULIA_NUM_THREADS=$(cat /proc/cpuinfo | grep -c "cpu cores") &&
singularity exec $SINGULARITY_IMG julia $@
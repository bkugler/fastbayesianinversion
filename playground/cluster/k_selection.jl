# entry point for a cluster simulation of K selection
# this script should be run for different values of 'context_id' and 'Ystd'

using FastBayesianInversion.Store
using FastBayesianInversion.Log
using Dates

include("../hapke_models/models.jl")
include("../k_selection.jl")

using .KSel

Log.init()

const st = Store.Storage(ROOT_PATH_STORE)
Ks = KSel.Krange

if length(ARGS) >= 2
    context_id = ARGS[1]
    if context_id == "Nontronite"
        model = HapkeNontronite()
    elseif context_id == "Glace"
        model = HapkeGlaceMars()
    elseif context_id == "MukundpuraBloc"
        model = HapkeMukundpuraBloc()
    else
        error("Context not supported: $(context_id)")
    end

    # hint for std: 0.1, 0.01, 0.001
    Ystd = parse(Float64, ARGS[2])

    @info "K selection on $(Models.label(model)) with std = $Ystd"
    flush(Base.stdout) # useful in batch mode

    KSel.train_select_K(st, model, Ks, Ystd)

    @info "Done."
end
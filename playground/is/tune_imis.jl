# TODO: use new API in Pipeline.TuneImis 

# This file compares IS and IMIS methods, with respect to computation times
# and prediction error (MCMC being used as a ground truth)

using LaTeXStrings
using Dates
using Plots
using Profile
using LinearAlgebra

using FastBayesianInversion.Probas
using FastBayesianInversion.Log
using FastBayesianInversion.Gmms
using FastBayesianInversion.Gllim
using FastBayesianInversion.Predictions
using FastBayesianInversion.Store
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.TuneImis
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Shared

include("../hapke_models/models.jl")
include("../env.jl")

Plots.pyplot()
Log.init()

st = Store.Storage(ROOT_PATH_STORE)

default_indexes = collect(1:5)

struct Setup
    gllim_mean::Vector
    merged_gmm::Probas.Gmm{Float64}
    input::TuneImis.Input
end
inputs(setups::Vector{Setup}) = [s.input for s in setups]

function gllim_perf(setups::Vector{Setup})
    err = 0.
    errs = zeros(Is.get_L(setups[1].input.is_model))
    for setup in setups
        err += norm(setup.gllim_mean .- setup.input.mcmc_mean)
        errs .+= abs.(setup.gllim_mean .- setup.input.mcmc_mean)
    end
    err /= length(setups)
    errs ./= length(setups)
    TuneImis.Perf(err, errs, Millisecond(0))
end


function setup(run_gllim::Bool, run_mcmc::Bool, indexes::Vector{Int})
    std_train, std_ratio = 0.001, 20
    
    context = HapkeNontronite()
    _datas = load_observations(context)
    Yobs, wavelengths  = _datas.Yobs, _datas.wavelengths
    Yobs_std = Yobs ./ std_ratio

    gllim_params = Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(50, 30, 15, 1e-12)
        ),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )
    merging_params = Predictions.MergingOptions(3)

    if run_gllim
        X, Y = Models.data_training(context, 50_000, std_train)
        trained_gllim, _ = Gllim.train(gllim_params, X, Y)
        Store.save(st, trained_gllim, Store.FO_GLLIM, context, std_train, gllim_params, :solo)
    end
    trained_gllim = Store.load(st, Store.FO_GLLIM, context, std_train, gllim_params, :solo)

    mcmc_params, nb_try = Mcmc.ParamsMH(std_train), 4

    # for each observations, setup the inversion
    @info "Inversion setups..."
    Log.indent()
    setups::Vector{Setup} = []
    for n in indexes
        @debug "index $n..."
        Log.indent()
        yobs, obs_std = Yobs[:, n], Yobs_std[:, n]

        if run_mcmc
            out, _ = Mcmc.complete_inversion(context, repeat(yobs, 1, nb_try), repeat(obs_std, 1, nb_try), mcmc_params)
            ref = sum(o.mean for o in out) ./ nb_try
            ref, _ = Models.from_physical(context, ref, ref) # complete inversion are in physical space
            Store.save(st, ref, Store.FO_PREDICTIONS, context, yobs, obs_std, mcmc_params, nb_try)
        end
        ref = Store.load(st, Store.FO_PREDICTIONS, context, yobs, obs_std, mcmc_params, nb_try)

        mean, _, model, _, _, merged_gmm = Shared.setup_inversion(context,
            yobs, obs_std, trained_gllim, std_train, merging_params)

        push!(setups, Setup(mean, merged_gmm, TuneImis.Input(ref, model)))
        Log.deindent()
    end
    Log.deindent()
    setups
end

# Register and plot the execution time of IMIS
# We try several budget (N_xxx), with several repartition of $K$ and $B$
struct Settings
    N_iss::Vector{Int}
    imis_range::TuneImis.ImisParamsRange
    N_try::Int # repeat to regularize
end

function run_perf(settings::Settings, run::Bool, run_gllim::Bool, run_mcmc::Bool)
    setups = setup(run_gllim, run_mcmc, default_indexes)
    savepath = Store.get_path(st, Store.FO_GRAPHS, settings, default_indexes)
    gp = gllim_perf(setups)

    if !run 
        out_is, out_imis = Store.load(st, Store.FO_BENCHMARK, settings, default_indexes)
        return out_is, out_imis, gp, savepath
    end

    @info "Is inversions..."
    Log.indent()
    out_is = Vector{TuneImis.ExpIs}(undef, length(settings.N_iss))
    Threads.@threads for i in 1:length(settings.N_iss)
        N_is = settings.N_iss[i]
        perf = TuneImis.perform(inputs(setups), settings.N_try, N_is)
        out_is[i] = TuneImis.ExpIs(N_is, perf)
    end
    Log.deindent()

    out_imis = TuneImis.run_imiss(inputs(setups), settings.imis_range, settings.N_try)

    Store.save(st, (out_is, out_imis), Store.FO_BENCHMARK, settings, default_indexes)
    out_is, out_imis, gp, savepath
end

settings_test = Settings( # test values, should be very fast to run
    [200,500],
    TuneImis.ImisParamsRange(
        [100,300],
        [0.1, 0.2],
        collect(1:5)
    ),
    2
)

settings = Settings(
    [10_000, 20_000, 100_000,200_000],
    TuneImis.ImisParamsRange(
        [200, 500, 2_000],
        [0.2, 0.5],
        collect(1:3:25)
    ),
    20
)

function _by_N0_B(out_imis::Vector{TuneImis.ExpImis})
    out = Dict{Pair{Int,Int},Vector{TuneImis.ExpImis}}()
    for exp in out_imis
        l = get(out, exp.params.N0 => exp.params.B, [])
        push!(l, exp)
        out[exp.params.N0 => exp.params.B] = l
    end
    out
end

function _dict_shapes(out_imis::Vector{TuneImis.ExpImis})
    B_to_shape = Dict{Int,Symbol}()
    for (i, B) in enumerate(Set(exp.params.B for exp in out_imis))
        B_to_shape[B] = _shapes[i]
    end
    B_to_shape
end

const _shapes = [ :circle, :star5, :diamond, :hexagon, :cross, :xcross, :utriangle, :dtriangle, :rtriangle, :ltriangle, :pentagon, :heptagon, :octagon, :star4, :star6, :star7, :star8, :vline, :hline, :+, :x]
""" Plot the precision and the computing times """
function plot_perf(out_is::Vector{TuneImis.ExpIs}, out_imis::Vector{TuneImis.ExpImis}, gllim_perf::TuneImis.Perf, savepath=nothing)
    p_err  = plot(ylabel=L"Prediction error $|| x - x_{MCMC} ||$", yminorgrid=true, yminorticks=10)
    p_times = plot(ylabel="Computation time (ms)", yminorgrid=true, yminorticks=10)
    p_tradeoff = plot(ylabel=L"Prediction error $|| x - x_{MCMC} ||$", xlabel="Computation time (ms)", xminorgrid=true, xminorticks=20)
    p_legend = plot(framestyle=:none, legend=:left)
    # errors component by component
    L = length(out_is[1].perf.errs)
    p_errs = [plot() for l in 1:L]

    # ----- GLLiM
    hline!(p_err, [gllim_perf.err], label="GLLIM")
    for l in 1:L
        hline!(p_errs[l], [gllim_perf.errs[l]], label="x$l - GLLIM")
    end

    # ----- IS
    plot!(p_err, [exp.Ns for exp in out_is], [exp.perf.err for exp in out_is], markersize=9, markershape=:rect,  label="IS")
    plot!(p_times, [exp.Ns for exp in out_is], [exp.perf.ct.value for exp in out_is],  markersize=9, markershape=:rect, label="IS")
    plot!(p_tradeoff, [exp.perf.ct.value for exp in out_is], [exp.perf.err for exp in out_is], markersize=9, markershape=:rect, label="IS")
    for l in 1:L
        plot!(p_errs[l], [exp.Ns for exp in out_is], [exp.perf.errs[l] for exp in out_is], markersize=9, markershape=:rect,  label="x$l - IS")
    end

    # ----- IMIS
    ds = _dict_shapes(out_imis)
    for (key, value) in pairs(_by_N0_B(out_imis))
        N0, B, ns = key.first, key.second, [Is.Ns(exp.params) for exp in value]
        plot!(p_err, ns, [exp.perf.err for exp in value],
            markershape=ds[B], markersize=7, label="IMIS - N0 = $N0 - B = $B", xlabel="Ns")
        plot!(p_times, ns, [exp.perf.ct.value for exp in value],
            markershape=ds[B], markersize=7, label="IMIS - N0 = $N0 - B = $B", xlabel="Ns")
        plot!(p_tradeoff, [exp.perf.ct.value for exp in value], [exp.perf.err for exp in value],
            markershape=ds[B], markersize=7, label="IMIS - N0 = $N0 - B = $B")
        for l in 1:L
            plot!(p_errs[l], ns, [exp.perf.errs[l] for exp in value],
            markershape=ds[B], markersize=7, label="x$l - IMIS - N0 = $N0 - B = $B", xlabel="Ns")
        end
    end


    # dummy serie to plot legend
    plot!(p_legend, [0], [0], label="Ns : total number of samples")
    plot!(p_legend, [0], [0], label="N0 : number of samples of the initial sampling")
    plot!(p_legend, [0], [0], label="B : number of samples of each iteration")


    p = plot(p_err, p_times, p_tradeoff, p_legend, p_errs..., size=(1500, 1000))
    if savepath !== nothing
        png(p, savepath)
        @debug "Plot saved in $savepath"
    end
    p
    end



function plot_time_imis(cts::Vector{Millisecond}, ct_is::Millisecond)
    p1 = scatter(Bs, cts ./ ct_is, label="Speed ratio IMIS / IS - Ns = $Ns", xlabel=L"$B$", minorgrid=true)
    p2 = scatter(Bs, Kmaxs, label=L"Number of iterations $K$", xlabel=L"$B$", minorgrid=true)
    # scatter!(Bs, log.(Ns .- Bs .* Kmaxs), label=L"Number of initial sampling $N_0$")
    plot(p1, p2)
end
# plot_time_imis(cts, ct_is)

function plot_error_imis_is(errs::Vector)
    err_is, err_imis = [p.first for p in errs], [p.second for p in errs]
    p = plot(Bs, err_is, label="Average error - IS", xlabel="Size of chunks \$B\$")
    plot!(p, Bs, err_imis, label="Average error - IMIS")
    p
    end
# plot_error_imis_is(errs)

res = run_perf(settings, true, false, false)
plot_perf(res...)
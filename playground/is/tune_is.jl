using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Models
using FastBayesianInversion.Store
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Predictions
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.Mcmc

using Plots
using LinearAlgebra

include("../hapke_models/models.jl")
include("../env.jl")

Plots.pyplot()
Log.init()

st = Store.Storage(ROOT_PATH_STORE)

std_train, std_ratio = 0.001, 20

context = HapkeNontronite()
_datas = load_observations(context)
Yobs, wavelengths  = _datas.Yobs, _datas.wavelengths
Yobs_std = Yobs ./ std_ratio

mcmc_params = Mcmc.ParamsMH(std_train)
invs_mcmc, _ = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params)
Xmcmc = Pipeline.aggregate(invs_mcmc)
Xmcmc, _ = Models.from_physical(context, Xmcmc, similar(Xmcmc))

n = 33

gllim_params = Gllim.Params(
    Gllim.MultiInitParams(
        10,
        Gmms.EmParams(50, 30, 15, 1e-12)
    ),
    Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
        Gllim.DefaultStoppingParams(200, 1e-5),
        1e-12
    )
)

function inverse_one(model::Is.IsModel, Ns, B, K)
    X_is, W_is = zeros(Models.get_L(context), Ns), zeros(Ns)
    Is.importance_sampling!(model, X_is, W_is)

    imis_params = Is.ImisParams(Ns - B * K, B, K)
    X_imis, W_imis = Is.imis_ext(imis_params, model)

    L, Ns = size(X_imis)
    mean_is, mean_imis = zeros(L), zeros(L)
    for j in 1:Ns
        mean_is .+= W_is[j] * X_is[:, j] # sum(weights) = 1
        mean_imis .+= W_imis[j] * X_imis[:, j] # sum(weights) = 1
    end

    mean_is, X_is, W_is, mean_imis, X_imis, W_imis
end

function _plot_samples(Xs::Matrix, weigths::Vector, mean_is::Vector)
    @assert all(isfinite.(Xs)) 
    @assert all(isfinite.(weigths)) 
    alpha = [min(1, w * 10) for w in weigths] # to be able to see points
    p1 = scatter(mean_is[3:3], mean_is[4:4], label="IS Mean")
    scatter!(p1, Xs[3,:], Xs[4,:], xlabel="b", ylabel="c", label="IS samples",
    xlims=(0, 1), ylims=(0, 1),
    alpha=alpha,
    markersize=0, markersize=3)
    scatter!(p1, Xmcmc[3, n:n], Xmcmc[4, n:n], label="MCMC Ref")

    p2 = scatter(mean_is[1:1], mean_is[2:2], label="IS Mean")
    scatter!(p2, Xs[1,:], Xs[2,:], xlabel="omega", ylabel="theta", label="IS samples",
    xlims=(0, 1), ylims=(0, 1),
    alpha=alpha,
    markersize=0, markersize=3)
    scatter!(p2, Xmcmc[1, n:n], Xmcmc[2, n:n], label="MCMC Ref")

    p1, p2
end

function _plot_means(means::Matrix, label::String, mean::Vector, centroids::Matrix)
    xs, ys = range(0, 1, length=200), range(0, 1, length=200)
    p1 = scatter(means[3,:], means[4,:], xlabel="b", ylabel="c", label=label,
    xlims=(0, 1), ylims=(0, 1),
    markersize=0, markersize=3, colorbar=false)
    scatter!(p1, mean[3:3], mean[4:4], label="GLLiM - Mean")
    scatter!(p1, Xmcmc[3, n:n], Xmcmc[4, n:n], label="MCMC Ref")
    scatter!(p1, centroids[3, :], centroids[4, :], markershape=:rect, label="Gllim centroids")
    # contour!(p1, xs, ys, density1, levels=50, markeralpha=0.2)

    p2 = scatter(means[1,:], means[2,:], xlabel="omega", ylabel="theta", label=label,
    xlims=(0, 1), ylims=(0, 1),
    markersize=0, markersize=3, colorbar=false)
    scatter!(p2, mean[1:1], mean[2:2], label="GLLiM - Mean")
    scatter!(p2, Xmcmc[1, n:n], Xmcmc[2, n:n], label="MCMC Ref")
    scatter!(p2, centroids[1, :], centroids[2, :], markershape=:rect, label="Gllim centroids")
    # contour!(p2, xs, ys, density2, levels=50, markeralpha=0.2)

    p1, p2
end

function repeat_inversion(run_gllim::Bool, nb_try, Ns)
    if run_gllim
        X, Y = Models.data_training(context, 50_000, std_train)
        trained_gllim, _ = Gllim.train(gllim_params, X, Y)
        Store.save(st, trained_gllim, Store.FO_GLLIM, context, std_train, gllim_params, :solo)
    end
    trained_gllim::Gllim.GLLiM = Store.load(st, Store.FO_GLLIM, context, std_train, gllim_params, :solo)

    yobs, obs_std = Yobs[:, n], Yobs_std[:, n]

    mean, _, model, _, _, merged_gmm = Shared.setup_inversion(context,
        yobs, obs_std, trained_gllim, std_train, Predictions.MergingOptions(2))
    # density1 = (x, y) -> Probas.gmm_density([x,y], Probas.Gmm{Float64}(weights, centroids[3:4, :], [cholesky(C[3:4, 3:4]).L for C in covs]), zeros(2))
    # density2 = (x, y) -> Probas.gmm_density([x,y], Probas.Gmm{Float64}(weights, centroids[1:2, :], [cholesky(C[1:2, 1:2]).L for C in covs]), zeros(2))

    means_is = zeros(Models.get_L(context), nb_try)
    means_imis = zeros(Models.get_L(context), nb_try)
    Xs_is, Ws_is = Vector(undef, nb_try), Vector(undef, nb_try)
    Xs_imis, Ws_imis = Vector(undef, nb_try), Vector(undef, nb_try)
    Threads.@threads for i in 1:nb_try
    # for i in 1:nb_try
        mean_is, Xs_is[i], Ws_is[i], mean_imis, Xs_imis[i], Ws_imis[i] = inverse_one(model, Ns, 10, 1)
        means_is[:, i] = mean_is
        means_imis[:, i] = mean_imis
    end

    p1, p2 = _plot_means(means_is, "IS means - Ns = $Ns", mean, merged_gmm.means)
    p3, p4 = _plot_means(means_imis, "IMIS means", mean, merged_gmm.means)

    subplots = [p1, p2, p3, p4]
    # for i in 1:nb_try
    #     sp1, sp2 = _plot_samples(Xs_is[i], Ws_is[i], means_is[:, i])
    #     push!(subplots, sp1, sp2)
    # end
    plot(subplots..., size=(4 * 200, 200 * length(subplots)), layout=(length(subplots) ÷ 2, 2))
end



repeat_inversion(false, 50, 50)
using LaTeXStrings
using Dates
using Plots
using Profile
using LinearAlgebra

using FastBayesianInversion.Probas
using FastBayesianInversion.Log
using FastBayesianInversion.Gmms
using FastBayesianInversion.Gllim
using FastBayesianInversion.Predictions
using FastBayesianInversion.Store
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.TuneImis
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Shared

include("../hapke_models/models.jl")
include("../env.jl")

Plots.pyplot()
Log.init()

st = Store.Storage(ROOT_PATH_STORE)

obs_index = 34

gllim_params = Gllim.Params(
    Gllim.MultiInitParams(10,  Gmms.EmParams(100, 30, 20, 1e-12)), 
    Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(300, 1e-5), 1e-12)
)
training_params = Shared.TrainingParams(
    100_000, 
    0.001, 
    gllim_params 
)
merging_params = Predictions.MergingOptions(2)

function setup(run_gllim::Bool)
    std_train = 0.001
    
    context = HapkeHowardite()
    _datas = load_observations(context)
    Yobs, wavelengths  = _datas.Yobs, _datas.wavelengths
    Yobs_std = _datas.Yobs_std

    trained_gllim, _, _ = Shared.generate_train_gllim(context, training_params, st, run_gllim) 

    # for each observations, setup the inversion
    @info "Inversion setup..."
    Log.indent()
 
    yobs, obs_std = Yobs[:,obs_index], Yobs_std[:,obs_index]

    _, _, model, _, _, _ = Shared.setup_inversion(context,
            yobs, obs_std, trained_gllim, std_train, merging_params)

    Log.deindent()
    model
end


s = setup(false)
serialize("test/imis_hapke.ser",s)
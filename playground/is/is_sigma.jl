using FastBayesianInversion.Log
using FastBayesianInversion.Is
using FastBayesianInversion.Probas

using LinearAlgebra
using Statistics
using Plots
using LaTeXStrings

Log.init()
Plots.pyplot()

""" Simple model, for which the mean is known """
struct GaussianModel <: Is.IsModel
    chol_covs_target::CholCovs{Float64}
    GaussianModel(std::Float64) = new([LowerTriangular(hcat(std)), LowerTriangular(hcat(std))])
end

Is.get_L(::GaussianModel) = 1
function Is.log_p(m::GaussianModel, x::VecOrSub, ::Int)
    # Probas.log_gaussian_density(x, [0.5], UniformScaling(0.05), 1)
    log(Probas.gmm_density(x, [0.3 0.7], m.chol_covs_target))
end
Is.get_proposition(::GaussianModel) = Is.GaussianMixtureProposition{Float64}([0.7, 0.3], [0.4 0.8], [LowerTriangular(hcat(0.1)), LowerTriangular(hcat(0.5))])

struct Perf
    dispertion::Number
    err::Number # averaged
end

function run_is(stds::Vector{Float64}, Nss::Vector{Int}, Ntry=10)
    ref = [0.5]
    out::Vector{Vector{Perf}} = []
    for std in stds
        @debug "std = $std"
        model = GaussianModel(std)
        L = Is.get_L(model)
        proposition = Is.get_proposition(model)
        tmp::Vector{Perf} = []
        Log.indent()
        for Ns in Nss
            @debug "Ns = $Ns"
            means = zeros(L, Ntry)
            Threads.@threads for ntry in 1:Ntry
                pred, _ = Is.importance_sampling(model, Is.IsParams(Ns),Is.NonUniformityParams(1))
                means[:, ntry] .= pred.mean
            end
            # compute the dispertion (trace(Sigma))
            m = sum(means; dims=2) / Ntry
            dispertion = sum(norm(means[:,i] .- m)^2 for i in 1:Ntry) / Ntry
            err = Statistics.mean(norm(means[:,i] .- ref)  for i in 1:Ntry)
            push!(tmp, Perf(dispertion, err))
        end
        Log.deindent()
        push!(out, tmp)
    end
    out
end


function plot_is(out::Vector{Vector{Perf}}, stds, Nss)
    p_err = plot(xlabel= "Ns", ylabel=L"Averaged error $|| x_{pred} - x_{ref}||$", yscale=:log10)
    p_dispertion = plot(xlabel= "Ns", ylabel=L"Dispertion $tr(\Sigma)$", yscale=:log10)
    for (std, ve) in zip(stds, out)
        plot!(p_err, Nss, [p.err for p in ve], label = "std = $std", markersize=4, markershape = :rect)
        plot!(p_dispertion, Nss, [p.dispertion for p in ve], label = "std = $std", markersize=4, markershape = :rect)
    end
    plot(p_err, p_dispertion)
end

stds, Nss =[1, 0.1, 0.01], 3 .^ (3:12)
out = run_is(stds,Nss)
plot_is(out, stds, Nss)
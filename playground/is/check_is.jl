using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Store
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Is
using FastBayesianInversion.Predictions
using FastBayesianInversion.Models
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.Mcmc

using Plots
using Statistics
using LinearAlgebra

include("../hapke_models/models.jl")
include("../env.jl")

Log.init()
Plots.pyplot()

st = Store.Storage(ROOT_PATH_STORE)

struct Res
    gllim::Vector
    mcmc::Vector
    is::Vector{Is.IsInversion}
end
is_errs(res::Res) = [norm(res.mcmc .- v.mean) for v in res.is]
gllim_err(res::Res) = norm(res.mcmc .- res.gllim)


function load_observations(m::Models.InjectifHard)
    Xobs, Yobs = Models.data_observations(m, 100)
    Data2D(Yobs, similar(Yobs), 1:100)
end

function inversions(context::Models.Model, run_gllim::Bool, run_mcmc::Bool, run_is::Bool, N_obs::Int, N_iss::Vector{Int}, std_ratio::Int=20)
    std_train = 0.001

    _datas = load_observations(context)
    Yobs  = _datas.Yobs[:, 1:N_obs]
    Yobs_std = Yobs ./ std_ratio

    gllim_params = Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(50, 30, 15, 1e-12)
        ),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )
    merging_params = Predictions.MergingOptions(10)

    if run_gllim
        X, Y = Models.data_training(context, 50_000, std_train)
        trained_gllim, _ = Gllim.train(gllim_params, X, Y)
        Store.save(st, trained_gllim, Store.FO_GLLIM, context, std_train, gllim_params, :solo)
    end
    trained_gllim = Store.load(st, Store.FO_GLLIM, context, std_train, gllim_params, :solo)

    mcmc_params, nb_try = Mcmc.ParamsMH(std_train), 5

    # for each observations, setup the inversion
    @info "Inversion..."
    Log.indent()
    ress::Vector{Res} = []
    for n in 1:N_obs
        @debug "index $n..."
        Log.indent()
        yobs, obs_std = Yobs[:, n], Yobs_std[:, n]

        if run_mcmc
            out, _ = Mcmc.complete_inversion(context, repeat(yobs, 1, nb_try), repeat(obs_std, 1, nb_try), mcmc_params)
            ref = sum(o.mean for o in out) ./ nb_try
            ref, _ = Models.from_physical(context, ref, ref) # complete inversion are in physical space
            Store.save(st, ref, Store.FO_PREDICTIONS, context, yobs, obs_std, mcmc_params, nb_try)
        end
        ref = Store.load(st, Store.FO_PREDICTIONS, context, yobs, obs_std, mcmc_params, nb_try)

        
        if run_is
            mean, _, model, _, _, merged_gmm = Shared.setup_inversion(context,
            yobs, obs_std, trained_gllim, std_train, merging_params)
            @debug "Computing IS..."
            iss = Vector{Is.IsInversion}(undef, length(N_iss))
            Threads.@threads for i in 1:length(N_iss)
                N_is = N_iss[i]
                X_is, W_is = zeros(Is.get_L(model), N_is), zeros(N_is)
                Is.importance_sampling!(model, X_is, W_is)
                iss[i], _ =  Is.exploit_samples(X_is, W_is, Is.NonUniformityParams(1))
            end
            Store.save(st, (mean, iss), Store.FO_PREDICTIONS, context, yobs, obs_std, N_iss, :is)
        end
        (mean, iss) = Store.load(st, Store.FO_PREDICTIONS, context, yobs, obs_std, N_iss, :is)

        push!(ress, Res(mean, ref, iss))
        Log.deindent()
    end
    Log.deindent()
    ress
end

function plot_inversions(ress::Vector{Res}, N_iss::Vector{Int}, savepath=nothing)
    L = length(ress[1].gllim)
    p_xs = [plot() for _ in 1:L]

    # average marginal errors over the observations
    # and compute the common scale
    max = -Inf
    for l in 1:L
        gllim = Statistics.mean(abs(res.gllim[l] - res.mcmc[l]) for res in ress)
        iss = [Statistics.mean(abs(res.is[i].mean[l] - res.mcmc[l]) for res in ress) for i in 1:length(N_iss)]
        hline!(p_xs[l], [gllim], label="\$x_$l\$ - GLLiM")
        scatter!(p_xs[l], N_iss, iss, label="\$x_$l\$ - IS")
        max = maximum([max, gllim, maximum(iss)])
    end
    for l in 1:L
        ylims!(p_xs[l], 0, max * 1.1)
    end

    # average errors over the observations
    gllim = Statistics.mean(gllim_err(res) for res in ress)
    iss = [Statistics.mean(is_errs(res)[i] for res in ress) for i in 1:length(N_iss)]
    p_errs = plot()
    hline!(p_errs, [gllim], label="GLLiM")
    scatter!(p_errs, N_iss, iss, label="Is")

    # average diagnostic over the observations
    ess = [Statistics.mean(res.is[i].diagnostics.ess for res in ress) for i in 1:length(N_iss)]
    p_ess = scatter(N_iss, ess, label="Ess")

    p = plot(p_errs, p_ess, p_xs..., dpi=200, size=(1800, 1200))
    if savepath !== nothing
        png(p, savepath)
        @debug "Plotted in $savepath"
    end
end

context = HapkeNontronite()
# context = Models.InjectifHard()
N_iss, std_ratio  = (10:60:1100).^2, 200
ress = inversions(context, false, false, false,  5, N_iss, std_ratio)
plot_inversions(ress, N_iss, Store.get_path(st, Store.FO_GRAPHS, context, N_iss, std_ratio; label="check_is"))
using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Predictions
using FastBayesianInversion.Models
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Pipeline.Mcmc
using FastBayesianInversion.Comptimes
using FastBayesianInversion.Schemes
using FastBayesianInversion.Shared
using FastBayesianInversion.Pipeline.TuneImis

using LaTeXStrings
using Plots 

include("../plotting/plots.jl")
include("../hapke_models/models.jl")
include("../env.jl")

Plots.pyplot()
Log.init()
st = Store.Storage(ROOT_PATH_STORE)

# TODO: fix IS
function imis_centroids(run, params::Pipeline.ProcedureParams, obs_std_ratio::Number)
    context = Models.DoubleHard()
    Nobs = 100

    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio


    preds_vec, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
    Pipeline.regularize_inversions!(preds_vec, [ Schemes.GllimMergedCentroid(""), Schemes.IsCentroid(""), Schemes.ImisCentroid("")])
    ip = params.inv_params.imis_params

    # Plotting
    configs = [
        # Schemes.GllimMean => Pipeline.SerieConfig(N.GllimMean, :basic, "red"; alpha = 0.5),
        # Schemes.IsMean => Pipeline.SerieConfig(N.IsMean, :dashed, "red"; alpha = 0.7),
        Schemes.GllimMergedCentroid(1) => Pipeline.SerieConfig(L"x_{centroid,1}", :circle, "blue"),
        Schemes.GllimMergedCentroid(2) => Pipeline.SerieConfig(L"x_{centroid,2}", :circle, "blue"),
        Schemes.IsCentroid(1) => Pipeline.SerieConfig(L"x_{Is-centroid,1}" * " " * latexstring("(N_{IS} = $(params.inv_params.N_is))"), :circle, "orange"),
        Schemes.IsCentroid(2) => Pipeline.SerieConfig(L"x_{Is-centroid,2}", :circle, "orange"),
        Schemes.ImisCentroid(1) => Pipeline.SerieConfig(L"x_{Imis-centroid,1}" * " " * latexstring("(N_0 = $(ip.N0), B = $(ip.B), J = $(ip.J))"), :circle, "purple"),
        Schemes.ImisCentroid(2) => Pipeline.SerieConfig(L"x_{Imis-centroid,2}", :circle, "purple"),
    ]
    preds = Pipeline.format_inversions(preds_vec, configs)

    preds = [preds...,
        Pipeline.Prediction(Xobs; config=Pipeline.SerieConfig(L"x_{obs,1}", :basic, "green"; alpha=0.3)),
        Pipeline.Prediction(Xobs_alt; config=Pipeline.SerieConfig(L"x_{obs,2}", :basic, "green"; alpha=0.3)),
    ]

    path = Store.get_path(st, Store.FO_GRAPHS, context, params; label="double_solutions")
    plot_components(preds;  savepath=path, varnames=Models.variables(context), xlabel=L"n", average_y_err=true)
end

gllim_params = Gllim.Params(
    Gllim.MultiInitParams(10,  Gmms.EmParams(50, 30, 20, 1e-12)),
    Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(300, 1e-5), 1e-12)
)
training_params = Shared.TrainingParams(
    50_000,
    0.01,
    gllim_params
)
inversions_params = Pipeline.InversionParams(
    0.01,
    Predictions.MergingOptions(2),
    Is.ImisParams(1000, 100, 20), # will be tuned later
    false,
    false,
    Is.NonUniformityParams(5),
    0
)
params = Pipeline.ProcedureParams(training_params, inversions_params)
obs_std_ratio = 400
imis_centroids(0, params,  obs_std_ratio)
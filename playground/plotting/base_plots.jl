using Plots
using LinearAlgebra
using LaTeXStrings
import Colors

using FastBayesianInversion.Noise 
using FastBayesianInversion.Models 
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Schemes
using FastBayesianInversion.Shared

mutable struct _config 
    LEGEND_FONT_SIZE::Int
    LEGEND_BACKGROUND::Colors.RGBA
    DPI::Int
end
const Config = _config(13, Colors.RGBA(0.9, 0.9, 0.9, 0.3), 200) 

"""
 Returns last colors 
"""
last_color(p::Plots.Plot) = p.series_list[end].plotattributes[:linecolor]
all_colors(p::Plots.Plot) = [ l.plotattributes[:linecolor] for l in p.series_list ]

"""
 Plot a fill zone around mean 
"""
function _plot_cov!(p::Plots.Plot, mean::Vector{T}, cov::Vector{T}, x_labels::Vector, label) where T <: Number
    plot!(p, x_labels, mean + sqrt.(cov), fillrange=mean - sqrt.(cov), fillalpha=0.10, label=label * " (Std)",
    linealpha=0, fillcolor=last_color(p))
end



# ---------------------------------------------------------------------------------------------------------- #
# ----------------------------------------------- x Predictions -------------------------------------------- # 
# ---------------------------------------------------------------------------------------------------------- #

# avoid over-precision which flood the graph
function axe_formatter(x) 
    if x >= 0.001 
        return string(round(x, digits=3))
    elseif x >= 0.0001
        return string(round(x, digits=4))
    else
        return string(round(x, digits=5))
    end
end

Color = Union{String,Symbol,Colors.RGBA}
SerieColor = Union{Color,Vector{T}} where T <: Color

""" 
Plotting options for one serie of results 
"""
struct PlotOptions
    label::String
    color::SerieColor
    alpha::Number
    """ :none means scatter plot """
    linestyle::Symbol
    hide_std::Bool
    hide_Xerr::Bool 
    markershape::Symbol
    markersize::Number
end
function PlotOptions(label::String,color::SerieColor,alpha::Number, linestyle::Symbol;
    markershape::Symbol=:rect, markersize::Number=5,
    hide_std::Bool=false,  hide_Xerr::Bool=true)
    PlotOptions(label, color, alpha,  linestyle, hide_std, hide_Xerr, markershape, markersize)
end


function _plot!(subplot, x::Vector, x_ticks::Vector, options::PlotOptions)
    kwargs = Dict{Symbol,Any}(
        :label => options.label, 
        :color => options.color,
        :linealpha => options.alpha,
        :markeralpha => options.alpha,
        :markershape => options.markershape, 
        :markersize => options.markersize,
        :markerstrokewidth => 0.1,
        :yformatter => axe_formatter, 
        :linewidth => 2,
        :markerstrokealpha => 0.4
    )
    if options.markersize == 0 
        kwargs[:markeralpha] = 0 
    end
    if options.linestyle == :none 
        scatter!(subplot, x_ticks, x; kwargs...)
    else 
        kwargs[:linestyle] = options.linestyle
        plot!(subplot, x_ticks, x; kwargs...)
    end
end

# # diagnostics plot
# subp_diagnostics = plot(xlabel=xlabel, ylabel="diagnostic",
#                  yscale=:log10,
#                 title="$(main_title) - Diagnostics",
#                 background_color_legend=Config.LEGEND_BACKGROUND,
#                 legendfontsize=Config.LEGEND_FONT_SIZE,
#                 )
# linestyles = [:solid, :dash, :dot, :dashdot, :dashdotdot]
# for pred in predictions
#     for ((label, diagnos), ls) in zip(pairs(pred.diagnostics), Iterators.cycle(linestyles))
#         plot!(subp_diagnostics, x_ticks, diagnos; color=config.color, linestyle=ls,
#                 label="$label - $(config.label)")
#     end
# end

# subp_Xerr = plot(xlabel=xlabel, ylabel=raw"$|| x_{obs} - x_{pred} ||$", yscale=:log10,
# title="Error", background_color_legend=Config.LEGEND_BACKGROUND,
# legendfontsize=Config.LEGEND_FONT_SIZE,
# )
# for pred in predictions
#     if !config.hide_Xerr
#         _plot!(subp_Xerr, pred.Xerr, x_ticks, config)
#     end
# end

""" Returns an invisible plot to host the legend """
function legend_subplot() 
    plot(; framestyle=:none, legend=:right, background_color_legend=Config.LEGEND_BACKGROUND,  legendfontsize=Config.LEGEND_FONT_SIZE)
end


"""
 If `hide_diagnostics`, the legend is plotted in place of diagnostics 
"""
function plot_components(predictions::Vector{Pair{Shared.MethodResult,PlotOptions}};  
    savepath=nothing, x_ticks=nothing, xlabel="observation index", varnames=nothing, main_title="", y_lims=nothing, average_y_err=false)
    L, N = size(predictions[1].first.Xmean)
    x_ticks = (x_ticks === nothing) ? collect(1:N) : x_ticks
    varnames = (varnames === nothing) ? ["x$i" for i in 1:L] : varnames

    # outer legend plot : plot without data
    subp_legend = legend_subplot()
    # reconstruction error
    reconstruction_legend = raw"$\frac{|| y_{obs} - F(x) ||}{|| y_{obs} ||} $"
    subpY_relative_err = plot(xlabel=xlabel, ylabel=reconstruction_legend,
                        yscale=:log10, legend=false,
                        title="Relative reconstruction error")
    for (pred,  config) in predictions
        _plot!(subpY_relative_err, pred.Yerr, x_ticks, config)
    end
    if average_y_err
        for (pred, config) in predictions
            avg = sum(pred.Yerr) / N
            hline!(subpY_relative_err, [avg]; color=config.color, linestyle=:dot)
        end
        plot!(subp_legend, [], [], label="average reconstruction error", linestyle=:dot)
    end
  
    subplots_X = []
    for l in 1:L 
        ylim = y_lims === nothing ? :auto : y_lims[l]
        subp = plot(xlabel=xlabel, legend=false, ylabel=varnames[l],
                    title=(l == 1) ? "Prediction by components" : "", color_palette=:inferno,
                    ylims=ylim, background_color_legend=Config.LEGEND_BACKGROUND,
                    legendfontsize=Config.LEGEND_FONT_SIZE,
                    )
        for (pred, config) in predictions
            Xmean = pred.Xmean
            _plot!(subp, Xmean[l,:], x_ticks, config)
            if l == 1  # we add the legend on a blank plot
                _plot!(subp_legend, [], [], config)
            end
            if !config.hide_std
                _plot_cov!(subp, Xmean[l,:], pred.Xcovs[l,:], x_ticks, config.label)
                if l == 1  # we add the legend on a blank plot
                    _plot_cov!(subp_legend, zeros(0), zeros(0), zeros(0), config.label)
                end
            end
        end
        
        push!(subplots_X, subp)
    end

    # we eventually aggregate the subplots
    # for L variable, with legend and Y err, we want the following layout 
    #   | X .. X | | Y | 
    #   | X .. X | | L |

    subplots = []

    first_row_X = length(subplots_X) % 2 == 0  ? (length(subplots_X) ÷ 2) : (length(subplots_X) ÷ 2 + 1)
    push!(subplots, subplots_X[1:first_row_X]...)

    if length(subpY_relative_err.series_list) > 0
        push!(subplots, subpY_relative_err)
    end

    if length(subplots_X) >= first_row_X + 1
        push!(subplots, subplots_X[first_row_X + 1:end]...)  
    end

    push!(subplots, subp_legend)

    if length(subplots) % 2 == 1
        push!(subplots, plot(; framestyle=:none))
    end
    mainplot = plot(subplots..., layout=(2, Int(length(subplots) / 2)), 
        dpi=Config.DPI,  size=(length(subplots) * 350, 1000))  
    if savepath !== nothing
        Plots.png(mainplot, savepath)
        @debug "Saved in $(savepath)"
    end
    mainplot
end

""" 2D plots, components by components """
function plot_variabilities(vars::Vector{Pipeline.Variability}; savepath=nothing, x_ticks=nothing, xlabel="observation index", varnames=nothing)
    non_unif_mat = hcat([v.non_uniformity for v in vars]...) # L x N
    disp_mat = hcat([v.dispersion_cmp for v in vars]...) # L x N
    L, N = size(non_unif_mat)
    x_ticks = (x_ticks === nothing) ? collect(1:N) : x_ticks
    varnames = (varnames === nothing) ? ["x$i" for i in 1:L] : varnames

    p_dispersion = plot(x_ticks, Matrix(disp_mat'), label=hcat(varnames...), 
    yscale=:log10, ylabel="dispersion", xlabel=xlabel, markershape=:rect, markersize=1, markeralpha=0.5)
    plot!(p_dispersion, x_ticks, [ var.dispersion for var in vars]; label="total", markershape=:rect, markersize=1, markeralpha=0.5)
    
    p_non_uniformity = plot(x_ticks, Matrix(non_unif_mat');
        xlabel=xlabel,ylabel="non uniformity",labels=hcat(varnames...), markershape=:rect, markersize=1, markeralpha=0.5, ylim=(0, 1.5))

    mainplot = plot(p_dispersion, p_non_uniformity,
        dpi=Config.DPI, size=(1000, 600))  
    if savepath !== nothing
        Plots.png(mainplot, savepath)
        @debug "Saved in $(savepath)"
    end
    mainplot
end

# ---------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Noise estimation ------------------------------------------ #
# ---------------------------------------------------------------------------------------------------------- #

__cov_to_vec(m::Union{UniformScaling,Number}) = m * [1]
__cov_to_vec(m) = diag(m)

"""
Plot noise estimation iterations, by comparing with the ground truth
"""
function plot_noise_estimation(history::Noise.History{T},
                                synthetic::Noise.Bias,  estimator::Noise.Bias; savepath=nothing, colors::Pair{String,String}="red" => "blue") where {T}

    true_std = __cov_to_vec(synthetic.sigma)                            
    errs = [norm(sqrt.(__cov_to_vec(t[1].sigma)) .- true_std) for t in history]
    Niter = length(errs)
    p2 = plot(errs, xlabel="Iteration \$r\$", 
        label=L"|| \sigma^r_D - \sigma_D ||_{2}",
        legendfontsize=Config.LEGEND_FONT_SIZE,
        color=colors.second,
        # ylims = (1e-4, 1)
        ylabel="standard deviation error", 
        yscale=:log10)

    estim_std = sqrt.(__cov_to_vec(estimator.sigma))
    err_estim = norm(estim_std .- true_std)
    plot!(p2, 1:Niter, err_estim .* ones(Niter), linestyle=:dash, 
        color=colors.first,
        label=L"|| \sigma^{est}_D - \sigma_D ||_{2}")

    p = plot(p2, dpi=Config.DPI)
    if savepath !== nothing
        Plots.png(p, savepath)
        @info "Figure wrote in $(savepath)"
    end
    p
end



# ---------------------------------------------------------------------------------------------------------- #
# -------------------------------------------- RTLS representation ----------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

function plot_rtls(context::Models.Hapke, rtlss::Vector{Matrix{T}}, labels=nothing; add_title="", savepath=nothing) where {T}
    I = length(rtlss)
    labels = (labels === nothing) ? fill("Observations", I) : labels
    p = plot(title=Models.label(context) * " - RTLS space - " * add_title, xlabel="\$k_L\$", ylabel="\$k_G\$", zlabel="\$k_V\$")
    for i in 1:I
        rtls = rtlss[i]
        scatter!(p, rtls[1,:], rtls[2,:], rtls[3,:], markershape=:auto, label=labels[i])
    end
    if savepath !== nothing
        Plots.png(p, savepath)
        @info "Figure wrote in $(savepath)"
    end
    p
end
plot_rtls(c, rtls::Matrix) = plot_rtls(c, [rtls])


# ---------------------------------------------------------------------------------------------------------- #
# --------------------------------------------- Density ---------------------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

function plot_density_2D(f::Function, L::Int, xrefs::Tuple{Vector,String} ...; xnames=nothing, levels=50, savepath=nothing, resolution=100, xlims=nothing) 
    xnames = xnames === nothing ? ["x$i" for i in 1:L] : xnames
        xlims = xlims === nothing ? [(0, 1) for i in 1:L] : xlims
    nbPlots::Int, current = L * (L - 1) / 2, 1
    subplots = []
    for i = 1:L 
        xs = range(xlims[i][1], length=resolution, stop=xlims[i][2])
        for j = (i + 1):L
            ys = range(xlims[j][1], length=resolution, stop=xlims[j][2])
            @debug "Plot $(current) / $(nbPlots)..."
            current += 1
            sb = plot(xlabel=xnames[i], ylabel=xnames[j], title="density $(xnames[i]), $(xnames[j])", 
                legend=i == 1 && j == 2, colorbar=false, margin=5mm, tickfontsize=5, yformatter=axe_formatter)

            partialF = (x, y) -> f(x, y, i, j)
            contour!(sb, xs, ys, partialF, levels=levels)
            for (xref, label) in xrefs
                scatter!(sb, [xref[i]], [xref[j]],  label=label, markersize=3, markerstrokewidth=0.2)
    end
            push!(subplots, sb)
        end
    end
    p = plot(subplots..., dpi=200,  plot_title="density")
    if savepath !== nothing
        Plots.png(p, savepath)
        @debug "Saved in $(savepath)"
    end
    p
end

function plot_density_1D(f::Function, L::Int, xrefs::Tuple{Vector,String} ...; xnames=nothing, savepath=nothing, resolution=100, subplot_size=(600, 400), 
        xlims=nothing, label="density") 
    xnames = xnames === nothing ? ["x$i" for i in 1:L] : xnames
    xlims = xlims === nothing ? [(0, 1) for i in 1:L] : xlims
    nbPlots::Int, current = L, 1
    subplots = []
    for i = 1:L 
        xs = range(xlims[i][1], length=resolution, stop=xlims[i][2])
        x_ticks = round.(range(xlims[i][1], length=10, stop=xlims[i][2]) * 100) ./ 100
        @debug "Plot $(current) / $(nbPlots)..."
        current += 1
        sb = plot(xlabel=xnames[i], ylabel=label, title="density $(xnames[i])", 
                legend=i == 1, colorbar=false, margin=5mm, tickfontsize=5, xlims=xlims[i], yticks=[], xticks=x_ticks)
        partialF = (x) -> f(x, i)
    plot!(sb, xs, partialF.(xs))
        for (xref, label) in xrefs  
            plot!(sb, [xref[i]], seriestype=:vline, label=label, markersize=3, markerstrokewidth=0.2)
        end
        push!(subplots, sb)
    end
    p = plot(subplots..., layout=(1, L), dpi=100,  plot_title="density", size=(subplot_size[1] * L, subplot_size[2]))
    if savepath !== nothing
        Plots.png(p, savepath)
        @debug "Saved in $(savepath)"
    end
    p
end

# ---------------------------------------------------------------------------------------------------------- #
# --------------------------------------------- Map -------------------------------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

# function latlong_to_xy(latlong::Matrix)
#     _, N = size(latlong)
#     origin = LLA(latlong[1,1], latlong[2,1], 0)
#     trans = ENUfromLLA(origin, wgs84)
#     xs, ys = [], []
#     for i in 1:N
#         enu = trans(LLA(latlong[1,i], latlong[2,i], 0))
#         push!(xs, enu.n)
#         push!(ys, enu.e)
#     end
#     xs, ys
# end

# function plot_map(values::Vector{Tuple{Vector{T},String}}, latlong::Matrix; markersize=8.5, figsize=(1000, 1000), color_palette=:rainbow) where {T}
#     xs, ys = latlong_to_xy(latlong)
#     min = minimum(minimum(t[1]) for t in values)
#     max = maximum(maximum(t[1]) for t in values)
#     subp = []
#     for (vals, label)  in values
#         push!(subp, scatter(xs, ys, zcolor=vals, title=label, colorbar=:best,
#         markersize=markersize, markercolor=color_palette, clims=(min, max), label=label,
#         legendfontsize=Config.LEGEND_FONT_SIZE, legend=:left,
#         xlims=(minimum(xs), maximum(xs) * (1 + 0.15)) # colorbar bug
#         ))
#     end
#     plot(subp..., dpi=Config.DPI, size=figsize)
# end

# function _normalize(data::Matrix)
#     vmin, vmax = minimum(data), maximum(data)
#     if 0 <= vmin && vmax <= 1 
#         # we dont normalize valid series
#         return data
#     end
#     if vmin == vmax 
#         # degenerated case 
#         return ones(size(data)) .* 0.5 
#     end
#     return (data .- vmin) ./ (vmax - vmin)
# end

# """ Plot a map of RGB colors for each element of `values` """
# function plot_map_color(values::Vector{Tuple{Matrix{T},String}}, latlong::Matrix; markersize=8.5, figsize=(1000, 1000)) where {T}
#     xs, ys = latlong_to_xy(latlong)
#     subp = []
#     for (vals, label)  in values
#         vals = _normalize(vals)
#         N, _ = size(vals)        
#         colors = [RGB(vals[n,1], vals[n,2], vals[n,3]) for n in 1:N]
#         push!(subp, scatter(xs, ys, mc=colors, title=label, markersize=markersize, label=label,
#         legendfontsize=Config.LEGEND_FONT_SIZE ,
#         ))
#     end
#     plot(subp..., dpi=Config.DPI, size=figsize)
# end


# ---------------------------------------------------------------------------------------------------------- #
# ---------------------------------------- Hapke's observables (polar plot) -------------------------------- #
# ---------------------------------------------------------------------------------------------------------- #

"""
Plots geometries in the principal plan, that is with azimut = 0 or 180.
"""
function plot_polar(Yobs::Matrix, geometries::Matrix, labels::Vector, colors::Vector;
        inc=nothing, savepath=nothing, title::Union{Function,Nothing}=nothing, hide_legend=false) 
    D, _ = size(geometries)
    
    # group by incidence
    sorted_indexes = Dict{Tuple{Int,Int},Vector{Int}}() # (azimut, incidence) -> emergences
    for d in 1:D
        is_principal = geometries[d,3] == 0 || geometries[d,3] == 180
        
        match_inc = inc === nothing || geometries[d, 1] == inc
        key = (Int(round(geometries[d, 3])), Int(round(geometries[d, 1])),)
        if is_principal # we can merge 0 and 180 (see below)
            key = (0, key[2])
        end
        if match_inc  
            l = get(sorted_indexes, key, Vector{Int}())
            push!(l, d)
        sorted_indexes[key] = l
    end
    end
    subp = []
    subp_legend = plot(; framestyle=:none)
        first = true 
        # colors must be a (1, _) matrix 
    colors = permutedims(hcat(colors))

    # special case for inc = 60
    mask_60 = [round(geometries[d, 1]) == 60 for d in 1:D]
    mask_others = .~ mask_60
    ymax, ymax_60 = 0, 0
    if sum(mask_60) != 0 
        ymax_60 = maximum(Yobs[mask_60, :])
    end
    if sum(mask_others) != 0
        ymax = maximum(Yobs[mask_others, :])
    end
    
    if title === nothing 
        title = (inc, azi) -> "incidence $inc, azimut $azi"
    end 
    
    for k in sort(collect(keys(sorted_indexes)))
        v = sorted_indexes[k]
        emes = []
        for i in v 
            eme, azi = geometries[i, 2], geometries[i, 3]
            if azi == 180 
                eme =  90 + eme
            else 
                eme = 90 - eme
            end
            push!(emes, eme * π / 180)
        end
        # sort emergences for pretty visualisation
        perm = sortperm(emes)
        azi, inc = k
        inc_rad = (90 - inc) * π / 180
        p = plot(emes[perm], Yobs[v[perm], :], color=colors, proj=:polar, marker=:auto, 
            legend=false, 
            xlims=(0, π), 
            ylims=(0, inc == 60 ? ymax_60 : ymax),
            title=title(inc, azi), 
            label=permutedims(labels),
            legendfontsize=Config.LEGEND_FONT_SIZE,
            alpha=0.5,
            markerstrokewidth=0.2
        )
        plot!(p, [inc_rad, inc_rad], [0.,maximum(Yobs[v, :]) * 1.1], label="incidence")
        push!(subp, p)
        # copy the legend 
        if first 
            plot!(subp_legend, [1], Yobs[1:1, :], color=colors, label=permutedims(labels))
            plot!(subp_legend, [1], [1], label="incidence")
        end
        first = false
end 
    if !hide_legend
        push!(subp, subp_legend)
    end
    p = plot(subp..., dpi=Config.DPI, size=(length(subp) * 250, 800 * length(subp) / 5))
    if savepath !== nothing 
        Plots.png(p, savepath)
    @info "Saved in $savepath"
    end
    p
end

function plot_hockey_stick(contexts::Vector, preds::Vector{Dict{String,Shared.MethodResult}}, methods::Vector{String}, savepath::String)
    # target empirical relation 
    bref = collect(range(0, 1; length=100))
    cref = Models.HapkeFunc.Crelation(bref)

    subplots = [ ]
    for method in methods
        variables = Models.variables(contexts[1])
        subp = plot(; xlabel=variables[3], ylabel=variables[4], ylims=(-0.2, 1.2),  title=method)
        plot!(subp, bref, cref, label="Empirical hockey-stick relation")
        push!(subplots, subp)
    end

    shapes = [ :circle, :rect, :utriangle, :diamond, :hexagon, :cross, :xcross, :dtriangle, :rtriangle, :ltriangle, :pentagon, :heptagon, :octag, :star5]
    for (context, context_preds, shape) in zip(contexts, preds, shapes) 
        for (i, method) in enumerate(methods) # for different methods 
            pred::Shared.MethodResult = context_preds[method]
            _, imax = size(pred.Xmean)
            xerror, yerror = sqrt.(pred.Xcovs[3, :]), sqrt.(pred.Xcovs[4, :])
    # colors = [RGBA(0.1, 1 - i, i, 1) for i in range(0, 1; length=imax)]
            scatter!(subplots[i], pred.Xmean[3,:], pred.Xmean[4,:], label=Models.label(context), markershape=shape, alpha=0.7, markerstrokealpha=0.3, markersize=5,
            xerror=xerror, yerror=yerror, linealpha=0.7)
        end
    end

    p = plot(subplots..., layout=(1, length(subplots)), 
        size=(500 * length(subplots), 600), dpi=Config.DPI)
    png(p, savepath)
    @info "Plotted in $savepath"
end

using FastBayesianInversion.Models
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Shared
using Plots 
using LaTeXStrings
using RCall

include("../k_selection_r.jl")
include("base_plots.jl")

using .KSel

function plot_k_selection(Ks::Vector, res::KSel.KSelection, Ystd::Float64, D::Int; savepath=nothing)
    # different scales 
    p1 = plot(Ks, hcat(res.direct_error, res.sigma_error), 
    labels=["Reconstruction error" L"\overline{\mathrm{Tr}\Sigma}"],
    background_color_legend=Config.LEGEND_BACKGROUND)
    plot!(p1, Ks, ones(length(Ks)) .* Ystd^2 * D, label=L"D \sigma^2")

    p2 = plot(Ks, hcat(
        res.bic,
        #  res.bic_test
    ), 
    labels="BIC", # "BIC - Test dataset"
    background_color_legend=Config.LEGEND_BACKGROUND)

    i_best_1 = best_K_bic(res)
    i_best_2 = best_K_bic_test(res)

    K_best1, K_best2 = Ks[i_best_1], Ks[i_best_2]
    vline!(p2, [K_best1] ; label="\$K_{best, BIC} = $(K_best1)\$")
    # vline!(p2, [K_best2] ; label="\$K_{best, BIC-Test} = $(K_best2)\$")


    p3 = plot_slope_heuristic(Ks, res)
    p = plot(p1, p2, p3, plot(title="\$\\sigma\$ = $Ystd", framestyle=:none); layout=(2, 2))    
    if savepath !== nothing 
        png(p, savepath)
        @info "Plotted in $savepath"
    end
end


function plot_slope_heuristic(Ks::Vector, res::KSel.KSelection)
    i_best, a, b = best_K_slope_heuristic(res)
    Kbest = Ks[i_best]
    ll, dims = res.log_likelihood, res.dims
    ll_pen = ll - 2 * a * dims

    p = plot(dims, hcat(ll, ll_pen, a .* dims .+ b),
        dpi=300,
        labels=[L"L"  L"L_{pen}"  "Robust linear fit"],
        xlabel="Number of parameters",
        background_color_legend=Config.LEGEND_BACKGROUND,
        )
    vline!(p, [dims[i_best]] ; label="\$K_{best} = $Kbest\$")
    p
end

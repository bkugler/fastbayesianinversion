# We use the prior version of the Gllim conditional density
# to use previous inversion and increase regularity in an educated manner

using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Models
using FastBayesianInversion.Predictions
using FastBayesianInversion.Sobol
using FastBayesianInversion.Store
using FastBayesianInversion.Shared
using FastBayesianInversion.Is
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Schemes

using LinearAlgebra

include("env.jl")
include("hapke_models/models.jl")
include("plotting/base_plots.jl")

Plots.pyplot()
Log.init()
st = Store.Storage(ROOT_PATH_STORE)

GllimWithPrior = "gllim_prior" # additional scheme
GllimWithPriorWeak = "gllim_prior_weak" # additional scheme
GllimWithPriorBase = "gllim_prior_base" # additional scheme

function _train_inverse(c::Models.Model, Yobs::Matrix, Yobs_std::Matrix, run::Number, weak_prior_factor::Number)
    train_std = 0.0001
    _gllim_params = Gllim.Params(
        Gllim.FunctionnalInitParams(60, Models.closure(c)),
        Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(Gllim.DefaultStoppingParams(150, 1e-5), 1e-12)
    )
    _training_params = Shared.TrainingParams(100_000, train_std, _gllim_params)
    gllim, _, _ = Shared.generate_train_gllim(c, _training_params, st, run >= 2)

    _inv_params = Pipeline.InversionParams(
        train_std, 
        Predictions.MergingOptions(2),
        Is.ImisParams(2000, 1000, 20),
        false,
        false,
        Is.NonUniformityParams(1),
        0
    )
    _, N = size(Yobs)
    L = Models.get_L(c)

    # IMIS inversion, as a reference 
    preds, _, _ = Pipeline.complete_inversion(c, Yobs, Yobs_std, Pipeline.ProcedureParams(_training_params, _inv_params), min(run, 1), st)

    # output
    Xmean, Xcovs = zeros(L, N), zeros(L, N)
    Xmean_with_prior1, Xcovs_with_prior1 = zeros(L, N), zeros(L, N)
    Xmean_with_prior2, Xcovs_with_prior2 = zeros(L, N), zeros(L, N)
    Xmean_with_prior3, Xcovs_with_prior3 = zeros(L, N), zeros(L, N)
  
    # for the first observation, no prior
    mean_prior, cov_prior = Gllim.predict_with_std(gllim, Yobs[:, 1], Yobs_std[:, 1])
    mean_prior_base, cov_prior_base = mean_prior, cov_prior
    Xmean[:, 1], Xcovs[:, 1] = mean_prior, diag(cov_prior)
    Xmean_with_prior1[:, 1], Xcovs_with_prior1[:, 1] = mean_prior, diag(cov_prior)
    Xmean_with_prior2[:, 1], Xcovs_with_prior2[:, 1] = mean_prior, diag(cov_prior)
    Xmean_with_prior3[:, 1], Xcovs_with_prior3[:, 1] = mean_prior, diag(cov_prior)
   
    # then we use the previous mean/cov as a prior 
    for n in 2:N
        # we shift the covariance of the GLLiM model 
        sigma_obs = Diagonal(Yobs_std[:, n].^2) # we want the covariance
        gllim_shifted = Gllim.add_bias(gllim, 1., 0., sigma_obs)
        gllim_star = Gllim.inversion(gllim_shifted)
            
        # we get the inverse density from the shifted inverse
        w, m = Gllim.conditionnal_density(gllim_star, Yobs[:, n])
        s = gllim_star.Σ
        cov_base = Probas.covariance_mixture(w, m, s)
        Xmean[:, n], Xcovs[:, n] = Probas.mean_mixture(w, m), diag(cov_base)
        
        # we apply the prior from previous inversion ...
        w2, m2, s2 = Gllim.conditionnal_density_with_prior(w, m, s, mean_prior, cov_prior * weak_prior_factor)
        Xmean_with_prior2[:, n], Xcovs_with_prior2[:, n] = Probas.mean_mixture(w2, m2), diag(Probas.covariance_mixture(w2, m2, s2))

        # Gllim as prior
        w2, m2, s2 = Gllim.conditionnal_density_with_prior(w, m, s, mean_prior_base, cov_prior_base)
        Xmean_with_prior3[:, n], Xcovs_with_prior3[:, n] = Probas.mean_mixture(w2, m2), diag(Probas.covariance_mixture(w2, m2, s2))

        #  ... with different covariance; and save the result for the next iteration
        w2, m2, s2 = Gllim.conditionnal_density_with_prior(w, m, s, mean_prior, cov_prior)
        mean_prior = Probas.mean_mixture(w2, m2)
        cov_prior = Probas.covariance_mixture(w2, m2, s2)
        Xmean_with_prior1[:, n], Xcovs_with_prior1[:, n] = mean_prior, diag(cov_prior)

        mean_prior_base, cov_prior_base = Xmean[:, n], cov_base
    end
    
    # to physical 
    Xmean, Xcovs = Models.to_physical(c,  Xmean, Xcovs)
    Xmean_with_prior1, Xcovs_with_prior1 = Models.to_physical(c,  Xmean_with_prior1, Xcovs_with_prior1)
    Xmean_with_prior2, Xcovs_with_prior2 = Models.to_physical(c,  Xmean_with_prior2, Xcovs_with_prior2)
    Xmean_with_prior3, Xcovs_with_prior3 = Models.to_physical(c,  Xmean_with_prior3, Xcovs_with_prior3)

    Dict(
        Schemes.ImisMean => preds[Schemes.ImisMean],
        Schemes.GllimMean => Shared.MethodResult(Xmean, Xcovs=Xcovs, scheme=Schemes.GllimMean),
        GllimWithPrior => Shared.MethodResult(Xmean_with_prior1, Xcovs=Xcovs_with_prior1, scheme=GllimWithPrior),
        GllimWithPriorWeak => Shared.MethodResult(Xmean_with_prior2, Xcovs=Xcovs_with_prior2, scheme=GllimWithPriorWeak),
        GllimWithPriorBase => Shared.MethodResult(Xmean_with_prior3, Xcovs=Xcovs_with_prior3, scheme=GllimWithPriorBase)
    )
end

function _plot_inversions(context::Models.Model, preds::Dict{String,Shared.MethodResult}, wavelengths::Vector)
    plot_data = [
        preds[Schemes.ImisMean] => PlotOptions("IMIS", "green", 0.8, :solid, hide_std=true, markersize=0),
        preds[Schemes.GllimMean] => PlotOptions("GLLiM", "blue", 0.4, :solid, hide_std=true),
        preds[GllimWithPrior] => PlotOptions("GLLiM with cascade prior", "red", 0.4, :solid, hide_std=true),
        preds[GllimWithPriorWeak] => PlotOptions("GLLiM with cascade weak prior", "orange", 0.4, :dash, hide_std=true),
        preds[GllimWithPriorBase] => PlotOptions("GLLiM with base prior", "teal", 0.4, :dash, hide_std=true),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, :with_prior; label="gllim_with_prior")
    plot_components(plot_data;  savepath=path, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")
end


# c = HapkeOlivine()
c = HapkeHowardite()
data = load_observations(c)
Yobs = data.Yobs
# Yobs_std = data.Yobs ./ 20
Yobs_std = data.Yobs_std
invs = _train_inverse(c, Yobs, Yobs_std, 0, 10000)
_plot_inversions(c, invs, data.wavelengths)
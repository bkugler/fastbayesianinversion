using FastBayesianInversion.Probas
using FastBayesianInversion.Models
using FastBayesianInversion.Predictions
using FastBayesianInversion.Is
using Dates 

""" 
Implementation of the interface needed by `Is.IsModel`, 
representing the posterior distribution
"""
struct IsMean <: Is.IsModel
    proposition::Is.IsProposition

    context::Models.Model
    y::VecOrSub{Float64}
    cov::DiagCov{Float64}
    x0::Vector{Float64}
    Gamma_chol::CholCov{Float64}

    IsMean(proposition, context, y, cov, x0, Gamma) =  new(proposition, context, y, cov, x0, cholesky(Gamma).L)
end

@inline Is.get_proposition(m::IsMean) = m.proposition
# we want to compute the expectancy of X so F(X) = X and D = L
@inline Is.get_L(m::IsMean) = Models.get_L(m.context)
function Is.log_p(m::IsMean, x::VecOrSub{T}, ::Int) where T
    Models.log_conditionnal_density(m.context, x, m.y,  m.cov) + Probas.log_gaussian_density(x, m.x0, m.Gamma_chol, Is.get_L(m))
end

function _safe_cholesky(covs::FullCovs{T}) where T
    chol_covs = CholCovs{T}(undef, length(covs))
    for (i, M) in enumerate(covs)
        chol_covs[i] =  Probas.safe_cholesky(M)
    end
    chol_covs
end

function setup_proposition(weights, means, covs) 
    ti = Dates.now()

    merging_options = Predictions.MergingOptions(3, 1e-4) # since K is large we merge more agressively
    # we merge the mixture
    weights_merged_gllim, means_merged_gllim, covs_merged_gllim = Predictions.merge(merging_options, weights, means, covs)

    # to avoid spurious errors on weak components
    chol_covs_merged_centroids = _safe_cholesky(covs_merged_gllim)
    merged_posterior = Probas.Gmm{Float64}(weights_merged_gllim, means_merged_gllim, chol_covs_merged_centroids)

    @debug "Merged in $(Dates.now() - ti)"
    Is.GaussianMixtureProposition{Float64}(merged_posterior)
end
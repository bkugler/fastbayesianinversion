# Assimilation for snow retrodiffusion, ported from the code from X.V. Phan - BK 2021
# Follows the article
# 1D-Var multilayer assimilation of X-band SAR data into a detailed snowpack model
# X. V. Phan et all - 2014
using LinearAlgebra

include("backscattering.jl")


"""
    Computes an educated covariance matrix for X parameters from thickness (in m).
    sig_d and sig_rho represent the standard deviation of the errors
    on x, for optical diameter and snow density respectively
    Here, they must be given in m and g/cm3
"""
function guess_profile_covariance(thickness::Vector{T}, sig_d::Number, sig_rho::Number) where T
    NL = length(thickness)

    # distance entre couches
    dist = zeros(Float64, NL, NL)
    for i = 1:NL
        for j = i:NL
            dist[i,j] = sum(thickness[i:j]) - (thickness[i] + thickness[j]) / 2
            dist[j,i] = dist[i,j]
        end
    end
    dist .*= 100 # in cm


    # sig_d = 0.0003; % erreur de d (sigma de d) en m
    # sig_rho = 0.065; % erreur de rho Sigma de rho densit (g/cm3)

    # calculate error covariance matrix B
    # X is the concatenation of diameters and density

    # These values are issued from an ensemble of slightly perturbed Crocus runs, obtained by
    # varying their meteorological inputs over one winter season. The deviations between these
    # runs, considered to be elementary perturbations, were then
    # statistically studied and fitted with the model of Eq. (21) for
    # each pair of variables
    a_dd, b_dd = 0.11, 1
    a_rhorho, b_rhorho = 0.13, 1
    a_drho, b_drho = 0.15, 0.66
    B = zeros(Float64, 2 * NL, 2 * NL)
    for i = 1:NL
        for j = i:NL
            B[i,j] = exp(-a_dd * dist[i,j]) * b_dd * sig_d^2					    # d - d
            B[i + NL,j] = exp(-a_drho * dist[i,j]) * b_drho * sig_d * sig_rho       # d - rho
            B[i + NL,j + NL] = exp(-a_rhorho * dist[i,j]) * b_rhorho * sig_rho^2  # rho - rho
        end
    end
    for i = 1:2 * NL
        for j = i + 1:2 * NL
            B[j,i] = B[i,j]
        end
    end

    @assert isposdef(B)
    Symmetric(B)
end

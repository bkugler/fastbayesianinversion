# Snow retrodiffusion, ported from the code from M. Gay - BK 2021


""" HH or VV """
abstract type PolarModele end
struct PolarHH <: PolarModele end
struct PolarVV <: PolarModele end

struct BackScatteringModel{P <: PolarModele}
    l_as::Number # air-snow correlation length, in cm
    sigma_as::Number # air-snow rms height, in cm
    
    l_si::Number # snow-ice correlation length, in cm
    sigma_si::Number # snow-ice rms height, in cm
end

polar(::BackScatteringModel{PolarVV}) = "VV"
polar(::BackScatteringModel{PolarHH}) = "HH"


#  Matrices de transmission Fresnel
function _fresnel_transmission(::BackScatteringModel{PolarHH}, n1::Complex{T}, n2::Complex{T}, theta1::T, theta2::T)where T <: Real
    (n1 * cos(theta1) - n2 * cos(theta2)) / (n1 * cos(theta1) + n2 * cos(theta2))
end

#  Matrices de transmission Fresnel
function _fresnel_transmission(::BackScatteringModel{PolarVV}, n1::Complex{T}, n2::Complex{T}, theta1::T, theta2::T)where T <: Real
    (n2*cos(theta1) - n1*cos(theta2))/(n2*cos(theta1) + n1*cos(theta2));
end

function _copolar_retrodiffusion(::BackScatteringModel{PolarHH}, theta::T,kl::T,ks::T,epsilon::Complex{T}) where T <: Real
    Shh,_,_,_ = _iem_ss(theta, kl, ks, epsilon, :g, 1.)
    Shh
end

function _copolar_retrodiffusion(::BackScatteringModel{PolarVV}, theta::T,kl::T,ks::T,epsilon::Complex{T}) where T <: Real
    _,_,_,Svv =  _iem_ss(theta, kl, ks, epsilon, :g, 1.)
    Svv
end

""" Converts from height profile (in cm) to thickness profile (in m)"""
function thickness_from_height(h::Vector{T}) where T <:  Real
    NL = length(h);
    thickness = zeros(NL);
    for i = 1:NL-1
        thickness[i] = h[i] - h[i+1];
    end;
    thickness[NL] = h[NL];
    thickness ./= 100;
    thickness
end


"""
    Returns the electromagnetic backscattering coefficient for the given snow pack:
        - d : diameter of the snow flakes for each layer (in meters)
        - rho : density of snow for each layer, in [0,1]
        - thickness : thickness of each layer, highest layer first (in meters) (see thickness_from_height)
    It depends on the chosen polarisation (Modele), and hyper parameters:
        - freq: frequency in Ghz
        - theta0d: incidence angle, in degrees
    It also returns the phase vector (with length the number of layers).
"""
function snow_backscattering(m::BackScatteringModel{PT}, freq::T, theta0d::Real, d::Vector{T}, rho::Vector{T}, thickness::Vector{T})  where {PT<:PolarModele , T <: Real}

    NL = length(rho);

    theta0 = theta0d*pi/180;
    k0 = 2*pi*freq*10/3;
    T0=300;
    TempK=265;
    
    # --------------------------------------------------
    #  Calcul de la permittivite complexe de la glace pure
    # --------------------------------------------------
    alpha=(0.00504+0.0062*(T0/TempK-1))*exp(-22.1*(T0/TempK-1)); # Hufford 1991
    B1=0.0207; # en K/GHz
    b=335; #  en K
    B2=1.16*10^(-11); # en GHz^-3
    beta=(B1/TempK*exp(b/TempK)/((exp(b/TempK)-1)*(exp(b/TempK)-1)))+(B2*freq*freq)+(exp(-10.02+0.0364*(TempK-273)));
    epssecd_ice=alpha/freq+beta*freq;  #  partie imaginaire de la permittivité dielectrique complexe de la glace
    epsilon_ice = 3.15 + epssecd_ice*im;# 2.5*(6E-4*f1.^(-1) + 65E-6*f1.^(1.07))*im; #  permittivite de la glace
     
    # -----------------------------------------------------
    #  Rugosite des interfaces
    # -----------------------------------------------------

    l_as, sigma_as = m.l_as / 100, m.sigma_as / 100
    l_si, sigma_si = m.l_si / 100, m.sigma_si / 100

    # ---------------------------------------
    #  Permittivite de  l'interface neige-sol
    # ---------------------------------------
    eps_ground = 3.2 + 0.002*im;
    # eps_ground=3+0.001*im; #  Publication Tan 2015 IEEE 
    # eps_ground=5+1.5*im; #  Frozen ground Tan et al. 2015
    
    f_v = zeros(T,NL);
    L = zeros(T,NL);
    A = zeros(Complex{T},NL);
    eps_g = zeros(Complex{T},NL);
    dt_eg = zeros(T,NL);
    eps = zeros(Complex{T},NL);
    theta = zeros(T,NL);
    T_in = zeros(T,NL);
    T_out = zeros(T,NL);
    kappa = zeros(T,NL);
    Att_up = zeros(Complex{T},NL);
    Att_down = zeros(Complex{T},NL);
    P = zeros(Complex{T},NL);
    sigma_vol = zeros(Complex{T},NL);
    
    # taille optique de grain ?
    
    # ----------------------------------------------------------------------------------------
    #  Calcul de la permittivite effective de la neige seche
    # ----------------------------------------------------------------------------------------
    for i=1:NL
        f_v[i] = rho[i]/0.917;
        L[i] = (2*d[i]/3)*(1-f_v[i]); #  longueur correlation de Stogryn
        A[i] = 3*f_v[i]*(1-epsilon_ice) - 2 + epsilon_ice; #  variable temporaire
        eps_g[i] = 1/4*(-A[i] + sqrt(A[i]^2 + 8*epsilon_ice)); # eps quasi-static
        dt_eg[i] = f_v[i]*abs(3*eps_g[i]*(epsilon_ice - eps_g[i])/(epsilon_ice + 2*eps_g[i]))^2 + (1 - f_v[i])*abs(3*eps_g[i]*(1 - eps_g[i])/(1 + 2*eps_g[i]))^2; # variance de eps_g
        eps[i] = eps_g[i] + 4/3*k0^3*im*L[i]^3*sqrt(eps_g[i])*dt_eg[i]; # effective permitivite
    end;
    
    #  theta et theta_i de chaque couche
    # TODO: vérifier l'utilisation ou non de la partie réelle
    theta[1] = asin(real(sqrt(1/eps[1]))*sin(theta0));
    for i = 2:NL
        theta[i] = asin(real(sqrt(eps[i-1]/eps[i])*sin(theta[i-1])));
    end;

    # ----------------------------------------------------------------------------------------
    #  Retrodiffusion de surface
    # ----------------------------------------------------------------------------------------

    sigma_s=real(_copolar_retrodiffusion(m, theta0,k0*l_as,k0*sigma_as, eps[1]));

    # -------------------------------------------------------------------------------
    #  Retrodiffusion volumique
    # -------------------------------------------------------------------------------
    
    #  Matrices de transmission Fresnel
    for i = 1:NL
        if i == 1
            n1 = Complex{T}(1)
            theta1 = theta0;
        else
            n1 = sqrt(eps[i-1]);
            theta1 = theta[i-1];
        end;
        n2 = sqrt(eps[i]);
        theta2 = theta[i];

        r = _fresnel_transmission(m, n1, n2, theta1, theta2)

        T_in[i] = 1-(abs(r)^2);
        T_out[i] = 1-(abs(r)^2);
    end;

    #  Attenuation
    for i = 1:NL
        kappa[i] = 2*imag(k0*sqrt(eps[i]));
    end;
    
    Att_up[1] = T_out[1]*exp(-kappa[1]*thickness[1]/cos(theta[1]));
    Att_down[1] = exp(-kappa[1]*thickness[1]/cos(theta[1]))*T_in[1];
    for i = 2:NL
        Att_up[i] = Att_up[i-1]*T_out[i]*exp(-kappa[i]*thickness[i]/cos(theta[i]));
        Att_down[i] = exp(-kappa[i]*thickness[i]/cos(theta[i]))*T_in[i]*Att_down[i-1];
    end;
    
    #  Coef de diffusion P
    for i = 1:NL
        P[i] = k0^4*eps_g[i]^2*dt_eg[i]*L[i]^3/(2*pi)*(1 + 4*k0^2*eps_g[i]*L[i]^2)^(-3/2)*cos(3*atan(2*k0*sqrt(eps_g[i])*L[i]));
    end;
    
    # ---------------------------------------------------------------------
    #  Coef retrodiffusion volumique
    # ---------------------------------------------------------------------
    sigma_vol[1] = (T_out[1]*(1 - exp(-2*kappa[1]*thickness[1]/cos(theta[1])))/(2*kappa[1])*P[1]*T_in[1]);
    for i = 2:NL
        sigma_vol[i] = ((Att_up[i-1]*T_out[i]*(1 - exp(-2*kappa[i]*thickness[i]/cos(theta[i])))/(2*kappa[i])*P[i]*T_in[i]*Att_down[i-1]));
    end;
    sigma_vol = sigma_vol*4*pi*cos(theta0);
    phase_vol::Vector{T}=atan.(imag.(sigma_vol) ./ real.(sigma_vol));
    phase_vol=(phase_vol*180)/pi;
    phase=phase_vol;

    # ----------------------------------------------------------------------------------------
    #  Retrodiffusion interface neige-sol
    # ----------------------------------------------------------------------------------------
    kl_g = real(k0*sqrt(eps[NL]))*l_si;
    ks_g = real(k0*sqrt(eps[NL]))*sigma_si;

    Sxx = _copolar_retrodiffusion(m, theta[NL],k0*l_si,k0*sigma_si,eps_ground/eps[NL])
    sigma_g = real(4*pi*cos(theta0)*Att_up[NL]*Sxx*Att_down[NL])

    # ------------------------------------------------------------------------------
    #  Retrodiffusion totale
    # ------------------------------------------------------------------------------
    if (ks_g>4)
        sigma_0=real(sigma_s+sum(sigma_vol));
    else
        sigma_0=real(sigma_s+sum(sigma_vol)+sigma_g);
    end
    return sigma_0, phase
end


"""
    Returns Shhhh,Shhvv,Svvhh,Svvvv : the reals coefficients of copolar retrodiffusion.
    Retrodiffusion model from IEM (Fung 1992) with
    reflexion coefficients modification (Wu 2001-2004)
    (validation Fung 1992, 2004.)
    Simple diffusion for low and medium steeps.
    theta   : incidence angle (in radians)
    kl      : product wave number * correlation length
    ks      : product wave number * heights standard deviation
    epsilon : dielectric constant
    type    : surface spectrum kind : :g for gaussien or :e for exponeniel
    fonc_transition : reflexion coefficient (default to 1)
"""
function _iem_ss(theta::T,kl::T,ks::T,epsilon::Complex{T},type::Symbol,fonc_transition::T) where T <: Real
    eps_ = eps(T)

    # Domaine de validité
    if ((ks>4)|(ks*kl/sqrt(abs(epsilon))>1.2))
        error("invalid spectrum parameters")
    end;

    sq = sqrt(epsilon-sin(theta)^2)
    kz = cos(theta)

    # Definition des coefficients de reflexion
    if (fonc_transition == 1)
        Rh,Rv = _R_transition(theta,kl,ks,epsilon)
    elseif (fonc_transition == 2)
        Rv = (epsilon*kz-sq)/(epsilon*kz+sq)
        Rh = (kz-sq)/(kz+sq)
    else (fonc_transition == 3)
        Rh = (1-sqrt(epsilon))/(1+sqrt(epsilon))
        Rv = -Rh
    end


    # Composantes de Kirchhoff
    fhh=-2 *Rh/kz
    fvv=2 *Rv/kz

    Rv = (epsilon*kz-sq)/(epsilon*kz+sq)
    Rh = (kz-sq)/(kz+sq)


    # Composantes complementaires
    Ahh =  -(sin(theta)^2 *(1+Rh)^2)/kz
    Bhh = (epsilon-1)/(kz^2)
    Fhh  = Ahh*Bhh
    Avv = (sin(theta)^2 *(1+Rv)^2)/kz
    Bvv = (1-1 /epsilon)+((epsilon-sin(theta)^2-epsilon*(kz^2))/((epsilon*kz)^2))
    Fvv  = Avv*Bvv
    # Ahh = (sin(theta)^2/kz - sq)*(1 + Rh)^2
    # Bhh = 2*sin(theta)^2*(1/kz - 1/sq)*(1 - Rh^2)
    Chh = ((sin(theta)^2/kz - (1 + sin(theta)^2)/sq))/(1 - Rh)^2
    # Fhh  = - Ahh + Bhh - Chh
    # Avv = (sin(theta)^2/kz - sq/epsilon)*((1 + Rv)^2)
    # Bvv = 2*sin(theta)^2*(1/kz - 1/sq)*(1 - Rv^2)
    Cvv = ((sin(theta)^2/kz - epsilon*(1 + sin(theta)^2)/sq))/(1 - Rv)^2
    #Fvv  = Avv - Bvv + Cvv

    n = 1
    delta_shh = 1
    delta_svv = 1
    A = 1

    # Expression des coefficients de la somme
    f=1
    Shhhh::T,Shhvv::Complex{T},Svvhh::Complex{T},Svvvv::T=0,0,0,0

    if type == :g
    ############# GAUSSIEN ###############
        # Constante devant le terme de retrodiffusion
        A=kl^2 *exp(-2 *ks^2 *kz^2)/4

        while max(delta_svv)>eps_ && max(delta_shh)>eps_
            f=f*n
            delta_hh = ((kz*ks)^(2*n)*((abs(2 ^n*fhh*exp(-(ks*kz)^2)+Fhh))^2)*exp(-(kl*sin(theta))^2 /n)/(n*f))
            Shhhh=Shhhh+delta_hh
            delta_shh = abs(delta_hh/Shhhh)
            Shhvv=Shhvv+((kz*ks)^(2*n)*((2 ^n*fhh*(exp(-(ks*kz)^2))+Fhh)*conj(2 ^n*fvv*(exp(-(ks*kz)^2))+Fvv))*exp(-(kl*sin(theta))^2 /n)/(n*f))
            Svvhh=Svvhh+((kz*ks)^(2*n)*((2 ^n*fvv*(exp(-(ks*kz)^2))+Fvv)*conj(2 ^n*fhh*(exp(-(ks*kz)^2))+Fhh))*exp(-(kl*sin(theta))^2 /n)/(n*f))
            delta_vv = ((kz*ks)^(2*n)*((abs(2 ^n*fvv*exp(-(ks*kz)^2)+Fvv))^2)*exp(-(kl*sin(theta))^2 /n)/(n*f))
            Svvvv=Svvvv+delta_vv
            delta_svv = abs(delta_vv/Svvvv)
            n += 1
        end
    elseif type == :e
    ############# EXPONENTIEL ###############
        # Constante devant le terme de rétrodiffusion
        A=kl^2 *exp(-2 *ks^2 *kz^2)/2

        # Expression des coefficients de la somme
        while delta_svv>eps_ & delta_shh>eps_
            f=f*n
            delta_hh =((kz*ks)^(2 *n)*((abs(2 ^n*fhh+Chh))^2)*((1+(2*kl*sin(theta)/n)^2)^-1.5/(n^2 *f)))
            Shhhh=Shhhh+delta_hh
            delta_shh = abs(delta_hh/Shhhh)
            Shhvv=Shhvv+((kz*ks)^(2 *n)*((2 ^n*fhh+Chh)*conj(2 ^n*fvv+Cvv))*((1+(2*kl*sin(theta)/n)^2)^-1.5/(n^2 *f)))
            Svvhh=Svvhh+((kz*ks)^(2 *n)*((2 ^n*fvv+Cvv)*conj(2 ^n*fhh+Chh))*((1+(2*kl*sin(theta)/n)^2)^-1.5/(n^2 *f)))
            delta_vv = ((kz*ks)^(2 *n)*((abs(2 ^n*fvv+Cvv))^2)*((1+(2*kl*sin(theta)/n)^2)^-1.5/(n^2 *f)))
            Svvvv=Svvvv+delta_vv
            delta_svv = abs(delta_vv/Svvvv)
            n += 1
        end
    end

    # Coeff retrodifffusion finaux
    return A*Shhhh, A*Shhvv, A*Svvhh, A*Svvvv
end 


"""
    Returns Rht,Rhv, the transition of reflexion coefficients (V and H), from Wu 2001
    See `iem_ss` for the parameters meaning.
"""
function _R_transition(theta::T,kl::T,ks::T,epsilon::Complex{T}) where T
    eps_ = eps(T)
    # definition de la constante kz
    kz = cos(theta);
    sq = sqrt(epsilon-sin(theta).^2);

    Rh_0 = (1-sqrt(epsilon))./(1+sqrt(epsilon));
    Rv_0 = -Rh_0;

    Rh_theta = (kz-sq)./(kz+sq);
    Rv_theta = (epsilon.*kz-sq)./(epsilon.*kz+sq);

    Fp = 8 .* Rv_0.^2 .* sin(theta).^2 .* ((kz+sq)./(kz.*sq));

    S_0 = (abs(1+8*Rv_0./(Fp.*kz))).^(-2);

    n = 1;delta_Sp = 1;f = 1;num =0;denom = 0;Sp = 0;

    ############# GAUSSIEN ###############
    while max(delta_Sp)>eps_ 
        f=f*n;
        delta1 = (ks.*kz).^(2*n).*exp(-(kl.*sin(theta)).^2 ./n)./(sqrt(n).*f);
        delta2 = (ks.*kz).^(2*n).*exp(-(kl.*sin(theta)).^2 ./n).*(abs(Fp+(2 .^(n+2).*Rv_0)./(exp((ks.*kz).^2).*kz)).^2)./(sqrt(n).*f);
        num   = num+delta1;
        denom = denom+delta2;
        ex_Sp = Sp;
        Sp = abs(Fp).^2 .*num./denom;
        delta_Sp = abs((Sp-ex_Sp)./(Sp));
        n = n+1;
    end;        

    delta_p = 1-Sp./S_0;
    Rht = Rh_theta + (Rh_0-Rh_theta).*delta_p;
    Rvt = Rv_theta + (Rv_0-Rv_theta).*delta_p;
    return Rht, Rvt
end


using Dates:include
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Models
using FastBayesianInversion.Log
using FastBayesianInversion.Store
using FastBayesianInversion.Shared

using MAT
using Plots
using LaTeXStrings
using Dates

include("model.jl")
include("../../env.jl")
include("../../plotting/base_plots.jl")

Plots.pyplot()
Log.init()

# TODO: tracer et comparer y_obs et f(X_0)
# TODO: ajouter la correction_diametre = 1.6 sur le diamètre
# TODO: assimiler 1 polar (vv puis hh); assimiler à 30°; freq = 10 puis comparer avec inc: 40, 50, 60
# TODO: Idem avec 3 frequences 
# TODO: idem avec freq = 10, et inc = 30 et 40 
# These Audrey Martini, Laurent Ferro-famil, Sophie Allain, **Nicolas Longépé**

# TODO: Fichier .json avec score 
# 1 / (nb_dates * nb_inc) sum | F(x_assim) - y_obs | 
# norm x0 - xassim séparé taille de grain / densité



function test_forward_model()
    path = PATH_STORE_ASSIM * "Code/NoSRex_Simulation/Assim_NOSREX_X_vv_30deg.mat"
    data = matread(path)

    freq = 16.6; # frequence en GHz
    incidence = 50; #  angle d'incidence en degre

    hs = data["h_nosrex"][1, :]
    ds = data["d_nosrex"][1, :]
    rhos = data["rho_nosrex"][1, :]
    shs, svs = Vector{Float64}(), Vector{Float64}()
    @show hs
    for (i, h) in enumerate(hs)
        if size(h) == (0, 0)
            continue  # skip empty values
        end
        @show h
        h = h[1, :][1:end - 1]
        d = ds[i][1, :][1:end - 1]
        rho = rhos[i][1, :][1:end - 1]
        @assert size(h) == size(d) && size(d) == size(rho)

        mh = BackScatteringModel{PolarHH}(8.4, 0.4, 8, 0.7)
        mv = BackScatteringModel{PolarVV}(8.4, 0.4, 8, 0.7)
        sh, _ = snow_backscattering(mh, freq, incidence, d, rho, h)
        sv, _ = snow_backscattering(mv, freq, incidence, d, rho, h)
        push!(shs, sh)
        push!(svs, sv)
    end

    p = scatter(1:length(shs), 10 .* log10.(hcat(shs, svs)), labels=["HH"  "VV"], ylabel="\$\\sigma_0 (dB)\$")
    png(p, PATH_STORE_ASSIM * "direct.png")
end
# test_forward_model()

function test_learning()
    freq = 16.6; # frequence en GHz
    incidence = 50.; #  angle d'incidence en degre
    heights = [69.0,67.0,60.0,57.0,51.0,37.0,30.0,19.0,16.0,10.0,0.0] # length 11

    bm = BackScatteringModel{PolarVV}(8.4, 0.4, 8, 0.7)
    context = SnowPackModel(bm, freq, incidence, heights)

    X, Y = Models.data_training(context, 100_000, 0.001)
    @assert all(isfinite.(X)) "X dataset is corrupted"
    @assert all(isfinite.(Y)) "Y dataset is corrupted"

    gp = Gllim.Params(
        Gllim.FunctionnalInitParams(200, Models.closure(context)),
        Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(
            Gllim.DefaultStoppingParams(300, 1e-5),
            1e-12
        )
    )

    trained_gllim, ll_history = Gllim.train(gp, X, Y)
    p = plot(ll_history)
    png(p, PATH_STORE_ASSIM * "likelihood.png")
end
# test_learning()

struct AssimInput
    heights::Vector # in cm
    grain_size::Vector # in mm
    rho::Vector # in [0,1000]
    sigma_obs_db::Number # 10 log10
    date::String
end


struct SnowProfil
    date::Dates.Date
    thickness::Vector{Float64} # m
    grain_size::Vector{Float64} #  mm
    density::Vector{Float64} # in [0,1]
end

st = Store.Storage(ROOT_PATH_STORE)

# profils NosREx band X VV (incidence 40)
function test_assimilation(run::Int)

    correction_diametre = 2.6; # 1.6 band Ku facteur de correction du diametre visuel entre 2-3
    freq = 10.2; # fréquence en GHz
    incidence = 40;
    bm = BackScatteringModel{PolarVV}(0.8, 0.3, 0.8, 0.07)

    inputs = [
        # profil 01 dec 2010
        AssimInput(
            [25, 20, 15, 10],
            [0.8, 1, 1.8, 1.5],
            [284, 284, 228, 248],
            -18.21,
            "2010/12/1",
        ),

        # profil 07 dec 2010
        AssimInput(
            [28, 24, 20, 14, 11, 10, 6],
            [0.5, 1.3, 2, 3, 2.5, 2.8, 3],
            [240, 240, 240, 256, 256, 256, 256],
            -18.21,
            "2010/12/7",
        ),

        # profil 15 dec 2010
        AssimInput(
            [29, 28, 21, 18, 16, 14, 9, 8],
            [0.8, 0.8, 1, 1.5, 1.8, 2.5, 2, 2.5],
            [100, 100, 220, 220, 220, 224, 224, 224],
            -18.39,
            "2010/12/15",
        ),

        # profil 20 dec 2010
        AssimInput(
            [34, 30, 29, 23, 19, 14, 11],
            [0.8, 0.8, 0.8, 1.5, 2, 3, 2.5],
            [224, 224, 200, 212, 200, 200, 253],
            -18.39,
            "2010/12/20",
        ),
        
        # profil 04 jan 2011
        AssimInput(
            [39, 37, 35, 29, 28, 20, 19, 17, 11],
            [1.8, 0.5, 0.8, 0.8, 1.5, 2, 2.3, 2.5, 3.5],
            [100, 100, 216, 200, 240, 240, 200, 200, 243],
            -18.04,
            "2011/1/4",
        ),

        # profil 12 jan 2011
        AssimInput(
            [45, 43, 40, 36, 30, 19, 10],
            [1.3, 0.8, 0.5, 0.5, 0.8, 3, 3],
            [100, 188, 188, 264, 238, 276, 280],
            -18.04,
            "2011/1/12",
        ),

        # profil 18 jan 2011
        AssimInput(
            [52, 49, 46, 36, 29, 18, 16, 10, 8],
            [0.8, 2, 0.8, 0.5, 1.8, 2, 2.5, 2.5, 3],
            [196, 196, 208, 248, 220, 244, 244, 244, 248],
            -18.04,
            "2011/1/18",
        ),
        
        # profil 26 jan 2011
        AssimInput(
            [49, 48, 42, 37, 35, 25, 20, 15, 13, 10],
            [0.5, 0.5, 0.5, 0.8, 0.8, 1.5, 3, 3.5, 3.5, 3.5],
            [100, 100, 200, 200, 232, 208, 208, 200, 244, 256],
            -17.8,
            "2011/1/26",
        ),

        # profil 01 fev 2011
        AssimInput(
            [53, 52, 49, 39, 34, 28, 21, 18, 13, 10],
            [0.5, 0.5, 0.5, 0.5, 0.8, 1.8, 2, 2.5, 3, 4],
            [100, 252, 204, 204, 248, 248, 212, 240, 276, 304],
            -17.8,
            "2011/2/1",
        ),
        
        # profil 08 fev 2011
        AssimInput(
            [69, 67, 60, 57, 51, 37, 30, 19, 16, 10],
            [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 2, 2, 2],
            [100, 208, 208, 208, 198, 192, 214, 214, 248, 236],
            -17.8,
            "2011/2/8",
        ),
        
        # profil 23 fev 2011
        AssimInput(
            [60, 59, 49, 29, 22],
            [0.5, 0.8, 1, 1.8, 2.5],
            [252, 228, 229, 200, 243],
            -17.8,
            "2011/2/23",
        ),

        # profil 01 mar 2011
        AssimInput(
            [59, 58, 47, 40, 32, 27, 18],
            [0.5, 0.5, 0.8, 0.8, 1, 1.5, 2],
            [200, 260, 224, 224, 236, 252, 232],
            -17.5,
            "2011/3/1",
        ),
        
        # profil 02 mar 2011
        # AssimInput(
        #     = [61, 54, 50, 42, 31, 22, 18, 10],
        #     = [0.3, 0.3, 0.5, 0.5, 0.8, 1.5, 2, 2],
        #     = [280, 264, 264, 220, 272, 264, 300, 274],
        #     = -15.9,
        #     "2011/3/2",
        # ),

        # profil 04 mar 2011
        AssimInput(
            [60, 56, 48, 42, 33, 26, 18],
            [0.5, 0.8, 0.8, 1, 1.3, 1.8, 2.5],
            [272, 232, 232, 216, 264, 264, 280],
            -19.95,
            "2011/3/4",
        ),
        
        # profil 08 mar 2011
        AssimInput(
            [61, 57, 55, 42, 32, 25, 20, 7, 5],
            [0.5, 0.5, 0.8, 1, 1.3, 2, 1.8, 2.5, 3],
            [280, 248, 248, 220, 272, 232, 242, 268, 268],
            -19.08,
            "2011/3/8",
        ),
        
        # profil 15 mar 2011
        AssimInput(
            [77, 76, 72, 60, 45, 25, 18],
            [0.5, 0.5, 0.5, 0.5, 1, 2, 3],
            [100, 100, 275, 247, 236, 260, 266],
            -17.52,
            "2011/3/15",
        ),

        # profil 17 mar 2011
        AssimInput(
            [72, 70, 58, 50, 32, 26, 18, 10],
            [0.5, 0.5, 0.5, 0.8, 1, 2, 3, 4],
            [288, 256, 272, 243, 256, 220, 256, 256],
            -17.52,
            "2011/3/17",
        ),
        
        # profil 22 mar 2011
        AssimInput(
            [75, 72, 68, 65, 57, 42, 28, 17],
            [0.8, 0.5, 0.5, 0.5, 0.5, 0.8, 1.3, 2.5],
            [200, 272, 272, 288, 288, 228, 292, 225],
            -17.52,
            "2011/3/22",
        ),
        
        # profil 29 mar 2011
        AssimInput(
            [70, 69, 67, 66, 55, 43, 30, 25, 20],
            [0.5, 0.5, 0.5, 0.5, 0.5, 0.8, 1, 1.5, 3],
            [288, 288, 288, 312, 288, 258, 248, 280, 276],
            -17.17,
            "2011/3/29",
        ),
    ]

    # assess the grain size range
    minimum(minimum(s.grain_size / correction_diametre) for s in inputs)
    maximum(maximum(s.grain_size / correction_diametre) for s in inputs)
    min_diameter, max_diameter = 0, 2 # mm

    if run >= 1
        out = []
        for input in inputs
            thickness = thickness_from_height(input.heights)
            context = SnowPackModel(bm, freq, incidence, thickness, min_diameter, max_diameter)
            D = Models.get_D(context)

            gp = Gllim.Params(
                Gllim.FunctionnalInitParams(200, Models.closure(context)),
                Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(
                    Gllim.DefaultStoppingParams(300, 1e-5),
                    1e-12
                )
            )

            trained_gllim, _, dura = Shared.generate_train_gllim(context, Shared.TrainingParams(
                100_000, 0.0001, gp
            ), st, run >= 2)
            @info "trained in $dura"

          # assimilation
            x0 = [
                (input.grain_size / correction_diametre)...,
                (input.rho / 1000)...,
            ]
            sigma_obs = 10^(input.sigma_obs_db / 10)
            covX::FullCov{Float64} = guess_profile_covariance(thickness, 0.1, 0.08)
            
            x0, covX = Models.from_physical(context, x0, covX)

            # covY::FullCov{Float64} = Symmetric(0.03 * ones(D, D)) # from the article
            covY::FullCov{Float64} = Symmetric(0.0000036 * ones(D, D)) # from the matlab code

            trained_gllim = Gllim.add_bias(trained_gllim, 1., 0., covY)
            gllim_star = Gllim.inversion(trained_gllim)
            weights, means = Gllim.conditionnal_density(gllim_star, [sigma_obs])
            covs = gllim_star.Σ

            w2, m2, _ = Gllim.conditionnal_density_with_prior(weights, means, covs, x0, covX)
            x_pred  =  Probas.mean_mixture(w2, m2)
            x_pred, _ = Models.to_physical(context, x_pred, similar(x_pred))

            push!(out, x_pred)
        end
        Store.save(st, out, Store.FO_PREDICTIONS, :assim_snow_pack, inputs)
    end 
    out = Store.load(st, Store.FO_PREDICTIONS, :assim_snow_pack, inputs)

    # plot 
    subplots = []
    for (input::AssimInput, output) in zip(inputs, out[1:4])
        NL = length(input.heights)
        # cov =  guess_profile_covariance(thickness, 0.1, 0.08)
        # @show eigvals(cov[1:NL, 1:NL])
        # @show eigvals(cov[(NL + 1):2 * NL, (NL + 1):2 * NL])
        data::Matrix{Float64} = hcat(input.grain_size / correction_diametre, output[1:NL], input.rho / 1000, output[NL + 1:2 * NL])
        labels = [L"$d_{prior}$ (mm)" L"$d_{assim}$ (mm)" L"$\rho_{prior}$ (in [0, 1])" L"$\rho_{assim}$ (in [0, 1])"]
        markers = [:star :star :rect :rect]
        styles = [:solid :dash :solid :dash]
        colors = [:blue :blue :red :red]
        p = plot(1:NL, data,  xlabel="$NL layers (measure day $(input.date))", labels=labels, markers=markers, c=colors, style=styles, alpha=0.5)
        push!(subplots, p)
    end
    p = plot(subplots..., size=(1000, 1000), dpi=300)
    path = Store.get_path(st, Store.FO_GRAPHS, :assim_snow_pack, inputs; label="assim_snowpack")
    png(p, path)
    @info "Plotted in $path"

    input_profiles = [SnowProfil(Date(input.date, "y/m/d"), thickness_from_height(input.heights), input.grain_size / correction_diametre, input.rho / 1000)  for input in inputs]

    sigma_obs = [input.sigma_obs_db for input::AssimInput in inputs]
    input_profiles, sigma_obs, out
end



function plot_assim_data(inputs::Vector{SnowProfil}, sigma_obs::Vector, xmeans::Vector)
    freq_assim = 10.2; # fréquence en GHz
    incidence = 40;
    bm = BackScatteringModel{PolarVV}(0.8, 0.3, 0.8, 0.07)
    
    # compute initial and assimilated backscattering ... 
    sigma_no_assim = []
    sigma_assim = []
    for (profil, output) in  zip(inputs, xmeans) 
        sigma, _  = snow_backscattering(bm, freq_assim, incidence, profil.grain_size * 1e-3, profil.density, profil.thickness)
        push!(sigma_no_assim, 10 * log10(sigma))

        NL = length(output) ÷ 2
        sigma, _  = snow_backscattering(bm, freq_assim, incidence, output[1:NL],  output[NL + 1:2 * NL], profil.thickness)
        # sigma, _  = snow_backscattering(bm, freq_assim, incidence, output[1:NL],  output[NL + 1:2 * NL], profil.thickness)
        push!(sigma_assim, 10 * log10(sigma))
    end 

    ymin = min(minimum(sigma_no_assim), minimum(sigma_assim), minimum(sigma_obs)) - 1 
    ymax = max(maximum(sigma_no_assim), maximum(sigma_assim), maximum(sigma_obs)) + 1
    path = Store.get_path(st, Store.FO_GRAPHS, :assim_snow_pack, inputs; label="assim_snowpack")
    dates = [s.date for s in inputs]
    p = scatter(dates, sigma_no_assim; label="Reconstruction (without assim.)", ylims=(ymin, ymax))
    scatter!(p, dates, sigma_obs; label="Observation")
    png(p, path * "sigma_no_assim")

    p = scatter(dates, sigma_no_assim; label="Reconstruction (without assim.)", ylims=(ymin, ymax))
    scatter!(p, dates, sigma_obs; label="Observation")
    scatter!(p, dates, sigma_assim; label="Reconstruction (with assim.)", ylims=(ymin, ymax), linestyle=:dash)
    png(p, path * "sigma_assim")
    
    subplots = []
    for (i, profil) in enumerate(inputs)
        NL = length(profil.thickness)
        heights = reverse(cumsum(reverse(profil.thickness)) * 100)
        pbar = bar([0.75], hcat(heights)'; c=:grey77, ylabel="layers height (cm)", labels=hcat(["Layer $j" for j in 1:NL]...), legend=:none, xticks=:none)
            # cov =  guess_profile_covariance(thickness, 0.1, 0.08)
            # @show eigvals(cov[1:NL, 1:NL])
            # @show eigvals(cov[(NL + 1):2 * NL, (NL + 1):2 * NL])
        output = xmeans[i]
        data::Matrix{Float64} = hcat(profil.grain_size, output[1:NL] * 1000, profil.density, output[NL + 1:2 * NL])
        labels = [L"$d_{prior}$ (mm)" L"$d_{assim}$ (mm)" L"$\rho_{prior}$ (in [0, 1])" L"$\rho_{assim}$ (in [0, 1])"]
        markers = [:star :star :rect :rect]
        styles = [:solid :dot :solid :dot]
        colors = [:blue :blue :red :red]
        p = plot(1:NL, data,  xlabel="$NL layers (measure day $(profil.date))", labels=labels, markers=markers, c=colors, style=styles, alpha=0.5, background_color_legend=Config.LEGEND_BACKGROUND)
        push!(subplots, plot(pbar, p, layout=grid(1, 2, widths=[0.2, 0.8])))
    end

    p = plot(subplots..., layout=(6, 3), size=(1500, 2000))
    path = Store.get_path(st, Store.FO_GRAPHS, :assim_snow_pack, inputs; label="assim_snowpack")
    png(p, path * "profil")
    @info "Plotted in $path"
end

inputs, sigma_obs, out = test_assimilation(1)
plot_assim_data(inputs, sigma_obs, out)
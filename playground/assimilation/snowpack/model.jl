using FastBayesianInversion.Probas
using FastBayesianInversion.Models

include("assimilation.jl")

""" Given fixed polarision, frequency, incidence and heights,
links the backscattering to the diameters of flakes and density of snow """
struct SnowPackModel <: Models.Model
    model::BackScatteringModel
    frequency::Float64 # in Ghz
    theta0d::Float64 # in degrees
    thickness::Vector{Float64} # in m

    min_diameter::Float64 # in mm
    max_diameter::Float64 # in mm 
end

Models.label(m::SnowPackModel) = "Snow pack backscattering"
Models.get_L(m::SnowPackModel) = 2 * length(m.thickness)
Models.get_D(m::SnowPackModel) = 1

# x is the concatenation of diameters and density
function Models.f!(m::SnowPackModel, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T
    NL = length(x) ÷ 2
    diameters = 1e-3 * ((m.max_diameter - m.min_diameter) * x[1:NL] .+ m.min_diameter) # from [0,1] to [min mm, max mm]
    density = x[NL + 1:2 * NL]
    s, _ = snow_backscattering(m.model, m.frequency, m.theta0d, diameters, density, m.thickness)
    out[1] = s
end

# diameters range : [0.2mm, 1mm], density range : [0,1]
function Models.to_physical(m::SnowPackModel, x::VecOrSub{T}, cov::VecOrSub{T}) where T
    NL = length(x) ÷ 2
    out_x, out_cov = copy(x), copy(cov)
    out_x[1:NL] = 1e-3 * ((m.max_diameter - m.min_diameter) * x[1:NL] .+ m.min_diameter)
    out_cov[1:NL] .*= (1e-3 * (m.max_diameter - m.min_diameter))^2
    out_x, out_cov
end


""" Inverse to `to_physical` """
function Models.from_physical(m::SnowPackModel, x::VecOrSub{T}, cov::VecOrSub{T}) where T
    NL = length(x) ÷ 2
    out_x, out_cov = copy(x), copy(cov)
    out_x[1:NL] = 1e+3 * (x[1:NL] .- m.min_diameter) / (m.max_diameter - m.min_diameter)
    out_cov[1:NL] ./= (1e-3 * (m.max_diameter - m.min_diameter))^2
    out_x, out_cov
end

""" Handle full covariance """
function Models.from_physical(m::SnowPackModel, x::VecOrSub{T}, cov::FullCov{T}) where T
    NL = length(x) ÷ 2
    out_x, out_cov = copy(x), copy(cov)
    out_x[1:NL] = 1e+3 * (x[1:NL] .- m.min_diameter) / (m.max_diameter - m.min_diameter)
    out_cov.data[1:NL, 1:NL] ./= (1e-3 * (m.max_diameter - m.min_diameter))^2
    out_x, out_cov
end

""" Array of variable names """
Models.variables(m::SnowPackModel) = [
    ["D$i" for i in 1:get_L(m) ÷ 2]...,
    ["rho$i" for i in 1:get_L(m) ÷ 2]...,
]

""" Aggregate D scalar SnowPackModel """
struct SnowPackModelVector <: Models.Model 
    models::Vector{SnowPackModel}
end

Models.label(m::SnowPackModelVector) = "Snow pack backscatterings"
Models.get_L(m::SnowPackModelVector) = 2 * length(m.models[1].thickness)
Models.get_D(m::SnowPackModelVector) = length(m.models)

# x is the concatenation of diameters and density
function Models.f!(m::SnowPackModelVector, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where T
    NL = length(x) ÷ 2
    diameters = 1e-3 * ((m.max_diameter - m.min_diameter) * x[1:NL] .+ m.min_diameter) # from [0,1] to [min mm, max mm]
    density = x[NL + 1:2 * NL]
    for d in 1:D
        subm = m.models[d]
        s, _ = snow_backscattering(subm.model, subm.frequency, subm.theta0d, diameters, density, subm.thickness)
        out[d] = s
    end
    end

# diameters range : [0.2mm, 1mm], density range : [0,1]
function Models.to_physical(m::SnowPackModelVector, x::VecOrSub{T}, cov::VecOrSub{T}) where T
   Models.to_physical(m.models[1], x, cov)
end


""" Inverse to `to_physical`: density is in [0,1], diameters in [0.2mm, 1mm] """
function Models.from_physical(m::SnowPackModelVector, x::VecOrSub{T}, cov::VecOrSub{T}) where T
    Models.from_physical(m.models[1], x, cov)
end
function Models.from_physical(m::SnowPackModelVector, x::VecOrSub{T},  cov::FullCov{T}) where T
    Models.from_physical(m.models[1], x, cov)
end



""" Array of variable names """
Models.variables(m::SnowPackModelVector) = Models.variables(m.models[1])
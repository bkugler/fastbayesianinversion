# This script generate a test file used to check the implementation of the forward model
using JSON

include("backscattering.jl")

N_sample = 100

function generate_test_set(model, freq, incidence)
    out = []
    for n in 1:N_sample
        NL = rand(1:10)
        thickness_orig = Int.(round.(1 .+ 20 * rand(NL))) # in cm
        heights = reverse(cumsum(thickness_orig))
        thickness = thickness_from_height(heights)
        densities = rand(NL) # in [0,1]
        grain_sizes = 1e-3 * (0. .+ (4 * rand(NL) )) # in [0mm, 4mm], expressed in meters
        sigma, _  = snow_backscattering(model, freq, incidence,  grain_sizes,  densities, thickness)
        @assert isfinite(sigma)
        data = Dict(
            "heights" => heights,
            "thickness" => thickness,
            "densities" => densities,
            "grain_sizes" => grain_sizes,
            "sigma" => sigma
            )
        push!(out, data)
    end
    out
end

out = []
for freq in [10.2, 13.3, 16.7]
    for incidence in [30,40,50,60]
        model_vv = BackScatteringModel{PolarVV}(0.8, 0.1, 0.8, 0.07)
        model_hh = BackScatteringModel{PolarHH}(0.8, 0.1, 0.8, 0.07)

        push!(out, Dict(
            "frequency" => freq,
            "incidence" => incidence,
            "polar" => "VV",
            "dataset" => generate_test_set(model_vv, freq, incidence)
        ))

        push!(out, Dict(
            "frequency" => freq,
            "incidence" => incidence,
            "polar" => "HH",
            "dataset" => generate_test_set(model_hh, freq, incidence)
        ))

    end
end

write("playground/assimilation/snowpack/test_snowpack_model.json", JSON.json(out))
using Dates:length, include, isfinite
# assimilation of the data from NOSREX measurements ()
using FastBayesianInversion.Store
using FastBayesianInversion.Gllim
using FastBayesianInversion.Shared
using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Is


include("model.jl")
include("../../env.jl")
include("mcmc.jl")
include("imis.jl")
include("../../plotting/base_plots.jl")

using Dates 
using JSON 
using Plots 
using LaTeXStrings

Plots.pyplot()
Log.init()

struct RadarObs{T <: PolarModele} # only HH or VV are supported
    frequency::Float64 # Ghz
    incidence::Int
    sigma_obs::Float64
    sigma_std::Float64
end

polar(::RadarObs{PolarHH}) = PolarHH
polar(::RadarObs{PolarVV}) = PolarVV

struct SnowpackProfil
    date::Dates.Date
    thickness::Vector{Float64} # m
    grain_size::Vector{Float64} #  mm, without diameter correction
    density::Vector{Float64} # in [0,1]
    temperature::Float64 # averaged, in Kelvin
    observations::Vector{RadarObs}
end

function select_obs(sp::SnowpackProfil, freq::Float64, inc::Int, pol)::RadarObs
    for obs in sp.observations 
        if obs.frequency == freq && obs.incidence == inc && polar(obs) == pol
            return obs
        end 
    end
    error("observation not found for $freq $inc $pol")
end

function _load_snowpack_data()
    file = ROOT_DATA_PATH * "SNOWPACK/NOSREX-II-2010-2011.json"
    tmp = JSON.parsefile(file)

    out::Vector{SnowpackProfil} = []
    for (i, profil) in enumerate(tmp)
        thickness = profil["thickness"] ./ 100 # cm to m
        if length(thickness) == 0 
            @warn "Skipping empty observation $i"
            continue
        end

        temperature = 273.15 + (sum(profil["temperature"]) / length(profil["temperature"]))
        date = Date(profil["date"], "y-m-d")
        radars = profil["radars"]
        obss::Vector{RadarObs} = []
        for radar in radars 
            T = PolarModele

            if radar["polar"] == "VV"
                T = PolarVV
            elseif radar["polar"] == "HH"
                T = PolarHH
            else 
                continue
            end

            obs = RadarObs{T}(
                radar["frequency"] / 1_000_000_000,
                radar["incidence"],
                10^(radar["sigma"] / 10),
                10^(radar["stdev"] / 10)
            )
            push!(obss, obs)
        end

        push!(out, SnowpackProfil(
            date,
            thickness,
            profil["grain_size"],
            profil["density"],
            temperature,
            obss
        ))
    end

    out
end

datas = _load_snowpack_data()
correction_diametre = 2.6 

function _plot_input(datas::Vector{SnowpackProfil})
    # datas = datas[6:end - 1] 
    dates = [s.date for s in datas]
    freq = 10.2
    incidence = 30 
    obs = [10 * log10(select_obs(s, freq, incidence).sigma_obs) for s in datas]
    
    # compute the initial predicted backscattering
    bm = BackScatteringModel{PolarVV}(0.8, 0.1, 0.8, 0.07)
    obs_init = []
    for s::SnowpackProfil in datas 
        sigma, _  = snow_backscattering(bm, freq, incidence, s.grain_size * 1e-3 / correction_diametre,  s.density, s.thickness)
        push!(obs_init, 10 * log10(sigma))
    end

    p = scatter(dates, obs; xlabel="Date", ylabel=L"\sigma_0 \; (dB)", label="Observation", markershape=:rect, title="Frequency $freq Ghz - Incidence $incidence ° - Polar VV", color=:red, size=(700, 400))
    scatter!(p, dates, obs_init, label="Initial reconstruction", markershape=:rect, color=:blue)
    p
end

# _plot_input(datas)

# assess the grain size range
minimum(minimum(s.grain_size / correction_diametre) for s in datas)
maximum(maximum(s.grain_size / correction_diametre) for s in datas)
min_diameter, max_diameter = 0, 2 # mm

st = Store.Storage(ROOT_PATH_STORE)

# assimilation at one frequency, one incidence and one polarisation
# params is either noting, ImisParams or MHParams
function exp1(datas::Vector{SnowpackProfil}, params, freq, incidence, pol, run::Int)
    bm = BackScatteringModel{pol}(0.8, 0.1, 0.8, 0.07)
    Y_cov = 0.0000036
    std_train = 0.000001
    Y_Sigma = Diagonal((std_train^2 .+ Y_cov) * ones(1))

    scheme = typeof(params)
    if run >= 1
        out = []
        for (i, input::SnowpackProfil) in enumerate(datas)
            @info "Input $i"

            context = SnowPackModel(bm, freq, incidence, input.thickness, min_diameter, max_diameter)
            D = Models.get_D(context)

            # setup
            x0 = [
                (input.grain_size / (correction_diametre * 1000))..., # in metters
                (input.density)...,
            ]
            sigma_obs = select_obs(input, freq, incidence, pol).sigma_obs
            covX::FullCov{Float64} = guess_profile_covariance(input.thickness, 0.001, 0.08)
            
            x0, covX = Models.from_physical(context, x0, covX)

            yobs = [sigma_obs]
            # covY::FullCov{Float64} = Symmetric(0.03 * ones(D, D)) # from the article
            covY::FullCov{Float64} = Symmetric(Y_cov * ones(D, D)) # from the matlab code
    
            if scheme in [Nothing,Is.ImisParams]
                gp = Gllim.Params(
                    Gllim.FunctionnalInitParams(100, Models.closure(context)),
                    Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(
                        Gllim.DefaultStoppingParams(150, 1e-5),
                        1e-12
                    )
                )
        
                trained_gllim, _, dura = Shared.generate_train_gllim(context, Shared.TrainingParams(
                    100_000, 0.000001, gp
                ), st, run >= 2)
                if run >= 2
                    @info "trained in $dura"
                end

                trained_gllim = Gllim.add_bias(trained_gllim, 1., 0., covY)
                gllim_star = Gllim.inversion(trained_gllim)
                weights, means = Gllim.conditionnal_density(gllim_star, yobs)
                covs = gllim_star.Σ
    
                w2, m2, Sigma2 = Gllim.conditionnal_density_with_prior(weights, means, covs, x0, covX)
               
                if scheme == Nothing
                    x_pred = Probas.mean_mixture(w2, m2)
                else 
                    proposition = setup_proposition(w2, m2, Sigma2)
                    model = IsMean(proposition, context, yobs, Y_Sigma, x0, covX)
                    is_pred, _ = Is.importance_sampling(model, params, Is.NonUniformityParams(5))
                    x_pred = is_pred.mean
                end
            elseif scheme == ParamsMH
                x_pred = posterior_mean(context, yobs, Y_Sigma, x0, covX, params)
                x_pred, _ = Models.to_physical(context, x_pred, similar(x_pred))
            else 
                error("invalid scheme $scheme")
            end

            x_pred, _ = Models.to_physical(context, x_pred, similar(x_pred))
            push!(out, x_pred)
        end
        Store.save(st, out, Store.FO_PREDICTIONS, :assim_snow_pack1,  params, datas, freq, incidence, pol, Y_cov)
    end 
    out = Store.load(st, Store.FO_PREDICTIONS, :assim_snow_pack1,  params, datas, freq, incidence, pol, Y_cov)
end

# compute the backscattering (through the direct model) for the assimilated profils
# for the 4 incidences
function export_reconstruction(datas::Vector{SnowpackProfil}, assims::Vector, freq_assim::Number, incidence_assim::Int, polar_assim::Type{T}; save=false)where T <: PolarModele
    bm = BackScatteringModel{polar_assim}(0.8, 0.1, 0.8, 0.07)
    out = []    
    incidences = [30, 40, 50, 60]
    for (profil::SnowpackProfil, output) in zip(datas, assims)
        radars = []
        NL = length(profil.thickness)
        for incidence in incidences
            sigma, _  = snow_backscattering(bm, freq_assim, incidence, output[1:NL],  output[NL + 1:2 * NL], profil.thickness)
            if sigma < 0 
                @warn "Sigma negative $sigma => replacing by 0"
                sigma = 0
            end
            push!(radars, Dict(
                "frequency" => freq_assim,
                "incidence" => incidence,
                "polar" => polar(bm),
                "sigma" => 10 * log10(sigma)
            ))
        end

        push!(out,Dict(
            "date" => profil.date,
            "radars" => radars,
            "distance_grain_size" =>  norm(profil.grain_size .- output[1:NL] * 1000),
            "distance_density" => norm(profil.density .- output[NL + 1:2 * NL]),
        ))
    end

    if save 
        path = Store.get_path(st, Store.FO_PREDICTIONS)
        path = path * "Assim_$(polar(bm))_$(freq_assim)GHz_$(incidence_assim)deg_Simul_30_40_50_60deg.json"
        write(path, JSON.json(out))
        @info "JSON exported in $path"
    end 
    
    out
end

function reconstruction_score(datas::Vector{SnowpackProfil}, outs::Vector) 
    score = 0 
    for (data, out) in zip(datas, outs) 
        radars = out["radars"]
        mask_valid = [ isfinite(radar["sigma"]) for radar in radars ]
        if sum(mask_valid) == 0 
            continue
        end
        radars = radars[mask_valid]
        for radar in radars
            yobs = select_obs(data, radar["frequency"], radar["incidence"], radar["polar"] == "HH" ? PolarHH : PolarVV).sigma_obs
            yobs = 10 * log10(yobs)
            score += abs(yobs - radar["sigma"]) / length(radars)
        end
    end
    score /= length(datas)
    return score
end

function plot_assim_data(datas::Vector{SnowpackProfil}, xmeans::Vector)
    subplots = []
    for (i, profil) in enumerate(datas)
        NL = length(profil.thickness)
        heights = reverse(cumsum(profil.thickness) * 100)
        pbar = bar([0.75], hcat(heights)'; c=:grey77, ylabel="layers height (cm)", labels=hcat(["Layer $j" for j in 1:NL]...), legend=:none, xticks=:none)
    # cov =  guess_profile_covariance(thickness, 0.1, 0.08)
    # @show eigvals(cov[1:NL, 1:NL])
    # @show eigvals(cov[(NL + 1):2 * NL, (NL + 1):2 * NL])
        output = xmeans[i]
        data::Matrix{Float64} = hcat(profil.grain_size / correction_diametre, output[1:NL] * 1000, profil.density, output[NL + 1:2 * NL])
        labels = [L"$d_{prior}$ (mm)" L"$d_{assim}$ (mm)" L"$\rho_{prior}$ (in [0, 1])" L"$\rho_{assim}$ (in [0, 1])"]
        markers = [:star :star :rect :rect]
        styles = [:solid :dash :solid :dash]
        colors = [:blue :blue :red :red]
        p = plot(1:NL, data,  xlabel="$NL layers (measure day $(profil.date))", labels=labels, markers=markers, c=colors, style=styles, alpha=0.5)
        push!(subplots, plot(pbar, p, layout=grid(1, 2, widths=[0.2, 0.8])))
    end

    p = plot(subplots..., size=(2000, 2000))
    path = Store.get_path(st, Store.FO_GRAPHS, datas; label="profils")
    png(p, path)
    @info "Saved in $path"
end

function plot_backscattering(freq, inc, pol, datas::Vector{SnowpackProfil}, exported, metas, label)
    # datas, assims = datas[2:end - 1], assims[2:end - 1]
    dates = [s.date for s in datas]
    bm = BackScatteringModel{pol}(0.8, 0.1, 0.8, 0.07)

    incidences = [
        30, 
    40,
     50, 60
    ]
    colors = [:red, :blue, :magenta, :green]
    p = plot(;  size=(1200, 800), title="Frequency $freq Ghz - Inc $inc - Polar $(polar(bm))", legendfontsize=16)
    for (j, incidence)  in enumerate(incidences)
        # select the observation
        obs = [10 * log10(select_obs(s, freq, incidence, pol).sigma_obs) for s in datas]

        # compute the initial predicted backscattering
        obs_init = []
        for s::SnowpackProfil in datas 
            sigma, _  = snow_backscattering(bm, freq, incidence, s.grain_size * 1e-3 / correction_diametre,  s.density, s.thickness)
            push!(obs_init, 10 * log10(sigma))
        end

        # compute the reconstruction of the backscattering after assimilation
        obs_assim = []
        for output in exported
            for radar in output["radars"]
                if radar["incidence"] == incidence
                    push!(obs_assim, radar["sigma"])
                    break
                end
            end
        end

        common_opts = Dict(
            :color => colors[j],
             :markeralpha => 0.3, :markerstrokewidth => 0, :ylims => (-30, 0))
        plot!(p, dates, obs; xlabel="Date", ylabel=L"\sigma_0 \; (dB)", label="Observation (inc $incidence)", linestyle=:dash,markershape=:cross,common_opts...)
        # plot!(p, dates, obs_init; label="Initial simulation (inc $incidence)", markershape=:star, common_opts...)
        plot!(p, dates, obs_assim; label="Simulation after assim. (inc $incidence)", markershape=:rect, common_opts...)
    end
    label = label * "_assim_reconstruction_$(polar(bm))_$(freq)GHz_inc$(inc).png"
    path = Store.get_path(st, Store.FO_GRAPHS, datas, metas; label=label)
    png(p, path)
    @info "Saved in $path"
end

function plot_snow_profiles(datas::Vector{SnowpackProfil}, xmeans::Vector, metas, label)
    # profiles 
    subplots = []
    datas = datas[4:5]
    xmeans = xmeans[4:5]
    for (i, profil) in enumerate(datas)
        NL = length(profil.thickness)
        heights = reverse(cumsum(reverse(profil.thickness)) * 100)
        pbar = bar([0.75], hcat(heights)'; c=:grey77, ylabel="layers height (cm)", labels=hcat(["Layer $j" for j in 1:NL]...), legend=:none, xticks=:none)
            # cov =  guess_profile_covariance(thickness, 0.1, 0.08)
            # @show eigvals(cov[1:NL, 1:NL])
            # @show eigvals(cov[(NL + 1):2 * NL, (NL + 1):2 * NL])
        output = xmeans[i]
        data::Matrix{Float64} = hcat(profil.grain_size, output[1:NL] * 1000, profil.density, output[NL + 1:2 * NL])
        labels = [L"$d_{prior}$ (mm)" L"$d_{assim}$ (mm)" L"$\rho_{prior}$ (in [0, 1])" L"$\rho_{assim}$ (in [0, 1])"]
        markers = [:star :star :rect :rect]
        styles = [:solid :dot :solid :dot]
        colors = [:blue :blue :red :red]
        p = plot(1:NL, data,  xlabel="$NL layers (measure day $(profil.date))", labels=labels, markers=markers, c=colors, style=styles, alpha=0.5, background_color_legend=Config.LEGEND_BACKGROUND)
        push!(subplots, plot(pbar, p, layout=grid(1, 2, widths=[0.2, 0.8])))
    end
    Nrow = Int(round(sqrt(length(datas))))
    Ncol = Int(ceil(length(datas) / Nrow))
    push!(subplots, [ plot() for _ in 1:(Nrow * Ncol - length(datas))]...)
    p = plot(subplots..., layout=(Nrow, Ncol), size=(Ncol * 400, Nrow * 300))
    path = Store.get_path(st, Store.FO_GRAPHS, :assim_snow_pack, datas, metas; label=label * "_profils")
    png(p, path)
    @info "Plotted in $path"
end


function test()
    freq = 10.2
    incidence = 30
    pol = PolarVV

    preds_gllim = exp1(datas, nothing, freq, incidence, pol, 1)

    params_imis = Is.ImisParams(2000, 500, 20)
    preds_imis = exp1(datas, params_imis, freq, incidence, pol, 0)

    # params_mcmc = ParamsMH(10_000_000, 5_000_000, 0.05)
    # preds_mcmc = exp1(datas, params_mcmc, freq, incidence, pol, 0)

    # plot_assim_data(datas, preds)
    plot_backscattering(freq, incidence, pol, datas, export_reconstruction(datas, preds_gllim, freq, incidence, pol), nothing, "GLLiM")
    plot_backscattering(freq, incidence, pol, datas, export_reconstruction(datas, preds_imis, freq, incidence, pol), params_imis, "IMIS")
    # plot_backscattering(freq, incidence, pol, datas, export_reconstruction(datas, preds_mcmc, freq, incidence, pol), params_mcmc, "MCMC")

    plot_snow_profiles(datas, preds_gllim, nothing, "GLLiM")
    plot_snow_profiles(datas, preds_imis, params_imis, "IMIS")
    # export_reconstruction(datas, preds_imis, freq, incidence, pol)
end
test()

function import_plot_backstracttering(freq, incidence, pol, filepath)
    obs = JSON.parsefile(filepath)
    @show length(obs)
    plot_backscattering(freq, incidence, pol, datas[6:end - 1], obs, :gradient, "Gradient")
end

# import_plot_backstracttering(13.3, 30, PolarVV, "/Users/kuglerb/Documents/docs/DATA/SNOWPACK/assim_gradient/Assim_VV_13.3GHz_30deg_Simul_30_40_50_60deg.json")

function run_one_imis(freq, incidence, pol)
    params_imis = Is.ImisParams(2000, 500, 20)
    preds_imis = exp1(datas, params_imis, freq, incidence, pol, 0)

    # plot_backscattering(freq, incidence, pol, datas, preds_imis, params_imis, "IMIS")

    res = export_reconstruction(datas, preds_imis, freq, incidence, pol; save=true)

    # plot_backscattering(freq, incidence, pol, datas, res, params_imis, "IMIS")

    return reconstruction_score(datas, res)
end 

function run_assims()
    scores = []
    # for freq in [10.2, 13.3, 16.7]
    for freq in [10.2, 13.3, 16.7]
        for incidence in [30, 40, 50, 60]
            for pol in [PolarVV, PolarHH]
                score = run_one_imis(freq, incidence, pol)
                push!(scores, Dict(
                    "frequency" => freq,
                    "incidence" => incidence,
                    "polar" => pol == PolarVV ? "VV" : "HH",
                    "error" => score
                ))
            end
        end
    end

    path = Store.get_path(st, Store.FO_PREDICTIONS)
    path = path * "assim_errors_reconstruction.json"
    write(path, JSON.json(scores, 2))
    @info "JSON scores exported in $path"
end

run_assims()

using KissMCMC
using FastBayesianInversion.Probas

struct ParamsMH 
    Niter::Int
    Nkeep::Int
    alpha::Float64

    function ParamsMH(Niter, Nkeep, alpha) 
        Niter >= Nkeep || error("Number of iterations must be greater than samples to keep.")
        new(Niter, Nkeep, alpha)
    end
end

function posterior_mean(c::Models.Model, y::Vector{T}, Sigma::DiagCov{T}, x0::Vector, Gamma::FullCov, params::ParamsMH) where T <: Number
    L = Models.get_L(c)

    # target density
    logpdf(x::Vector{T}) = Models.log_conditionnal_density(c, x, y, Sigma) + Probas.log_gaussian_density(x, x0, cholesky(Gamma).L, L)
    # samples the proposal (or jump) distribution
    sample_prop_normal(theta::Vector{T}) = params.alpha * randn(T, L) .+ theta
    # starting points
    theta0 = x0

    thetas, accept_ratio = KissMCMC.metropolis(logpdf, sample_prop_normal, theta0,
        niter=params.Niter, use_progress_meter=false, nburnin=params.Niter - params.Nkeep)
    # @show accept_ratio
    points =  hcat(thetas...)
    x_pred = (sum(points; dims=2) ./ params.Nkeep)[:, 1]
    x_pred
end

"""
Median filter over mean and stds neighbours, variable per variable
`c` and `Yobs` are needed for the reconstuction error 
"""
function median_filter(serie::Pred.Prediction, c::Models.Models.Model, Yobs::Matrix, config::Pred.SerieConfig; width=1)::Pred.Prediction
    L, N = size(serie.Xmean)
    withCov = serie.Xcovs !== nothing
    outMean, outCovs = copy(serie.Xmean), withCov ? copy(serie.Xcovs) : zeros(L, N)

   # border values are left unchanged

    for n in 1 + width:N - width
        for l in 1:L 
            outMean[l, n] = Statistics.median(outMean[l, n - width:n + width]) # 2*width + 1 points window
            if withCov
                outCovs[l, n] = Statistics.median(outCovs[l, n - width:n + width]) # 2*width + 1 points window
            end
        end
    end

   # reconstruction error 
    Yerr = Y_relative_err(c, Yobs, Models.from_physique(c, outMean), 1., zeros(c.D))
    return Pred.Prediction(outMean, withCov ? outCovs : nothing ; Yerr=Yerr, config=config)
end

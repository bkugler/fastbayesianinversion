# This file compares two approaches for GLLiM initialisation:  a traditionnal one (random->k-means->GMM)
# and a functionnal one (using the context)
using Plots
using LaTeXStrings

using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Gllim
using FastBayesianInversion.Gmms
using FastBayesianInversion.Models
using FastBayesianInversion.Perf
using FastBayesianInversion.Store

include("hapke_models/models.jl")
include("plotting/base_plots.jl")

st = Store.Storage(ROOT_PATH_STORE)

Log.init()
Plots.pyplot()

""" Results over the iteration """
struct Res
    log_ll::Vector{Float64} # likehood
    sigma_error::Vector{Float64} # trace Sigma
    F_error::Vector{Float64} # average error || F(x) - F_train(x)||
end

struct Exp
    K::Int
    n_iters::Vector{Int}
    gmm::Res
    func::Res
end

function _perf(c::Models.Model, Ntrain::Int, X, Y, init_params::Gllim.InitParams, n_iters::Vector{Int})
    D, _ = size(Y)
    Log.indent()
    log_rnk, log_ll = Gllim.initialisation(X, Y, init_params)
    Log.deindent()

    out = Res([], [], [])
    push!(out.log_ll, log_ll / Ntrain)

    @debug "Additionnal training after init..."
    Log.indent()
    for n_iter in n_iters
        @debug "n_iter = $n_iter"
        Log.indent()
        params = Gllim.TrainParams{Float64,FullCov{Float64},FullCov{Float64}}(Gllim.DefaultStoppingParams(n_iter, -1), 1e-12)
        gllim, log_lls = Gllim.train(log_rnk, X, Y, params)

        sigma = Perf.sigma_error(gllim) / D # comparable to the value of a uniform cov
        F_error = Perf.direct_error(c, gllim)

        push!(out.log_ll, log_lls[end] / Ntrain)
        push!(out.sigma_error, sigma)
        push!(out.F_error, F_error)

        Log.deindent()
    end
    Log.deindent()

    out
end

function run_perf(c::Models.Model, run::Bool)
    if !run 
        return Store.load(st, Store.FO_BENCHMARK, :compare_init, c)
    end

    Ntrain, Ystd= 50_000, 0.001
    # we generate one training dataset, used by both methods
    X, Y = Models.data_training(c, Ntrain, Ystd)

    n_iters = [1, 5, 10, 30, 50]
    Ks = [30, 60]
    out::Vector{Exp} = []
    for K in Ks
        @info "K = $K"

        scheme_gmm = Gllim.MultiInitParams(10, Gmms.EmParams(K, 30, 10, 1e-12))
        scheme_func = Gllim.FunctionnalInitParams(K, Models.closure(c))

        res_gmm = _perf(c, Ntrain, X, Y, scheme_gmm, n_iters)
        res_func = _perf(c, Ntrain, X, Y, scheme_func, n_iters)

        push!(out, Exp(K, n_iters, res_gmm, res_func))
    end
    Store.save(st, out, Store.FO_BENCHMARK, :compare_init, c)
    out
end

function plot_perf(out::Vector{Exp}, savepath=nothing)
    p_ll = plot(ylabel="Log-likehood", xlabel="iterations")
    p_sigma = plot(ylabel=L"Average trace of $\Sigma$", xlabel="iterations")
    p_direct = plot(ylabel=L"Average direct error $||F(x) - F_{est}(x)||$", xlabel="iterations")

    for exp in out
        plot!(p_ll, [0, exp.n_iters...], exp.gmm.log_ll, label="Gmm - K = $(exp.K)", markershape=:rect, markersize=4) # add initial likehood before any training
        plot!(p_ll, [0, exp.n_iters...], exp.func.log_ll, label="Func - K = $(exp.K)", color=last_color(p_ll), markershape=:star, markersize=4)

        plot!(p_sigma, exp.n_iters, exp.gmm.sigma_error, label="Gmm - K = $(exp.K)", markershape=:rect, markersize=4)
        plot!(p_sigma, exp.n_iters, exp.func.sigma_error, label="Func - K = $(exp.K)", color=last_color(p_ll), markershape=:star, markersize=4)

        plot!(p_direct, exp.n_iters, exp.gmm.F_error, label="Gmm - K = $(exp.K)", markershape=:rect, markersize=4)
        plot!(p_direct, exp.n_iters, exp.func.F_error, label="Func - K = $(exp.K)", color=last_color(p_ll), markershape=:star, markersize=4)
    end
    p = plot(p_ll, p_sigma, p_direct, dpi=200, size=(1300, 1000))

    if p !== nothing
        png(p, savepath)
        @debug "saved in $savepath"
    end
    p
end

c = HapkeNontronite()
# c = HapkeHowardite()
savepath = Store.get_path(st, Store.FO_GRAPHS, c, :init)
out = run_perf(c, false)
plot_perf(out, savepath)
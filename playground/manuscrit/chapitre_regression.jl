using LinearAlgebra
include("shared.jl")

function double_solutions_simple(run::Int)
    context = Models.DoubleSolutions(1)
    Nobs =  100
    obs_std_ratio = 100
    K = 60
    train_std = 0.01
    params_is = Pipeline.ProcedureParams(
        Shared.TrainingParams(
            10_000,
            train_std,
            GllimParams(K)
        ),
        InversionParams(train_std, Is.IsParams(100)), 
    )

    Xobs, Yobs = Models.data_observations(context, Nobs)
    # Yobs .+= 0.001 * randn(size(Yobs))
    Xobs_alt = Models.alternative_solution(context, Xobs)

    Yobs_std = Yobs ./ obs_std_ratio


    preds, _, _, _ = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)

    # before regularization
    Xcentroid1 = deepcopy(preds[Schemes.GllimMergedCentroid(1)])
    Xcentroid2 = deepcopy(preds[Schemes.GllimMergedCentroid(2)])

    reg_meths = [ Schemes.GllimMergedCentroid("")]

    Pipeline.regularize_inversions!(preds, reg_meths)

    # plot 1 : Gllim + sampling
    plot_data = [
        # preds[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.8, :solid, hide_std=true),
        # preds[Schemes.IsMean] => PlotOptions(N.IsMean, "yellow", 0.7, :dash, hide_std=true),
        # preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "orange", 0.7, :dash, hide_std=true),
        Xcentroid1 => PlotOptions(N.GllimMergedCentroid1 * " before regularization", "blue", 0.4, :dash, hide_std=true),
        Xcentroid2 => PlotOptions(N.GllimMergedCentroid2 * " before regularization", "blue", 0.4, :dash, hide_std=true),
        preds[Schemes.GllimMergedCentroid(1)] => PlotOptions(N.GllimMergedCentroid1 * " after regularization", "red", 0.3, :solid, hide_std=true, markersize=4),
        preds[Schemes.GllimMergedCentroid(2)] => PlotOptions(N.GllimMergedCentroid2 * " after regularization", "red", 0.3, :solid, hide_std=true, markersize=4),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xalt1, "green", 0.3, :solid, hide_std=true, markersize=0),
        Shared.MethodResult(Xobs_alt) => PlotOptions(N.Xalt2, "green", 0.3, :solid, hide_std=true, markersize=0),
    ]
    path = PATH_GRAPHS_MANUSCRIT * "lissage_centroides"
    plot_components(plot_data;  savepath=path, varnames=[L"x"], xlabel=L"n")
end

"""
 use infinite norm so error stays in [0,1] and ignore nan values
"""
function _err(pred, ref)
    err = maximum(abs.(pred .- ref); dims=1)
    Statistics.mean(err[isfinite.(err)])
end

# Xobs is in math space, to have comparable result accross contexts
function _average_error(res::Shared.MethodResult, context::Models.Model, Xobs::Matrix)
    # x error
    Xpred = res.Xmean
    # we normalise to have comparable values (in [0,1])
    Xpred = Models.from_physical(context, Xpred)
    err_X, _  = _err(Xpred, Xobs)
            
    # residual error
    err_Ypred, _  = _safe_err(res.Yerr)
    
    err_X, err_Ypred
end

function _plot_comparaison_is(err_Xs::Matrix, err_Ys::Matrix, times::Matrix, labels::Matrix, markershape::Matrix, exp_label::String)
    exp_label = lowercase(replace(replace(exp_label, " " =>""), "," => "-"))

    _, Nexp = size(err_Xs)

    # plot 1 : X errors
    p1 = scatter(1:Nexp, err_Xs'; labels=labels, markershape=markershape, markersize=6, yscale=:log10, xlabel="Experience index", ylabel=L"Error on $\mathbf{x}$", legendfontsize = 10)

    # plot 2 : Y errors
    p2 = scatter(1:Nexp, err_Ys'; labels=labels, markershape=markershape,markersize=6, yscale=:log10, xlabel="Experience index", ylabel=L"Reconstruction error $R(\mathbf{x})$", legendfontsize = 10)

    # plot 3 : times
    p3 = plot(1:Nexp, times' ./ 1000; labels=labels, yscale=:log10, xlabel="Experience", ylabel="Computation time (sec.)", legendfontsize = 10)

    p = plot(p1, p2, p3, layout=(1, 3),  size=(1500, 500))
    path = PATH_GRAPHS_MANUSCRIT * "comparaison_is_$(exp_label)_1"
    png(p, path)
    @info "Plotted in $path"


    # plot 1 : X errors / time 
    p1 = scatter(times' ./ 1000, err_Xs'; labels=labels, markershape=markershape, markersize=6, yscale=:log10, xlabel="Computation time (sec.)", ylabel=L"Error on $\mathbf{x}$", legendfontsize = 10)

    # plot 2 : Y errors / time
    p2 = scatter(times' ./ 1000, err_Ys'; labels=labels, markershape=markershape,markersize=6, yscale=:log10, xlabel="Computation time (sec.)", ylabel=L"Reconstruction error $R(\mathbf{x})$", legendfontsize = 10)

    p = plot(p1, p2,  size=(1500, 500))
    path = PATH_GRAPHS_MANUSCRIT * "comparaison_is_$(exp_label)_2"
    png(p, path)
    @info "Plotted in $path"
end


# compare x_err, y_err and computation time 
function comparison_importance_sampling_mean(context::Models.Model, run::Int)
    obs_std_ratio = 50

    # one learning step is enough for all the parameters
    train_std = 0.001
    training_params = TrainingParams(train_std, 50)
    Shared.generate_train_gllim(context, training_params, st, run >= 2)
    
    params_is = [Is.IsParams(Ns) for Ns in 100 .* collect(1:5:100)]
    #  Ns / 10 + 9 * Ns / 10
    params_imis1 = [Is.ImisParams(p.N_is ÷ 10, p.N_is ÷ 20, 18) for p in params_is]
    params_imis2 = [Is.ImisParams(p.N_is ÷ 20, p.N_is ÷ 40, 9) for p in params_is]

    Xobs_math, Yobs = Models.data_observations(context, 100)
    Yobs_std = Yobs ./ obs_std_ratio

    Nexp = length(params_is)
    err_Xs, err_Ys, times = zeros(4, Nexp), zeros(4, Nexp), zeros(4, Nexp)
    for (i, sampling) in enumerate(params_is)
        @info "IS parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y = _average_error(preds[Schemes.IsMean], context, Xobs_math)
        err_X_gllim, err_Y_gllim = _average_error(preds[Schemes.GllimMean], context, Xobs_math)

        err_Xs[1, i] = err_X_gllim
        err_Xs[2, i] = err_X

        err_Ys[1, i] = err_Y_gllim
        err_Ys[2, i] = err_Y

        times[1, i] = Dates.value(ct.gllim_only_prediction)
        times[2, i] = Dates.value(ct.prediction)
    end
    times[1, :] .= Statistics.mean(times[1,:]) 

    for (i, sampling) in enumerate(params_imis1)
        @info "IMIS (1) parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y = _average_error(preds[Schemes.ImisMean], context, Xobs_math)

        err_Xs[3, i] = err_X
        err_Ys[3, i] = err_Y
        times[3, i] = Dates.value(ct.prediction)
    end

    for (i, sampling) in enumerate(params_imis2)
        @info "IMIS (2) parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y = _average_error(preds[Schemes.ImisMean], context, Xobs_math)

        err_Xs[4, i] = err_X
        err_Ys[4, i] = err_Y
        times[4, i] = Dates.value(ct.prediction)
    end

    _plot_comparaison_is( err_Xs, err_Ys, times, [N.GllimMean N.IsMean N.ImisMean * " - 1" N.ImisMean * " - 2"], [:rect :circle :utriangle :utriangle], Models.label(context))
end

function _average_error_centroids(res1::Shared.MethodResult, res2::Shared.MethodResult, context::Models.Model, Xobs::Matrix, Xalt::Matrix)
    # x error:  we match centroids predictions, to either "ref" or "alt".
    X1,_  = Models.from_physical(context, res1.Xmean, res1.Xmean)
    X2,_  = Models.from_physical(context, res2.Xmean, res2.Xmean)
    _, N = size(Xobs)
    X1ref, X2ref = similar(Xobs), similar(Xobs)
    for n in 1:N
        if norm(X1[:, n] .- Xobs[:, n])  < norm(X1[:, n] .- Xalt[:,n]) # x1 is closer to Xobs
            X1ref[:, n] = Xobs[:, n]
        else
            X1ref[:, n] = Xalt[:, n]
        end

        if norm(X2[:, n] .- Xobs[:, n])  < norm(X2[:, n] .- Xalt[:,n]) # x1 is closer to Xobs
            X2ref[:, n] = Xobs[:, n]
        else
            X2ref[:, n] = Xalt[:, n]
        end
    end

    err_X = (_err(X1, X1ref) + _err(X2, X2ref)) / 2

    # residual error
    err_Ypred = (Statistics.mean(res1.Yerr[isfinite.(res1.Yerr)]) + Statistics.mean(res2.Yerr[isfinite.(res2.Yerr)])) / 2
    
    err_X, err_Ypred
end

function comparison_importance_sampling_centroids(run::Int)
    context = Models.DoubleHard()

    obs_std_ratio = 1000

    # one learning step is enough for all the parameters
    train_std = 0.0001
    training_params = TrainingParams(train_std, 50)
    Shared.generate_train_gllim(context, training_params, st, run >= 2)
    
    params_is = [Is.IsParams(Ns) for Ns in 100 .* collect(1:5:100)]
    #  Ns / 10 + 9 * Ns / 10
    params_imis1 = [Is.ImisParams(p.N_is ÷ 10, p.N_is ÷ 20, 18) for p in params_is]
    params_imis2 = [Is.ImisParams(p.N_is ÷ 20, p.N_is ÷ 40, 9) for p in params_is]

    Xobs_math, Yobs = Models.data_observations(context, 100)
    Xobs_alt = Models.alternative_solution(context, Xobs_math)

    Yobs_std = Yobs ./ obs_std_ratio


    Nexp = length(params_is)
    err_Xs, err_Ys, times = zeros(4, Nexp), zeros(4, Nexp), zeros(4, Nexp)
    for (i, sampling) in enumerate(params_is)
        @info "IS parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y = _average_error_centroids(preds[Schemes.IsCentroid(1)], preds[Schemes.IsCentroid(2)],
            context, Xobs_math, Xobs_alt)

        err_X_gllim, err_Y_gllim = _average_error_centroids(preds[Schemes.GllimMergedCentroid(1)], preds[Schemes.GllimMergedCentroid(2)],
        context, Xobs_math, Xobs_alt)


        err_Xs[1, i] = err_X_gllim
        err_Xs[2, i] = err_X

        err_Ys[1, i] = err_Y_gllim
        err_Ys[2, i] = err_Y

        times[1, i] = Dates.value(ct.gllim_only_prediction)
        times[2, i] = Dates.value(ct.prediction)
    end
    times[1, :] .= Statistics.mean(times[1,:]) 

    for (i, sampling) in enumerate(params_imis1)
        @info "IMIS (1) parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y = _average_error_centroids(preds[Schemes.ImisCentroid(1)], preds[Schemes.ImisCentroid(2)],
        context, Xobs_math, Xobs_alt)


        err_Xs[3, i] = err_X
        err_Ys[3, i] = err_Y
        times[3, i] = Dates.value(ct.prediction)
    end

    for (i, sampling) in enumerate(params_imis2)
        @info "IMIS (2) parameters $i / $Nexp"
        params = Pipeline.ProcedureParams(
            training_params,
            InversionParams(train_std, sampling),
        )
        preds, _, _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)
        err_X, err_Y =  _average_error_centroids(preds[Schemes.ImisCentroid(1)], preds[Schemes.ImisCentroid(2)],
        context, Xobs_math, Xobs_alt)

        err_Xs[4, i] = err_X
        err_Ys[4, i] = err_Y
        times[4, i] = Dates.value(ct.prediction)
    end


    labels = [L"\hat{\mathbf{x}}_{centroid}" L"\hat{\mathbf{x}}_{IS-centroid}" L"\hat{\mathbf{x}}_{IMIS-centroid}" L"\hat{\mathbf{x}}_{IMIS-centroid}"]
    _plot_comparaison_is(err_Xs, err_Ys, times, labels , [:rect :circle :utriangle :utriangle], Models.label(context))
end


function indice_dispersion(run)
    params = ProcedureParams(0.0001, 20, Is.IsParams(10_000))
    Nobs = 100

    # cas uni-modal
    context = InjectifSimple()

    Xobs, Yobs = Models.data_observations(context, Nobs)
    Yobs_std = Yobs ./ 100

    preds , variabilities, _, _  = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
    disp = [v.dispersion / v.mean_factor for v in variabilities]
    bound = ones(Nobs)
    p = plot(1:Nobs, hcat(disp, bound); labels = [L"\tilde{V}" "bound"], yscale = :log10)
    savepath = PATH_GRAPHS_MANUSCRIT * "dispersion_simple"
    png(p, savepath)
    @info "Plotted in $savepath"

    # cas bi-modal
    context = Models.DoubleSolutions(1)

    Xobs, Yobs = Models.data_observations(context, Nobs)
    Xalt = Models.alternative_solution(context, Xobs)
    Yobs_std = Yobs ./ 100

    preds , variabilities, _, _  = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
    disp = [v.dispersion for v in variabilities]

    p1 = plot(1:Nobs, disp; label = L"V_{disp}", yscale = :log10)
    p2 = plot(1:Nobs, hcat(Xobs[1,:], Xalt[1,:]); color = "green", labels = [N.Xalt1 N.Xalt2])

    # # variation of the std
    # Yobs_std = 1 ./ (Yobs .+ 0.001)

    # preds , variabilities, _, _  = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
    # disp = [v.dispersion for v in variabilities]
    # disp_not_scaled =[v.dispersion / v.mean_factor for v in variabilities]
    # @show [v.mean_factor for v in variabilities]

    # p3 = plot(1:Nobs, hcat(disp, disp_not_scaled); labels = [L"V_{disp}" L"\tilde{V}"], linestyle = [:dash :dot], alpha = 0.7, yscale = :log10)

    p = plot(p1,p2, layout = (2,1))
    savepath = PATH_GRAPHS_MANUSCRIT * "dispersion_double"
    png(p, savepath)
    @info "Plotted in $savepath"

end


function plot_example_mixture()
    allocated = zeros(1)
    target = Probas.Gmm{Float64}([0.5, 0.5], [0.3 0.7], [CholCov{Float64}(diagm(0.1 *
    ones(1))), CholCov{Float64}(diagm(0.1 *
    ones(1)))])
    full_mixture = Probas.Gmm{Float64}([0.2, 0.2, 0.3, 0.2], [0.2 0.35 0.6 0.8], [CholCov{Float64}(diagm(0.05 *
    ones(1))), CholCov{Float64}(diagm(0.06 *
    ones(1))), CholCov{Float64}(diagm(0.1 *
    ones(1))), CholCov{Float64}(diagm(0.03 *
    ones(1)))])

    xs = range(0, 1; length = 100)
    ys_target = [Probas.gmm_density([x], target, allocated) for x in xs]
    ys_full = [Probas.gmm_density([x], full_mixture, allocated) for x in xs]

    p = plot(xs, ys_target; label = "Target distribution")
    vline!(p, [0.5]; label = "Mean prediction \$\\bar{x}_G\$")
    plot!(p, xs, ys_full; label = "Full mixture", s = :dash)
    p
    png(p, "/home/bkugler/Documents/latex/slides_soutenance/images/mixture.png")
end
plot_example_mixture()



# double_solutions_simple(st, 0)
# comparison_importance_sampling_mean(Models.InjectifHard(), 0)
# comparison_importance_sampling_mean(HapkeCeres(), 2)
# comparison_importance_sampling_mean(HapkeNontronite(), 0) # conclusion is less sharp than with Ceres
# comparison_importance_sampling_centroids(1)
# indice_dispersion(0)

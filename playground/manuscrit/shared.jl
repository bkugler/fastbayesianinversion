using FastBayesianInversion.Log
using FastBayesianInversion.Probas
using FastBayesianInversion.Gmms
using FastBayesianInversion.Gllim
using FastBayesianInversion.Predictions
using FastBayesianInversion.Models
using FastBayesianInversion.Pipeline
using FastBayesianInversion.Shared
using FastBayesianInversion.Schemes
using FastBayesianInversion.Store
using FastBayesianInversion.Is

using Serialization
using Plots
using LaTeXStrings
using Dates
using Statistics
using JSON 

include("../env.jl")
include("../plotting/base_plots.jl")
include("../article/notations.jl")
include("../article/latex_tables.jl")
include("../hapke_models/models.jl")
include("simple_injectif.jl")

Log.init()
Plots.pyplot()
Config.LEGEND_BACKGROUND = Colors.RGBA(0.9, 0.9, 0.9, 0.7)
Config.DPI = 300
Config.LEGEND_FONT_SIZE = 18

st = Store.Storage(ROOT_PATH_STORE)


GllimParams(K) = Gllim.Params(
        Gllim.MultiInitParams(
            10,
            Gmms.EmParams(K, 10, 15, 1e-12)
        ) ,
       Gllim.TrainParams{Float64,FullCov{Float64},DiagCov{Float64}}(
            Gllim.DefaultStoppingParams(200, 1e-5),
            1e-12
        )
    )

""" Provides decent default values """
TrainingParams(train_std, K) =  Shared.TrainingParams(
    50_000,
    train_std,
    GllimParams(K)
)

# no centroids nor non-uniformity computations; no std increase
InversionParams(train_std, sampling::Is.SamplingParams) = Pipeline.InversionParams(
    train_std, 
    Predictions.MergingOptions(2),
    sampling,
    false,
    false,
    Is.NonUniformityParams(1),
    0
)

ProcedureParams(train_std, K::Int, sampling::Is.SamplingParams) = Pipeline.ProcedureParams(
    TrainingParams(train_std, K),
    InversionParams(train_std, sampling),
)

""" assume training is already done and don't repeat it """
_no_re_train(run::Int) = min(run, 1)
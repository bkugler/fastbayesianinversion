include("shared.jl")

function noise_synthetique(context::Models.Hapke, run::Int)
    train_params   =  TrainingParams(0.0001, 50)
    Nobs = 2000
    std_targets = [0.2, 0.03]
    std_init =  0.5
    max_iterations =   25
    D = Models.get_D(context)
    init = Noise.Bias(1., 0. * ones(D), Diagonal(std_init^2 * ones(D)))
    imis_params = Is.ImisParams(2000, 400, 20)
    noise_params = Noise.EmIsGLLiMParams(
        targets=Noise.Targets(false, false, true), 
        init=init, max_iterations=max_iterations,
        imis_params=imis_params,
    )
    params = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params)
    
    for std_target in std_targets
        synthetic = Noise.Bias(1., 0. * ones(D), Diagonal(std_target^2 * ones(D)))
        # we save the Yobs between runs
        if run >= 1
            Xobs, Yobs = Models.data_training(context, Nobs, std_target, synthetic.a, synthetic.mu)
            Store.save(st, [Xobs, Yobs], Store.FO_NOISE, context, std_target, synthetic)
        end
        Xobs, Yobs = Store.load(st, Store.FO_NOISE, context, std_target, synthetic)

        history, estimator, _ = Pipeline.complete_noise_estimation(context, Yobs, Xobs, params, run, st)

        path = Store.get_path(st, Store.FO_GRAPHS, context, synthetic, init, params; label="noise_synthetic_$(Models.label(context))")
        plot_noise_estimation(history, synthetic, estimator; savepath=path)

        # polar plot for the last iteration
        geom = Models.matrix(Models.geometries(context)) # D x 3
        stds = hcat(sqrt.(diag(synthetic.sigma)), sqrt.(diag(estimator.sigma)), sqrt.(diag(history[end][1].sigma)))
        plot_polar(stds, geom, [L"\sigma_D", L"\sigma^{est}_D", L"\sigma^{last}_D"], ["green", "red", "blue"]; savepath=path * "polar")
    end
end

function noise_reel(run::Int)
    train_params = TrainingParams(0.0001, 50)
    sigma_init =  1.
    max_iterations =  30
    contexts = [HapkeMukundpuraPoudre(), HapkeHowardite(), HapkeCeres()]

    imis_params = Is.ImisParams(2000, 400, 20)

    function _run_noise(context, datas::Data2D, run::Int)
        D = Models.get_D(context)

        # we compare diagonal constraint with isotropic constraint
        init_diag = Noise.Bias(1., 0. * ones(D), Diagonal(sigma_init * ones(D)))
        init_iso = Noise.Bias(1., 0. * ones(D), UniformScaling(sigma_init))

        noise_params_diag = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_diag, max_iterations=max_iterations, imis_params=imis_params)
        noise_params_iso = Noise.EmIsGLLiMParams(targets=Noise.Targets(false, false, true), init=init_iso, max_iterations=max_iterations, imis_params=imis_params)

        params_diag = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_diag)
        params_iso = Noise.EmProcedureParams(train_params.N_train, train_params.gllim_params, noise_params_iso)

        history_diag, _, _ = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_diag, run, st)
        run_iso = _no_re_train(run) # avoid re-learning
        history_iso, _, _ = Pipeline.complete_noise_estimation(context, datas.Yobs, nothing, params_iso, run_iso, st)

        # final estimation
        std_est_diag = sqrt.(diag(history_diag[end][1].sigma))
        std_est_iso = sqrt.(history_iso[end][1].sigma * ones(D)) # converted as vector

        # comparison with the obs mean noise 
        std_obs = Statistics.mean(datas.Yobs_std, dims=2)[:, 1]
        @info "Average noise on OBSERVATIONS: $(Statistics.mean(std_obs))"
        # polar plot for easy visualisation
        path = Store.get_path(st, Store.FO_GRAPHS, context, params_diag, datas.Yobs; label="noise_reel_$(Models.label(context))_$(length(datas.Yobs))")
        
        stds = hcat(std_obs, std_est_diag, std_est_iso)
        labels = [notation_obs_std, notation_bias_std * " - Diagonal",  notation_bias_std * " - Isotropic"]
        colors = ["black", "red", "orange"]

        # we dont plot the diagonal constraint
        # stds, labels, colors = stds[:, [1,3]], labels[[1,3]], colors[[1,3]]
        
        # titlefunc = (inc, azi) -> "$(Models.label(context)) - inc : $inc - azi : $azi"
        geom = Models.matrix(Models.geometries(context))
        plot_polar(stds, geom, labels, colors; savepath=path * "polar")
    end 
    
    datass = [load_observations(context) for context in contexts]
    for (context, datas) in zip(contexts, datass)
        _run_noise(context, datas, run)
    end

    @assert Models.matrix(Models.geometries(contexts[1])) == Models.matrix(Models.geometries(contexts[2]))
    @assert Models.matrix(Models.geometries(contexts[2])) == Models.matrix(Models.geometries(contexts[3]))
    # since the geometries are shared, we also run the estimation with the aggregated observations
    Yobs_agg = hcat([data.Yobs for data in datass]...)
    Yobs_std_agg = hcat([data.Yobs_std for data in datass]...)
    wavelength_agg = vcat([data.wavelengths for data in datass]...)
    _run_noise(contexts[1], Data2D(Yobs_agg, Yobs_std_agg, wavelength_agg), _no_re_train(run))
end

# noise_synthetique(HapkeNontronite(), 0)
# noise_synthetique(HapkeCeres(), 0)
noise_reel(0)
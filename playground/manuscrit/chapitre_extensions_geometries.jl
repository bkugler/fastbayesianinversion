include("../geometries_sensitivity.jl")

g_samples = SobolGeoms([10,50], [20, 40,70], [0,180,30,150, 70, 110])
geom = setup_geometries(g_samples)
D = length(geom.azi)

function particular()
    plan_principal = [d  for d in 1:D if geom.azi[d] in (0, 180)]
    plan_oblique1 = [d  for d in 1:D if geom.azi[d] in (30, 150)]
    plan_oblique2 = [d  for d in 1:D if geom.azi[d] in (50, 130)]
    plan_oblique3 = [d  for d in 1:D if geom.azi[d] in (70, 110)]
    plan_principal_oblique1 = [plan_principal..., plan_oblique1...]
    plan_principal_oblique2 = [plan_principal..., plan_oblique2...]
    plan_principal_oblique3 = [plan_principal..., plan_oblique3...]
    plan_principal_2oblique = [plan_principal..., plan_oblique1..., plan_oblique2...]
    plan_oblique_tous = [plan_oblique1..., plan_oblique2..., plan_oblique3...]
    all = collect(1:D)
# Js = [plan_principal, plan_oblique1, plan_oblique2, plan_oblique3, plan_principal_oblique1, plan_principal_oblique2, plan_principal_oblique3, plan_principal_2oblique, plan_oblique_tous, all]
# # compare emergence influence 
# Js_eme = [
#     [d for d in J if geom.eme[d] in (20, 70)] for J in Js
# ]
# colors = [repeat(["blue"], length(Js))..., repeat(["red"], length(Js_eme))...]
end

function systematic()
    # select subsets
    emes_azis = [(e, a) for e in g_samples.emes for a in (0, 30, 70)]
    Js::Vector{Vector{Int}} = []
    for ks in combinations(emes_azis)
        eme_azi_ok = [ks..., [(e, 180 - a) for (e, a) in ks]...] # symmetric azimuth
        J = [d for d in 1:D if (geom.eme[d], geom.azi[d]) in eme_azi_ok]
        push!(Js, J)
    end
    # we assess the importance of geometries outside the principal plan (azi = 0 or 180)

    c, out = run_determination(geom, Js, 0)
    # c, out = run_determination(geom, [[1,3,4], [1,2]], 1)
    plot_determinations(c, geom, out, nothing; show_labels=false)
end

function repeat_geoms(run)
    sub_geoms = Models.HapkeGeometries(geom.inc[[1]], geom.eme[[1]], geom.azi[[1]])
    Js = [ repeat([1], n) for n in 1:20 ]      
    c, out = run_determination(sub_geoms, Js, run)
    plot_determinations(c, geom, out, nothing; show_labels=false, show_geoms=false)
end

systematic()
repeat_geoms(0)

# after the systematic study, we found that the best x / y errors at D = 20 
# sort!(out, by=det -> sum(det.det))
# subset_D20 = [r for r in out if length(r.J) == 20][1:10]

# function plot_list_determination(c::Models.Model, res::Vector{Determination})
#     varnames = Models.variables(c)
#     L = Models.get_L(c)
#     plot_geometries(geom, [r.J for r in res])

#     subplots_det_gllim = []
#     subplots_det_imis = []
#     subplots_errors = []
#     Np = length(res)
#     for l in 1:L # plot variable per variable
#         ys_xerr = [r.xerr[l] for r in res]

#         # determination
#         ys_det = [r.det[l] for r in res]
#         p_det_gllim = scatter(1:Np, ys_det; title=varnames[l], xlabel="index", label="determination (Gllim)")
#         # ybests = [r.det[l] for r in res[best_indexes_det]]
#         # scatter!(p_det_gllim, ds, ybests; markershape=:rect, label="best determination (average over variables)")

#         # determination
#         ys_det_imis = [r.det_imis[l] for r in res]
#         p_det_imis = scatter(1:Np, ys_det_imis; title=varnames[l], xlabel="index", label="determination (Imis)", yscale=:log10)
#         # ybests = [r.det[l] for r in res[best_indexes_det_imis]]
#         # scatter!(p_det_imis, ds, ybests; markershape=:rect, label="best determination (average over variables)")

#         # x errors
#         p_xerr = scatter(1:Np, ys_xerr; title=varnames[l], xlabel="index", label="x error", yscale=:log10)
#         # ybests = [r.xerr[l] for r in res[best_indexes_xerr]]
#         # scatter!(p_xerr, ds, ybests; markershape=:rect, label="best x error (average over variables)")



#         # if show_labels
#         #     annots = ["$i" for i in 1:length(res)]
#         #     annotate!(p_det_gllim, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_det, annots))])
#         #     annotate!(p_det_gllim, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_det_imis, annots))])
#         #     annotate!(p_xerr, [(x + 1.2 * (-1)^i, y, a) for (i, (x, y, a)) in enumerate(zip(xs, ys_xerr, annots))])
#         # end

#         push!(subplots_det_gllim, p_det_gllim)
#         push!(subplots_det_imis, p_det_imis)
#         push!(subplots_errors, p_xerr)
#     end

#     plot(subplots_det_gllim..., subplots_det_imis..., subplots_errors...; layout=(3, L), size=(2000, 2000))
# end

# plot_list_determination(c,subset_D20)
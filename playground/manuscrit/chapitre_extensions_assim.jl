include("shared.jl")
include("../assimilation/snowpack/model.jl")

struct RadarObs{T <: PolarModele}# only HH or VV are supported
    frequency::Float64 # Ghz
    incidence::Int
    sigma_obs::Float64
    sigma_std::Float64
end

struct SnowpackProfil
    date::Dates.Date
    thickness::Vector{Float64} # m
    grain_size::Vector{Float64} #  mm
    density::Vector{Float64} # in [0,1]
    temperature::Float64 # averaged, in Kelvin
    observations::Vector{RadarObs}
end



function get_back_scattering_model(::RadarObs{T}) where T
    # 0.8, 0.3, 0.8, 0.07
    return BackScatteringModel{T}(8, 0.4, 8, 0.7)
end

function _load_snowpack_data()
    file = ROOT_DATA_PATH * "SNOWPACK/NOSREX-II-2010-2011.json"
    tmp = JSON.parsefile(file)

    out::Vector{SnowpackProfil} = []
    for (i, profil) in enumerate(tmp)
        thickness = profil["thickness"] ./ 100 # cm to m
        if length(thickness) == 0 
            @warn "Skipping empty observation $i"
            continue
        end

        temperature = 273.15 + (sum(profil["temperature"]) / length(profil["temperature"]))
        date = Date(profil["date"], "y-m-d")
        radars = profil["radars"]
        obss::Vector{RadarObs} = []
        for radar in radars 
            T = PolarModele

            if radar["polar"] == "VV"
                T = PolarVV
            elseif radar["polar"] == "HH"
                T = PolarHH
            else 
                continue
            end

            obs = RadarObs{T}(
                radar["frequency"] / 1_000_000_000,
                radar["incidence"],
                10^(radar["sigma"] / 10),
                10^(radar["stdev"] / 10)
            )
            push!(obss, obs)
        end

        push!(out, SnowpackProfil(
            date,
            thickness,
            profil["grain_size"],
            profil["density"],
            temperature,
            obss
        ))
    end

    out
end

function run_assim_data(inputs, run)

    out = []
    for profil::SnowpackProfil in inputs
        # we aggregate the various radar observations 
        yobs::Vector{Float64}, yobs_std::Vector{Float64} = [], []
        context = SnowPackModelVector([])
        for obs::RadarObs in profil.observations 
            if obs.frequency != 10.2 # for now, we select only one frequency
                continue
            end

            push!(yobs, obs.sigma_obs)
            push!(yobs_std, obs.sigma_std)
            
            bm = get_back_scattering_model(obs) # polar
            subm = SnowPackModel(bm, obs.frequency, obs.incidence, profil.thickness)
            push!(context.models, subm)
        end

        D = Models.get_D(context)

        gp = Gllim.Params(
            Gllim.FunctionnalInitParams(200, Models.closure(context)),
            Gllim.TrainParams{Float64,DiagCov{Float64},IsoCov{Float64}}(
                Gllim.DefaultStoppingParams(300, 1e-5),
                1e-12
            )
        )

        trained_gllim, _, dura = Shared.generate_train_gllim(context, Shared.TrainingParams(
            100_000, 0.00001, gp
        ), st, run >= 2)
        @info "Trained in $dura"

        # assimilation
        x0 = [
            profil.grain_size...,
            profil.density...,
        ]

        covX::FullCov{Float64} = guess_profile_covariance(profil.thickness, 0.1, 0.08)
        # covY::FullCov{Float64} = Symmetric(0.03 * ones(D, D)) # from the article
        # covY::FullCov{Float64} = Symmetric(0.0000036 * ones(D, D)) # from the matlab code
        covY::DiagCov{Float64} = Diagonal(yobs_std.^2)

        x0, covX = Models.from_physical(context, x0, covX)

        trained_gllim = Gllim.add_bias(trained_gllim, 1., 0., covY)
        gllim_star = Gllim.inversion(trained_gllim)
        weights, means = Gllim.conditionnal_density(gllim_star, yobs)
        covs = gllim_star.Σ

        w2, m2, _ = Gllim.conditionnal_density_with_prior(weights, means, covs, x0, covX)
        xpred = Probas.mean_mixture(w2, m2)
        push!(out, xpred)
    end

    out
end

inputs = _load_snowpack_data()

out = run_assim_data(inputs, 1)
# write(PATH_STORE_ASSIM * "nosrex_pred.json", JSON.json(out))
out = JSON.parsefile(ROOT_PATH_STORE * Store.FO_MISC * "nosrex_pred.json")

function plot_assim_data(inputs::Vector{SnowpackProfil}, xmeans::Vector)
    subplots = []
    for (i, profil) in enumerate(inputs[1:2])
        NL = length(profil.thickness)
        heights = reverse(cumsum(profil.thickness) * 100)
        pbar = bar([0.75], hcat(heights)'; c=:grey77, ylabel="layers height (cm)", labels=hcat(["Layer $j" for j in 1:NL]...), legend=:none, xticks=:none)
        # cov =  guess_profile_covariance(thickness, 0.1, 0.08)
        # @show eigvals(cov[1:NL, 1:NL])
        # @show eigvals(cov[(NL + 1):2 * NL, (NL + 1):2 * NL])
        output = xmeans[i]
        data::Matrix{Float64} = hcat(profil.grain_size, output[1:NL], profil.density, output[NL + 1:2 * NL])
        labels = [L"$d_{prior}$ (mm)" L"$d_{assim}$ (mm)" L"$\rho_{prior}$ (in [0, 1])" L"$\rho_{assim}$ (in [0, 1])"]
        markers = [:star :star :rect :rect]
        styles = [:solid :dash :solid :dash]
        colors = [:blue :blue :red :red]
        p = plot(1:NL, data,  xlabel="$NL layers (measure day $(profil.date))", labels=labels, markers=markers, c=colors, style=styles, alpha=0.5)
        push!(subplots, plot(pbar, p, layout=grid(1, 2, widths=[0.2, 0.8])))
    end

    plot(subplots..., size=(2000, 2000))
end

plot_assim_data(inputs, out)
using FastBayesianInversion.Mcmc
using FastBayesianInversion.Pipeline.TuneImis

include("shared.jl")
include("../plotting/plots.jl")

# performance comparison with MCMC
function _hapke_synthetique(context::Models.Model, run::Int; add_prop_std=0.)
    std_train = 0.0001

    imiss = [
        Is.ImisParams(1000, 200, 25),
        Is.ImisParams(2000, 400, 10),
        Is.ImisParams(2000, 400, 25),
    ]
    iss = [
        Is.IsParams(20_000),
        Is.IsParams(200_000),
        Is.IsParams(2_000_000),
    ]

    K = 50
    obs_std_ratio  = 1000
    # to keep MCMC-HMC computation time reasonnable, we
    # use rather small values
    Nobs = 100
    samples_MH = [10_000, 100_000, 1_000_000]

    # for D = 70, HMC 100_000 is too slow and not needed
    samples_HMC = [1_000, 10_000]
    if Models.get_D(context) >= 15 
        push!(samples_HMC, 20_000)
    else 
        push!(samples_HMC, 100_000)
    end 
    
    training_params = TrainingParams(std_train, K)

    Xobs_math, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs_math, similar(Xobs_math))
    Yobs_std = Yobs ./ obs_std_ratio

    # as a preliminary step, we calibrate imis params against a bound, with the help of reference
    # MCMC inversion
    # mcmc_params = Mcmc.ParamsHMC(10_000, 10_000 / 2, std_train)
    # Yobs, Yobs_std = Yobs[:, 1:5], Yobs_std[:, 1:5]
    # invs_mcmc_mh, _ = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params)
    # ref, _ = Models.from_physical(context, invs_mcmc_mh.Xmean, invs_mcmc_mh.Xcovs)
    # imis_params = TuneImis.tune(st, context, Yobs, Yobs_std, ref, training_params, 20_000, run >= 2)
    # @show imis_params
    # return 

    # we compare the 4 methods
    stats::Vector{MethodStats} = []
    measures::Vector{DoubleSolutionErrors} = []
    final_plot_data::Vector{Pair{Shared.MethodResult,PlotOptions}} = [] # for the article

    params_mcmc = [
        [Mcmc.ParamsMH(std_train;Niter=sample_mcmc, Nkeep=sample_mcmc / 2) for sample_mcmc in samples_MH]...
        [Mcmc.ParamsHMC(sample_mcmc, sample_mcmc / 2, std_train)  for sample_mcmc in samples_HMC]...
    ]
    for mcmc_param in params_mcmc
        ct_mcmc = Comptimes.Times(context, mcmc_param, Nobs)
        if run >= 3
            invs_mcmc, dura = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_param)
            Store.save(st, [invs_mcmc, dura], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_param)
        end
        invs_mcmc, ct_mcmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_param)
        
        isMH = mcmc_param isa Mcmc.ParamsMH
        # stats table
        label = isMH ? N.McmcMH : N.McmcHMC
        push!(stats,  MethodStats(label * " - I : $(_fmt_int(mcmc_param.Niter))",    
            BasicError(invs_mcmc, context, Xobs_math), ct_mcmc.prediction),
        )

        # final plot
        if !isMH && mcmc_param.Niter == 10_000
            push!(final_plot_data, invs_mcmc => PlotOptions(label, "teal", 0.5, :solid; markershape=:circle, hide_std=true))
        end
    end

    Shared.generate_train_gllim(context, training_params, st, run >= 2) # shared training

    get_params_inv(sampling, prop_std) = Pipeline.ProcedureParams(training_params,
    Pipeline.InversionParams(
        std_train,Predictions.MergingOptions(2),
        sampling, false,false,
        Is.NonUniformityParams(1), prop_std
    )
)

    for (j, imis) in enumerate(imiss)
        params = get_params_inv(imis, add_prop_std)

        preds, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)

        # final plot
        if Is.Ns(imis) == 12000
            push!(final_plot_data,
                # preds[Schemes.Best] => PlotOptions(N.Best, "blue", 0.5, :dash, hide_std=true),
                preds[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.5, :none, hide_std=true),
                # preds[Schemes.IsMean] => PlotOptions(N.IsMean, "orange", 1, :none; markershape=:rect, markersize=0.5, hide_std=true),
                preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "purple", 0.6, :solid; markershape=:circle, hide_std=true), 
                Shared.MethodResult(Xobs) => PlotOptions(N.Xobs, "green", 0.5, :solid, markersize=0, hide_std=true),
            )
        end

        # # first err table
        # if Is.Ns(imis) == 22500
        #     measure = _measure_errs(context, Xobs, preds)
        #     push!(measures, measure)
        # end

        # stats table: we only report the inversion time
        if j == 1 # include Gllim only once
            push!(stats,  MethodStats(N.GllimMean, BasicError(preds[Schemes.GllimMean], context, Xobs_math), ct.gllim_only_prediction))
        end
        push!(stats,    
            MethodStats(N.ImisMean * " - $(N.format_imis(imis))", BasicError(preds[Schemes.ImisMean], context, Xobs_math), ct.prediction),
            # MethodStats(N.Best * " - $(N.format_imis(imis))", BasicError(preds[Schemes.Best], context, Xobs_math), ct.prediction),
        )
    end

        for is in iss
        params = get_params_inv(is, add_prop_std)

        preds, _,  _, ct = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, _no_re_train(run), st)

        # final plot
        if is.N_is == 2_000_000
            push!(final_plot_data,
                # preds[Schemes.Best] => PlotOptions(N.Best, "blue", 0.5, :dash, hide_std=true),
                # preds[Schemes.IsMean] => PlotOptions(N.IsMean, "orange", 1, :none; markershape=:rect, markersize=0.5, hide_std=true),
                preds[Schemes.IsMean] => PlotOptions(N.IsMean, "orange", 0.6, :solid; markershape=:circle, hide_std=true),
            )
        end

        # # first err table
        # if is.N_is == 20_000
        #     measure = _measure_errs(context, Xobs, preds)
        #     push!(measures, measure)
        # end

        # stats table: we only report the inversion time
        # gllim is already included with imis
        push!(stats,
            MethodStats(N.IsMean * " - I : $(is.N_is)", BasicError(preds[Schemes.IsMean], context, Xobs_math), ct.prediction),
            # MethodStats(N.Best * " - I : $(is.N_is)", BasicError(preds[Schemes.Best], context, Xobs_math), ct.prediction),
        )
    end

    # comparison with MCMC (with computing time)
    label = Models.label(context)
    path = Store.get_path(st, Store.FO_GRAPHS, context, K, Nobs, params_mcmc, imiss, iss; label="hapke_synthetique_$label")
    caption = """
    Géométries de la série {\\em $label}. Comparaison des performances avec les méthodes MCMC en termes de temps de calcul,, d'erreur de prédiction et d'erreur de reconstruction. Les valeurs optimales sont en gras et les écart-types entre parenthèses.
    """
    latex_table = compare_stats_table("hapke_synthetique_$label", stats; caption=caption)
    write(path * "_table.tex", latex_table)
    @info "Latex exported in $(path)_table.tex"

    # # comparison with centroids (no MCMC, no computing time)
    # table_meths = [
    #     Schemes.GllimMean, Schemes.ImisMean,
    #     # Schemes.GllimMergedCentroid(1), Schemes.GllimMergedCentroid(2),
    #     # Schemes.IsCentroid(1), Schemes.IsCentroid(2),
    #     Schemes.ImisCentroid(1), Schemes.ImisCentroid(2),
    #     Schemes.Best,
    # ]
    # options = TableOptions(table_meths, false, false, false, false)
    # latex_table = render_errors("hapke_synthetique", measures, K_variants, options)
    # path = Store.get_path(st, Store.FO_GRAPHS, context, K_variants, Nobs, imiss; label="hapke_synthetique_centroids") * ".tex"
    # write(path, latex_table)
    # @info "Latex exported in $path"

    plot_components(final_plot_data;  savepath=path,xlabel=L"n", varnames=Models.variables(context))
end

# floor = 0 to desactivate it
# add_prop_std is added in a second pass to the observations 
# which fail without it
function hapke_synthetique_high_dim(run::Int)
    context = HapkeHighDim2()
    floor = 0.05
    add_prop_std = 5
    std_train = 0.005
    K = 100
    train_params = Shared.TrainingParams(
        100_000,
        std_train,
        Gllim.Params(
            Gllim.FunctionnalInitParams(K, Models.closure(context)),
            Gllim.TrainParams{Float64,FullCov{Float64},IsoCov{Float64}}(
                Gllim.DefaultStoppingParams(200, 1e-5),
                1e-12
            )
        )
    )
  
    imis = Is.ImisParams(2500, 1000, 20)
    get_params_imis(prop_std) = Pipeline.ProcedureParams(train_params,
        Pipeline.InversionParams(
            std_train,Predictions.MergingOptions(2),
            imis, false,false,
            Is.NonUniformityParams(1), prop_std
        )
    )
    # we start without the std trick
    params_imis = get_params_imis(0.)

    obs_std_ratio = 50
    Nobs = 100

    Xobs_math, Yobs = Models.data_observations(context, Nobs)

    Xobs, _ = Models.to_physical(context, Xobs_math, similar(Xobs_math))
    Yobs_std = Yobs ./ obs_std_ratio

    if floor > 0
        # very low std may give GLLiM predictions outside of [0,1]
        # which in turn break samplings steps (IS and IMIS)
        # the parameter add_prop_std is also a way to prevent this effect
        Yobs_std[ Yobs_std .< floor ] .= floor
    end

    # we compute our own MCMC inversions
    mcmc_params_mh = Mcmc.ParamsMH(std_train;Niter=1_000_000, Nkeep=1_000_000 / 2)
    # HMC is really slow in high dim, and does not require many iterations
    mcmc_params_hmc = Mcmc.ParamsHMC(1_000, 1_000 / 2, std_train)
    ct_mcmc_mh = Comptimes.Times(context, mcmc_params_mh, Nobs)
    ct_mcmc_hmc = Comptimes.Times(context, mcmc_params_hmc, Nobs)
    if run >= 3
        invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
        Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
        Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
    end
    invs_mcmc_mh, ct_mcmc_mh.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
    invs_mcmc_hmc, ct_mcmc_hmc.prediction = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)

    # preds_is, _,  _, ct_is = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_is, run, st)
    preds_imis, _,  _, ct_imis = Pipeline.complete_inversion(context, Yobs, Yobs_std, params_imis, run, st)

    path = Store.get_path(st, Store.FO_GRAPHS, context, mcmc_params_mh, mcmc_params_hmc, params_imis, obs_std_ratio, floor; label="hapke_high_dim_SNR_$(obs_std_ratio)_FLOOR_$(floor)_ADD_STD_$(add_prop_std)_K_$K")

    # plot before correcting the failures 
    plot_data = [
        preds_imis[Schemes.GllimMean] => PlotOptions(N.GllimMean, "red", 0.5, :solid),
        preds_imis[Schemes.ImisMean] => PlotOptions(N.ImisMean, "purple", 0.8, :none; markershape=:star, markersize=10),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xobs, "green", 0.6, :solid; markersize=0, hide_std=true),
    ]
    plot_components(plot_data; savepath=path * "_failure",xlabel=L"n", varnames=Models.variables(context))

        
    # handle failures 
    mask_failure = Shared.failures(preds_imis[Schemes.ImisMean], context)
    dst_indexes = (1:Nobs)[mask_failure]
    @info "Fixing $(length(dst_indexes)) obs. with additional std $add_prop_std ..."
    params_imis_std = get_params_imis(add_prop_std)
    preds_imis_std, _,  _, _ = Pipeline.complete_inversion(context, Yobs[:, mask_failure], Yobs_std[:, mask_failure], params_imis_std, _no_re_train(run), st)
    for (i, j) in enumerate(dst_indexes)
        Shared.merge_result!(preds_imis, preds_imis_std, j, i)
    end

    preds = preds_imis  
    reg_meths = [ Schemes.GllimMergedCentroid(""),
    Schemes.ImisCentroid("")]
    Pipeline.regularize_inversions!(preds, reg_meths)
    
    # main plot
    plot_data = [
        preds[Schemes.Best] => PlotOptions(N.Best, "blue", 0.5, :dash; markershape=:circle),
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean, "purple", 0.8, :none; markershape=:star, markersize=10),
        invs_mcmc_mh => PlotOptions(N.McmcMH,  "brown", 0.3, :dash),
        invs_mcmc_hmc => PlotOptions(N.McmcHMC,  "teal", 0.3, :dash),
        Shared.MethodResult(Xobs) => PlotOptions(N.Xobs, "green", 0.6, :solid; markersize=0, hide_std=true),
    ]
    plot_components(plot_data; savepath=path * "_fixed",xlabel=L"n", varnames=Models.variables(context))

    # stat table 
    stats = [
        MethodStats(N.McmcMH * " - I : $(_fmt_int(mcmc_params_mh.Niter))", BasicError(invs_mcmc_mh, context, Xobs_math), ct_mcmc_mh.prediction), 
        MethodStats(N.McmcHMC * " - I : $(_fmt_int(mcmc_params_hmc.Niter))", BasicError(invs_mcmc_hmc, context, Xobs_math), ct_mcmc_hmc.prediction), 
        MethodStats(N.ImisMean * " - $(N.format_imis(imis))", BasicError(preds[Schemes.ImisMean], context, Xobs_math), ct_imis.prediction),
        MethodStats(N.Best * " (idem)", BasicError(preds[Schemes.Best], context, Xobs_math), ct_imis.prediction),
    ]
    latex_table = compare_stats_table("hapke_high_dim_mcmc", stats)
    write(path * "_table.tex", latex_table)
    @info "Latex exported in $(path)_table.tex"
end


function _export_result_json(context::Models.Model, series::Dict{String,Shared.MethodResult})
        pred_imis = series[Schemes.ImisMean]
        pred_best = series[Schemes.Best]
        pred_centroid1 = series[Schemes.ImisCentroid(1)]
        pred_centroid2 = series[Schemes.ImisCentroid(2)]
    
        d = Dict(
            "geometries" => Models.matrix(Models.geometries(context)),
            "imis" => pred_imis.Xmean,
            "imis_std" => sqrt.(pred_imis.Xcovs),
            "best" => pred_best.Xmean,
            "best_std" => sqrt.(pred_best.Xcovs),
            "centroid1" => pred_centroid1.Xmean,
            "centroid1_std" => sqrt.(pred_centroid1.Xcovs),
            "centroid2" => pred_centroid2.Xmean,
            "centroid2_std" => sqrt.(pred_centroid2.Xcovs),
        ) 
        path = Store.get_path(st, Store.FO_MISC, :export, context; label=Models.label(context)) * ".json"
        write(path, JSON.json(d))
        @info "exported in $path"
end

# inversions of real observations (2D), no mcmc
# if obs_std_ratio is noting, use Yobs_std, else use max(0.01, Yobs / obs_std_ratio)
function _hapke_lab_real(context::Models.Hapke, exp::String, run::Int, obs_std_ratio::Union{Number,Nothing}, std_train::Number, add_prop_std::Number; with_mcmc=false)
    datas::Data2D  = load_observations(context, exp)
    Yobs, Yobs_std = datas.Yobs, datas.Yobs_std
    if obs_std_ratio !== nothing
        Yobs_std = Yobs ./ obs_std_ratio # first std
        # we follow the best practice in photometric measurement
        Yobs_std[ Yobs_std .< 0.01 ] .= 0.01 # floor at 0.01
    end
    wavelengths = datas.wavelengths

    training_params = TrainingParams(std_train, 50)
    inv_params_imis = Pipeline.InversionParams(
        std_train,Predictions.MergingOptions(2),
        Is.ImisParams(2500, 1000, 20), false,false,
        Is.NonUniformityParams(10_000),add_prop_std
    )

    _, Nobs = size(Yobs)

  

    # we then invert the model with Gllim + sampling 
    params = Pipeline.ProcedureParams(training_params, inv_params_imis)
    preds, variabilities, _, _ = Pipeline.complete_inversion(context, Yobs, Yobs_std, params, run, st)
        
    if with_mcmc
        samples_mcmc = 1_000_000
        mcmc_params_mh = Mcmc.ParamsMH(std_train;Niter=samples_mcmc, Nkeep=samples_mcmc / 2)
        # mcmc_params_hmc = Mcmc.ParamsHMC(samples_mcmc, samples_mcmc / 2, std_train)
        if run >= 3
            invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
            # invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
            Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
            # Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
        end
        invs_mcmc_mh, _ = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        preds[Schemes.McmcMetropolis] = invs_mcmc_mh
    end 

    if run < 0 
        return preds 
    end

    # --- plot -----
    label = "hapke_lab_$(Models.label(context))" 
    y_lims = [(0, 1), (0, 30), (0, 1), (0, 1)]
    # 1 - X values
    plot_data = [
        preds[Schemes.Best] => PlotOptions(N.Best,  "blue", 1, :dash),
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean,  "red", 0.5, :solid),
        preds[Schemes.McmcMetropolis] => PlotOptions(N.McmcMH,  "teal", 0.6, :dash),
    ]
    path = Store.get_path(st, Store.FO_GRAPHS, context, params; label=label)
    plot_components(plot_data;  savepath=path, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)", y_lims=y_lims)
    # 1 - bis - centroids
    plot_data = [
        preds[Schemes.ImisMean] => PlotOptions(N.ImisMean,  "red", 0.6, :solid),
        preds[Schemes.ImisCentroid(1)] => PlotOptions(N.ImisCentroid1,  "blue", 0.6, :none, markershape=:star),
        preds[Schemes.ImisCentroid(2)] => PlotOptions(N.ImisCentroid2,  "blue", 0.6, :none, markershape=:star),
    ]
    plot_components(plot_data;  savepath=path * "_centroids", varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)", y_lims=y_lims)

    # # 1 - ter - centroids gllim to diagnose edge cases
    # plot_data = [
    #     preds[Schemes.GllimMean] => PlotOptions(N.GllimMean,  "red", 0.6, :solid),
    #     preds[Schemes.GllimMergedCentroid(1)] => PlotOptions(N.GllimMergedCentroid1,  "blue", 0.6, :none, markershape=:star),
    #     preds[Schemes.GllimMergedCentroid(2)] => PlotOptions(N.GllimMergedCentroid2,  "blue", 0.6, :none, markershape=:star),
    # ]
    # plot_components(plot_data;  savepath=path * "_centroids_gllim", varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)", y_lims = y_lims)

    # 3 - variabilities
    plot_variabilities(variabilities, savepath=path * "_vars", varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)")


    # we also export the result in a json file 
    _export_result_json(context, preds)

    preds # for the hocker stick plot
end

function hapke_synthetique()
    _hapke_synthetique(HapkeNontronite(), 0; add_prop_std=0)
    _hapke_synthetique(HapkeMukundpuraBloc(), 0;add_prop_std=1) 

    # hapke_synthetique_high_dim(0)
end

function hapke_lab_minerals(run::Int)
    contexts = [
        HapkeNontronite(),
        HapkeMagnesite(),
        HapkeBasalt(),
        HapkeBasalticGlass(),
        HapkeOlivine(),
    ]
    preds::Vector{Dict{String,Shared.MethodResult}} = []
    for ct in contexts 
        res = _hapke_lab_real(ct, "", run, 20, 0.0001, 0; with_mcmc=true)
    push!(preds, res)
    end 

    labels = join([Models.label(ct) for ct in contexts], "_")
    path = Store.get_path(st, Store.FO_GRAPHS, contexts; label="hockey_stick_$labels")
    plot_hockey_stick(contexts, preds, [Schemes.ImisMean, Schemes.Best], path)
end

# control methods
function hapke_lab_minerals_mcmc(run::Int)
    contexts = [
        HapkeNontronite(),
        HapkeMagnesite(),
        HapkeBasalt(),
        HapkeBasalticGlass(),
        # _hapkeModel("BasalticGlass"; w_linear=false)
        HapkeOlivine(),
    ]
    std_train = 0.0001
    samples_mcmc = 1_000_000
    for context in contexts 
        datas::Data2D  = load_observations(context, "")
        Yobs, Yobs_std = datas.Yobs, datas.Yobs_std

        obs_std_ratio = 20
        Yobs_std = Yobs ./ obs_std_ratio # first std
        # we follow the best practice in photometric measurement
        Yobs_std[ Yobs_std .< 0.01 ] .= 0.01 # floor at 0.01

        wavelengths = datas.wavelengths

        # we compute our own MCMC inversions
        mcmc_params_mh = Mcmc.ParamsMH(std_train;Niter=samples_mcmc, Nkeep=samples_mcmc / 2)
        mcmc_params_hmc = Mcmc.ParamsHMC(samples_mcmc / 100, samples_mcmc / 100 / 2, std_train)
        if run >= 3
            invs_mcmc_mh, dura_mh = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_mh)
            invs_mcmc_hmc, dura_hmc = Mcmc.complete_inversion(context, Yobs, Yobs_std, mcmc_params_hmc)
            Store.save(st, [invs_mcmc_mh, dura_mh], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
            Store.save(st, [invs_mcmc_hmc, dura_hmc], Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)
        end
        invs_mcmc_mh, _ = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_mh)
        invs_mcmc_hmc, _ = Store.load(st, Store.FO_PREDICTIONS, context, Yobs, Yobs_std, mcmc_params_hmc)

        # --- plot -----
        label = "hapke_lab _$(Models.label(context))_mcmc" 
        y_lims = [(0, 1), (0, 30), (0, 1), (0, 1)]
        # 1 - X values
        plot_data = [
            invs_mcmc_mh => PlotOptions(N.McmcMH,  "teal", 0.6, :dash),
            invs_mcmc_hmc => PlotOptions(N.McmcHMC,  "purple", 0.6, :solid),
        ]
        path = Store.get_path(st, Store.FO_GRAPHS, context, mcmc_params_mh; label=label)
        plot_components(plot_data;  savepath=path, varnames=Models.variables(context), x_ticks=1000 .* wavelengths, xlabel="wavelength (nm)", y_lims=y_lims)
    end
end

function hapke_lab_meteorites(run::Int)
    contexts = [
        HapkeMukundpuraBloc(),
        HapkeMukundpuraPoudre(),
        HapkeBlackyBloc(),
        HapkeBlackyPoudre(),
    ]

    _geometries_table(contexts)

    _plot_observations(contexts)

    preds::Vector{Dict{String,Shared.MethodResult}} = []
    for ct in contexts
        res = _hapke_lab_real(ct, "", run, nothing, 0.0000001, 1)
    push!(preds, res)
    end

    labels = join([Models.label(ct) for ct in contexts], "_")
    path = Store.get_path(st, Store.FO_GRAPHS, contexts; label="hockey_stick_$labels")
    plot_hockey_stick(contexts, preds, [Schemes.ImisMean, Schemes.Best], path)
end

function _plot_observations(contexts::Vector{Models.Hapke})
    subplots = []
    subplots_y = []
    for ct in contexts
        lab = Models.label(ct)
        # plot the average std
        datas = load_observations(ct, "")
        std = datas.Yobs_std
        p = plot(1000 .* datas.wavelengths, sum(std; dims=1)' ./ Models.get_D(ct); label="average std", yscale=:log10, xlabel="$lab - wavelength (nm)", background_color_legend=Config.LEGEND_BACKGROUND)
        push!(subplots, p)

        geoms = Models.geometries(ct)
        geoms_indexes = [1,10,40,60]
        labels = hcat([ L"\theta_0 , \theta, \phi = %$(Int(round(geoms.inc[d]))), %$(Int(round(geoms.eme[d]))), %$(Int(round(geoms.azi[d])))"  for d in geoms_indexes]...)
        p = plot(1000 .* datas.wavelengths, datas.Yobs[geoms_indexes, :]'; labels=labels, xlabel="$lab - wavelength (nm)", background_color_legend=Config.LEGEND_BACKGROUND)
        push!(subplots_y, p)

        # show the average signal noise ratio
        snr = sum(datas.Yobs ./ datas.Yobs_std) / (length(datas.Yobs))
        @show Models.label(ct), snr
    end

    labels = join([Models.label(ct) for ct in contexts], "_")

    png(plot(subplots..., size=(1500, 1000)), Store.get_path(st, Store.FO_GRAPHS; label="$(labels)_std"))
    png(plot(subplots_y..., size=(1500, 1000)), Store.get_path(st, Store.FO_GRAPHS; label="$(labels)_y"))
end


# latex table 
function _geometries_table(cts::Vector{Models.Hapke})
    defs = join(["c | c | c" for _ in cts], " || ")
    header1 = join(["\\multicolumn{3}{c}{$(Models.label(ct))}" for ct in cts], " & ")
    header2 = join(["\$\\theta_0\$ & \$\\theta \$ & \$\\varphi\$ " for _ in cts], " & ")
    Dmax = maximum(Models.get_D(ct) for ct in cts)
    
    rows = []
    for d in 1:Dmax
        row = []
        for ct in cts
            geoms = Models.geometries(ct)
            if length(geoms.inc) < d 
                push!(row, " - & - & - ")
            else
                push!(row, "$(Int(round(geoms.inc[d]))) & $(Int(round(geoms.eme[d]))) & $(Int(round(geoms.azi[d])))")
            end
        end
        push!(rows, join(row, " & "))
    end
    rows_content = join(rows, "\\\\ \n")
    latex = """
    \\begin{longtable}{$defs}
    $header1 \\\\
    \\hline
    $header2 \\\\
    \\hline 
    $rows_content \\\\
    \\caption{ Géométries de mesure pour les échantillons de météorites.}
    \\end{longtable} 
    """

    path = PATH_GRAPHS_MANUSCRIT * "hapke/geometries_meteorites.tex"
    write(path, latex)
    @info "Latex written in $path"
end

function hapke_lab_planete(run::Int)
    contexts = [
        HapkeHowardite(),
        HapkeCeres(),
    ]

    _plot_observations(contexts)

    preds::Vector{Dict{String,Shared.MethodResult}} = []
    for ct in contexts
        res = _hapke_lab_real(ct, "", run, nothing, 0.0000001, 0.5)
    push!(preds, res)
    end

    labels = join([Models.label(ct) for ct in contexts], "_")
    path = Store.get_path(st, Store.FO_GRAPHS, contexts; label="hockey_stick_$labels")
    plot_hockey_stick(contexts, preds, [Schemes.ImisMean, Schemes.Best], path)
end

# we plot the 4 sets of geometries used 
function plot_mesures_geometries()
    for ct in [
        HapkeNontronite(),
        HapkeMukundpuraBloc(),
        HapkeBlackyPoudre(),
        HapkeGlaceMars(),
        HapkeHighDim2()
    ]
        geoms::Models.HapkeGeometries = Models.geometries(ct)


        subplots = []
        for (j, inc) in enumerate(unique(geoms.inc))
            ids = geoms.inc .== inc
            azi = geoms.azi * pi / 180  # azimuth en radians
            p = scatter(azi[ids], geoms.eme[ids], proj=:polar, label="geometrie", legendfontsize=12, title="incidence: $(round(inc)) °", background_color_legend=Config.LEGEND_BACKGROUND)
            push!(subplots, p)
        end

        path = Store.get_path(st, Store.FO_GRAPHS, ct; label="geometries_$(Models.label(ct))")
        S = sqrt(length(subplots))
        p = plot(subplots...; size=(S * 500, S * 500))
        png(p, path)
        @info "Plotted in $path"
    end
end

function K_selection()
    # computation has been done on cluster

    Ks = KSel.Krange
    Ystds = [0.1, 0.01, 0.001]
    contexts = [HapkeNontronite(), HapkeGlaceMars()]

    for ct in contexts
        for Ystd in Ystds
            res, _ = Store.load(st, Store.FO_MISC, ct, Ks, Ystd)

            savepath = Store.get_path(st, Store.FO_GRAPHS, ct, Ystd; label="K_selection_$(Ystd)_$(Models.label(ct))")
            plot_k_selection(Ks, res, Ystd, Models.get_D(ct); savepath=savepath)
        end
    end
end

# hapke_synthetique()

# hapke_lab_minerals(0)
# hapke_lab_minerals_mcmc(3)
# _hapke_lab_real(HapkeBasalt(), "", 2, 20)
# hapke_lab_meteorites(0)
# hapke_lab_planete(0)

plot_mesures_geometries()
# K_selection()


# using ReadSav
# function plot_reference_mcmc()
#     f = ReadSav.readsav("/Users/kuglerb/Desktop/data_BAS/MCMC-MH_Schmidt2019/data_BAS_corr_allwave.sav")
#     var_indices = [4, 3, 1, 2]
#     X = f["ESTIM_M_ALL"][var_indices, :]
#     X_std = f["VAR_M_ALL"][var_indices, :]
#     ylims_ = [(0, 1), (0, 30), (0, 1), (0, 1)]
#     names = ["w", "theta", "b", "c"]
#     plots = [plot(1:100, X[i, :]; ylims=ylims_[i], ylabel=names[i], label="MCMC ref") for i in 1:4]
#     p = plot(plots...)
#     png(p, "/Users/kuglerb/Desktop/data_BAS/bas.png")
# end
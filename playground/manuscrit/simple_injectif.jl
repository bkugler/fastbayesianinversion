# implementation of a very simple injective functionnal

using FastBayesianInversion.Models

struct InjectifSimple <: Models.Model end

Models.label(::InjectifSimple) = "Simple injective function"
Models.get_L(m::InjectifSimple) = 1
Models.get_D(m::InjectifSimple) = 1
function Models.f!(::InjectifSimple, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T
    out .= exp.(x)
end

module Comptimes 

using Dates
import Base.copy 

const Invertion = :invertion
const Noise = :noise
const MCMC = :mcmc

"""
    Stores computation time and meta data describing the computation
"""
mutable struct Times
    exp_index::String
    kind::Symbol 
    learning::Millisecond 
    prediction::Millisecond
    gllim_only_prediction::Millisecond
    label::String 
    L::Int
    D::Int
    K::Int
    Ntrain::Int
    Nobs::Int
    Nis::Int
end

"""
 Returns the duration since`ti`
"""
function since(ti::DateTime)::Millisecond
    return Dates.now() - ti
end

const _fieldnames = fieldnames(Comptimes.Times)
function Base.copy(ct::Times) 
    Comptimes.Times([getfield(ct, k) for k in _fieldnames]...)
end

end
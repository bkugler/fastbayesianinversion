"""
 Add one to all numbers greater or equal to j 
"""
function _add_one(perm::Vector{Int}, j::Int)
    out = similar(perm)
    for i in eachindex(perm)
        out[i] = (perm[i] >= j) ? perm[i] + 1 : perm[i] 
    end
    out
end

function genere_permutations(N::Int)
    if N == 1
        return [[1]]
    end 
    out::Vector{Vector{Int}} = []
    perms = genere_permutations(N - 1)

    for j in 1:N 
        for perm in perms
            perm = _add_one(perm, j)
            complete_perm = [ [j] ; perm ]
            push!(out, complete_perm)
        end
    end
    out
end

"""
Re-order centroids to obtain better regularity . 

`series` is an array of series of prediction (length K). Each predictions must have the same size (L, N)
Returns an array of permuations (length N)
"""
function regularization(series::Vector{Matrix{T}})::Vector{Vector{Int}} where T
    Xs = cat(series..., dims=3)   # size L, N, K  
    L, N, K = size(Xs)
    perms = genere_permutations(K)
    out = Vector{Vector{Int}}(undef, N)
    
    # start with Xs original values
    out[1] = 1:K # identity
    current_choice = Xs[:,1,:] 
    
    # v1 and v2 are matrix of permutted solutions
    function mesure_diff(v1::Matrix, v2::Matrix) 
        diffs = maximum(abs.(v1 .- v2), dims=1) # for each solution
        sum(diffs) # aggregate
    end

    for n in 1:N - 1
        diff = [ mesure_diff(current_choice, Xs[:, n + 1, p]) for p in perms ]
        best_p = perms[argmin(diff)]
        out[n + 1] = best_p
        current_choice = Xs[:, n + 1, best_p]
    end
    out
end
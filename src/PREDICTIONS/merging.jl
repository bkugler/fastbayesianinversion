using LinearAlgebra
using Random

using ..Probas

# --------------------------------------- Mixture merging --------------------------------------- #

"""
Return weight, and set in place mean and cov of merged gaussians eq (2-3-4)
"""
function _merge_2_gaussians!(w1::T, m1::VecOrSub{T}, C1::Symmetric{T,Matrix{T}}, w2::T, m2::VecOrSub{T}, C2::Symmetric{T,Matrix{T}},
                            out_m, out_cov, L::Int) where T
    logw = log(w1 + w2)
    if logw == - Inf  # extreme case, no influence on the mixture anyway
        return 0.
    end
    w1s = exp(log(w1) - logw)
    w2s = exp(log(w2) - logw)
    out_m .= (w1s .* m1) .+ (w2s .* m2)

    out_cov[:,1] .= m1 .- m2 # tmp storage
    for l1 in 1:L 
        for l2 in 2:L
            out_cov[l1,l2] = w1s * C1[l1,l2] + w2s * C2[l1,l2] + w1s * w2s * (out_cov[l1,1] * out_cov[l2,1])
        end 
    end
    for l in 2:L # using symetry
        out_cov[l,1] = out_cov[1,l]
    end 
    out_cov[1,1] = w1s * C1[1,1] + w2s * C2[1,1] + w1s * w2s * out_cov[1,1]^2 
    exp(logw)
end

"""
Compute upper bound for discrimination eq (21).
Returns discrimination value and merged gaussian
"""
function B(w1::T, m1::VecOrSub{T}, C1::Symmetric{T,Matrix{T}}, w2::T, m2::VecOrSub{T}, C2::Symmetric{T,Matrix{T}},
            out_m, out_cov, L::Int) where T
    out_w = _merge_2_gaussians!(w1, m1, C1, w2, m2, C2, out_m, out_cov, L)
    try
        _, _, _  = LinearAlgebra.logdet(out_cov), LinearAlgebra.logdet(C1), LinearAlgebra.logdet(C2)
    catch 
        C1 += Diagonal(1e-4 * ones(L))
        C2 += Diagonal(1e-4 * ones(L))
        out_cov .+= Diagonal(1e-4 * ones(L))
    end
    ld0, ld1, ld2  = LinearAlgebra.logdet(out_cov), LinearAlgebra.logdet(C1), LinearAlgebra.logdet(C2)
    d = 0.5 * ((w1 + w2) * ld0 - (w1 * ld1) - (w2 * ld2))
    d, out_w
end 


function find_pair_to_merge(weights::VecOrSub{T}, means::MatOrSub{T}, covs::Vector{Symmetric{T,Matrix{T}}}) where T
    L, K = size(means)
    best_disc, best_merged_w, best_merged_m, best_merged_cov = Inf, 0., zeros(T, L), zeros(T, L, L)
    best_i, best_j = 0, 0
    merged_w, merged_m, merged_cov  = 0., zeros(T, L), zeros(T, L, L)
    @inbounds for i in 1:K
        @inbounds for j in (i + 1):K
            d, merged_w = B(weights[i], view(means, :, i), covs[i], weights[j], view(means, :, j), covs[j], merged_m, merged_cov, L)
            if d < best_disc
                best_disc, best_merged_w = d, merged_w
                best_merged_m .= merged_m
                best_merged_cov .= merged_cov
                best_i, best_j = i, j
            end
        end
    end
    best_i, best_j, best_merged_w, best_merged_m, Symmetric(best_merged_cov)
end

function _K_step(current_weights::Vector{T}, current_means::Matrix{T}, 
    current_covs::FullCovs{T}) where T

    L, K = size(current_means)
    new_weights = zeros(K - 1)
    new_means = zeros(L, K - 1)
    new_covs::FullCovs{T} = fill(Symmetric(zeros(L, L)), K - 1)

    i, j, merged_w, merged_m, merged_cov = find_pair_to_merge(current_weights, current_means, current_covs)
    keep_w = [current_weights[k] for k in 1:K if !(k == i || k == j)]
    keep_m = hcat([current_means[:,k] for k in 1:K if !(k == i || k == j)]...)
    keep_cov = [current_covs[k] for k in 1:K if !(k == i || k == j)]
    @inbounds for k in 1:(K - 2)
        new_weights[k] = keep_w[k]
        new_means[:,k] .= keep_m[:,k]
        new_covs[k] = keep_cov[k]
    end
    new_weights[K - 1] = merged_w
    new_means[:, K - 1] .= merged_m
    new_covs[K - 1] = merged_cov

    new_weights, new_means, new_covs
end

struct MergingOptions
    K_merged::Int
    threshold::Float64
end
MergingOptions(K_merged::Int) =  MergingOptions(K_merged, 1e-10) # default values


"""
    Merges the given GMM model.
    Components with weight under `threshold` are first merged in 
    one preliminary step (usefull when K is very large)
"""
function merge(options::MergingOptions, weights::VecOrSub{T}, means::MatOrSub{T}, covs::FullCovs{T}) where T
    current_weights, current_means, current_covs = bulk_merge(weights, means, covs, options)
    K = length(current_weights)
    while K > options.K_merged
        current_weights, current_means, current_covs = _K_step(current_weights, current_means, current_covs)
        K -= 1
    end
    current_weights, current_means, current_covs
end 


function bulk_merge(weights::Vector, means::Matrix{T}, covs::FullCovs{T}, options::MergingOptions) where T 
    L, K = size(means)
    zipped = collect(zip(weights, (means[:,k] for k in 1:K), covs))
    s = sort(zipped; by=t -> t[1], rev=true)

    # we keep at least the best K_merged components
    out_w = [ t[1] for t in s[1:options.K_merged]]
    out_m::Vector{Vector{T}} = [ t[2] for t in s[1:options.K_merged]]
    out_c::FullCovs{T} = [ t[3] for t in s[1:options.K_merged]]

    to_merged_w::Vector{T}, to_merged_m::Vector{Vector{T}}, to_merged_c::FullCovs{T} = [], [], []
    for (w, m, c) in s[options.K_merged + 1:end]
        if w <= options.threshold
            push!(to_merged_w, w)
            push!(to_merged_m, m)
            push!(to_merged_c, c)
        else 
            push!(out_w, w)
            push!(out_m, m)
            push!(out_c, c)
        end
    end
    if length(to_merged_w) > 0 
        to_merged_m_mat::Matrix{T} = hcat(to_merged_m...)
        merged_w = sum(to_merged_w) 
        if merged_w != 0
            merged_m = Probas.mean_mixture(to_merged_w, to_merged_m_mat) / merged_w
            merged_c = Probas.covariance_mixture(to_merged_w  ./ merged_w, to_merged_m_mat, to_merged_c) * merged_w^2
            push!(out_w, merged_w)
            push!(out_m, merged_m)
            push!(out_c, merged_c)
        end
    end
    out_m_mat::Matrix{T} = hcat(out_m...)
    out_w, out_m_mat, out_c
end


""" Post traitement for GMM predictions : merging of centers, mode finding and regularization """
module Predictions 

include("merging.jl")
include("permutations.jl")
include("mode_finding.jl")

end
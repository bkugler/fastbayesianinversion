""" Implements some indicators to assess the multi-modality 
and the non-uniformity of the posterior """
module Indics

using LinearAlgebra
using Statistics

struct WeightedMean{T <: Number}
    mean::Vector{T}
    err::T 
end

""" Computes an estimator of the dispertion 
of the predictions. 
Formally, it is trace(Var(X)) / (L*C)
where C depends only on L, and X is a discrete random variable with law:
P(X = xi) = exp(-0.5 . err^2 )
"""
function dispersion(preds::Vector{WeightedMean{T}}) where T
    L = length(preds[1].mean)

    # filter out invalid solutions
    preds = [pred for pred in preds if all(isfinite.(pred.mean)) && isfinite(pred.err)]
    if length(preds) == 0 # pathological case
        return Inf, zeros(L)
    end

    mean = zeros(L)
    s = 0.
    for pred in preds 
        pertinence = exp(-0.5 * pred.err^2)
        s += pertinence
        mean .+= pertinence .* pred.mean
    end
    mean ./= s # normalisation

    # theoretical upper bound see Lim & McCann 2020 
    # 'Geometrical bounds for the variance and recentered moments'
    bound = 0.25 - (norm(mean .- 0.5)^2 / L)
    
    out = 0.
    for pred in preds
        out += exp(-0.5 * pred.err^2) * sum((pred.mean .- mean).^2) # norm squared
    end
    out = out / (s * L) # normalisation
    out / bound, mean
end

# returns the 4 first k-statistics (Fisher 1928; Kenney and Keeping 1951)
function _k_statistics(X::Vector{T}) where T 
    N = length(X)

    m = sum(X) / N 
    m2 = sum((X .- m).^2) / N
    m3 = sum((X .- m).^3) / N
    m4 = sum((X .- m).^4) / N

    k1 = m
    k2 = N * m2 / (N - 1)
    k3 = N^2 * m3 / ((N - 1) * (N - 2))
    k4 = N^2 * ((N + 1) * m4 - 3 * (N - 1) * m2^2) / ((N - 1) * (N - 2) * (N - 3))
    [k1, k2, k3, k4]
end

const _k1_unif = 0.5
const _k2_unif = 1 / 12
const _k3_unif = 0 
const _k4_unif = -1 / 120

# from F. Schmidt, J. Fernando / Icarus 260 (2015) 
function non_uniformity(X::Vector{T}) where T
    k1, k2, k3, k4 = _k_statistics(X)
    max(
        abs((k1 - _k1_unif) / _k1_unif),
        abs((k2 - _k2_unif) / _k2_unif),
        abs((k3 - _k3_unif) / (1 / 60)), # special case since k3_unif = 0
        abs((k4 - _k4_unif) / _k4_unif)        
    )
end

end
""" Helpers shared between differents inversion methods """
module Shared 
export CompleteInversion, update, Y_relative_err

using ...Log
using ...Store 
using ...Probas
using ...Gllim 
using ...Predictions
using ...Models
using ...Is 
using ...Comptimes

using LinearAlgebra
using Dates 

"""
    Store the detailed result of one invertion scheme (for one observation)
"""
struct CompleteInversion 
    key::String

    mean::Vector{Float64}
    cov::Vector{Float64} # might be zero when not supported by the inversion scheme
    y_err::Float64
    x_err::Float64 # for testing, we may know the "true" x; for real data, it should be ignored

    diagnostics::Dict{String,Float64}
    origin::String # for best prediction, key of the methods yielding it; enables to track back the chosen scheme
end
CompleteInversion(key::String, pred::Is.IsInversion) = CompleteInversion(key, pred.mean, pred.cov, Inf, Inf, Dict("ESS"  => pred.diagnostics.ess, "ES" => pred.diagnostics.es, "QN" => pred.diagnostics.qn), "")
CompleteInversion(key::String, mean::Vector; cov=nothing) = CompleteInversion(key, mean, cov === nothing ? similar(mean) : cov, Inf, Inf, Dict(), "")

function Y_relative_err(context::Models.Model, yobs::VecOrSub, xpred::VecOrSub, a, mu)
    L, D = length(xpred), length(yobs)
    valid = Models.is_valid(xpred, L)
    if !valid 
        return Inf 
    end
    calc = zeros(D)
    Models.f!(context, xpred, calc, D)
    calc .= a * calc .+ mu .- yobs
    return norm(calc) / norm(yobs) # relative
end
function Y_relative_err(context, Yobs, X, a, mu)
    _, N  = size(Yobs)
    out = zeros(N)
    for n in 1:N 
        out[n] = Y_relative_err(context, Yobs[:,n], X[:, n], a, mu)
    end
    return out
end

""" Store the results of several methods for one observation """
OneInversion = Dict{String,CompleteInversion}

"""
Store a serie of predictions, for one method and many observations, with optionnal diagnostics.
"""
struct MethodResult
    scheme::String # scheme
    
    Xmean::Matrix # the main result
    Xcovs::Matrix # zero when not supported
    
    Yerr::Vector # || y - F(x) ||_2 / || y ||_2
    Xerr::Vector # || x_obs - x_pred ||_2
    
    diagnostics::Dict{String,Vector} # label values
end

""" Set 'dst' at index 'dst_index' with the values of 'src' at 'src_index' """
function merge_result!(dst::MethodResult, src::MethodResult, dst_index::Int, src_index::Int)
    dst.Xmean[:, dst_index] .= src.Xmean[:, src_index]
    dst.Xcovs[:, dst_index] .= src.Xcovs[:, src_index]
    dst.Yerr[dst_index] = src.Yerr[src_index]
    dst.Xerr[dst_index] = src.Xerr[src_index]
    for (k, v) in pairs(dst.diagnostics)
        v[dst_index] = src.diagnostics[k][src_index]
end
end

""" Set 'dst' at index 'dst_index' with the values of 'src' at 'src_index' """
function merge_result!(dst::Dict{String,MethodResult}, src::Dict{String,MethodResult}, dst_index::Int, src_index::Int)
    for (k, v) in pairs(dst) 
        merge_result!(v, src[k], dst_index, src_index)
    end
end

""" Returns a mask of indices for which the result is zero (in math space) """
function failures(res::MethodResult, context::Models.Model)
    Xmean, _ = Models.from_physical(context, res.Xmean, res.Xmean)
    return [v == zeros(size(v)) for v in eachcol(Xmean)]
end

function MethodResult(X::Matrix; Xcovs=nothing, scheme="") 
    N = size(X, 2)
    Xcovs = Xcovs === nothing ? zeros(size(X)) : Xcovs
    MethodResult(scheme, X, Xcovs, NaN * ones(N), NaN * ones(N), Dict())
end

""" Extract the given scheme from the inversions """
function MethodResult(invs::Vector{OneInversion}, scheme::String)
    L, N = length(invs[1][scheme].mean), length(invs)
    means, covs, yerr, xerr = zeros(L, N), zeros(L, N), zeros(N), zeros(N)
    diagnostics = Dict{String,Vector}()
    for n in 1:N
        pred = invs[n][scheme]
        means[:,n] = pred.mean
        covs[:, n] = pred.cov
        yerr[n] = pred.y_err
        xerr[n] = pred.x_err

        for (label, diagn) in pairs(pred.diagnostics)
            # if the diag doest not exists, create it 
            ds = get!(diagnostics, label, NaN * zeros(N))
            ds[n] = diagn
    end
    end
    MethodResult(scheme, means, covs, yerr, xerr, diagnostics)
end

""" All the inversions method are applied to one observation
at a time. However, we generally want to plot them as series. 
This method goes from Obs -> Methods to Methods -> Obs,
assuming that all the observations have the same methods.
"""
function predictions_series(invs::Vector{OneInversion})::Dict{String,MethodResult}
    length(invs) > 0 || error("empty predictions")

    schemes = keys(invs[1])
    out = Dict{String,MethodResult}()
    for scheme in schemes 
        out[scheme] = MethodResult(invs, scheme)
    end 
    out
    end


""" Groups the data-generating params and the GLLiM hyperpameters """
struct TrainingParams 
    N_train::Int # size of the training set
    train_std::Float64 
    gllim_params::Gllim.Params
end

""" Convenience wrapper to generate training data, perform learning and save the result on disk."""
function generate_train_gllim(context::Models.Model, training_params::TrainingParams, st::Store.Storage, run::Bool)
    if run # do training
        Log.indent()

        ti = Dates.now()
        X, Y = Models.data_training(context, training_params.N_train, training_params.train_std)
        @assert all(isfinite.(X)) "X dataset is corrupted"
        @assert all(isfinite.(Y)) "Y dataset is corrupted"
        trained_gllim, ll_history = Gllim.train(training_params.gllim_params, X, Y)
        dura = Comptimes.since(ti)
        
        Store.save(st, [trained_gllim, ll_history, dura],  Store.FO_GLLIM, context, training_params)
        Log.deindent()
    end    
    trained_gllim::Gllim.GLLiM, ll_history, duration = Store.load(st, Store.FO_GLLIM, context, training_params)
    trained_gllim, ll_history, duration
end


""" 
    Implementation of the interface needed by `Is.IsModel`, 
    representing the posterior distribution
"""
struct IsMean <: Is.IsModel
    proposition::Is.IsProposition

    context::Models.Model
    y::VecOrSub{Float64}
    cov::DiagCov{Float64}
end

@inline Is.get_proposition(m::IsMean) = m.proposition
# we want to compute the expectancy of X so F(X) = X and D = L
@inline Is.get_L(m::IsMean) = Models.get_L(m.context)
function Is.log_p(m::IsMean, x::VecOrSub{T}, ::Int) where T
    Models.log_conditionnal_density(m.context, x, m.y,  m.cov)
end

""" 
    Implementation of the interface needed by `Is.IsModel`, 
    representing the posterior distribution, regularized around
    a candidate for mode.
"""
struct IsMode <: Is.IsModel
    proposition::Is.IsProposition

    context::Models.Model
    y::VecOrSub{Float64}
    cov::DiagCov{Float64}

    regularizer::Is.GaussianProposition{Float64}
    function IsMode(proposition::Is.IsProposition, context::Models.Model, y::VecOrSub{Float64},
        cov::DiagCov{Float64},mode::VecOrSub{Float64},mode_cov::CholCov{Float64}) 
        new(proposition, context, y, cov, Is.GaussianProposition(mode, mode_cov))
    end
end
    
@inline Is.get_proposition(m::IsMode) = m.proposition
# we want to compute the expectancy of X so F(X) = X and D = L
@inline Is.get_L(m::IsMode) = m.regularizer.L
function Is.log_p(m::IsMode, x::VecOrSub{T}, ::Int) where T
    lg = Is.log_q(m.regularizer, x)
    Models.log_conditionnal_density(m.context, x, m.y,  m.cov) + lg
end


function _safe_cholesky(covs::FullCovs{T}) where T
    chol_covs = CholCovs{T}(undef, length(covs))
    for (i, M) in enumerate(covs)
        chol_covs[i] =  Probas.safe_cholesky(M)
    end
chol_covs
end

""" Performs the GLLiM + merging steps of the inversion. May be used to perform additionnal inversion technics."""
function setup_inversion(context::Models.Model, yobs::VecOrSub{T}, ystd::VecOrSub{T}, trained_gllim::Gllim.GLLiM{T}, train_std::T, merging_options::Predictions.MergingOptions; add_prop_std::T=0.) where T
    D = trained_gllim.D
    K_m = merging_options.K_merged

    # we shift the covariance of the GLLiM model 
    sigma_obs = Diagonal(ystd.^2) # we want the covariance
    gllim_shifted = Gllim.add_bias(trained_gllim, 1., 0., sigma_obs)
    gllim_star = Gllim.inversion(gllim_shifted)

    # we get the inverse density from the shifted inverse
    weights_all_i, means_all_i = Gllim.conditionnal_density(gllim_star, yobs)
    full_posterior = Probas.Gmm{T}(weights_all_i, means_all_i, _safe_cholesky(gllim_star.Σ))

    # we merge the mixture
    weights_merged_gllim, means_merged_gllim, covs_merged_gllim = Predictions.merge(merging_options, weights_all_i, means_all_i, gllim_star.Σ)

    # we compute the mean and cov of the mixture
    mean_gllim = Probas.mean_mixture(weights_merged_gllim, means_merged_gllim)
    cov_gllim = diag(Probas.covariance_mixture(weights_merged_gllim, means_merged_gllim, covs_merged_gllim))

    # to avoid spurious errors on weak components
    chol_covs_merged_centroids = _safe_cholesky(covs_merged_gllim)
    merged_posterior = Probas.Gmm{T}(weights_merged_gllim, means_merged_gllim, chol_covs_merged_centroids)

    # we choose the (target) stat model : Sigma = Sigma_train + Sigma_obs
    Sigma = Diagonal(train_std^2 .* ones(D)) + sigma_obs
    
    merged_posterior_prop = merged_posterior
    if add_prop_std != 0 # if an additional std for the proposition is given, we have to invert and merge again 
        # we shift the covariance of the GLLiM model 
        sigma_obs = Diagonal((ystd .+ add_prop_std).^2) # we want the covariance
        gllim_shifted = Gllim.add_bias(trained_gllim, 1., 0., sigma_obs)
        gllim_star = Gllim.inversion(gllim_shifted)

        # we get the inverse density from the shifted inverse
        weights_all_i, means_all_i = Gllim.conditionnal_density(gllim_star, yobs)

        # we merge the mixture
        weights_merged_gllim, means_merged_gllim, covs_merged_gllim = Predictions.merge(merging_options, weights_all_i, means_all_i, gllim_star.Σ)

        # to avoid spurious errors on weak components
        chol_covs_merged_centroids = _safe_cholesky(covs_merged_gllim)
        merged_posterior_prop = Probas.Gmm{T}(weights_merged_gllim, means_merged_gllim, chol_covs_merged_centroids)
    end

    proposition = Is.GaussianMixtureProposition{T}(merged_posterior_prop)
    model = IsMean(proposition, context, yobs, Sigma)

    model_modes = Vector{IsMode}(undef, K_m)
    for k in 1:K_m # proposition around merged centroids
        mode_mean_prop, mode_cov_prop = merged_posterior_prop.means[ :, k],  merged_posterior_prop.chol_covs[k]
        proposition = Is.GaussianProposition(mode_mean_prop, mode_cov_prop)
        
        mode_mean, mode_cov = merged_posterior.means[ :, k],  merged_posterior.chol_covs[k]
        model_modes[k] = IsMode(proposition, context, yobs, Sigma, mode_mean, mode_cov)
    end

    mean_gllim, cov_gllim, model, model_modes, full_posterior, merged_posterior
end


end 
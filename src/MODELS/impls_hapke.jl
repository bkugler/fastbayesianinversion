# use Hapke.jl to implement Model 


using LinearAlgebra
using LaTeXStrings

include("HapkeFunc.jl")

using .HapkeFunc

""" Geometries of measurements, in degrees """
struct HapkeGeometries
    inc::Vector{Float64}
    eme::Vector{Float64}
    azi::Vector{Float64}
    function HapkeGeometries(inc, eme, azi) 
        if !(length(inc) == length(eme) == length(azi))
            error("geometries must have same length")
        end
        new(inc, eme, azi)
    end
end
HapkeGeometries(D::Int) = HapkeGeometries(zeros(D), zeros(D), zeros(D))
matrix(geom::HapkeGeometries) = hcat(geom.inc, geom.eme, geom.azi) # D x 3

struct Hapke <: Model
    _label::String
    geometries_infos::Vector{HapkeFunc.GeometryInfos{Float64}}
    partiel::Bool # true to ignore B0 & H 
    w_linear::Bool # if true, change F to a linearized version in w (useful for values of w near 1)
    options::HapkeFunc.HapkeOptions
end
Hapke(label::String, geometries::HapkeGeometries; partiel=true, w_linear=true, options=HapkeFunc.HapkeOptions()) = Hapke(
    label,
    [HapkeFunc.GeometryInfos(inc, eme, azi) for (inc, eme, azi) in zip(geometries.inc, geometries.eme, geometries.azi)],
    partiel, w_linear, options)

label(m::Hapke) = m._label
get_L(m::Hapke) = m.partiel ? 4 : 6 
get_D(m::Hapke) = length(m.geometries_infos)

_w(wtilde) = 1 - (1 - wtilde)^2
_wtilde(w) = 1 - sqrt(1 - w)

"""
    If `w_linear` is `true`, we work with a "linear" version of Hapke, with respect to omega. 
    If `partiel` is `true`, we use default values for H and B0.
"""
function f!(m::Hapke, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where {T <: Number}
    r, b, c =  x[2] * m.options.theta0_max, x[3] * m.options.b_max, x[4]
    w = x[1]
    if m.w_linear 
        w = _w(w)
    end
    if m.partiel 
        h, b0 = m.options.default_h, m.options.default_b0
    else 
        h, b0 =  m.options.h_min + (1 - m.options.h_min) * x[5], x[6]
    end 
    @inbounds for d in 1:D 
        out[d] = HapkeFunc.hapke(w, r, b, c, h, b0, m.geometries_infos[d], m.options.variant)
    end
end

# from math to physic, diagonal
_scaling_mat(m::Hapke) = m.partiel ? 
    [1,m.options.theta0_max,m.options.b_max,1] : 
    [1,m.options.theta0_max,m.options.b_max,1,1,1]

function to_physical(m::Hapke, x::VecOrSub{T}, cov::VecOrSub{T}) where T 
    A = _scaling_mat(m)
    x_scaled = A .* x
    if m.w_linear 
        x_scaled[1] =  _w(x_scaled[1])
    end
    if !m.partiel  
        x_scaled[5] = m.options.h_min + (1 - m.options.h_min) * x_scaled[5]
    end
    cov_scaled = A .* cov .* A 
    x_scaled, cov_scaled
end
function from_physical(m::Hapke, x::VecOrSub{T}, cov::VecOrSub{T}) where T 
    A = 1 ./ _scaling_mat(m)
    x_scaled = A .* x
    if m.w_linear 
        x_scaled[1] =  _wtilde(x_scaled[1])
    end
    if !m.partiel  
        x_scaled[5] = (x_scaled[5] - m.options.h_min) / (1 - m.options.h_min)
    end
    cov_scaled = A .* cov .* A 
    x_scaled, cov_scaled
end

""" Array of variable names """
function variables(m::Hapke) 
    if m.partiel 
        [L"\omega", L"\bar{\theta}", L"b", L"c"] 
    else 
        [L"\omega", L"\bar{\theta}", L"b", L"c", L"H", L"B_{0}"]
    end
end

""" Return the geometric angles used by the model """
function geometries(m::Hapke)::HapkeGeometries
    out  = HapkeGeometries(get_D(m))
    for (d, info) in enumerate(m.geometries_infos)
        out.inc[d], out.eme[d], out.azi[d], = HapkeFunc.geometry(info)
    end
    out
end
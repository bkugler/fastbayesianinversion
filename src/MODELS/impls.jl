# Simple models, to test the methodology


# -------------------------------- Linear --------------------------------

""" Linear function """
struct Linear <: Model
    _label::String
    matrix::Matrix{Float64}
end
Linear() = Linear("Linear", 1. .* [1   0    0   0;
                                                         0   0.5  0   0;
                                                         0   0    1   0;
                                                         0   0    0   3;
                                                         0.2 0    0   0;
                                                         0   -0.5 0   0;
                                                         -0.2 0   -1  0;
                                                         -1  0    0   0;
                                                         0   0    0   -0.7;
                                                         0   0    0   0])
LinearSmall() = Linear("Linear", 0.5 .* [0.9 0.1;
                                                            0 0.7;
                                                            0 0])
LinearTrivial() = Linear("Linear", 0.5 * ones(1, 1))

label(m::Linear) = m._label
get_L(m::Linear) = size(m.matrix)[2]
get_D(m::Linear) = size(m.matrix)[1]
f!(m::Linear, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T = out .= m.matrix * x


# --------------------------- Surface -----------------------------

""" Surface """
struct SimpleSurface <: Model
end

label(::SimpleSurface) = "Simple surface"
get_L(::SimpleSurface) = 2
get_D(::SimpleSurface) = 1
f!(::SimpleSurface, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T = out[1] = x[1]^2 + x[2]^3

# ---------------------- Injective non linear ----------------------

""" Non-linear and injective function """
struct InjectifHard <: Model
    linear_part::Matrix 
end
InjectifHard() = InjectifHard(0.5 * [1   2    2   1;
                                    0   0.5  0   0;
                                    0   0    1   0;
                                    0   0    0   3;
                                    0.2 0    0   0;
                                    0   -0.5 0   0;
                                    -0.2 0   -1  0;
                                    -1  0    2   0;
                                    0   0    0   -0.7])
label(::InjectifHard) = "Complex, non-linear, injective function"
get_L(m::InjectifHard) = size(m.linear_part)[2]
get_D(m::InjectifHard) = size(m.linear_part)[1]
function f!(m::InjectifHard, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where T 
    x = exp.(x) # exp part
    out .= m.linear_part * x # linear part
    # quadratic part
    L = get_L(m)
    @inbounds for l in 1:L
        out[l] += 5 * (x[l] + 0.5)^2
    end
    for l in (L + 1):min(2 * L, D)
        out[l] = (x[l - L] + 0.5)^2
    end
end

# ----------------------- Double solution -----------------------

struct DoubleSolutions <: Model
    L::Int
end

label(::DoubleSolutions) = "Simple function with 2 solutions"
get_L(m::DoubleSolutions) = m.L
get_D(m::DoubleSolutions) = m.L
function f!(::DoubleSolutions, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T
    out .= x
    out[1] = (x[1] - 0.5)^2 # yields 2 solution
end

function alternative_solution(::DoubleSolutions, X::MatOrSub) 
    out = copy(X)
    out[1, :] = 1 .- out[1, :]
    out
end

# ----------------------- Triple solutions -----------------------

struct TripleSolutions <: Model
    L::Int
end

label(::TripleSolutions) = "Simple function with 3 solutions"
get_L(m::TripleSolutions) = m.L
get_D(m::TripleSolutions) = m.L
function f!(::TripleSolutions, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T
    # yields 3 solution in [0, 1], since f(x + 2/3) = f(2/3 - x) = f(x)
    out .=  0.5 .* (1 .+ cos.(3 * pi .* x))
end

""" Override to handle 3 solutions. `margin` acts as a separator between Xs"""
function data_observations(m::TripleSolutions, Nobs::Int, margin::Number)
    X, _ = invoke(data_observations, Tuple{Model,Int}, m, Nobs)
    # map to [margin, 1/3-margin]
    X1 = margin .+ (1 / 3 - margin - margin) .* X
    X2, X3 = X1 .+ 2 / 3, 2 / 3 .- X1
    Yobs = F(m, X1)
    X1, X2, X3, Yobs
end


# ------------------ Non-linear with double solutions ------------------
struct DoubleHard <: Model
    matrix::Matrix{Float64} # linear part 
end
DoubleHard() = DoubleHard([1   2    2   1;
                            0   0.5  0   0;
                            0   0    1   0;
                            0   0    0   3;
                            0.2 0    0   0;
                            0   -0.5 0   0;
                            -0.2 0   -1  0;
                            -1  0    2   0;
                            0   0    0   -0.7])

label(::DoubleHard) = "Non-linear function with 2 solutions"
get_L(m::DoubleHard) = size(m.matrix)[2]
get_D(m::DoubleHard) = size(m.matrix)[1]
function f!(c::DoubleHard, x::VecOrSub{T}, out::VecOrSub{T}, ::Int) where T 
    L = get_L(c)
    x = copy(x)
    x[L - 1] = (x[L - 1] - 0.5)^2 / 0.25 # non injectif
    x .= exp.(x) # non linear
    
    out .= 0.5 * c.matrix * x
    @inbounds for l in 1:L
        out[l] += 5 * (x[l] + 0.5)^2
    end
    for l in L + 1:(2 * L)
        out[l] = (x[l - L] + 0.5)^2
    end
end

function alternative_solution(ct::DoubleHard, X::MatOrSub)
    X = copy(X)
    L = get_L(ct) 
    X[L - 1, :] = 1 .- X[L - 1,:]
    X
end
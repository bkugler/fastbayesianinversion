using Random
using Sobol 

using ..Probas

""" 
    Base type for all functionnal models 
    Must implement the following methods
"""
abstract type Model end

label(m::Model) = error("label not implemented for type $(typeof(m))")
get_L(m::Model) = error("get_L not implemented for type $(typeof(m))")
get_D(m::Model) = error("get_D not implemented for type $(typeof(m))")

""" Write f(`x`) in `out`, which may be non zero. `D` is the precomputed length of `out`.
`x` is always in [0,1]^L (eventually normalized beforehand), so re-scaling should be applyied here if needed
"""
f!(m::Model, x::VecOrSub{T}, out::VecOrSub{T}, D::Int) where T = error("f! not implemented for type $(typeof(m))")

""" Convert a mathematical value in [0,1] and a diagonal covariance to physical ones """
to_physical(m::Model, x::VecOrSub{T}, cov::VecOrSub{T}) where T = x, cov
""" Inverse to `to_physical` """
from_physical(m::Model, x::VecOrSub{T}, cov::VecOrSub{T}) where T = x, cov

""" Array of variable names """
variables(m::Model) = ["x$i" for i in 1:get_L(m)]

""" For double solutions context, return the alternatives """
alternative_solution(m::Model, X::MatOrSub) = nothing


# -------------------------------- Wrappers --------------------------------

"""
Compute Y = F(X) for a matrix.
"""
function F(m::Model, X::MatOrSub{T}) where T
    L, N = size(X)
    D = get_D(m)
    out = zeros(T, D, N)
    @inbounds for n in 1:N 
        f!(m, view(X, :, n), view(out, :, n), D)
    end
    out
end

""" ModelClosure acts like a function, but its serialization refers
to the underlying model, making it usable as meta data for storage. """
struct ModelClosure <: Function
    _f::Function
    _m::Model
end

# make ModelClosure callable
function (ifunc::ModelClosure)(x::VecOrSub)
    ifunc._f(x)
end

function Base.show(io::IO, x::ModelClosure)
    write(io, "closure for model ")
    show(io, x._m)
end

""" Returns a closure computing `F` for a single vector """ 
function closure(m::Model)
    D = get_D(m)
    _f = function F(x::VecOrSub)
        tmp = zeros(D)
        f!(m, x, tmp, D)
        tmp 
    end
    ModelClosure(_f, m)
end
    
function to_physical(m::Model, Xmean::Matrix{T}, Xcov::Matrix{T}) where T
    L, N = size(Xmean)
    Xmean_scaled = zeros(L, N)
    Xcov_scaled = zeros(L, N)
    for n in 1:N 
        Xmean_scaled[:,n], Xcov_scaled[:, n] = to_physical(m, Xmean[:, n], Xcov[:, n])
    end
    Xmean_scaled, Xcov_scaled
end

function from_physical(m::Model, Xmean::Matrix{T}, Xcov::Matrix{T}) where T
    L, N = size(Xmean)
    Xmean_scaled = zeros(L, N)
    Xcov_scaled = zeros(L, N)
    for n in 1:N 
        Xmean_scaled[:,n], Xcov_scaled[:, n] = from_physical(m, Xmean[:, n], Xcov[:, n])
    end
    Xmean_scaled, Xcov_scaled
end

function from_physical(m::Model, Xmean::Matrix{T}) where T
    out, _ = from_physical(m, Xmean, similar(Xmean))
    out 
    end

# ------------------------- Statistical model ------------------------- 

"""
Check whether `x` is in [0,1]^L 
"""
function is_valid(x::VecOrSub{T}, L::Int) where T
    @inbounds for l in 1:L 
        if (x[l] > 1) || (x[l] < 0)
            return false
        end
    end
    true
    end


"""
Compute the log density of y | x, given the model Y = a.F(X) + mu + eps , 
where eps ~ N(0, Sigma).
It's also the log density of x | y, up to the unknow constant p(y),
assuming X is uniform.
"""
function log_conditionnal_density(m::Model, x::VecOrSub{T}, y::VecOrSub{T}, 
                                a::T, mu::Union{Vector{T},T}, Sigma::CovType{T}) where T
    L, D = get_L(m), get_D(m)
    if !is_valid(x, L) return - Inf end
    tmp = zeros(T, D)
    f!(m, x, tmp, D)
    tmp .= a .* tmp .+ mu 
    Probas.log_gaussian_density(tmp, y, Sigma, get_D(m))
end
""" Assume  a = 1 and mu = 0 """
log_conditionnal_density(m::Model, x, y, Sigma) = log_conditionnal_density(m, x, y, 1., 0., Sigma)

"""
    Generate data suitable for GLLiM training, following the same model as `log_conditionnal_density`.
    `X` are generated with Sobol method, and scrambled
"""
data_training(m::Model, N::Int, Ystd::Float64) =  data_training(m, N, Ystd, 1., 0.)

_add_offset(Y, mean::Number) = Y .+ mean
_add_offset(Y, mean::Vector) = ( (D, N) = size(Y); Y .+ reshape(mean, D, 1) )
function data_training(m::Model, N::Int, Ystd::Number, a::Number, mu::Union{Number,Vector})
    L = get_L(m)
    s = Sobol.SobolSeq(L)
    X::Matrix{Float64} = hcat([next!(s) for i in 1:N]...) # type hint needed to avoid infinite recursion. Bug ?
    noise = 0.05 * (rand(size(X)...) .- 0.5) # scrambling
    mask = (X .> 0.025) .& (X .< 0.975)
    X[mask] .+= noise[mask] # avoid to reach the boundaries
    Y = F(m, X)
    Y = _add_offset(a * Y, mu)
    Y .+= Ystd * randn(size(Y))
    X, Y 
    end

""" Generate arbitrary (X, Y) values, useful for synthetic experiments. No noise on Y is added. """
function data_observations(m::Model, Nobs::Int)
    _wave_sample(phase_index) = 0.4 * sin.(collect(1:Nobs) / Nobs * 2 * pi  .+ phase_index * pi / 4) .+ 0.5

    Xobs =  vcat([ _wave_sample(l)' for l in 1:get_L(m)]...)
    Yobs = F(m, Xobs)
    Xobs, Yobs
end
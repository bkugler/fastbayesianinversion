"""
    Defines an interface to the physical model to invert,
    which exposes context information, dimensionnality, and the actual functionnal.
"""
module Models 

include("defs.jl")
include("impls.jl")
include("impls_hapke.jl") # hapke model 

end
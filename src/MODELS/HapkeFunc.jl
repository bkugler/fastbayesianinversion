"""
    Implementation of Hapke's radiative model in Julia. (Benoit Kugler & Sylvain Douté 2020)
    Physical argument order : w, theta0, b, c, H, B0
"""
module HapkeFunc 

abstract type Variant end

""" HapkeOptions exposes default values for the hyper parameters
of the Hapke model """
struct HapkeOptions 
    variant::Variant
    theta0_max::Float64 
    # to avoid numerical issue near 1:
    # when azi = 0 and inc = eme, b = 1 yields infinite (NaN) reflectance
    b_max::Float64
    default_b0::Float64
    default_h::Float64
    h_min::Float64 # to avoid numerical issue near 0
end 
HapkeOptions() = HapkeOptions(Variant2002(), 30., 0.99, 0, 0.1, 0.01) # default values

struct Variant2002 <: Variant end
struct Variant1993 <: Variant end


""" Store pre-computed values linked to a geometry of observation """
struct GeometryInfos{T <: Number}
    geom_infos::NTuple{8,T}
    g::T
    cosg::T
    alpha::T
end

function GeometryInfos(inc::T, eme::T, azi::T) where T
    theta0r = inc * pi / 180
    thetar = eme * pi / 180
    phir = azi * pi / 180
    geom_infos = _geom_roughness(theta0r, thetar, azi)
    cosg, g = phase_angle_rad(theta0r, thetar, phir)
    alpha = 4 * cos(theta0r)

    GeometryInfos{T}(geom_infos, g, cosg, alpha)
end
function geometry(info::GeometryInfos)
    # to keep in sync with _geom_roughness
    theta0r = info.geom_infos[1]
    thetar = info.geom_infos[2]
    azir = info.geom_infos[7]
    return theta0r * 180 / pi, thetar * 180 / pi, azir * 180 / pi
end

# -------------------------------- Helpers --------------------------------
function fast_e1_e2(R, x::T, x0::T) where T
    tr = tan(R)
    tr2 = tr^2
    trtx = tr * tan(x)
    trtx0 = tr * tan(x0)
    if trtx == 0
        e1x = 0
        e2x = 0
    else
        e1x = exp(-2 / (trtx * pi))
        e2x = exp(-1 / (trtx^2 * pi ))
    end 
    if trtx0 == 0
        e1x0 = 0
        e2x0 = 0
    else
        e1x0 = exp(-2 / (trtx0  * pi))
        e2x0 = exp(-1 / (trtx0^2 * pi ))
    end
    e1x, e2x, e1x0, e2x0, tr
end


function h_variant(::Variant2002, x::T, y::T) where T
    (1 + 2 * x) / (1 + 2 * x * y)
end

function h_variant(::Variant1993, x::T, y::T) where T # eq 3 from icarus Schimdt
    u = (1 - y) / (1 + y)
    1 / ( 1 - (1 - y) * x * (  u  +  (1 - (0.5 + x) * u) * log1p(1 / x) ))
end

"""
    Only geometrical-dependant computations for roughness. Input are in radians, except `azi`.
"""
function _geom_roughness(theta0r::T, thetar::T, azi::T) where T
    cose = cos(thetar)
    sine = sin(thetar)
    cosi = cos(theta0r)
    sini = sin(theta0r)

    azir = azi * pi / 180.  # radians

    f::T = 0
    if azi != 180
        f = exp(-2 * tan(azir / 2))
    end
    theta0r, thetar, cose, sine, cosi, sini, azir, f
end

"""
    phi in degrees for numerical stability
"""
function compute_roughness(geom_infos::NTuple{8,T}, R) where T
    theta0, theta, cose, sine, cosi, sini, phir, f = geom_infos

    xidz = 1 / sqrt(1 + pi * (tan(R)^2))
    e1R, e2R, e1R0, e2R0, tR = fast_e1_e2(R, theta, theta0)

    mu_b = xidz * (cose + sine * tR * e2R / (2 - e1R))
    mu0_b = xidz * (cosi + sini * tR * e2R0 / (2 - e1R0))

    if theta0 <= theta
        mu0_e = xidz * (cosi + sini * tR * (cos(phir) * e2R + (sin(phir / 2)^2) * e2R0) / (
                2 - e1R - (phir / pi) * e1R0))
        mu_e = xidz * (cose + sine * tR * (e2R - (sin(phir / 2)^2) * e2R0) / (
                2 - e1R - (phir / pi) * e1R0))

        S = mu_e * cosi * xidz / mu_b / mu0_b / (1 - f + f * xidz * cosi / mu0_b)
    else
        mu0_e = xidz * (cosi + sini * tR * (e2R0 - sin(phir / 2)^2 * e2R) / (
                2 - e1R0 - (phir / pi) * e1R))

        mu_e = xidz * (cose + sine * tR * (cos(phir) * e2R0 + sin(phir / 2)^2 * e2R) / (
                2 - e1R0 - (phir / pi) * e1R))

        S = mu_e * cosi * xidz / mu_b / mu0_b / (1 - f + f * xidz * cose / mu_b)
    end
    mu0_e, mu_e, S
end

function phase_angle_rad(theta0r::T, thetar::T, azir::T)where T
    cosg = cos(theta0r) * cos(thetar) + sin(thetar) * sin(theta0r) * cos(azir)
    cosg, acos(cosg)
end
phase_angle(geom::Vector) = phase_angle_rad(geom[1] * pi / 180, geom[2] * pi / 180, geom[3] * pi / 180)[2] * 180 / pi



""" Henyey-Greenstein function """
function phg2(b::T, c::T, cosg::T) where T
    bc = 2 * b * cosg
    b2 = b^2
    P =  (1 - c) * (1 - b2) / ((1 + bc + b2)^1.5)
    P = P + c * (1 - b2) / ((1 - bc + b2)^1.5)
    P
    end

# compute the intermediate quantities 
function _hapke_steps(variant::Variant, w::T, r1::T, h::T, b0::T, geom::GeometryInfos{T}) where T
    r = r1 * pi / 180.  # radians
    MUP, MU, S = compute_roughness(geom.geom_infos, r)

    B = b0 * h / ( h + tan(geom.g / 2) )
    gamma = sqrt(1 - w)
    H0 = h_variant(variant, MUP, gamma)
    H = h_variant(variant, MU, gamma)
    a = (w / (MU + MUP)) * MUP  / geom.alpha
    a, S, B, H0, H
end

""" Entry point """
function hapke(w::T, theta_bar::T, b::T, c::T, h::T, b0::T, geom::GeometryInfos{T}, variant::Variant) where T
    P = phg2(b, c, geom.cosg)
    a, S, B, H0, H = _hapke_steps(variant, w, theta_bar, h, b0, geom)
    reff = a *  ((1 + B) * P + (H0 * H) - 1)  * S 
    reff 
end

""" 
Given a reflectance, compute the phase function without the need
of b and c 
"""
function phase_from_reflectance(y::T, w::T, r1::T, h::T, b0::T,  geom::GeometryInfos{T}, variant::Variant) where T
    a, S, B, H0, H = _hapke_steps(variant, w, r1, h, b0, geom)
    P = ( y / (a * S) - H0 * H + 1 ) / (1 + B)
    P
end 


""" Empirical relation known as Hockey-Stick """
function Crelation(b)
    cp =  3.29 * exp.(-17.4 * (b.^2)) .- 0.908
    (cp .+ 1) / 2
end


end
""" Implements a convenient on disk storage,
based on hyper-parameters of the experiments
"""
module Store

using CRC32c
using Serialization
using Dates

"""
Possibles subdirectorys
"""
const FO_GLLIM = "Gllim/"
const FO_NOISE =  "Noise/"
const FO_GRAPHS =  "Graphs/"
const FO_BENCHMARK =  "Benchmark/"
const FO_MISC =  "Misc/"
const FO_PREDICTIONS =  "Predictions/"

struct Storage
    base_path::String # separator terminated
end

get_hash(metas...) = join([string(CRC32c.crc32c(repr(meta))) for meta in metas], "-")

remove(st::Storage) = rm(st.base_path, recursive=true)

"""
Return a suitable path to save data 
"""
function get_path(st::Storage, folder, metas... ; format_filename=nothing, label="")
    h = get_hash(metas...)
    if format_filename !== nothing
        h = format_filename(h)
    end
    
    dir = st.base_path * folder 
    if !isdir(dir)
        mkpath(dir)
    end
    if label != ""
        label  = label * "__" # to better visualisation
    end
    dir * label  * h
end 

function save(st::Storage, data, folder::String, metas...)
    meta_data = Dict("__data__" => data, "__saved__" => Dates.now())
    path = get_path(st, folder, metas...)
    f = open(path, "w")
    serialize(f, meta_data)
    close(f)
    @debug "Saved in $(path)"
end


function load(st::Storage, folder::String, metas...)
    path = get_path(st, folder, metas...)
    f = nothing
    try
        f = open(path, "r")
    catch e
        error("StoreError: can't find $path")
    end
    d = deserialize(f)
    close(f)
    dir = folder[1:end - 1]
    @debug "Loaded data from $(dir) (saved on $(d["__saved__"]))"
    d["__data__"]
end


end
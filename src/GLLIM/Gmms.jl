"""
Implements a simple EM algorithm for learning GMM. Initialization is made with kmeans
    
"""
module Gmms 
    
using LinearAlgebra
using Random

using ..Probas
using ..Log  

mutable struct EmParams 
    K::Int 
    max_iterations::Int
    max_iterations_kmeans::Int
    cov_floor::Float64
end


# ---------------------------------------------------------------------------------------------- #
# ------------------------------------------- Kmeans ------------------------------------------- #
# ---------------------------------------------------------------------------------------------- #

function kmeans(X::Matrix{T}, initial_centers::Matrix{T}, max_iterations::Int) where T
    L, N = size(X)
    _, K = size(initial_centers)

    # starts with given centers
    centers = copy(initial_centers)
    assignments = zeros(Int, N) # filled by e step
    cluster_sizes = zeros(Int, K) # filled by e step

    for _ in 1:max_iterations
    # e step 
        cluster_sizes .= 0 # reset
        for n in 1:N 
            nearest_index, smallest_distance = 1, Inf
            for k in 1:K 
                dist = norm(view(X, :, n) .- view(centers, :, k))
                if dist < smallest_distance 
                    nearest_index = k 
                    smallest_distance = dist
                end
            end
            assignments[n] = nearest_index
            cluster_sizes[nearest_index] += 1
        end

    # m step 
    # for unused cluster, we keep the previous value 
    # for the others we reset 
        centers[:, cluster_sizes .!= 0] .= 0. 
        for (n, center_index) in enumerate(assignments)
            centers[:, center_index] .+= X[:, n] # vector n is assigned to cluster center_index
        end
    # normalize
        for (k, size) in enumerate(cluster_sizes)
            if size != 0
                centers[:, k] ./= size
            end
        end
    end
    centers, assignments
end


# ---------------------------------------------------------------------------------------------- #
#  Simple GMM learning implementation, with multi threading. Add covariance numerical stability  #
# ---------------------------------------------------------------------------------------------- #

function _e_step!(X::MatOrSub{T}, pi::Vector{T}, c::Matrix{T}, Sigma::FullCovs{T},
out_logrnk::MatOrSub{T}, out_rk::VecOrSub{T}) where T
    N, K = size(out_logrnk)
    # for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        Probas.log_gaussian_density!(X, view(c, :, k), Sigma[k], view(out_logrnk, :, k)) # so far in log
        @inbounds for n in 1:N
            out_logrnk[n,k] += log(pi[k])
        end
        out_rk[k] = 0. # reset
    end

    log_ll = 0.
    @inbounds for n in 1:N  # sum et normalisation 
        log_rk = Probas.logsumexp(view(out_logrnk, n, :), K)
        @inbounds for k in 1:K
            out_logrnk[n,k] -= log_rk 
            out_rk[k] += exp(out_logrnk[n,k])
        end
        log_ll += log_rk # compute whole log_likelihood
    end
    log_ll
end

function _m_step!(X::MatOrSub{T}, log_rnk::MatOrSub{T}, rk::VecOrSub{T},
    pi_out, c_out, Sigma_out, cov_floor::T) where T
    N, K = size(log_rnk)
    L, _ = size(c_out)
    # for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        pi_out[k] = rk[k] / N
        tmp_c = zeros(T, L)
        tmp_Sigma = zeros(T, L, L)
        coeffs = zeros(T, N)
        @inbounds for n in 1:N 
            coeff = exp(log_rnk[n,k] - log(rk[k]))
            @inbounds for l in 1:L 
                tmp_c[l] += coeff * X[l,n] 
            end
            coeffs[n] = coeff
        end
        u = zeros(T, L)
        @inbounds for n in 1:N 
            coeff = coeffs[n]
            @inbounds for l in 1:L 
                u[l] = tmp_c[l] - X[l,n]
            end
            @inbounds for l1 in 1:L 
                for l2 in 1:l1  
                    tmp_Sigma[l2,l1] += coeff * u[l2] * u[l1]
                end
            end
        end
        @inbounds for l1 in 1:L  # numerical regularisation
            tmp_Sigma[l1,l1] += cov_floor
        end
        c_out[:,k] = tmp_c
        Sigma_out[k] = Symmetric(tmp_Sigma)
    end

end

"""
 Performs em training, against `X`, with initial values given 
 Also returns the "labels" and the log-likehood
"""
function em(gmm::Probas.Gmm{T}, X::Matrix{T}, max_iterations::Int, cov_floor::T) where T
    weights, means, covs = copy(gmm.weights), copy(gmm.means), [Symmetric(c * c') for c in gmm.chol_covs]
    _, N = size(X)
    L, K = size(means)

    Log.indent()
    @debug("Running $max_iterations iter. EM with $K Gaussians in $L dimensions")
    Log.deindent()
    
    out_logrnk, out_rk, log_ll  = zeros(T, N, K), zeros(T, K), -Inf
    for i in 1:max_iterations
        log_ll =  _e_step!(X, weights, means, covs, out_logrnk, out_rk)
        _m_step!(X, out_logrnk, out_rk, weights, means, covs, cov_floor)
    end
    Probas.Gmm{T}(weights, means, [cholesky(Matrix{T}(S)).L for S in covs]), out_logrnk, log_ll
end


"""
 Performs em training against `X`. Initialization of the assignments
is made with kmeans, which in turn is initialized with `K`random elements
of `X`.
"""
function em(X::Matrix{T}, params::EmParams) where T
    K = params.K
    L, N = size(X)

    indexes = randperm(N)[1:K] # choose sample for centers start 
    centers = X[:, indexes] 
    
    Log.indent()
    @debug "Running $(params.max_iterations_kmeans) iter. initialization with kmeans"
    _, labels = kmeans(X, centers, params.max_iterations_kmeans)
    Log.deindent()

    # compute rnk from labels 
    rnk = zeros(N, K)
    for (n, center_index) in enumerate(labels)
        rnk[n, center_index] = 1
    end
    rnk ./= sum(rnk, dims=2) # normalize
    rk = sum(rnk, dims=1)[1, :]
    log_rnk = log.(rnk)

    # m step 
    weights = zeros(T, K)
    means = zeros(T, L, K)
    covs = FullCovs{T}(K, L)
    _m_step!(X, log_rnk, rk, weights, means, covs, params.cov_floor)
    gmm = Probas.Gmm{T}(weights, means, [cholesky(Matrix{T}(S)).L for S in covs])

    # finally, run the EM steps
    return em(gmm, X, params.max_iterations, params.cov_floor)
end

end
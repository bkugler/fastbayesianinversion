using Printf

using ..Gmms
using ..Log 

include("gllim_defs.jl")

""" Group initialisation method. Must implement `getK` and `setK!` """
abstract type InitParams end

getK(p::InitParams) = error("method 'getK' not implemented for type $(typeof(p))")
setK!(p::InitParams, K::Int) = error("method 'setK!' not implemented for type $(typeof(p))")

include("gllim_init_func.jl")

"""
 Performs GMM initialisation, with `nb_starts` parallel runs 
"""
struct MultiInitParams <: InitParams
    nb_starts::Int 
    em_params::Gmms.EmParams
end
getK(p::MultiInitParams) = p.em_params.K
setK!(p::MultiInitParams, K::Int) = p.em_params.K = K


"""
    Create and initialize a GLLiM model, using a joint GMM 
    model, itself initialized through kmeans (with random start) 
    Also returns the log-likelihood
"""
function initialisation(X::Matrix{T}, Y::Matrix{T}, em_params::Gmms.EmParams) where T
    L, _ = size(X)
    joint_data = vcat(X, Y) # joint model 

    Log.indent()
    @debug "GLLiM initialisation with joint GMM"
    gmm, log_rnk, log_ll = Gmms.em(joint_data, em_params) # run gmm
    Log.deindent()
    GLLiM(gmm, L), log_rnk, log_ll # convert back to gllim; the likelihood is invariant
end

"""
    Create and initialize a (full) GLLiM model, with several joint GMM runs. 
    The result is returned as the log of the labels r_nk = P(Z = K | X,Y)
"""
function initialisation(X::Matrix{T}, Y::Matrix{T}, params::MultiInitParams) where T
    L, N = size(X)
    D, _ = size(Y)
    K = getK(params)
    best_ll, best_log_rnk = -Inf, nothing

    @debug "Starting $(params.nb_starts) initialisations ..."
    Log.indent()
    for i in 1:params.nb_starts
        @debug "Start $(i) ..." 
        _, log_rnk, log_likehood = initialisation(X, Y, params.em_params)
        @debug "--> average log likelihood : $(@sprintf("%.3f", log_likehood / N))"
        if log_likehood >= best_ll
            best_log_rnk = log_rnk
            best_ll = log_likehood 
        end
    end
    Log.deindent()

    best_log_rnk, best_ll
end


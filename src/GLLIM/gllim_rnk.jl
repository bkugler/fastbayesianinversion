using ..Probas 

"""
 Compute the density of (x,y). Returns log(rnk) and log(phi(x,y))
"""
function next_rnk(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_π::VecOrSub{Ty},
                c::MatOrSub{Ty}, Γ::CovsType{Ty}, A::Vector{Matrix{Ty}}, 
                b::MatOrSub{Ty},  Σ::CovsType{Ty}) where {Ty}

    L, K = size(c)
    D, N = size(Y)

    out_log_rnk = zeros(Ty, N, K)
    out_logll = zeros(Ty, N)

    NTHREADS = Threads.nthreads()
    tmp_DNs = zeros(Ty, D, N, NTHREADS)
    tmp_Ns = zeros(Ty, N, NTHREADS)

    # @inbounds for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        tid = Threads.threadid()
        Probas.log_gaussian_density!(X, view(c, :, k), Γ[k], view(out_log_rnk, :, k))

        Ak = A[k]
        @inbounds for n in 1:N
            @inbounds for d in 1:D
                tmp_DNs[d,n, tid] = 0.
                @inbounds for i in 1:L
                    tmp_DNs[d,n, tid] += Ak[d,i] * X[i,n]
                end
                tmp_DNs[d,n, tid] += b[d,k]
            end
        end

        Probas.log_gaussian_density!(Y, view(tmp_DNs, :, :, tid), Σ[k], view(tmp_Ns, :, tid))
        log_pik = log_π[k]
        
        @inbounds for n in 1:N
            out_log_rnk[n,k] += tmp_Ns[n, tid] + log_pik  # in log
        end

    end
    Probas.normalize_log!(out_log_rnk, out_logll)
    out_log_rnk, out_logll
end
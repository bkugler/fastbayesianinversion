using LinearAlgebra
using Sobol 

using ..Probas

include("gllim_rnk.jl")

"""
    Init the GLLiM model, using the given functionnal F, and its derivative (computed by finite difference)
    This pattern makes sense essentially when X is uniform, and Y = F(X) + eps
    The result is guaranteed to be deterministic.
"""
mutable struct FunctionnalInitParams <: InitParams 
    """ Signature : F(x::VecOrSub) -> y::Vector """
    K::Int
    F::Function
    eps_diff_finite::Float64
end
FunctionnalInitParams(K::Int, F::Function) = FunctionnalInitParams(K, F, 1e-7)

getK(p::FunctionnalInitParams) = p.K
setK!(p::FunctionnalInitParams, K::Int) = p.K = K

""" Returns a GLLiM initialized with F """
function initialisation_gllim(X::Matrix{T}, Y::Matrix{T}, params::FunctionnalInitParams) where T
    L, N = size(X)
    D, _ = size(Y)
    K = params.K

    @debug "Starting functionnal initialisation..."

    seq = Sobol.SobolSeq(L)
    # a Sobol sequence is deterministic; this result depends only on L and K
    c = hcat([next!(seq) for i in 1:K]...)
    π = ones(K) / K # uniform 
    A = [ Probas.dF_diff_finite(params.F, c[:, k], params.eps_diff_finite, D) for k in 1:K]
    b = hcat([params.F(c[:, k]) .- A[k] * c[:, k] for k in 1:K]...)

    # use rnk = 1/N to initialize Γ and Σ
    Γ, Σ = FullCovs{T}(K, L), FullCovs{T}(K, D)
    for k in 1:K
        G, S = zeros(T, L, L), zeros(T, D, D)
        for n in 1:N
            # Gamma
            u = X[:, n] - c[:, k]
            G += u * u'
            # Sigma
            v = Y[:, n] - A[k] * X[:, n] - b[:, k]
            S += v * v'
        end
        Γ[k] = Symmetric(G ./ N)
        Σ[k] = Symmetric(S ./ N)
    end

    Gllim.GLLiM(π, c, Γ, A, b, Σ)
end

"""
    Create and initialize a (full) GLLiM model, using `F`and `dF`.
    The result is returned as the log of the labels r_nk = P(Z = K | X,Y)
"""
function initialisation(X::Matrix{T}, Y::Matrix{T}, params::FunctionnalInitParams) where T
    g = initialisation_gllim(X, Y, params)
    log_rnk, log_ll = next_rnk(X, Y, log.(g.π), g.c, g.Γ, g.A, g.b, g.Σ)
    log_rnk, sum(log_ll)
end

using LinearAlgebra
import Base.+, Base.one, Base.*

using ..Probas

struct GLLiM{T <: AbstractFloat,GT <: CovType{T},ST <: CovType{T}}
    #   Computed properties :
    #   K::Int "number of Gaussians"
    #   D::Int "dimension of Gaussian on Y"
    #   L::Int "dimension of Gaussian on X"
    "weights (size K)"
    π::Vector{T}
    "means (size K x l)"
    c::Matrix{T}
    "covariances (size K x L for diagonal, or K x (L^2) for full)"
    Γ::Vector{GT}
    "Linear components (size K x D x L)"
    A::Vector{Matrix{T}}
    "Intercepts size(K x D)"
    b::Matrix{T}
    "covariances (size K x D for diagonal, or K x (D^2) for full)"
    Σ::Vector{ST}
end
GLLiM(π::Vector{T}, c::Matrix{T}, Γ::Vector{GT},A::Vector{Matrix{T}} , b::Matrix{T}, Σ::Vector{ST}) where {T,GT <: CovType{T},ST <: CovType{T}} = GLLiM{T,GT,ST}(π, c, Γ, A, b, Σ) 
    
function Base.getproperty(gllim::GLLiM, a::Symbol)
    if a == :K
        length(gllim.π)
    elseif a == :D
        size(gllim.b)[1]
    elseif a == :L 
        size(gllim.c)[1]
    else
        getfield(gllim, a)
    end
end

"""
 Instanciation. Output is not a matematically valid model and needs to be filled.
"""
function GLLiM{T,GT,ST}(K::Int, L::Int, D::Int) where {T <: AbstractFloat,GT <: CovType{T},ST <: CovType{T}}
    π = zeros(K)
    c, b = zeros(L, K), zeros(D, K)
    A = [zeros(D, L) for _ in 1:K]
    Γ = [ GT(L) for _ in 1:K ] 
    Σ = [ ST(D) for _ in 1:K]
    GLLiM(π, c, Γ, A, b, Σ)
end

"""
Random initialisation, with full covariance matrices (useful for debugging purpose)
"""
function GLLiM_rand(K::Int, L::Int, D::Int)
    π = rand(K)
    π ./= sum(π)
    c, b = rand(L, K), rand(D, K)
    A = [rand(D, L) for _ in 1:K]
    Γ =  [Probas.FullCov_rand(L) for _ in 1:K]
    Σ =  [Probas.FullCov_rand(D) for _ in 1:K]
    GLLiM(π, c, Γ, A, b, Σ)
end


# ---------------------------------------------------------------------------------------------- #
#  -------------------------------- GLLiM <-> JointGMM conversion ------------------------------ #
# ---------------------------------------------------------------------------------------------- #


"""
From GMM to GLLiM 
`L` is X dimension, `gmm` is as GaussianMixture on (X,Y)
"""
function GLLiM(gmm::Probas.Gmm{T}, L::Int) where T
    V = [Symmetric(c * c') for c in gmm.chol_covs] # from cholesky
    LplusD, K = size(gmm.means)
    D = LplusD - L
    pi = gmm.weights
    c = gmm.means[1:L, :]
    Gamma = [Symmetric(m[1:L, 1:L]) for m in V]
    V_xy = [m[1:L, (L + 1):LplusD] for m in V]
    V_xyT = [m' for m in V_xy]
    A = Vector{Matrix{T}}(undef, K)
    b = zeros(T, D, K)
    Sigma = FullCovs{T}(K, D)
    for k in 1:K
        v, g, ck = V_xyT[k], Gamma[k], c[:, k]
        Ak = v * inv(g)
        A[k] = Ak
        b[:, k] = gmm.means[(L + 1):LplusD, k] .- (Ak * ck)
        Sigma[k] = Symmetric(V[k][(L + 1):LplusD, (L + 1):LplusD] .- Ak * g * Ak')
    end
    GLLiM(pi, c, Gamma, A, b, Sigma)
end

"""
From GLLiM to GMM 
"""
function Probas.Gmm(gllim::GLLiM{T}) where T
    K, LplusD = gllim.K, gllim.L + gllim.D
    rho = gllim.π
    m = zeros(T, LplusD, K)
    V = FullCovs{T}(K, LplusD)
    for k in 1:K
        Ak, Gk = gllim.A[k], gllim.Γ[k]
        m[:, k] = vcat(gllim.c[:,k], Ak * gllim.c[:,k] .+ gllim.b[:,k])
        AGk = Ak * Gk
        Vyk = gllim.Σ[k] + AGk * (Ak')
        V[k] = Symmetric(cat(cat(Gk, AGk', dims=2), cat(AGk, Vyk, dims=2), dims=1))
    end
    Probas.Gmm{T}(rho, m, [cholesky(V[k]).L for k in 1:K])
end


# -------------------------------------- Translation -------------------------------------- #
+(U::FullCov{T}, V::FullCov{T}) where T = Symmetric(invoke(+, Tuple{AbstractArray{T,2},AbstractArray{T,2}}, U, V))
+(U::FullCov{T}, V::DiagCov{T}) where T = Symmetric(invoke(+, Tuple{AbstractArray{T,2},AbstractArray{T,2}}, U, V))
+(U::DiagCov{T}, V::FullCov{T}) where T = Symmetric(invoke(+, Tuple{AbstractArray{T,2},AbstractArray{T,2}}, U, V))
one(U::FullCov{T}) where T = Symmetric(invoke(one, Tuple{AbstractArray{T,2}}, U))
*(a::T, U::FullCov{T}) where {T <: Number} = Symmetric(invoke(*, Tuple{T,AbstractArray{T,2}}, a, U))
"""
Add a, mu, Sigma to given gllim (A -> a * A, b -> ab + mu, S -> a2 S + Sigma)
"""
function add_bias(gllim::GLLiM{T,gT,sT}, a::U, mean::Vector{T}, cov::CovType{T}) where {T <: Number,U <: Number,sT <: CovType{T},gT <: CovType{T}}
    K = gllim.K
    A = [a * Ak for Ak in gllim.A ]
    b = hcat([ a * gllim.b[:,k] .+ mean for k in 1:K ]...)
    Σ = [ a^2 * gllim.Σ[k] + cov for k in 1:K ]
    GLLiM(gllim.π, gllim.c, gllim.Γ, A, b, Σ)
end
add_bias(gllim, a, mean::T, cov) where {T <: Number} = add_bias(gllim, a, mean * ones(gllim.D), cov)
add_bias(gllim, a, mean, cov::T) where {T <: Number} = add_bias(gllim, a, mean, UniformScaling(cov))
add_bias(gllim, a, mean::T, cov::U) where {T <: Number,U <: Number} = add_bias(gllim, a, mean, UniformScaling(cov))

# --------------------------------------- Subset --------------------------------------- #

""" returns the sub-model obtained by keeping only the given dimension on Y """
function sub_gllim(gllim::GLLiM{T,gT}, ds::Vector{Int}) where {T,gT <: CovType{T}}
    sub_A = [A[ds, :] for A in gllim.A]
    sub_b = gllim.b[ds, :]
    sub_Sigma = [Probas.sub_cov(S, ds) for S in gllim.Σ]
    GLLiM(gllim.π, gllim.c, gllim.Γ, sub_A, sub_b, sub_Sigma)
end
sub_gllim(gllim::GLLiM, d::Int) = sub_gllim(gllim, [d])






# ---------------------------------------------------------------------------------------- #
# ---------------------------------------- Inverted model -------------------------------- #
# ---------------------------------------------------------------------------------------- #

function cGamma_star(gllim::GLLiM) 
    K = gllim.K

    # (9)
    cS = hcat([gllim.A[k] * gllim.c[:,k] .+ gllim.b[:,k] for k in 1:K]...)  

    # (10)
    ΓS = [LinearAlgebra.Symmetric(gllim.Σ[k] + gllim.A[k] * gllim.Γ[k] * gllim.A[k]') for k in 1:K ] 
    cS, ΓS
end

"""
GLLiM parameters inversion
Returns a gllim instance, with full covariances.
"""
function inversion(gllim::GLLiM{T}) where T
    K, D, L = gllim.K, gllim.D, gllim.L
    b, c = gllim.b, gllim.c

    cS, ΓS = cGamma_star(gllim) 

    ΣS = [ LinearAlgebra.Symmetric(zeros(L, L)) for k in 1:K]
    AS = [ zeros(L, D) for k in 1:K ]
    bS = zeros(L, K)
    # @inbounds Threads.@threads for k in 1:K 
    @inbounds for k in 1:K 
        i = inv(gllim.Σ[k]) * gllim.A[k]

        if isapprox(gllim.A[k], zeros(D, L))
            sigS = gllim.Γ[k]
            bs = c[:,k]
        else
            ig = inv(gllim.Γ[k])
            sigS = inv(ig +  gllim.A[k]' * i)  # (14)
            bs = sigS * (ig * view(c, :, k) - i' * view(b, :, k))  # (13)
        end
        aS = sigS * i'  # (12)

        ΣS[k] = LinearAlgebra.Symmetric(sigS)
        AS[k] = aS
        bS[:,k] = bs
    end
    GLLiM(gllim.π, cS, ΓS, AS, bS, ΣS)
end

struct ConditionnalDensityCache{T <: Number}
    out_weights::VecOrSub{T}
    out_means::MatOrSub{T}
    allocated::Vector{T}
    chol_covs::CholCovs{T}
    pre_factors::Vector{T}
end
function ConditionnalDensityCache{T}(gllim::GLLiM{T}) where {T <: Number}
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D
    out_weights = zeros(T, K)
    out_means = zeros(T, D, K)
    allocated = zeros(T, L)
    chol_covs = CholCovs{T}(undef, K)
    pre_factors  = zeros(T, K)
    for k in 1:K
        c = cholesky(gllim.Γ[k]).L 
        chol_covs[k] = c
        pre_factors[k] =  Probas.pre_factor(c, L)
    end
    ConditionnalDensityCache{T}(out_weights, out_means, allocated, chol_covs, pre_factors)
end

"""
Returns the direct contionnal density Y knowing X.
May be applied to gllim* and Y to obtain the inverse conditionnal density

Returns `weights` (K, N) and `means` (_, K,N)
"""
function conditionnal_density(gllim::GLLiM{T}, X::Matrix{T}) where T
    L, N = size(X)
    K::Int, D::Int = gllim.K, gllim.D
    means = zeros(T, D, K, N)
    weights = zeros(T, K, N)
    allocated = zeros(T, L, N)

    chol_covs = CholCovs{T}(undef, K)
    pre_factors  = zeros(T, K)
    for k in 1:K
        c = cholesky(gllim.Γ[k]).L 
        chol_covs[k] = c
        pre_factors[k] =  Probas.pre_factor(c, L)
    end 
    @inbounds Threads.@threads for n in 1:N
        cache = ConditionnalDensityCache{T}(view(weights, :, n), view(means, :, :, n), zeros(T, L), chol_covs, pre_factors)
        conditionnal_density!(gllim, view(X, :, n), cache)
    end
    weights, means
end




"""
 
Implementation for one observation
    chol_covs and pre_factors refer to the X covariances Gamma 
"""
function conditionnal_density!(gllim::GLLiM{T}, x::VecOrSub{T}, cache::ConditionnalDensityCache{T}) where T 
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D

    @inbounds for k in 1:K
        Ak, b  = gllim.A[k], gllim.b
        @inbounds for d in 1:D 
            cache.out_means[d, k] = b[d, k]
            for l in 1:L 
                cache.out_means[d, k] +=  Ak[d,l] * x[l] 
            end
        end
        cache.out_weights[k] =  Probas.log_gaussian_density!(x, view(gllim.c, :, k), cache.chol_covs[k], L, cache.pre_factors[k], cache.allocated)
        cache.out_weights[k] += log(gllim.π[k]) 
    end 

    s = Probas.logsumexp(cache.out_weights)
    cache.out_weights .= exp.(cache.out_weights .- s) # so far we used log
end

# Convenience (not optimized) wrapper
function conditionnal_density(gllim::GLLiM{T}, x::VecOrSub{T}) where T
    K::Int, L::Int, D::Int = gllim.K, gllim.L, gllim.D
    cache = ConditionnalDensityCache{T}(gllim)
    conditionnal_density!(gllim, x, cache)
    cache.out_weights, cache.out_means
end

"""
 Shift the "basic" conditionnal density with the additionnal prior information 
 given by a Gaussian Mixture Model.
 Returns weights (K), means (_, K) and covs FullCovs (K)
"""
function conditionnal_density_with_prior(weights::VecOrSub{T}, means::Matrix{T}, covs::FullCovs{T}, prior::Probas.Gmm{T}) where T
    L, K = size(means)
    I = prior.K
    K_out = K * I # resulting mixture is a double sum
    out_weights, out_means, out_covs = zeros(K_out), zeros(L, K_out), FullCovs{T}(undef, K_out)

    for k in 1:K 
        S_k = covs[k]
        S_k_inv = inv(S_k)
        mu_k = view(means,:, k)
        log_det_k, log_w_k = logdet(S_k), log(weights[k])
        for i in 1:I
            G_i = prior.chol_covs[i] * prior.chol_covs[i]'
            G_i_inv = prior.inv_covs[i]
            mu_i = view(prior.means,:, i)
            log_det_i = logdet(G_i) 
            
            S_k_i = inv(G_i_inv + S_k_inv)
            x_k_i = S_k_i * (G_i_inv * mu_i + S_k_inv * mu_k)
            B_k_i = inv(G_i + S_k)
            # in log, for numerical stability
            log_beta_k_i = 0.5 *(logdet(S_k_i) - log_det_k - log_det_i) - 0.5 * (mu_k - mu_i)' * B_k_i * (mu_k - mu_i)

            index = (k-1)*I + i  # into 1:K_out
            
            out_weights[index] = log_beta_k_i + log_w_k + log(prior.weights[i])
            out_means[:,index] = x_k_i
            out_covs[index] = S_k_i
        end
    end
    # normalisation 
    alpha = Probas.logsumexp(out_weights, K_out)
    for k in 1:K_out
        out_weights[k] = exp(out_weights[k] - alpha)
    end

    out_weights, out_means, out_covs
end
function conditionnal_density_with_prior(w, m, c, x::VecOrSub{T}, Gamma::CovType{T}) where T
    L , _ = size(m)
    gmm = Probas.Gmm{T}(ones(T, 1), hcat(x), [cholesky(zeros(L,L)+Gamma).L])
    conditionnal_density_with_prior(w, m, c,  gmm)
end 

"""
    Direct model 
"""
F_gllim(gllim, X::MatOrSub) = predict(gllim, X; with_cov=false)


"""
Mean prediction, with covariance.
When used with gllim, X , gives F_gllim(x).
When used with gllim_star, Y gives F^-1_gllim(y)
"""
function predict(gllim::GLLiM, X::Matrix; with_cov=true)
    weights, means = conditionnal_density(gllim, X)
    covs = with_cov ? gllim.Σ : nothing
    Probas.mean_cov_mixtures(weights, means, covs)
end 


"""
Sample according to conditionnal law of X knowing Y.
Return samplings (L, `nb_samples`, N)
"""
function predict_sample(gllim_star::GLLiM{T}, Y::Matrix{T},
    nb_samples::Int) where T
    weights, means = conditionnal_density(gllim_star, Y)
    Probas.gmm_sampling(weights, means, gllim_star.Σ, nb_samples)
end

""" Invert the given observation with an additional std """
function predict_with_std(gllim::GLLiM{T}, yobs::VecOrSub{T}, std::VecOrSub{T})  where T
    # we shift the covariance of the GLLiM model 
    sigma_obs = Diagonal(std.^2) # we want the covariance
    gllim_shifted = add_bias(gllim, 1., 0., sigma_obs)
    gllim_star = inversion(gllim_shifted)
        
    # we get the inverse density from the shifted inverse
    weights_all_i, means_all_i = conditionnal_density(gllim_star, yobs)
        
    # compute the mean prediction (with std)
    mean = Probas.mean_mixture(weights_all_i, means_all_i)
    cov = Probas.covariance_mixture(weights_all_i, means_all_i, gllim_star.Σ)
    mean, cov
end
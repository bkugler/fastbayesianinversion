# Implementation of EM algorithm for GLLiM 

using Printf
using ..Log

include("gllim_init.jl")

# -------------------------------- Stopping criteria --------------------------------- #

"""
 Enable flexible stopping criterion 
Must implement `has_converged(params, previous_logll, current_logll, current_iter)`
"""
abstract type StoppingParams end
has_converged(params::StoppingParams, previous_logll, current_logll, current_iter) = error("has_converged not implemented for type $(typeof(params))")

"""
 Mix between iteration number and likelihood stagnation ;
 (current_logll - previous_logll) / previous_logll < threshold
 Passing a negative threshold ensure that `max_iterations` will be performed.
"""
struct DefaultStoppingParams <: StoppingParams
    max_iterations::Int
    relative_ratio_threshold::Float64
end

# --------------------------------- Hyper parameters --------------------------------- #

struct TrainParams{T <: Number,GT <: CovType{T},ST <: CovType{T}}
    stop::StoppingParams
    cov_floor::T
end

"""
 Covariance constraints are expressed as type parameters 
"""
struct Params{T <: Number,GT <: CovType{T},ST <: CovType{T}}
    init::InitParams
    train::TrainParams{T,GT,ST}
end

# ---------------------------------------------------------------------------------- #
# ------------------------------------- M step ------------------------------------- #
# ---------------------------------------------------------------------------------- #

function _compute_c(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty,
                         ck_T::VecOrSub{Ty}) where {Ty}
    L, N = size(T)
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for l in 1:L
            ck_T[l] += s * T[l,n]
        end
    end
end

"""
 Gamma iso 
"""
function _compute_Gamma(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                         Gamma_T::IsoCovs{Ty}, k::Int, ::VecOrSub{Ty}) where {Ty}
    L, N = size(T)
    gamma = 0.
    if L != 0
        @inbounds for n in 1:N
            _tmp = 0.
            s = exp(0.5 * (log_rnk[n] - log_rk))
            @inbounds for l in 1:L
                _tmp += (s * (T[l,n] - ck_T[l]))^2
            end
            _tmp /= L
            gamma += _tmp
        end
    end
    Gamma_T[k] = UniformScaling(gamma)
end

"""
 Gamma diag 
"""
function _compute_Gamma(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                        Gamma_T::DiagCovs{Ty}, k::Int, ::VecOrSub{Ty}) where {Ty}
    L, N = size(T)
    gk = Gamma_T[k]
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:L
            gk[l,l] += (s * (T[l,n] - ck_T[l]))^2
        end
    end
end

"""
 Gamma full 
"""
function _compute_Gamma(T::MatOrSub{Ty}, log_rnk::VecOrSub{Ty}, log_rk::Ty, ck_T::VecOrSub{Ty},
                                Gamma_T::FullCovs{Ty}, k::Int, tmp::VecOrSub{Ty}) where {Ty}
    L, N = size(T)
    gk = Gamma_T[k].data
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:L
            tmp[l] = s * (T[l,n] - ck_T[l])
        end

        @inbounds for l1 in 1:L
            @inbounds for l2 in 1:L
                gk[l1,l2] += tmp[l1] * tmp[l2]
            end
        end
    end
end

function _add_numerical_stability(cov_floor::Ty, covs::Union{FullCovs{Ty},DiagCovs{Ty}}, k::Int) where {Ty}
    D, _ = size(covs[k])
    @inbounds for d in 1:D
        covs[k][d,d] += cov_floor
    end
end

function _add_numerical_stability(cov_floor::Ty, covs::IsoCovs{Ty}, k::Int) where {Ty}
    covs[k] = UniformScaling(covs[k] + cov_floor)
end



function _compute_Ak(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                      log_rk::Ty, Ak::MatOrSub{Ty},
                      xk_bar::VecOrSub{Ty}, yk_bar::VecOrSub{Ty}, X_stark::MatOrSub{Ty},
                      Y_stark::MatOrSub{Ty}) where {Ty}

    L, _ = size(X)
    D, N = size(Y)
    s = 0.
    sum = 0.
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for l in 1:L
            xk_bar[l] += s * X[l,n]
        end
        @inbounds for d in 1:D
            yk_bar[d] += s * Y[d,n]
        end
        sum += s
    end
    @inbounds for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        @inbounds for l in 1:L
            X_stark[l,n] = s * (X[l,n] -  xk_bar[l]) # (33)
        end

        @inbounds for d in 1:D
            Y_stark[d,n] = s  * (Y[d,n] - yk_bar[d])  # (34)
        end
    end
    try
        Ak .= Y_stark * X_stark' * pinv(X_stark * X_stark') # (31)
    catch e 
        # we keep old Ak
    end
end


function _compute_bk(X::MatOrSub{Ty}, Y::MatOrSub{Ty},
                log_rnk::VecOrSub{Ty}, log_rk::Ty, Ak::MatOrSub{Ty},
                bk::VecOrSub{Ty}) where {Ty}
    D, N = size(Y)
    L, _ = size(X)

    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for d in 1:D
            u = 0.
            @inbounds for l in 1:L
                u += Ak[d,l] * X[l,n]
            end
            bk[d] += s * (Y[d,n] - u)
        end
    end
end

"""
 Sigma full 
"""
function _compute_Sigmak(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                          log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                          tmp::VecOrSub{Ty}, Sigmak::FullCovs, k::Int) where {Ty}

    
    D, N = size(Y)
    L, _ = size(X)
    sk = Sigmak[k].data
    @inbounds for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        @inbounds for d in 1:D
            u = 0.
            @inbounds for l in 1:L
                u += Ak[d,l] * X[l,n]
            end
            tmp[d] = (Y[d,n] - u - bk[d])
        end

        @inbounds for d1 in 1:D
            @inbounds for d2 in 1:D
                sk[d1,d2] += s * tmp[d1] * tmp[d2]
            end
        end
    end
end

"""
 Sigma diag 
"""
function _compute_Sigmak(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                                log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                             tmp::VecOrSub{Ty}, Sigmak::DiagCovs, k::Int) where {Ty}
    D, N = size(Y)
    L, _ = size(X)
    sk = Sigmak[k]
    for n in 1:N
        s = exp(log_rnk[n] - log_rk)
        for d in 1:D
            u = 0.
            for l in 1:L
                u += Ak[d,l] * X[l,n]
            end
            sk[d,d] += s * ((Y[d,n] - u - bk[d])^2)
        end
    end
end

"""
 Sigma iso 
"""
function _compute_Sigmak(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::VecOrSub{Ty},
                        log_rk::Ty, Ak::MatOrSub{Ty}, bk::VecOrSub{Ty},
                         tmp::VecOrSub{Ty}, Sigmak::IsoCovs{Ty}, k::Int) where {Ty}
    D, N = size(Y)
    L, _ = size(X)
    sig = 0.
    for n in 1:N
        s = exp(0.5 * (log_rnk[n] - log_rk))
        for d in 1:D
            u = 0.
            for l in 1:L
                u += Ak[d,l] * X[l,n]
            end
            sig += (s * (Y[d,n] - u - bk[d]))^2
        end
    end
    Sigmak[k] = UniformScaling(sig / D)
end


function _create_tmps(L, D, N, Ty::Type{S}) where {S <: Number}
    xk_bar = zeros(Ty, L)  
    yk_bar = zeros(Ty, D)  
    X_stark = zeros(Ty, L, N)  
    Y_stark = zeros(Ty, D, N)
    inv_tmp = zeros(Ty, L, L)

    tmp_L = zeros(Ty, L)  
    tmp_D = zeros(Ty, D)  

    (tmp_L, tmp_D, xk_bar, yk_bar, X_stark, Y_stark)
end 


"""
 M step - Multi-threaded
"""
function next_theta(X::MatOrSub{Ty}, Y::MatOrSub{Ty}, log_rnk::MatOrSub{Ty}, GT::Type{GTT}, ST::Type{STT}, cov_floor::Ty)where
    {Ty,GTT <: CovType{Ty},STT <: CovType{Ty}}

    N, K = size(log_rnk)
    L, _ = size(X)
    D, _ = size(Y)

    # Out allocation 
    log_pi_out = zeros(Ty, K)
    c_out = zeros(Ty, L, K) 
    A_out = [ zeros(Ty, D, L) for k in 1:K ]
    b_out = zeros(Ty, D, K)
    Γ_out = [ GT(L) for _ in 1:K] 
    Σ_out = [ST(D) for _ in 1:K]

    # covariances must be zeros
    

    # @inbounds for k in 1:K
    @inbounds Threads.@threads for k in 1:K
        log_rk = Probas.logsumexp(log_rnk[:,k], N)
        if log_rk == -Inf  # degenerate cluster 
            log_pi_out[k] = -Inf
            _add_numerical_stability(cov_floor, Γ_out, k)
            _add_numerical_stability(cov_floor, Σ_out, k)
            @warn "degenerate cluster" k
            continue
        end
        # Temporary Memory allocation
        tmp_L, tmp_D, xk_bar, yk_bar, X_stark, Y_stark = _create_tmps(L, D, N, Ty)

        log_pi_out[k] = log_rk - log(N)

        _compute_c(X, view(log_rnk, :, k), log_rk, view(c_out, :, k))

        _compute_Gamma(X, view(log_rnk, :, k), log_rk, view(c_out, :, k), Γ_out, k, tmp_L)
        _add_numerical_stability(cov_floor, Γ_out, k)

        _compute_Ak(X, Y, view(log_rnk, :, k), log_rk, A_out[k],
                    xk_bar,  yk_bar,  X_stark, Y_stark)

        _compute_bk(X, Y, view(log_rnk, :, k), log_rk, A_out[k], view(b_out, :, k))

        _compute_Sigmak(X, Y, view(log_rnk, :, k), log_rk, A_out[k], view(b_out, :, k),
                               tmp_D, Σ_out, k)
        _add_numerical_stability(cov_floor, Σ_out, k)
    end
    log_pi_out, c_out, Γ_out, A_out, b_out, Σ_out
end


# E step defined in gllim_rnk #

# ---------------------------------------------------------------------------------- #
# --------------------------------- Full algorithm --------------------------------- #
# ---------------------------------------------------------------------------------- #

function has_converged(params::DefaultStoppingParams, previous_logll, current_logll, current_iter)
    isfinite(current_logll) || return true # pathological -> stop early
    current_iter > params.max_iterations && return true # limit

    delta = current_logll - previous_logll
    delta < 0  && return false # shouldn't happend
    abs(delta / previous_logll) < params.relative_ratio_threshold # log_ll might be negative
end


"""
    Run EM algorithm to train a GLLiM model, initialized by a M-step with `log_rnk`
    Return the history of the log-likelihood as well
"""
function train(log_rnk::Matrix{T}, X::Matrix{T}, Y::Matrix{T},
    params::TrainParams{T,GT,ST}) where {T,GT <: CovType{T},ST <: CovType{T}}

    L, N = size(X)

    lls::Vector{T} = [] # history
    
    # init_logll = sum(logll) # save the value for stopping criteria
    current_logll = -Inf
    current_iter = 1
    while true
        # M & E steps
        log_π, c, Γ, A, b, Σ = next_theta(X, Y, log_rnk, GT, ST, params.cov_floor)
        log_rnk, logll = next_rnk(X, Y, log_π, c, Γ, A, b, Σ)
        
        # update logll trackers
        previous_logll = current_logll
        current_logll = sum(logll)
        if !isfinite(current_logll)  
            @warn "degenerate log-likelihood !"
        end
        
        # save log_likehood for export
        push!(lls, current_logll)

        @debug "iter. $(current_iter) average log-likelihood : $(@sprintf("%.6f", current_logll / N))"
        
        if has_converged(params.stop, previous_logll, current_logll, current_iter)
            out = GLLiM{T,GT,ST}(exp.(log_π), c, Γ, A, b, Σ)
            return out, lls
        end
        current_iter += 1
    end
end

"""
    Initialize a GLLiM model, then run the EM algorithm on it.
    Return the history of the log-likelihood as well
"""
function train(params::Params{T}, X::Matrix{T}, Y::Matrix{T}) where T
    L, N = size(X)
    D, _ = size(Y)
    K = getK(params.init)
    @info "Starting GLLiM training (N_train = $N, L = $L, D = $D, K = $K) ... "

    Log.indent()
    log_rnk_init, _ = initialisation(X, Y, params.init)
    Log.deindent()

    Log.indent()
    @info "Starting EM iterations ..."
    gllim, lls = train(log_rnk_init, X, Y, params.train)
    Log.deindent()

    gllim, lls
end
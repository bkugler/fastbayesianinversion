"""
 Defines shared type and helpers functions 
"""
module Probas 

using LinearAlgebra
using Random

export SubVect,
VecOrSub,
MatOrSub,
CubeOrSub,
IsoCov,
DiagCov,
FullCov,
CovType,
IsoCovs,
DiagCovs,
FullCovs,
CovsType,
CholCov,
CholCovs,
Gmm

const _LOG_2PI = log(2 * pi)

const SubVect{T} = SubArray{T,1,Matrix{T}}
const VecOrSub{T} = Union{Vector{T},SubArray{T,1,Vector{T}},SubVect{T},SubArray{T,1,Array{T,3}}}
const MatOrSub{T} = Union{Matrix{T},SubArray{T,2,Array{T,2}},SubArray{T,2,Array{T,3}},SubArray{T,2,Array{T,4}}}
const CubeOrSub{T} = Union{Array{T,3},SubArray{T,3,Array{T,4}}}

const IsoCov{T} = UniformScaling{T}
const DiagCov{T} = Diagonal{T,Vector{T}}
const FullCov{T} = Symmetric{T,Matrix{T}}
const CovType{T} = Union{DiagCov{T},FullCov{T},IsoCov{T}}

const IsoCovs{T} = Vector{IsoCov{T}}
const DiagCovs{T} = Vector{DiagCov{T}}
const FullCovs{T} = Vector{FullCov{T}}
const CovsType{T} = Union{DiagCovs{T},FullCovs{T},IsoCovs{T}}

const CholCov{T} = LowerTriangular{T,Matrix{T}}
const CholCovs{T} = Vector{CholCov{T}}


IsoCov_rand(::Int)  = UniformScaling(rand())
DiagCov_rand(L::Int)  = Diagonal(rand(L))
CholCov_rand(L::Int) =  LowerTriangular(rand(L, L) .+ Diagonal(ones(L)))
FullCov_rand(L::Int) = (u = CholCov_rand(L) ;  Symmetric(u * u'))

"""
 Zero storage
"""
IsoCov{T}(::Int) where {T <: AbstractFloat} = UniformScaling(zero(T))
DiagCov{T}(L::Int) where T = Diagonal(zeros(T, L))
FullCov{T}(L::Int) where T = Symmetric(zeros(T, L, L))
CholCov{T}(L::Int) where T = LowerTriangular(zeros(T, L, L))
IsoCovs{T}(K::Int, L::Int) where T = [ IsoCov{T}(L) for _ in 1:K ]
DiagCovs{T}(K::Int, L::Int) where T = [ DiagCov{T}(L) for _ in 1:K ]
FullCovs{T}(K::Int, L::Int) where T = [ FullCov{T}(L) for _ in 1:K ]
CholCovs{T}(K::Int, L::Int) where T = [ CholCov{T}(L) for _ in 1:K ]


""" Subsets """

sub_cov(cov::FullCov{T}, ds::Vector{Int}) where T = Symmetric(Matrix{T}(cov[ds, ds]))
sub_cov(cov::DiagCov{T}, ds::Vector{Int}) where T = Diagonal(Matrix{T}(cov[ds,ds]))
sub_cov(cov::IsoCov, ::Vector{Int}) = cov


# ----------------------------- Mixtures ----------------------------- #

"""
 Store a Gaussian mixture model. 

Some attributes are pre-computed, so the parameters must no be modified.
"""
struct Gmm{T <: Number}
    weights::Vector{T}
    means::Matrix{T}
    chol_covs::CholCovs{T}

    inv_covs::FullCovs{T} # computed from `chol_covs`
    pre_factors::Vector{T} # log, for density, computed from `chol_covs`
    L::Int # computed from means
    K::Int # computed from means 
    function Gmm{T}(weights::Vector{T}, means::Matrix{T}, chol_covs::CholCovs{T}) where {T <: Number}
        L, K = size(means)
        inv_covs = [(u = Matrix{T}(inv(chol_cov)); Symmetric(u' * u)) for chol_cov in chol_covs] 
        pre_factors = [ pre_factor(chol_cov, L)  for chol_cov in chol_covs]
        new(weights, means, chol_covs, inv_covs, pre_factors, L, K)
    end
end


"""
 Returns a matrix of covariance. 
"""
function covariance_mixture(weights::VecOrSub{T}, means::Matrix{T}, covs::Union{FullCovs{T},DiagCovs{T}}) where T
    D, K = size(means)
    p1 = zeros(D, D)
    sc = zeros(D)
    c = zeros(D, D)
    @inbounds for k in 1:K
        wk, mk, covk  = weights[k], view(means, :, k), covs[k]
        mul!(c, mk, mk')
        @inbounds for d in 1:D 
            sc[d] += wk * mk[d]
            @inbounds for d2 in 1:D 
                p1[d, d2] += wk * (c[d,d2] + covk[d,d2])
            end
        end
    end
    Symmetric(p1 .- (sc * sc'))
end


"""
Compute the mean of a mixture
"""
function mean_mixture(weights::Vector{T}, means::Matrix{T}) where T
    D, K = size(means)
    sc = zeros(D)
    mean_mixture!(weights, means, sc)
    sc
end

function mean_mixture!(weights::Vector{T}, means::Matrix{T}, out::VecOrSub{T}) where T
    D, K = size(means)
    out .= 0
    @inbounds for k in 1:K
        for d in 1:D 
            out[d] += weights[k] * means[d,k]
        end 
    end
end

"""
 Same covs for each n 
    Passing nothing disable covariance computation, and returns `nothing` instead
"""
function mean_cov_mixtures(weightss::Matrix{T}, meanss::Array{T,3}, covs::Union{FullCovs{T},DiagCovs{T},Nothing}) where T
    L, K, N = size(meanss)
    mean_mel = zeros(L, N)
    covs_mel = fill(Symmetric(zeros(L, L)), N)
    @inbounds Threads.@threads for n in 1:N
        mean_mel[:,n] = mean_mixture(weightss[:,n], meanss[:,:,n])
        if covs !== nothing 
            covs_mel[n] = covariance_mixture(weightss[:,n], meanss[:,:,n], covs)
        end
    end
    mean_mel, covs_mel
end

"""
 Different covariances for each n 
"""
function mean_cov_mixtures(weightss::Matrix{T}, meanss::Array{T,3}, covss::Vector{FullCovs{T}}) where T
    L, K, N = size(meanss)
    mean_mel = zeros(L, N)
    covs_mel = fill(Symmetric(zeros(L, L)), N)
    for n in 1:N
        mean_mel[:,n] = mean_mixture(weightss[:,n], meanss[:,:,n])
        covs_mel[n] = covariance_mixture(weightss[:,n], meanss[:,:,n], covss[n])
    end
    mean_mel, covs_mel
end

# -------------------------------------------------------------------------------------------------------------- #
# ----------------------------------------------- Gaussian density --------------------------------------------- # 
# -------------------------------------------------------------------------------------------------------------- #


# --------------------------------------- Low level optimized functions  --------------------------------------- #
"""
 Implementation for isometrique covariance 
"""
function log_gaussian_density(x::VecOrSub{T}, mu::VecOrSub{T}, cov::IsoCov{T}, D::Int, pre_factor::T) where T
    q = 0.
    @inbounds for d in 1:D
        q += (x[d] - mu[d])^2
    end
    q /= cov.λ
    pre_factor - 0.5 * q
end

"""
 Implementation for diagonal covariance 
"""
function log_gaussian_density(x::VecOrSub{T}, mu::VecOrSub{T}, cov::DiagCov{T}, D::Int, pre_factor::T) where T
    q = 0.
    @inbounds for d in 1:D
        q += ((x[d] - mu[d]) / sqrt(cov[d,d]))^2
    end
    pre_factor - 0.5 * q
end

"""
Implementation for full covariance, as cholesky factorization.
Performs triangular resolution and density computation all together.
Shape of cov_cholesky : D,D
Does no memory allocation (allocated does not need to be zero)
pre_factor must equal (-0.5 * D * _LOG_2PI) -  0.5 * log_det_Sigma  (avoid over computation)
"""
function log_gaussian_density!(x::VecOrSub{T}, mu::VecOrSub{T},
    cov_cholesky::CholCov{T}, D::Int, pre_factor::T, allocated::VecOrSub{T}) where T

    q = 0.
    tmp = 0.
    @fastmath @inbounds for d in 1:D
        tmp = x[d] - mu[d]
        @fastmath @inbounds for i in 1:(d - 1)
            tmp -= cov_cholesky[d,i] * allocated[i]
        end
        tmp /= cov_cholesky[d,d]
        allocated[d] = tmp
        q += tmp^2 
    end
    pre_factor - 0.5 * q
end

"""
Compatiblity wrapper
"""
log_gaussian_density!(x, mu, cov::Union{DiagCov,IsoCov}, D, pre_factor, allocated) = log_gaussian_density(x, mu, cov, D, pre_factor)


# -------------------------------------- wrapper : vectorized in X -------------------------------------- #
# -------------- Write in out, which has to be a vector of length N (maybe non null) -------------------- #

pre_factor(cov::UniformScaling, D) = -0.5 * D * (_LOG_2PI + log(cov.λ))
pre_factor(cov::Diagonal, D) = -0.5 * (D *  _LOG_2PI + logdet(cov))
pre_factor(std::CholCov, D) = (-0.5 * D * _LOG_2PI) - logdet(std) # logdet(std) is already half logdet(cov)

"""
Implementation for full covariance, as cholesky factorization or diagonal covariance.
Erase out (no null required)
"""
function log_gaussian_density!(X::MatOrSub{T}, mu::Union{SubVect{T},Vector{T}}, 
    cov::Union{CholCov{T},DiagCov{T},IsoCov{T}}, out::Union{SubVect{T},Vector{T}}) where T

    D, N = size(X)
    pf = pre_factor(cov, D)
    allocated = zeros(T, D)
    @inbounds for n in 1:N
        out[n] = log_gaussian_density!(view(X, :, n), mu, cov, D, pf, allocated)
    end
end

"""
 Different mean for each vector 
"""
function log_gaussian_density!(X::MatOrSub{T}, mu::MatOrSub{T}, 
    cov::Union{CholCov{T},DiagCov{T},IsoCov{T}}, out::Union{SubVect{T},Vector{T}}) where T

    D, N = size(X)
    pf = pre_factor(cov, D)
    allocated = zeros(T, D)
    @inbounds for n in 1:N
        out[n] = log_gaussian_density!(view(X, :, n), view(mu, :, n), cov, D, pf, allocated)
    end
end

# -------------------------------------- wrapper : convenience -------------------------------------- #
log_gaussian_density!(X, mu, cov::FullCov{T}, out) where T = log_gaussian_density!(X, mu, cholesky(cov).L, out)
log_gaussian_density(x::VecOrSub{T}, mu, cov, L) where T = log_gaussian_density!(x, mu, cov, L, pre_factor(cov, L), zeros(T, L))


# ----------------------------------------- wrapper : mixture ----------------------------------------- #
"""
 Implementation for one vector
"""
function gmm_density(x::VecOrSub{T}, gmm::Gmm{T}, allocated::VecOrSub{T}) where T
    r = 0.
    s = 0.
    @fastmath @inbounds for k in 1:gmm.K
        s = log_gaussian_density!(x, view(gmm.means, :, k), gmm.chol_covs[k], gmm.L, gmm.pre_factors[k], allocated)
        r += exp(s) * gmm.weights[k]
    end
    r
end


"""
 Implementation for weights equal to one
"""
function gmm_density(x::VecOrSub{T},  means::Matrix{T}, chol_covs::CholCovs{T}) where T
    L, K = size(means)
    r = 0.
    s = 0.
    allocated = zeros(T, L)
    @fastmath @inbounds for k in 1:K
        pf = pre_factor(chol_covs[k], L)
        s = log_gaussian_density!(x, view(means, :, k), chol_covs[k], L, pf, allocated)
        r += exp(s)
    end
    r
end

"""
 Implementation for weights equal to one, without allocating.
    `pre_factors` are the prefactors of `chol_covs`
"""
function gmm_density!(x::VecOrSub{T},  means::Matrix{T}, chol_covs::CholCovs{T},
    pre_factors::Vector{T},  allocated::Vector{T}) where T
    L, K = size(means)
    r = 0.
    s = 0.
    @fastmath @inbounds for k in 1:K
        pf = pre_factors[k]
        s = log_gaussian_density!(x, view(means, :, k), chol_covs[k], L, pf, allocated)
        r += exp(s)
    end
    r
end



"""
 Compute the density of Gaussian mixture given at given points 
"""
function gmm_density(Y::MatOrSub{T}, weights::Vector{T},  means::Matrix{T}, chol_covs::CholCovs{T}) where T
    L, N = size(Y)
    r = zeros(T, N)
    allocated = zeros(T, L)
    gmm = Gmm(weights, means, chol_covs)
    @inbounds for i in 1:N 
        r[i] = gmm_density(view(Y, :, i), gmm, allocated)
    end
    r
end

const REG_COVAR = UniformScaling(1e-8)
"""
    Perfoms a cholesky decomposition; if needed add a diagonal regularization term
    to increase numerical stability.
"""
function safe_cholesky(M::Union{FullCov{T},Matrix{T}}) where T
    try
        return cholesky(M).L
    catch
        M += REG_COVAR
        return cholesky(M).L
    end
end

""" compute the derivatie of F at x (using finite difference) """
function dF_diff_finite(F::Function, x::VecOrSub{T}, eps::Float64, D::Int) where T <: Number
    L = length(x)
    out = zeros(T, D, L)
    fx = F(x)
    for i in 1:L 
        xei = copy(x)
        xei[i] += eps
        out[:, i] = (F(xei) .- fx) ./ eps
    end
    return out
end

# -------------------------------------------------------------------------------------------------------------- #
# -------------------------------------------------- Sampling -------------------------------------------------- # 
# -------------------------------------------------------------------------------------------------------------- #

function _categorical(cumweights::Vector{T}, K::Int) where T
    alea = rand()
    k = 1
    while k <= K - 1 && alea > cumweights[k]
        k += 1
    end
    k
end

function gmm_sampling!(cumweights::Vector{T}, means::MatOrSub{T}, chol_covs::CholCovs{T}, out::VecOrSub{T}, tmp::VecOrSub{T}, K::Int, L::Int) where T
    k = _categorical(cumweights, K)
    chol_cov = chol_covs[k]
    randn!(tmp)
    @inbounds for i in 1:L
        u = 0.
        @inbounds for j in i:L
            u += chol_cov[i,j] * tmp[j]
        end
        out[i] = u + means[i,k]
    end
end


"""
    Samples from N Gaussian Mixture Models
    Returns shape L, `nb_samples`, N
    Implementation for the same list of covariances for each n.     
"""
function gmm_sampling(weights_list::Matrix{T}, means_list::Array{T,3},
    covs_list::Vector{Symmetric{T,Matrix{T}}}, Ns) where T

    L, K, N = size(means_list)
    out = zeros(T, L, Ns, N)
    tmp = zeros(T, L)

    chols = [ cholesky(c).L for c in covs_list]
    cumweights = zeros(T, K)
    for n in 1:N
        cumsum!(cumweights, view(weights_list, :, n))
        for s in 1:Ns 
            gmm_sampling!(cumweights, view(means_list, :, :, n), chols, view(out, :, s, n), tmp, K, L)
        end
    end
    out
end

""" Sample `J` points from `X` with proba given by `weights` """
function resampling(X::Matrix{T}, weights::Vector{T}, J::Int) where T
    cumweights = cumsum(weights)
    L, N = size(X)
    out = zeros(T, L, J)
    for j in 1:J
        n = _categorical(cumweights, N)
        out[:, j] = X[:, n]
    end
    out
end

# -------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------------- Misc ---------------------------------------------------- # 
# -------------------------------------------------------------------------------------------------------------- #
function logsumexp(logx::VecOrSub{T}, L::Int) where T
    min, max = Inf, -Inf
    @inbounds for l in 1:L 
        if logx[l] < min 
            min = logx[l]
        end
        if logx[l] > max 
            max = logx[l]
        end
    end
    if min == -Inf && max == -Inf 
        return -Inf
    end
    if min == +Inf && max == +Inf 
        return +Inf
    end
    s_max, s_min = 0., 0.
    @inbounds for l in 1:L
        s_max += exp(logx[l] - max)
        s_min += exp(logx[l] - min)
    end
    out_max =    log(s_max) + max 
    out_min =    log(s_min) + min 
    if isfinite(out_min) 
        out_min
    else 
        out_max
    end 
end
logsumexp(x) = logsumexp(x, length(x))

""" performs the operation log(c1 * exp(log_p1) + c2 * exp(log_p2)) with numerical stability """
function weighted_logsumpexp(logp1::T, logp2::T, c1::Int, c2::Int) where T
    m = max(logp1, logp2)
    log(c1 * exp(logp1 - m) + c2 * exp(logp2 - m)) + m
end

"""
    Given log(rnk), write in place log( rnk / (sum_k rnk) )
    Write log ( sum_k rnk ) in out_ll 
"""
function normalize_log!(log_rnk::MatOrSub{T}, out_ll::VecOrSub{T}) where T
    N, K = size(log_rnk)
    @inbounds for n in 1:N
        # numerical issue : we compute log sum exp with rescaled log
        out_ll[n] = logsumexp(log_rnk[n,:])
        @inbounds for k in 1:K
            log_rnk[n,k] = log_rnk[n,k] - out_ll[n]
        end
    end
end

end # module

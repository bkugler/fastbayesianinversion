module FastBayesianInversion

# helpers
include("Log.jl")
include("Probas.jl")
include("Monitor.jl")
include("Schemes.jl")
include("Store.jl")
include("Comptimes.jl")

include("GLLIM/Gmms.jl")
include("GLLIM/Gllim.jl") # require Gmms

include("MODELS/Models.jl")

include("PREDICTIONS/Predictions.jl")
include("PREDICTIONS/Indics.jl")

include("PERF/Perf.jl")

include("IS/Is.jl")

include("Shared.jl")

include("MCMC/Mcmc.jl")

include("NOISE/Noise.jl")


include("SOBOL/Sobol.jl")

include("PIPELINE/Pipeline.jl")


end # module

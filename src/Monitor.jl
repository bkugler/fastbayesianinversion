module Moni

"""
Monitor is a thread-safe counter which 
log a progress
"""
mutable struct Monitor
    lock::ReentrantLock
    progress::Int
    total::Int
    step::Int
    function Monitor(total::Int; step=nothing) 
        if step === nothing 
            step = total > 1000 ? (total ÷ 200) : 10
        end
        new(ReentrantLock(), 0, total, step)
    end
end

function update!(m::Monitor)
    lock(m.lock)
    m.progress += 1
    if m.progress % m.step == 0
        percent = round(100 * m.progress / m.total; digits=2)
        @debug "Inversion $(m.progress) ($percent %)"
        flush(Base.stdout) # usefull on batch mode (cluster)
    end
    unlock(m.lock)
end


end
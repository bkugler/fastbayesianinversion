using LinearAlgebra
using Distributions
using Random

using ..Probas

export IsProposition, IsModel, IsInversion, IsDiagnostics

include("proposition.jl")

"""
    Define the target and proposition densities 
"""
abstract type IsModel end 

"""
    Returns the dimension of the spaces X
"""
get_L(m::IsModel) = error("get_L not implemented for type $(typeof(m))")

"""
    Returns the parameters that define the proposition density 
"""
get_proposition(m::IsModel)::IsProposition = error("get_proposition not implemented for type $(typeof(m))")

"""
    Computes the target density at `x`, the sampled point.
    `j` is the index of the sample, used for example to cache
    intermediate values
"""
log_p(m::IsModel, x::VecOrSub, j::Int) = error("log_p not implemented for type $(typeof(m))")


""" SamplingParams is one sampling inversion method, like IS and IMIS
Concrete types must implement the scheme method """
abstract type SamplingParams end

""" centroid is 0 for the mean """
scheme(p::SamplingParams, centroid::Int) = error("scheme not implemented for $(typeof(p))")
Ns(p::SamplingParams) = error("Ns not implemented for $(typeof(p))")

""" Parametrize the estimation of the non-uniformity parameter """
struct NonUniformityParams 
    J::Int 
end

struct IsDiagnostics
    ess::Number
    es::Number
    qn::Number
end

"""
Detailled result of the inversion procedure, for one observation, 
and one variant
"""
struct IsInversion 
    mean::Vector
    cov::Vector # diagonal only 
    diagnostics::IsDiagnostics
end

using LinearAlgebra
using Distributions
using Random

using ..Probas
using ..Indics
using ..Schemes

export IsProposition, IsModel, IsInversion, IsDiagnostics

include("defs.jl")

struct IsParams <: SamplingParams 
    N_is::Int
end
scheme(::IsParams, centroid::Int) = centroid == 0 ? Schemes.IsMean : Schemes.IsCentroid(centroid)
Ns(p::IsParams) = p.N_is

"""
    Performs importance sampling, by writing in `samples` and `weights`.
"""
function importance_sampling!(model::IsModel, Xs::MatOrSub{T}, weights::VecOrSub{T}) where {T <: Number}
    Ns = length(weights)
    log_qx, log_px = zeros(T, Ns), zeros(T, Ns)
    importance_sampling!(model, Xs, weights, log_px, log_qx)
end

"""
This version also writes the intermediate densities in log_px and log_qx
"""
function importance_sampling!(model::IsModel, Xs::MatOrSub{T}, weights::VecOrSub{T}, log_px::VecOrSub{T}, log_qx::VecOrSub{T}) where {T <: Number}
    prop = get_proposition(model)
    _, Ns = size(Xs)

    max_log_px = -Inf # re scaling to avoid numerical issues

    @inbounds for j in 1:Ns
        x = view(Xs, :, j)
        # sample
        sampling!(prop, x)
        # compute target density
        log_px[j] = log_p(model, x,  j)
        if log_px[j] > max_log_px
            max_log_px = log_px[j]
        end
        # compute proposiiton density
        log_qx[j] = log_q(prop, x)
    end

    sum_ws = 0.
    # for j in 1:Ns
    @inbounds for j in 1:Ns
        w = exp(log_px[j] -  max_log_px - log_qx[j])
        if !isfinite(w)
            w = 0
        end
        weights[j] = w
        sum_ws += w
    end
    if sum_ws == 0 # then all weights are zero
        return 
    end
    @inbounds for j in 1:Ns
weights[j] /= sum_ws
    end
end

""" Compute the mean and cov, and diagnostics """
function exploit_samples(Xs::Matrix{T}, weights::VecOrSub{T}, non_unif::NonUniformityParams) where {T}
    @assert all(isfinite.(Xs))
    L, Ns = size(Xs)
    outMean, outCov = zeros(L), zeros(L)
    es, max_w, sum_w2 = 0, 0., 0.

    # Mean predictor and diagnostics
        @inbounds for j in 1:Ns
        valid = isfinite(weights[j])
        if !valid # avoid spurious samples
            weights[j] = 0
            Xs[:, j] .= 0
        end

        outMean .+= weights[j] * Xs[:, j]
        
        if valid 
            es += 1
        end
        sum_w2 += weights[j]^2
        if weights[j] >= max_w
            max_w = weights[j]
        end
    end

    ess = 1 / sum_w2
    qn = max_w
    diagnostic = IsDiagnostics(ess, es, qn)


    # Variance predictor (diagonal)
    tmp = zeros(L)
    @fastmath @inbounds for j in 1:Ns 
        valid = weights[j] != 0
        if valid
            tmp .= Xs[:, j] .- outMean   
            outCov .+= weights[j]  * tmp.^2 
        end   
    end


    # compute the non-uniformity parameter
    X = Probas.resampling(Xs, weights, non_unif.J)
    ks = zeros(T, L)
    for l in 1:L
        ks[l] = Indics.non_uniformity(X[l, :])
    end
    @assert all(isfinite.(outMean))
    @assert all(isfinite.(outCov))
    IsInversion(outMean, outCov, diagnostic), ks
end

"""
    Use importance sampling to compute the mean and the covariance of the target density.
    Also computes the non-uniformity criterion
"""
function importance_sampling(model::IsModel, params::IsParams, non_unif::NonUniformityParams)
    Xs, weights = importance_sampling(model, params)
    exploit_samples(Xs, weights, non_unif)
end

""" Returns samples and weights """
function importance_sampling(model::IsModel, params::IsParams)
    L = get_L(model)
    Xs, weights = zeros(L, params.N_is),  zeros(params.N_is)
    importance_sampling!(model, Xs, weights)
    Xs, weights
end
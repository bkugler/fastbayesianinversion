using ..Probas

"""
    Define the proposition density
    Must implement `log_q` and `sampling!`.
    Should implement `covariance` to be use in IMIS.
"""
abstract type IsProposition end 

"""
    Returns the log of the proposition density.

    Concrete types should probably use a cache to avoid allocating at each call
"""
log_q(p::IsProposition, x::VecOrSub) = error("log_q not implemented for type $(typeof(p))")

"""
    Sample one proposition point.

    Concrete types should probably use a cache to avoid allocating or computing `L` at each call
"""
sampling!(p::IsProposition, out) = error("sampling! not implemented for type $(typeof(p))")

"""
    Returns the matrix of covariance of the proposition law
"""
covariance(p::IsProposition) = error("covariance not implemented for type $(typeof(p))")


struct GaussianProposition{T <: Number} <: IsProposition
    mean::VecOrSub{T}
    cov::CholCov{T}
    # cached for performance 
    L::Int
    pre_factor::T
    allocated::Vector{T}
    function GaussianProposition(mean::VecOrSub{T}, cov::CholCov{T}) where T
        L = length(mean)
        pf = Probas.pre_factor(cov, L)
        allocated = zeros(T, L)
        new{T}(mean, cov, L, pf, allocated)
    end
end
GaussianProposition(mean, cov::DiagCov{T}) where T = GaussianProposition(mean, cholesky(Matrix{T}(cov)).L)

function log_q(prop::GaussianProposition, x::VecOrSub)
    Probas.log_gaussian_density!(x, prop.mean, prop.cov, prop.L, prop.pre_factor, prop.allocated)
end

function sampling!(prop::GaussianProposition{T}, out::VecOrSub{T}) where T
    gaussian_sampling!(prop.allocated, prop.L, prop.mean, prop.cov, out)
end
function covariance(p::GaussianProposition{T}) where T
    Symmetric(Matrix{T}(p.cov * p.cov'))
end

function gaussian_sampling!(tmp::VecOrSub{T}, L::Int, mean::VecOrSub{T}, chol_cov::CholCov{T}, out::VecOrSub{T}) where T
    randn!(tmp)
    @inbounds for l in 1:L
        out[l] = mean[l]
        @inbounds for l2 in 1:L
            out[l] += chol_cov[l, l2] * tmp[l2]
        end
    end
end


# struct StudentProposition{T <: Number} <: IsProposition
#     mean::VecOrSub{T}
#     chol_cov::CholCov{T}
#     ν::T
# end

# function log_q(prop::StudentProposition, x::VecOrSub, tmp::VecOrSub) 
#     return 0. # log(1)
# end

# function sampling!(p::StudentProposition, out::VecOrSub{T}, tmp::VecOrSub{T}, L::Int) where T
#     randn!(tmp)
#     chi = rand(Distributions.Chisq(p.ν))
#     f = sqrt(chi / p.ν)
#     for l in 1:L 
#         u = 0.
#         for l2 in 1:L 
#             u += p.chol_cov[l, l2] * tmp[l2]
#         end
#         out[l] = u / f + p.mean[l]
#     end
# end

"""
    GMM density

    Must not be mutated
"""
struct GaussianMixtureProposition{T <: Number} <: IsProposition
    gmm::Gmm{T}
    # computed and cached for performance
    cumweights::Vector{T} 
    allocated::Vector{T}
    function GaussianMixtureProposition{T}(gmm::Gmm{T}) where T
        new{T}(gmm, cumsum(gmm.weights), zeros(T, gmm.L))
    end
end
GaussianMixtureProposition{T}(w, m, c) where T = GaussianMixtureProposition{T}(Gmm{T}(w, m, c))

function log_q(prop::GaussianMixtureProposition{T}, x::VecOrSub) where T
    log(Probas.gmm_density(x, prop.gmm, prop.allocated))
end

function sampling!(p::GaussianMixtureProposition{T}, out::VecOrSub{T}) where T
    Probas.gmm_sampling!(p.cumweights, p.gmm.means, p.gmm.chol_covs, out, p.allocated, p.gmm.K, p.gmm.L)
end

function covariance(p::GaussianMixtureProposition{T}) where T
    Probas.covariance_mixture(p.gmm.weights, p.gmm.means, [Symmetric(C * C') for C in p.gmm.chol_covs])
end


"""
 Uniform distribution on [0,1] 
"""
struct UniformProposition <: IsProposition
    L::Int
end

function sampling!(::UniformProposition, out::VecOrSub{T}) where T
    rand!(out)
end

log_q(::UniformProposition, x::VecOrSub) = all((0 .<= x) .& (x .<= 1)) ? 0 : -Inf

covariance(prop::UniformProposition) = (1 / 12) .* Symmetric(Diagonal(ones(prop.L)))


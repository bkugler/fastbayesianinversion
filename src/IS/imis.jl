# Implémentation of basic IMIS algorithm
# taken from Estimating and Projecting Trends in HIV/AIDS Generalized Epidemics Using Incremental Mixture Importance Sampling
# by Raftery & Bao (2010)
# The step are from part 3.1 

using ..Probas
using ..Schemes

include("importance_sampling.jl")

""" Returns N0 + B * J samples """
struct ImisParams <: SamplingParams
    N0::Int
    B::Int
    J::Int
    function ImisParams(N0::Int, B::Int, J::Int)
        B <= N0 || error("B must be lower than N0, got $B > $N0")
        new(N0, B, J)
    end
end
""" Samples `Ns` total points """
ImisParams(Ns::Int) = ImisParams(Ns ÷ 10, Ns ÷ 10, 9)

scheme(::ImisParams, centroid::Int) = centroid == 0 ? Schemes.ImisMean : Schemes.ImisCentroid(centroid)

Ns(params::ImisParams) = params.N0 + params.B * params.J

function _new_gaussian(X::Matrix, ws::Vector, precision_prop::Symmetric, Nk::Int, L::Int, B::Int)
    # a) 1 - find highest weigth ...
    _, jmax = findmax(view(ws, 1:Nk))
    xk = X[:, jmax]

    # 2 - and its neighboors ...
    ds, u_tmp, v_tmp = zeros(Nk), zeros(L), zeros(L)
    for j in 1:Nk
        for l in 1:L
        u_tmp[l] = xk[l] - X[l,j]
        end
        mul!(v_tmp, precision_prop, u_tmp)
        for l in 1:L
            ds[j] += u_tmp[l] * v_tmp[l] # Mahalanodis distance
        end
    end
    neighboors_idx = partialsortperm(ds, 1:B) # partialsortperm is in increasing order (more efficient than sortperm)

    # 3 - compute associated covariance
    Sigmak = zeros(L, L)
    for id in neighboors_idx
        u_tmp .= xk - view(X, :, id)
        # Raftery & Bao propose the formula
        #   w = (ws[id] + (1 / Nk)) / 2 (average between importance and 1/Nk)
        # but, according to Fasalio et al 2016, not weighting increases stability
        #   w = 1
        Sigmak .+=  u_tmp * u_tmp'
    end
    Sigmak ./= B # normalization
    Cholk = Probas.safe_cholesky(Sigmak)
    xk, Cholk
end


function _update_current!(X::Matrix, ws::Vector, xk::Vector, Cholk::CholCov, Nk::Int, L::Int, Nk1::Int, B::Int, log_px::Vector, log_qx::Vector)
    pf, logNk1 = Probas.pre_factor(Cholk, L), log(Nk1)
    tmp = zeros(L)
    # for existing points, we can update the weigths without computing the whole mixture:
    #   q_k+1 = (N_k / N_k+1) * q_k + (B / N_k+1) * phi_k+1
    for j in 1:Nk
        log_phi_k1 = Probas.log_gaussian_density!(view(X, :, j), xk, Cholk, L, pf, tmp) # re-using tmp is OK
        log_qx[j] = Probas.weighted_logsumpexp(log_qx[j], log_phi_k1, Nk, B) - logNk1
        ws[j] = log_px[j] - log_qx[j] # at this point, we store log w
    end
end

function _new_weights!(m::IsModel, prop::IsProposition, X::Matrix, ws::Vector, means::Matrix, chols::CholCovs, Nk::Int, L::Int, Nk1::Int, K::Int, N0::Int, B::Int, log_px::Vector, log_qx::Vector)
    # for new points, we have to compute the whole mixture (of K first components)
    means_current, chols_current = means[:, 1:K], chols[1:K]
    logNk1 = log(Nk1)
    tmp = zeros(L)
    pre_factors = [ Probas.pre_factor(chol, L) for chol in chols_current ]
    for j in Nk + 1:Nk1
        x = X[ :, j]
        q = log_q(prop, x) # initial part
        dm = Probas.gmm_density!(x, means_current, chols_current, pre_factors, tmp) # mixture (with weights = 1)
        log_qx[j] = Probas.weighted_logsumpexp(q, log(dm), N0, B) - logNk1 # resultant mixture
        log_px[j] = log_p(m, x, j)
        ws[j] = log_px[j] - log_qx[j] # at this point, we store log w
    end
end

function imis_ext(params::ImisParams, m::IsModel)
    prop, L = get_proposition(m), get_L(m)
    T = Float64
    N0, B, J = params.N0, params.B, params.J
    
    Nmax = Ns(params) # memory setup ; we allocate the maximum needed
    X = zeros(L, Nmax) # samples
    ws = zeros(Nmax) # weigths
    log_px, log_qx = zeros(Nmax), zeros(Nmax) # density values
    tmp = zeros(L)

    # 1 - initial stage: basic importance sampling
    importance_sampling!(m, view(X, :, 1:N0), view(ws, 1:N0), view(log_px, 1:N0), view(log_qx, 1:N0))
    
    # 2 - iterative importance sampling
    precision_prop = Symmetric(inv(covariance(prop)))

    # at each step we need the full mixture to compute qk for the new samples 
    # the weigths are constant (thus easy to compute), dont need to store them
    means, chols::CholCovs{T} = zeros(T, L, J), CholCovs{T}(J, L)

    # we do a maximum of J iteration
    @inbounds for K in 1:J
        Nk = N0 + (K - 1)B # before new samples 
        Nk1 = Nk + B # with new samples

        # a) new gaussian
        xk, Cholk = _new_gaussian(X, ws, precision_prop, Nk, L, B)
        
        means[:, K] = xk # save the mean ...
        chols[K] = Cholk # ... and the cholesky decomposition of the variance
        
        # b) new samples
        X[:, (Nk + 1):Nk1] .= xk .+ Cholk * randn(T, L, B)
        
        # c) update weigths
        #   1 - current points
        _update_current!(X, ws, xk, Cholk, Nk, L, Nk1, B, log_px, log_qx)
        
        #   2 - for new points, we have to compute the whole mixture (of K first components)
        _new_weights!(m, prop, X, ws, means, chols, Nk, L, Nk1, K, N0, B, log_px, log_qx)
        
        #   3 - now we can normalize ws and apply exp
        # we use logsumexp to avoid numerical instability due to zero machine
        log_sum_w = Probas.logsumexp(view(ws, 1:Nk1), Nk1)
        for j in 1:Nk1
            ws[j] = exp(ws[j] - log_sum_w)
        end
    end
    X, ws
end

""" Returns samples and weights computed with IMIS """
importance_sampling(model::IsModel, params::ImisParams) = imis_ext(params, model)

""" Importance sampling using IMIS """
importance_sampling(m::IsModel, params::ImisParams, non_unif::NonUniformityParams) = exploit_samples(imis_ext(params, m)..., non_unif)
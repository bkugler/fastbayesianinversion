"""
 Expose a custom logger with support for indendation
"""
module Log

using Logging

function formatter(level, _module, group, id, file, line)
    color, prefix, suffix = Logging.default_metafmt(level, _module, group, id, file, line)
    return color, prefix, ""
end

mutable struct _Logger <: AbstractLogger
    _logger::ConsoleLogger
    indent::Int
    _lock::ReentrantLock
    _Logger(level::Logging.LogLevel) = new(ConsoleLogger(stdout, level, meta_formatter=formatter), 0, ReentrantLock())
end

function Logging.handle_message(logger::_Logger, level, message, args...; kwargs...) 
    prefix = repeat("  ", logger.indent) 
    message = prefix * message
    Logging.handle_message(logger._logger, level, message, args...; kwargs...)
end

Logging.min_enabled_level(logger::_Logger) = Logging.min_enabled_level(logger._logger)

function Logging.shouldlog(logger::_Logger, level, _module, group, id) 
    # filter annoying TranscodingStreams
    if group == :stream
        return false 
    end
    return Logging.shouldlog(logger._logger, level, _module, group, id)
end

"""
 Must be called at least once, at startup 
"""
function init(level::Logging.LogLevel=Logging.Debug)
    global_logger(_Logger(level))
    nt = Threads.nthreads()
    @info "Logger set up. Running with $nt threads"
end

"""
 Add a level of indentation to the log. Should be followed by a `deindent` call
"""
function indent()
    lock(global_logger()._lock)
    global_logger().indent += 1
    unlock(global_logger()._lock)
end
function deindent()
    lock(global_logger()._lock)
    global_logger().indent -= 1
    unlock(global_logger()._lock)
end



end
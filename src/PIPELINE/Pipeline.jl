""" 
Aggregate the various prediction schemes 
into an opiniated inversion procedure.
"""
module Pipeline
export RGP, RP, R0 

include("procedure.jl")
include("utils.jl")
include("TuneImis.jl")
end
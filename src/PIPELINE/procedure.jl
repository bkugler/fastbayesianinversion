using Dates 
using LinearAlgebra 

using ..Gllim 
using ..Predictions 
using ..Models
using ..Is
using ..Probas
using ..Moni
using ..Store
using ..Noise 
using ..Log 
using ..Indics
using ..Schemes
using ..Comptimes
using ..Shared



"""
    Define hyper-parameters for the inversion of a serie of
    observations, in addition to those already specified
    in the training step.
    The dispersion and the best residual solutions depend
    on all the prediction schemes chosen.
"""
mutable struct InversionParams 
    train_std::Float64 # must coincide with the std used in GLLiM training
    merging_options::Predictions.MergingOptions
    sampling_params::Is.SamplingParams # which sampling method to use
    with_best_centroids::Bool # if true, includes the highest weight centroids of the full GMM
    with_mode_finding::Bool # if true, performs an additional Mode Finding algorithm on the full GMM
    non_uniformity::Is.NonUniformityParams 
    """ optional additional std to be add to the direct GLLiM model used as proposition
    law in sampling steps (both mean and centroids). Does not change the GLLiM standalone
    predictions nor the target posterior. A zero value has no influence, which is the default.
    """
    additional_proposition_std::Float64 
end 

""" Wrap all the parameters needed to 
perform a complete inversion procedure """
struct ProcedureParams 
    training_params::Shared.TrainingParams
    inv_params::InversionParams
    function ProcedureParams(tp::Shared.TrainingParams, ip::InversionParams) 
        tp.train_std == ip.train_std || error("Training and inversion std. must be coherent")
        new(tp, ip)
    end
end

struct Variability{T <: Number}
    dispersion::T
    dispersion_cmp::Vector{T} # per component
    mean_factor::T # relative Reconstruction error for the average of all the inversion error

    non_uniformity::Vector{T} # per component
end

function add_y_err(context, yobs, pred::CompleteInversion)::CompleteInversion  
    y_err = Y_relative_err(context, yobs, pred.mean, 1., 0.)
    CompleteInversion(pred.key, pred.mean, pred.cov, y_err, pred.x_err, pred.diagnostics, pred.origin)
end

function _x_denorm(context, xobs::VecOrSub, pred::CompleteInversion)::CompleteInversion    
    # X error, in math space (before de-normalisation)
    L = length(xobs)
    x_err =  norm(xobs .- pred.mean) / L

    # de-normalization 
    mean, cov = Models.to_physical(context, pred.mean, pred.cov)
    CompleteInversion(pred.key, mean, cov, pred.y_err, x_err, pred.diagnostics, pred.origin)
end

function best_residual(inversions::Vector{CompleteInversion})
    _, index = findmin([iv.y_err for iv in inversions])
    best = inversions[index]
    CompleteInversion(Schemes.Best, best.mean, best.cov, best.y_err, best.x_err, best.diagnostics, best.key)
end

_chol_cov_to_diag(chol::CholCov) = diag(chol * chol')

""" Return the array of the CompleteInversion for each centroid """
function merged_centroids_to_inversions(merged::Gmm)
    return [
        CompleteInversion(Schemes.GllimMergedCentroid(k), merged.means[:,k]; cov=_chol_cov_to_diag(merged.chol_covs[k]))   for k in 1:merged.K
    ]
end

""" Selects the K best centroids in the full mixture, in term of weigths.
Returns the best centroids and the corresponding CompleteInversion."""
function best_centroids_to_inversions(full::Gmm, K::Int)
    idxs = sortperm(full.weights; rev=true)[1:K]
    centroids1 = full.means[:, idxs]
    chol_covs_centroids = full.chol_covs[idxs]

    bc = [ CompleteInversion(Schemes.GllimBestCentroid(k), centroids1[:,k]; cov=_chol_cov_to_diag(chol_covs_centroids[k]))   for k in 1:K  ]
    return centroids1, bc
end


function _find_modes(params::InversionParams, complete_gmm::Gmm, merged_gmm::Gmm)
    L, K = merged_gmm.L, merged_gmm.K

    # always include merged centroids
    out = merged_centroids_to_inversions(merged_gmm)

    centroids1 = nothing
    if params.with_best_centroids
        # we select the K highest weights in the full mixture
        centroids1, bc = best_centroids_to_inversions(complete_gmm, K)
        push!(out, bc...)
    end

    if params.with_mode_finding
        merged_centroids = merged_gmm.means
        # we run mode finding starting from merged centroids ...
        # we keep only the modes, we replace non modes by the start     
        res2 = Predictions.search_modes(complete_gmm, merged_centroids, 1e-8)
        mmc = [ CompleteInversion(Schemes.GllimModeFromMergedCentroid(k), res.is_maximum ? res.x : merged_centroids[:, k], zeros(L), Inf, Inf, Dict("Mode" => res.is_maximum ? 20 + k : 1), "")   for (k, res) in enumerate(res2) ]
        push!(out, mmc...)

        # ... OR "best" centroids
        if params.with_best_centroids
            res1 = Predictions.search_modes(complete_gmm, centroids1, 1e-8)
            mbc = [ CompleteInversion(Schemes.GllimModeFromBestCentroid(k), res.is_maximum ? res.x : centroids1[:, k], zeros(L), Inf, Inf, Dict("Mode" => res.is_maximum ? 10 + k : 1), "")   for (k, res) in enumerate(res1) ]
            push!(out, mbc...)
        end
    end
    out
end

# use (relative) reconstuction as pertinence
# the dispersion is scaled by the error for the super mean
# to discriminate real multi-modal scenario from high variance uni-modal ones
function indice_multi_solutions(context::Models.Model, yobs::VecOrSub, preds::Vector{CompleteInversion})
    # vectorial
    d, mean = Indics.dispersion([Indics.WeightedMean(p.mean, p.y_err) for p in preds])
    mean_err = Y_relative_err(context, yobs, mean, 1., 0.)
    d *= mean_err
    # component wise 
    L = length(preds[1].mean)
    ds = zeros(L)
    for l in 1:L 
        ds[l], _ = Indics.dispersion([Indics.WeightedMean(p.mean[l:l], p.y_err) for p in preds])
        ds[l] *= mean_err
    end
    d, ds, mean_err
end


"""
    Run simple gllim only prediction.
    The std of the observation is added to the covariance of `trained_gllim`.
"""
function gllim_prediction(Yobs::Matrix{T}, Yobs_std::Matrix{T}, trained_gllim::Gllim.GLLiM{T}) where T
    D, N = size(Yobs)
    L = trained_gllim.L
    a, mu = 1., zeros(T, D)
    out_mean, out_cov = zeros(L, N), zeros(L, N)

    ti = Dates.now()
    Threads.@threads for n in 1:N
        # for n in 1:N
        yobs = Yobs[:, n]
        obs_std = Yobs_std[:, n]
        mean, cov = Gllim.predict_with_std(trained_gllim, yobs, obs_std)         
        out_mean[:, n] = mean
        out_cov[:, n] = diag(cov)
    end  
    dura = Comptimes.since(ti)
    return out_mean, out_cov,  dura
end

""" 
Implementation for one observation.
Return the "dispersion" of the solutions as well.
"""
function complete_inversion(context::Models.Model, yobs::VecOrSub{T}, ystd::VecOrSub{T}, xobs::VecOrSub{T}, trained_gllim::Gllim.GLLiM{T}, params::InversionParams)::Tuple{Shared.OneInversion,Variability{T}} where T
    K_m = params.merging_options.K_merged

    mean_mean_gllim, mean_cov_gllim, model, model_centroids, full_posterior::Probas.Gmm{T}, merged_posterior::Probas.Gmm{T} = Shared.setup_inversion(context, yobs, ystd, trained_gllim, params.train_std, params.merging_options; add_prop_std=params.additional_proposition_std)

    # we compare with mode finding, starting from best centroids and merged centroids
    modes = _find_modes(params, full_posterior, merged_posterior)

    # we regroup the differents predictions
    all_inversions =  [
        CompleteInversion(Schemes.GllimMean, mean_mean_gllim, mean_cov_gllim, Inf, Inf, Dict(), ""),
        modes...,
    ]

    # sampling computation
    mean_with_is, non_uniformity = Is.importance_sampling(model, params.sampling_params, params.non_uniformity)
    push!(all_inversions, CompleteInversion(Is.scheme(params.sampling_params, 0), mean_with_is))

    # we only use the non uniformity for the mean: dont do useless calculations
    no_non_unif = Is.NonUniformityParams(1)
    @inbounds for k in 1:K_m
        # proposition around merged centroids
        model_mode = model_centroids[k]

        centroid_with_is, _ = Is.importance_sampling(model_mode, params.sampling_params, no_non_unif)
        push!(all_inversions, CompleteInversion(Is.scheme(params.sampling_params, k), centroid_with_is))
    end

    # we compute y errors
    all_inversions = [add_y_err(context, yobs, iv) for iv in all_inversions]

    # we compute the "dispersion", before rescaling
    dispersion, dispersion_cmp, mean_factor = indice_multi_solutions(context, yobs, all_inversions)

    # we compute x errors and rescale x
    all_inversions = [_x_denorm(context, xobs, iv) for iv in all_inversions]

    # we find the best prediction in the residual sense
    best_inversion = best_residual(all_inversions)
    push!(all_inversions, best_inversion)

    # we return a dict for easy lookup 
    Dict((iv.key, iv) for iv in all_inversions), Variability(dispersion, dispersion_cmp, mean_factor, non_uniformity)
end


"""
    Invert observations one by one, with an already trained `GLLiM`
"""
function complete_inversion(context::Models.Model, Yobs::Matrix{T}, Yobs_std::Matrix{T}, trained_gllim::Gllim.GLLiM{T}, params::InversionParams, Xobs::Union{Matrix{T},Nothing}) where T
    D, N = size(Yobs)
    L = trained_gllim.L
    if Xobs === nothing # avoid handling edge cases 
        Xobs = zeros(T, L, N)
    end
    
    preds = Vector{Shared.OneInversion}(undef, N)
    variabilities = Vector{Variability}(undef, N) # additionnal diagnostics
    Log.indent()
    ti = Dates.now()
    mon = Moni.Monitor(N)
    Threads.@threads for n in 1:N
    # for n in 1:N
        yobs = Yobs[:, n]
        xobs = Xobs[:, n]
        obs_std = Yobs_std[:, n]
        
        preds[n], variabilities[n] = complete_inversion(context, yobs, obs_std, xobs, trained_gllim, params)
        
        Moni.update!(mon)
    end
    out = Shared.predictions_series(preds)
    dura = Comptimes.since(ti)
    Log.deindent()
    out, variabilities, dura
end

""" Run GLLiM and predictions """
const RGP = 2
""" Use already trained GLLiM, but run predictions """
const RP = 1 
""" Read results from disk """
const R0 = 0 

""" Convenience wrapper function """
function complete_inversion(context::Models.Model, Yobs::Matrix{T}, Yobs_std::Matrix{T}, params::ProcedureParams, run, st::Store.Storage; Xobs::Union{Matrix{T},Nothing}=nothing) where T
    ct = Comptimes.Times(context, params, size(Yobs)[2])

    # identify the experiment
    metadata = [context, params, Yobs, Yobs_std]
    
    if run >= RP # do predictions
        Log.indent()

        trained_gllim::Gllim.GLLiM, _, _ = Shared.generate_train_gllim(context, params.training_params, st, run >= 2)
        flush(Base.stdout) # useful in batch mode 

        @info "Starting inversions ..."
        ti = Dates.now()
        preds, variabilities = complete_inversion(context, Yobs, Yobs_std, trained_gllim, params.inv_params, Xobs)
        dura = Comptimes.since(ti)
        Store.save(st, (preds, variabilities, dura), Store.FO_PREDICTIONS, metadata...)

        # additional time measurement for the gllim only method
        _, _, dura_gllim_only = gllim_prediction(Yobs, Yobs_std, trained_gllim)
        Store.save(st, dura_gllim_only, Store.FO_BENCHMARK, metadata..., :gllim_time)

        Log.deindent()
    end

    preds, variabilities, dura = Store.load(st, Store.FO_PREDICTIONS, metadata...)
    ct.prediction = dura

    # we always need to load history & training time 
    _, ll_history, ct.learning  = Store.load(st, Store.FO_GLLIM, context, params.training_params)

    # we always need to load the gllim prediction time 
    ct.gllim_only_prediction  = Store.load(st, Store.FO_BENCHMARK, metadata..., :gllim_time)

    flush(Base.stdout) # useful in batch mode 
    preds, variabilities, ll_history, ct
end

""" Convenience wrapper function """
function complete_noise_estimation(context::Models.Model, Yobs::Matrix, Xobs::Union{Matrix,Nothing},
    params::Noise.EmProcedureParams, run, st::Store.Storage)

    ct = Comptimes.Times(context, params, size(Yobs)[2])

    if run >= RGP
        Log.indent()

        ti = Dates.now()
        # the perfect gllim is trained with infimum std
        X, Y = Models.data_training(context, params.N_train, 1e-6)
        trained_gllim, _ = Gllim.train(params.gllim_params, X, Y)
        dura = Comptimes.since(ti)
            
        Store.save(st, [trained_gllim, dura],  Store.FO_GLLIM, context, params.N_train, params.gllim_params)
        Log.deindent()
    end
    perfect_gllim, ct.learning = Store.load(st, Store.FO_GLLIM, context, params.N_train, params.gllim_params)

    if run >= RP
        estimator = nothing
        if Xobs !== nothing
            estimator = Noise.theorical_estimators(context, Xobs, Yobs)
        end

        ti = Dates.now()
        history = Noise.run(params.noise_params, Yobs, context, perfect_gllim)
        dura = Comptimes.since(ti)
        Store.save(st, [history, estimator, dura], Store.FO_NOISE, context, params, Yobs)
    end
    history, estimator, ct.prediction = Store.load(st, Store.FO_NOISE, context, params, Yobs)
    history, estimator, ct
end
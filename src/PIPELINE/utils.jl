using Colors 

using FastBayesianInversion.Models
using FastBayesianInversion.Gllim
using FastBayesianInversion.Noise 
using FastBayesianInversion.Predictions 
using FastBayesianInversion.Mcmc
using FastBayesianInversion.Is

using .Comptimes
using .Schemes
using .Shared

function Comptimes.Times(context::Models.Model, params::ProcedureParams, Nobs::Int) 
    nis = Is.Ns(params.inv_params.sampling_params)
    Comptimes.Times("", Comptimes.Invertion, Millisecond(0), Millisecond(0), Millisecond(0), "inversion - " * Models.label(context), Models.get_L(context), Models.get_D(context), Gllim.getK(params.training_params.gllim_params.init), params.training_params.N_train, Nobs, nis)
end
function Comptimes.Times(context::Models.Model, params::Noise.EmProcedureParams, Nobs::Int)
    Comptimes.Times("", Comptimes.Noise, Millisecond(0), Millisecond(0), Millisecond(0), "noise estimation - " * Models.label(context), Models.get_L(context), Models.get_D(context), Gllim.getK(params.gllim_params.init), params.N_train, Nobs, Is.Ns(params.noise_params.is_params))
end
function Comptimes.Times(context::Models.Model, mcmc_params::Mcmc.ParamsMCMC, Nobs::Int)
    Comptimes.Times("", Comptimes.MCMC, Millisecond(0), Millisecond(0), Millisecond(0), "MCMC - " * Models.label(context), Models.get_L(context), Models.get_D(context), 0, 0, Nobs, mcmc_params.Niter)
end


"""
Store a serie of predictions, with optionnal diagnostics, in order to facilitate plotting.
"""
struct Prediction
    id::String 

    Xmean::Matrix
    Xcovs::Matrix # zero for empty

    Yerr::Vector # || y - F(x) ||_2
    Xerr::Vector # || x_obs - x_pred ||_2

    diagnostics::Dict{String,Vector} # label values

    # config::SerieConfig
end
function Prediction(Xmean, Xcovs=nothing; diagnostics=Dict{String,Vector}(), Yerr=nothing, Xerr=nothing,
    config=SerieConfig("", :basic, :auto), id="")
    if Xcovs === nothing 
        Xcovs = zeros(size(Xmean))
    end
    Yerr = Yerr === nothing ? Inf * ones(size(Xmean)[2]) : Yerr
    Xerr = Xerr === nothing ? Inf * ones(size(Xmean)[2]) : Xerr
    Prediction(id, Xmean, Xcovs, Yerr, Xerr, diagnostics, config)
end

function aggregate(v::Vector{CompleteInversion})
    return hcat([inv.mean for inv in v]...)
end

function aggregate_std(v::Vector{CompleteInversion})
    return hcat([sqrt.(inv.cov) for inv in v]...)
end

function get_by_key(v::Vector{Prediction}, key::String)::Matrix
    for p in v 
        if p.id == key
            return p.Xmean
        end
    end
end

function _regularize_one_method!(invs::Dict{String,Shared.MethodResult}, prefix::String)
    # start by computing the matching methods 
    # assuming all points avec the same
    target_methods = [method for method in keys(invs) if startswith(method, prefix) ]

    # use the Xmean entry as basis for the regularization
    means::Vector{Matrix{Float64}} = [invs[method].Xmean for method in target_methods]

    # compute regularization
    perms = Predictions.regularization(means)

    # permute the n-th entry of the matrices
    function permute_vectors(perm::Vector{Int}, n::Int, mats::Vector{Matrix{Float64}})
        tmp = [mat[:, n] for mat in mats] # copy to avoid the side effect of the permutation
        tmp = tmp[perm] # apply the permutation
        for (k, v) in enumerate(tmp)
            mats[k][:,n] = v
        end
    end
    function permute_scalars(perm::Vector{Int}, n::Int, vecs::Vector{Vector{Float64}})
        tmp = [vec[n] for vec in vecs] # copy to avoid the side effect of the permutation
        tmp = tmp[perm] # apply the permutation
        for (k, v) in enumerate(tmp)
            vecs[k][n] = v
        end
    end

    # apply permutation, to all the data series: mean, cov, errs, diagnostics
    # means is already defined
    covs::Vector{Matrix{Float64}} = [invs[method].Xcovs for method in target_methods]
    yerrs::Vector{Vector{Float64}} = [invs[method].Yerr for method in target_methods]
    xerrs::Vector{Vector{Float64}} = [invs[method].Xerr for method in target_methods]
    diagnoss = []
    for key in keys(invs[target_methods[1]].diagnostics)
        push!(diagnoss, [invs[method].diagnostics[key] for method in target_methods])
    end

    for (n, perm) in enumerate(perms) # perm refers to the indexes of target_methods
        permute_vectors(perm, n, means)
        permute_vectors(perm, n, covs)
        permute_scalars(perm, n, yerrs)
        permute_scalars(perm, n, xerrs)
        for diagnos in diagnoss
            permute_scalars(perm, n, diagnos)
end
    end     
end

""" 
For each family of methods in `methods_prefixes`, 
swap the predictions among the family, if it increases the serie regularity 
Note that the original scheme of a prediction is lost: it should not matter 
since methods among a family are similar.
"""
function regularize_inversions!(invs::Dict{String,Shared.MethodResult}, methods_prefixes::Vector{String}) 
    for prefix in methods_prefixes
        _regularize_one_method!(invs, prefix)   
    end
end

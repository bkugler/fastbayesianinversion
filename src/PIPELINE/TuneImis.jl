""" Expose a tuning method to choose the best IMIS configuration
given a model, some observations, and a time limit, given as an IS equivalent
MCMC is computed and used as a reference.
"""
module TuneImis

using Dates 
using LinearAlgebra

using ..Probas
using ..Log
using ..Moni
using ..Models
using ..Gllim
using ...Gmms
using ..Is
using ..Store 
using ..Predictions 
using ..Shared
using ..Comptimes

struct Input 
    mcmc_mean::Vector
    is_model::Is.IsModel
end

""" Results of inversion, averaged over several tries and several observations """
struct Perf
    err::Number # with respect to ref prediction
    errs::Vector{Float64} # components per compoments (with respect to ref)
    ct::Millisecond # average computation time
end

struct ExpIs 
    Ns::Int
    perf::Perf
end

struct ExpImis 
    params::Is.ImisParams
    perf::Perf
end

function _perform_one(model::Is.IsModel, N_is::Int)
    X_is, W_is = zeros(Is.get_L(model), N_is), zeros(N_is)
    Is.importance_sampling!(model, X_is, W_is)
    pred, _ = Is.exploit_samples(X_is, W_is, Is.NonUniformityParams(1))
    pred
end
function _perform_one(model::Is.IsModel, params::Is.ImisParams)
    pred, _ =  Is.exploit_samples(Is.imis_ext(params, model)..., Is.NonUniformityParams(1))
    pred
end

""" For each input, run `nb_try` times inversion, defined by `param` """
function perform(setups::Vector{Input}, nb_try::Int, param::Union{Int,Is.ImisParams})
    err = 0.
    errs = zeros(Is.get_L(setups[1].is_model))
    ti = Dates.now()
    for setup in setups
        for _ in 1:nb_try
            pred = _perform_one(setup.is_model, param)
            
            err += norm(pred.mean .- setup.mcmc_mean)
            errs .+= abs.(pred.mean .- setup.mcmc_mean)
        end
    end
    total = nb_try * length(setups)
    ct = Comptimes.since(ti) ÷ total
    err /= total
    errs ./= total
    Perf(err, errs, ct)
end


# ref in mathematical space 
function _setup(st::Store.Storage, context::Models.Model, Yobs::Matrix, Yobs_std::Matrix, refs::Matrix, 
    training_params::Shared.TrainingParams,  run_gllim::Bool)
    _, N = size(Yobs)

    trained_gllim, _, _  = Shared.generate_train_gllim(context, training_params, st, run_gllim)

    merging_params = Predictions.MergingOptions(3)

    setups::Vector{Input} = [] 
    for n in 1:N 
        yobs, yobs_std = Yobs[:, n], Yobs_std[:, n]
        ref = refs[:, n]

        _, _, is_model, _, _, _ = Shared.setup_inversion(context, yobs, yobs_std, trained_gllim, 
            training_params.train_std, merging_params)

        push!(setups, Input(ref, is_model))
    end
    setups 
end

struct ImisParamsRange
    N0s::Vector{Int}
    B_ratios::Vector{Float64}
    Ks::Vector{Int}
end

function run_imiss(setups::Vector{Input}, range::ImisParamsRange, nb_try::Int)
    out_imis::Vector{ExpImis} = []
    @info "Imis inversions..."
    Log.indent()
    for N0 in range.N0s
        @info "N0 = $N0"
        Log.indent()
        for ratio in range.B_ratios
            B = Int(floor(N0 * ratio))
            @debug "B = $B"
            Log.indent()
            out = Vector{ExpImis}(undef, length(range.Ks))
            mon = Moni.Monitor(length(range.Ks); step=2)
            Threads.@threads for i in 1:length(range.Ks) 
                K = range.Ks[i]
                params = Is.ImisParams(N0, B, K)
                perf = perform(setups, nb_try, params)
                out[i] = ExpImis(params, perf)
                Moni.update!(mon)
            end 
            push!(out_imis, out...)
            Log.deindent()
        end
        Log.deindent()
    end
    Log.deindent()
    out_imis
end

function _select(perf_is::Perf, res::Vector{ExpImis})
    # we choose the closer to ref, among configs faster than IS 
    res = [r for r in res if r.perf.ct <= perf_is.ct && isfinite(r.perf.err)]
    _, index = findmin([r.perf.err for r in res])
    best = res[index]
    rd = x -> round(x, digits=2)
    @info "Best configuration is $(rd(perf_is.ct / best.perf.ct)) faster and $(rd(perf_is.err / best.perf.err)) better than IS"
    @debug "IS err : $(perf_is.err) ; IMIS err : $(best.perf.err)"
    best.params
end


_default_imis_range(L::Int) = ImisParamsRange(
    L .* [250, 500,1000], # proportionnal to the dimension
    [0.2, 0.4, 0.6], # must respect B <= N0
    collect(1:3:25)
)

""" 
    Uses `refs` (predictions in mathematical space) to tune IMIS params.
    Several configurations are tried: among the methods fastest than IS, the more precise (close to the reference)
    is selected.
"""
function tune(st::Store.Storage, context::Models.Model, Yobs::Matrix, Yobs_std::Matrix, refs::Matrix,
    training_params::Shared.TrainingParams, Ns::Int, run_gllim::Bool) 
    _,  Nobs = size(Yobs)
    @info "Tuning imis params (Nobs = $Nobs)..."

    setups = _setup(st, context, Yobs, Yobs_std, refs, training_params, run_gllim)

    # is is used as time bound 
    perf_is = perform(setups, 3, Ns)

    # try several imis and register perfs 
    L = Models.get_L(context)
    range = _default_imis_range(L)
    res = run_imiss(setups, range, 5)

    _select(perf_is, res)
end

end
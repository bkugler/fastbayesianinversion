""" Define prediction schemes, to help with clarity """
module Schemes 

const GllimMean =  "gllim_mean"
const GllimMergedCentroid(k) =  "gllim_merged_centroid_$k"
const IsMean =  "is_mean"
const IsCentroid(k) =  "is_centroid_$k"
const ImisMean =  "imis_mean"
const ImisCentroid(k) =  "imis_centroid_$k"
const Best = "best"
const McmcMetropolis = "mcmc_mh"
const McmcHamiltonian = "mcmc_hmc"

const GllimBestCentroid(k) =  "gllim_best_centroid_$k"
const GllimModeFromBestCentroid(k) =  "gllim_mode_from_best_centroid_$k"
const GllimModeFromMergedCentroid(k) =  "gllim_mode_from_merged_centroid_$k"
end


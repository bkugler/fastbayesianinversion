""" Wraps Metropolis Hasting and Hamiltonian Monte Carlo MCMC algorithms """
module Mcmc
    
using LinearAlgebra
using KissMCMC
using Mamba
using Dates
using Statistics

using ...Log 
using ..Probas
using ..Comptimes
using ..Schemes
using ..Moni
using ..Models
using ..Shared

abstract type ParamsMCMC end # must have  `std_train` and `Niter` fields, and implement scheme

_scheme(p::ParamsMCMC) = error("_scheme not implemented for $(typeof(p))")

"""
 Parametrize the MCMC inversion, using the Metropolis Hastings algorithm
"""
struct ParamsMH <: ParamsMCMC
    Niter::Int
    Nkeep::Int
    alpha::Float64

    # to compute the covariance sigma_train ^ 2 + (y_std) ^ 2
    std_train::Float64

    function ParamsMH(Niter, Nkeep, alpha, std_train) 
        Niter >= Nkeep || error("Number of iterations must be greater than samples to keep.")
        new(Niter, Nkeep, alpha, std_train)
    end
end
ParamsMH(std_train::Float64; Niter = 10_000_000, Nkeep = 5_000_000, alpha = 0.02) = ParamsMH(Niter, Nkeep, alpha, std_train)

_scheme(::ParamsMH) = Schemes.McmcMetropolis

"""
Parametrize the MCMC inversion, using the No-U-Turn Sampler extension to Hamiltonian Monte Carlo
"""
struct ParamsHMC <: ParamsMCMC
    Niter::Int
    Nkeep::Int

    # to compute the covariance sigma_train ^ 2 + (y_std) ^ 2
    std_train::Float64

    function ParamsHMC(Niter, Nkeep, std_train) 
        Niter >= Nkeep || error("Number of iterations must be greater than samples to keep.")
        Niter >= 200 || error("Mamba requires at least 200 iterations")
        new(Niter, Nkeep, std_train)
    end
end
_scheme(::ParamsHMC) = Schemes.McmcHamiltonian


function _sample_from_posterior(c::Models.Model, y::Vector{T}, Sigma::DiagCov{T}, params::ParamsMH) where T <: Number
    L = Models.get_L(c)

    # target density
    logpdf(x::Vector{T}) = Models.log_conditionnal_density(c, x, y, Sigma)
    # samples the proposal (or jump) distribution
    sample_prop_normal(theta::Vector{T}) = params.alpha * randn(T, L) .+ theta
    # starting points
    theta0 = 0.5 .+ zeros(L)

    thetas, accept_ratio = KissMCMC.metropolis(logpdf, sample_prop_normal, theta0,
        niter = params.Niter, use_progress_meter = false, nburnin = params.Niter - params.Nkeep)
    return hcat(thetas...)
end

function _sample_from_posterior(c::Models.Model, y::Vector{T}, Sigma::DiagCov{T}, params::ParamsHMC) where T
    L, D = Models.get_L(c), Models.get_D(c)
    inv_sigma = inv(Sigma)
    F = Models.closure(c)

    # target density
    logfgrad = function(x::DenseVector)
        logf = Models.log_conditionnal_density(c, x, y, Sigma)
        if logf == -Inf 
            return -Inf, zeros(L)
        end
        tmp = zeros(T, D)
        Models.f!(c, x, tmp, D)
        tmp .-= y
        jacobien = Probas.dF_diff_finite(F, x, 1e-8,D)
        grad = - jacobien' * inv_sigma * tmp
        logf, grad
    end

    # starting points
    theta0 = 0.5 .+ zeros(L)

    ## MCMC Simulation with No-U-Turn Sampling
    n,  burnin = params.Niter, params.Niter - params.Nkeep
    sim = Mamba.Chains(n, L, start = (burnin + 1))
    theta = Mamba.NUTSVariate(theta0, logfgrad)
    for i in 1:n
        Mamba.sample!(theta, adapt = (i <= burnin))
        if i > burnin
            sim[i, :, 1] = theta
        end
    end
    Matrix{T}(sim.value[:,:,1]')
end


"""
 Invert observations one by one, using MCMC
"""
function complete_inversion(c::Models.Model, Yobs::Matrix{T}, Yobs_std::Matrix{T}, params::ParamsMCMC)::Tuple{Shared.MethodResult, Millisecond} where T
    _, N = size(Yobs)

    @info "Starting $N inversions with MCMC..."
    Log.indent()
    
    ti = Dates.now()
    monitor = Moni.Monitor(N; step = 4)
    preds = Vector{Shared.OneInversion}(undef, N)

    # for n in 1:N
    Threads.@threads for n in 1:N
        yobs = Yobs[:, n]
        yobs_std = Yobs_std[:, n]

        Sigma = Diagonal(params.std_train^2 .+ yobs_std.^2)
        thetas = _sample_from_posterior(c, yobs, Sigma, params)
        preds[n] = Dict(_scheme(params) => _exploit_samples(c, yobs, thetas, _scheme(params)))
        Moni.update!(monitor)
    end
    out = Shared.MethodResult(preds, _scheme(params))
    Log.deindent()
    duration = Comptimes.since(ti)
    out, duration
end

function _exploit_samples(c::Models.Model, yobs::VecOrSub{T}, thetas::Matrix{T}, scheme::String)::CompleteInversion where T
    L, keep = size(thetas)
    a, mu = 1., zeros(Models.get_D(c))

    # computing estimators
    mean = (sum(thetas; dims = 2) ./ keep)[:, 1]
    cov  = zeros(L)
    for k in 1:keep
        u = (mean .- thetas[:, k]).^2 # only diagonal terms
        cov .+= u
    end
    cov /= keep

    # Y err (with x in mathematical space)
    y_err = Y_relative_err(c, yobs, mean, a, mu)

    # rescale x
    mean, cov = Models.to_physical(c, mean, cov)

    CompleteInversion(scheme, mean, cov, y_err, Inf, Dict(), "")
end


end
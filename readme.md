# Fast Bayesian Inversion 

This repository exposes statistical methods to deal with non-linear, data intensive inverse problems.
It is divided in two main parts : a Julia package, which contains the main algorithms, and a playground folder, used for quick prototyping or simulations.

The following targets the main package, the playground not being supposed to be re-used as it is.

## Overview of the package 

### GLLiM 
The methodology is build on the [GLLiM model](src/GLLIM/Gllim.jl) (which itself uses Gaussian Mixture Models). It can be used as a standalone approach : the training step is done with `Gllim.train`, the inversion with `Gllim.inversion`, then the fitted model can be used via `Gllim.conditionnal_density` or `Gllim.predict`. 

### Diagnostics 
The quality of the learning may be assessed with [these tools](src/PERF/Perf.jl).

### Prediction schemes 
The GMM predicted by GLLiM can be further exploited with mixture merging, mode finding and regularization, implemented in [Predictions](src/PREDICTIONS/Predictions.jl).

### Functional models 
The remaining modules assume the knowledge of a functional model, which can be encoded as a mathematical function. The module [Models](src/MODELS/Models.jl) provide some toy examples and an implementation of a real remote sensing application : the Hapke photometric model.

If you want to use the framework with your own model, just create a type which implements the methods needed by [`Models.Model`](src/MODELS/defs.jl).

### Importance sampling

With a functional model, one can use Importance Sampling to enhance GLLiM predictions, with the `Is.importance_sampling` method. A refined method, known as IMIS, is also available with the `Is.imis` method.

### Model calibration

This package also provides 2 tools to help analyzing the forward model : the [Sobol indices](src/SOBOL/Sobol.jl) can be efficiently approximated; and the adequation between the model and the observation may be assessed by the noise estimation provided by [`Noise.run`](src/Noise/em_is.jl).

